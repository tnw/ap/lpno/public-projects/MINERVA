c**********************************************************************
      module version
      implicit double precision(a-h,o-z)
      real,parameter :: Minerva_Version = 2.00
      character*18,parameter :: Version_Date='September 30, 2016' 
      end module version
c**********************************************************************
      module numbers
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,one=1.d0,two=2.d0,three=3.d0,four=4.d0,
     1          six=6.d0,eight=8.d0,ten=10.d0,half=0.5d0,fourth=0.25d0,
     2          hundred=100.d0,hundredth=0.01d0,quarter=0.25d0,
     3          giga=1.d9,deci=1.d-10,sixteen=16.d0,twoquart=2.25d0,
     4          tiny=1.d-20,tiny2=1.d-15,tenth=.1d0,one80=180.d0,
     5          rt2=1.414213562373095d0,rti2=0.7071067811865475d0,
     6          thrhalf=1.5d0,smaller=1.d-8,smallest=1.d-99,thou=1.d3,
     7          tenthou=1.d4,amicro=1.d-6,degtorad=1.745329252d-2,
     8          tricent=300.d0,ninety=90.d0,tenfour=1.d4)
      parameter(pi=3.141592653589793d0,twopi=6.283185307179586d0,
     1          sqrtpi=1.772453850905516d0,halfpi=1.570796326794897d0)
      parameter(watts=1.08874d9,Alfven=17045.d0,awfac=.09337d0,
     1          clight=2.99792458d8,cvac=2.99792458d10,
     2          ClassElecRad=2.8179d-17,echarge=1.6022d-19,  
     3          emass=.510999d0,restmass=5.10999d5,
     4          quadfac=1.48603d-2,pwfac=2.454353466d-3,
     5          plasma1=.0153244d0,plasma2=.0271618d0)
      parameter(n_grid=241)
      end module numbers
c**********************************************************************
      module modes
      implicit double precision(a-h,o-z)
      character*11 emmodes
      character*8 sde
      save a1,a2,asde1,asde2,fa1,fa2,fsde1,fsde2,aki1,aki2,w0,w02,
     1     factor,omega,z0,zwaste,P_in,P_beam,xdir,ydir,pfac,plnmax,
     2     phase_a,lnum,nnum,emmodes,sde,snltheta,csltheta
      dimension a1(:,:),a2(:,:),asde1(:,:),asde2(:,:),fa1(:,:),
     1          fa2(:,:),fsde1(:,:),fsde2(:,:),aki1(:,:),aki2(:,:),
     2          w0(:,:),w02(:,:),factor(:,:),omega(:),z0(:,:),
     2          zwaste(:,:),xdir(:),ydir(:),pfac(:,:),plnmax(:),
     3          P_in(:),P_beam(:),phase_a(:,:),lnum(:,:),nnum(:,:),
     4          snltheta(:,:),csltheta(:,:)
      allocatable a1,a2,asde1,asde2,fa1,fa2,fsde1,fsde2,aki1,aki2,w0,
     1            w02,factor,omega,z0,zwaste,xdir,ydir,pfac,plnmax,
     2            P_in,P_beam,phase_a,lnum,nnum,snltheta,csltheta
      data emmodes/'hermite'/,sde/'on'/
      end module modes
c**********************************************************************
      module modes_init
      implicit double precision(a-h,o-z)
      save da,pln,aki,akz
      dimension da(:,:),pln(:,:),aki(:,:),akz(:,:)
      allocatable da,pln,aki,akz
      end module modes_init
c**********************************************************************
      module waveguides
      implicit double precision(a-h,o-z)
      double precision akw_wg,bkw_wg,hakw,hbkw,uk,uk2,bkz
      save facx,facy,term1,term2,disp,ak0,sgn,te,tm,aki_wg,akz_wg,
     1     akln_wg,akln2_wg,pfac_wg,pfac2_wg,factor_wg,phase_wg,xln
     2     akw_wg,bkw_wg,hakw,hbkw,uk,uk2,bkz,frequency
      dimension facx(:),facy(:),term1(:),term2(:),factor_wg(:),ak0(:),
     1          disp(:),sgn(:),te(:),tm(:),akln_wg(:),aki_wg(:,:),
     2          akln2_wg(:),pfac_wg(:),pfac2_wg(:),phase_wg(:,:),
     3          akz_wg(:,:),uk(:),uk2(:),bkz(:)
      dimension xln(2,6,20)
      allocatable facx,facy,term1,term2,disp,ak0,sgn,te,tm,aki_wg,
     1            akln_wg,akln2_wg,pfac_wg,pfac2_wg,phase_wg,factor_wg,
     2            akz_wg,uk,uk2,bkz
      data (xln(2,1,i),i=1,20)/2.4048255577d0,5.5200781103d0,
     1     8.6537279129d0,11.7915344391d0,14.9309177086d0,
     2     18.0710639679d0,21.2116366299d0,24.3524715308d0,
     3     27.493479132d0,30.6346064684d0,33.7758202136d0,
     4     36.9170983537d0,40.0584257646d0,43.1997917132d0,
     5     46.3411883717d0,49.4826098974d0,52.6240518411d0,
     6     55.765510755d0,58.9069839261d0,62.0484691902d0/
      data (xln(2,2,i),i=1,20)/3.83171d0,7.01559d0,10.17347d0,
     1     13.32369d0,16.47063d0,19.61586d0,22.76008d0,25.90367d0,
     2     29.04683d0,32.18968d0,35.33231d0,38.47477d0,41.61709d0,
     3     44.75932d0,47.90146d0,51.04354d0,54.18555d0,57.32753d0,
     4     60.46946d0,63.61136d0/
      data (xln(2,3,i),i=1,20)/5.13562d0,8.41724d0,11.61984d0,
     1     14.79595d0,17.95982d0,21.11700d0,24.27011d0,27.42057d0,
     2     30.5692d0,33.71652d0,36.86286d0,40.00845d0,43.15345d0,
     3     46.29800d0,49.44216d0,52.58602d0,55.72963d0,58.87302d0,
     4     62.01622d0,65.15927d0/
      data (xln(2,4,i),i=1,20)/6.38016d0,9.76102d0,13.01520d0,
     1     16.22347d0,19.40942d0,22.58273d0,25.74817d0,28.90835d0,
     2     32.06485d0,35.21867d0,38.37047d0,41.52072d0,44.66974d0,
     3     47.81779d0,50.96503d0,54.11162d0,57.25765d0,60.40322d0,
     4     63.54840d0,66.69324d0/
      data (xln(2,5,i),i=1,20)/7.58834d0,11.06471d0,14.37254d0,
     1     17.61597d0,20.82693d0,24.01902d0,27.19909d0,30.37101d0,
     2     33.53714d0,36.69900d0,39.85763d0,43.01374d0,46.16785d0,
     3     49.32036d0,52.47155d0,55.62165d0,58.77084d0,61.91925d0,
     4     65.06700d0,68.21417d0/
      data (xln(2,6,i),i=1,20)/8.77148d0,12.33860d0,15.70017d0,
     1     18.98013d0,22.21780d0,25.43034d0,28.62662d0,31.81172d0,
     2     34.98878d0,38.15987d0,41.32638d0,44.48932d0,47.64940d0,
     3     50.80717d0,53.96303d0,57.11730d0,60.27025d0,63.42205d0,
     4     66.57289d0,69.72289d0/
      data (xln(1,1,i),i=1,19)/3.8317059702d0,7.0155866698d0,
     1     10.1734681351d0,13.3236919363d0,16.4706300509d0,
     2     19.6158585105d0,22.7600843806d0,25.9036720876d0,
     3     29.0468285349d0,32.189679911d0,35.3323075501d0,
     4     38.4747662348d0,41.6170942128d0,44.7593189977d0,
     5     47.9014608872d0,51.0435351836d0,54.1855536411d0,
     6     57.3275254379d0,60.4694578453d0/
      data (xln(1,2,i),i=1,20)/1.84118d0,5.33144d0,8.53632d0,
     1     11.70600d0,14.86359d0,18.01553d0,21.16437d0,24.31133d0,
     2     27.45705d0,30.60192d0,33.74618d0,36.88999d0,40.03344d0,
     3     43.17663d0,46.31960d0,49.46239d0,52.60504d0,55.74757d0,
     4     58.89000d0,62.03235d0/
      data (xln(1,3,i),i=1,20)/3.05424d0,6.70613d0,9.96947d0,
     1     13.17037d0,16.34752d0,19.51291d0,22.67158d0,25.82604d0,
     2     28.97767d0,32.12733d0,35.27554d0,38.42265d0,41.56893d0,
     3     44.71455d0,47.85964d0,51.00430d0,54.14860d0,57.29260d0,
     4     60.43635d0,63.57989d0/
      data (xln(1,4,i),i=1,20)/4.20119d0,8.01524d0,11.34592d0,
     1     14.58585d0,17.78875d0,20.97248d0,24.1449d0,27.31006d0,
     2     30.47027d0,33.62695d0,36.78102d0,39.93311d0,43.08365d0,
     3     46.23297d0,49.3813d0,52.52882d0,55.67567d0,58.82195d0,
     4     61.96775d0,65.11315d0/
      data (xln(1,5,i),i=1,20)/5.31755d0,9.28240d0,12.68191d0,
     1     15.96411d0,19.19603d0,22.40103d0,25.58976d0,28.76784d0,
     2     31.93854d0,35.10392d0,38.26532d0,41.42367d0,44.57962d0,
     3     47.73367d0,50.88616d0,54.03737d0,57.18752d0,60.33677d0,
     4     63.48526d0,66.63309d0/
      data (xln(1,6,i),i=1,20)/6.41562d0,10.51986d0,13.98719d0,
     1     17.31284d0,20.57551d0,23.80358d0,27.01031d0,30.20285d0,
     2     33.38544d0,36.56078d0,39.73064d0,42.89627d0,46.05857d0,
     3     49.21817d0,52.37559d0,55.53120d0,58.68528d0,61.83809d0,
     4     64.98980d0,68.14057d0/
      end module waveguides
c**********************************************************************
      module particles
      implicit double precision(a-h,o-z)
      save x,y,psi,px,py,pz,fx,fy,fpsi,fpx,fpy,fpz,weights,sumw,al,
     1     Rbrms,xavg,yavg,gamma0,ipart
      dimension x(:,:),y(:,:),psi(:,:),px(:,:),py(:,:),pz(:,:),fx(:,:),
     1  fy(:,:),fpsi(:,:),fpx(:,:),fpy(:,:),fpz(:,:),weights(:,:),
     2  sumw(:),al(:,:,:),Rbrms(:),xavg(:),yavg(:),gamma0(:,:),ipart(:)
      allocatable x,y,psi,px,py,pz,fx,fy,fpsi,fpx,fpy,fpz,weights,sumw,
     1            al,Rbrms,xavg,yavg,gamma0,ipart
      end module particles
c**********************************************************************
      module particles_ode
      implicit double precision(a-h,o-z)
      save qx,qy,qpsi,qpx,qpy,qpz,Prob,r2,mkill
      dimension qx(:,:),qy(:,:),qpsi(:,:),qpx(:,:),qpy(:,:),qpz(:,:),
     1          Prob(:,:),r2(:,:),mkill(:,:)
      allocatable qx,qy,qpsi,qpx,qpy,qpz,Prob,r2,mkill
      end module particles_ode
c**********************************************************************
      module particles_func
      implicit double precision(a-h,o-z)
      save arg,gama,r,snphi,csphi,wfacy,rsqr,elnfac,bx,by,wfac,bz
      dimension arg(:),gama(:),r(:),snphi(:),csphi(:),wfacy(:),rsqr(:),
     1          elnfac(:),bx(:),by(:),wfac(:),bz(:)
      allocatable arg,gama,r,snphi,csphi,wfacy,rsqr,elnfac,bx,by,wfac,
     1            bz
      end module particles_func
c**********************************************************************
      module wiggler
      implicit double precision(a-h,o-z)
      save betaw,bwi,zwi,zstart,zstop,akw,rkw,axkw,wfac1,wfac2,wfac3,
     1     wfac4,wigangle,zsatseg,dbwseg,dzwseg,ntype,m1,m2,m3,
     2     nwigup,nwigdn,nwigh,wigharms,sinephi,cosinephi
      dimension betaw(:),bwi(:),zwi(:),zstart(:),zstop(:),akw(:),
     1          rkw(:),axkw(:),wfac1(:),wfac2(:),wfac3(:),wfac4(:),
     2          wigangle(:),zsatseg(:),dbwseg(:),dzwseg(:),
     3          ntype(:),m1(:),m2(:),m3(:),nwigup(:),nwigdn(:),
     4          nwigh(:),wigharms(:,:)
      allocatable betaw,bwi,zwi,zstart,zstop,akw,rkw,axkw,wfac1,wfac2,
     1            wfac3,wfac4,wigangle,zsatseg,dbwseg,dzwseg,ntype,m1,
     2            m2,m3,nwigup,nwigdn,nwigh,wigharms
      end module wiggler
c**********************************************************************
      module wigg_errors
      implicit double precision(a-h,o-z)
      save bwerr,dzerr
      dimension bwerr(:,:),dzerr(:)
      allocatable bwerr,dzerr
      end module wigg_errors
c**********************************************************************
      module focus
      implicit double precision(a-h,o-z)
      save bfocus,zstt,zstp,snangle,csangle,zupdown,bqfocus,Qks,Qx0,
     1     Qy0,ndfoc,nfoc
      dimension bfocus(:),zstt(:),zstp(:),snangle(:),csangle(:),
     1          zupdown(:),bqfocus(:),Qks(:),Qx0(:),Qy0(:),ndfoc(:),
     2          nfoc(:)
      allocatable bfocus,zstt,zstp,snangle,csangle,zupdown,bqfocus,
     1            Qks,Qx0,Qy0,ndfoc,nfoc
      end module focus
c**********************************************************************
      module harmonics
      implicit double precision(a-h,o-z)
      save Psum,wharm,alphah,nh1,nh2
      dimension Psum(:,:),wharm(:,:),alphah(:,:),nh1(:,:),nh2(:,:)
      allocatable Psum,wharm,alphah,nh1,nh2
      end module harmonics
c**********************************************************************
      module gauss_weights
      implicit double precision(a-h,o-z)
      save xw,w
      dimension xw(:,:),w(:,:)
      allocatable xw,w
      end module gauss_weights
c**********************************************************************
      module arrays_init
      implicit double precision(a-h,o-z)
      save wdv,xdv,wphi,xphi,wpsi,xpsi
      dimension wdv(:),xdv(:),wphi(:),xphi(:),wpsi(:),xpsi(:)
      allocatable wdv,xdv,wphi,xphi,wpsi,xpsi
      end module arrays_init
c**********************************************************************
      module multi_beams
      implicit double precision(a-h,o-z)
      save x0,y0,dE,dI,gammab,sigmax,sigmay,sigmapx,sigmapy,xi,beam_i,
     1     costhx,costhy,sinthx,sinthy,dgamG,px0,py0,epsxi,epsyi,
     2     twissalphxi,twissalphyi,twissgamxi,twissgamyi,betatxi,
     3     betatyi
      dimension x0(:,:),y0(:,:),dE(:,:),dI(:,:),gammab(:,:),xi(:,:),
     1          sigmax(:,:),sigmay(:,:),sigmapx(:,:),sigmapy(:,:),
     2          beam_i(:,:),costhx(:,:),costhy(:,:),sinthx(:,:),
     3          sinthy(:,:),dgamG(:,:),px0(:,:),py0(:,:),epsxi(:,:),
     4          epsyi(:,:),twissalphxi(:,:),twissalphyi(:,:),
     5          twissgamxi(:,:),twissgamyi(:,:),betatxi(:,:),
     6          betatyi(:,:)
      allocatable x0,y0,dE,dI,gammab,sigmax,sigmay,sigmapx,sigmapy,xi,
     1            beam_i,costhx,costhy,sinthx,sinthy,dgamG,px0,py0,
     2            epsxi,epsyi,twissalphxi,twissalphyi,twissgamxi,
     3            twissgamyi,betatxi,betatyi
      end module multi_beams
c**********************************************************************
      module bessel
      implicit double precision(a-h,o-z)
      save xbes,besjl,besjp,fm1,alphab,rx,fm,fmk
      dimension xbes(:),besjl(:),besjp(:),fm1(:),alphab(:),rx(:),
     1      fm(:),fmk(:)
      allocatable xbes,besjl,besjp,fm1,alphab,rx,fm,fmk
      end module bessel
c**********************************************************************
      module rf_acc
      implicit double precision(a-h,o-z)
      save sinrf,cosrf,stheta,ctheta,sinrfm,cosrfm,sinrfp,cosrfp,
     1     besjm
      dimension sinrf(:),cosrf(:),stheta(:),ctheta(:),sinrfm(:),
     1          cosrfm(:),sinrfp(:),cosrfp(:),besjm(:)
      allocatable sinrf,cosrf,stheta,ctheta,sinrfm,cosrfm,sinrfp,
     1            cosrfp,besjm
      end module rf_acc
c**********************************************************************
      module FFT
      implicit double precision(a-h,o-z)
      save A1FFT,A2FFT
      dimension A1FFT(:,:,:),A2FFT(:,:,:)
      allocatable A1FFT,A2FFT
      end module FFT
c**********************************************************************
      module Head_Power
      implicit double precision(a-h,o-z)
      save Phead,a2head,lhead,nhead
      dimension Phead(:,:),lhead(:,:),nhead(:,:)
      allocatable Phead,lhead,nhead
      end module Head_Power
c**********************************************************************
      module Slice_Diags
      implicit double precision(a-h,o-z)
      parameter(n_spent=100)
      save P_slice,P00_slice,gmn,gmx,E_spent,P_spent,beam_ns
      dimension P_slice(:,:,:),gmn(:),gmx(:),E_spent(:),P_spent(:,:),
     1          beam_ns(:),P00_slice(:,:,:)
      allocatable P_slice,P00_slice,gmn,gmx,E_spent,P_spent,beam_ns
      end module Slice_Diags
c**********************************************************************
      module Spec_Diags
      implicit double precision(a-h,o-z)
      save P_spec
      dimension P_spec(:,:)
      allocatable P_spec
      end module Spec_Diags
c**********************************************************************
      module M_sqr
      implicit double precision(a-h,o-z)
      save A1_M2,A2_M2,asde1_M2
      dimension A1_M2(:,:,:),A2_M2(:,:,:),asde1_M2(:,:,:)
      allocatable A1_M2,A2_M2,asde1_M2
      end module M_sqr
c**********************************************************************
      module RFaccel
      implicit double precision(a-h,o-z)
      save Eacc,z1acc,z2acc,Pfacrf,omegarf,aklnrf,akzrf,xlnrf,
     1     Pinrf,rgkwrf,Prf,dgamdt,Gloss,lnumrf,nnumrf,moderf
      dimension Eacc(:,:),z1acc(:),z2acc(:),Pfacrf(:),omegarf(:),
     1          aklnrf(:),akzrf(:),xlnrf(:),Pinrf(:,:),rgkwrf(:),
     2          Prf(:,:),dgamdt(:,:),Gloss(:,:),lnumrf(:),
     2          nnumrf(:),moderf(:)
      allocatable Eacc,z1acc,z2acc,Pfacrf,omegarf,aklnrf,akzrf,xlnrf,
     1     Pinrf,rgkwrf,Prf,dgamdt,Gloss,lnumrf,nnumrf,moderf
      end module RFaccel
c**********************************************************************
      module bmapmod
      implicit double precision(a-h,o-z)
      save bzarr,bxarr,byarr
      dimension bzarr(:,:,:),bxarr(:,:,:),byarr(:,:,:)
      allocatable bzarr,bxarr,byarr
      end module bmapmod
c**********************************************************************
      module magmap
      implicit double precision(a-h,o-z)
      character*10 Bmapfile
      save delz,delx,dely,xfmax,yfmax,Bmapfile,nbx,nby,nbz
      dimension delz(:),delx(:),dely(:),xfmax(:),yfmax(:),Bmapfile(:),
     1          nbx(:),nby(:),nbz(:)
      allocatable delz,delx,dely,xfmax,yfmax,Bmapfile,nbx,nby,nbz
      end module magmap
c**********************************************************************
      module noise_array
      implicit double precision(a-h,o-z)
      save cfac
      dimension cfac(:,:)
      allocatable cfac
      end module noise_array
c**********************************************************************
      module opticsmod
      implicit double precision(a-h,o-z)
      save
      parameter (IOPC=8)
      character*20 Config_Minerva,Config_Mercury
      character*20 MercuryDFLfile,MinervaDFLfile,DFLfile
      character*3  MpiIO
      double complex cmplxcoefs
      dimension zd(3),Wz2(3),WSQ(3),Wa2(3)
      dimension cmplxcoefs(:,:)
      allocatable cmplxcoefs
      namelist/optics/ndfl,ndflx,ndfly,dxdy,dxgrid,dygrid,nslcs,cavLsc,
     1                reprate,nsigma,gridsize,
     2                Config_Minerva,Config_Mercury,
     3                MinervaDFLfile,MercuryDFLfile,
     4                MpiIO
      data Config_Minerva/'MinervaConfig.in'/,
     1     Config_Mercury/'MercuryConfig.in'/,
     2     nsigma/0/,gridsize/0.d0/,MpiIO/'yes'/
c
c        cavLsc = cavity length in meters
c       reprate = repetition rate of the electron beam in Hz
c   ndlfx,ndfly = number of grid points in x and y direction, defaults
c                 to ndfl
c dxgrid,dygrid = distance between grid points, defaults to dxdy
c         nslcs = number of slices
c        nsigma = factor to calculate grid size 
c                 (-nsigma*waist to +nsigma*waist)
c      gridsize = calculated size of the grid (-gridsize to +gridsize)
c         MpiIO = swith for using MPI-IO to write dfl files or normal 
c                 fortran IO (single core IO), values YES and NO
      end module opticsmod
c**********************************************************************
      module OPCmodes
      implicit double precision(a-h,o-z)
      save
      integer maxorder,lmin
      double precision delbx,delby,az,wz,wh,zr,wavelength,waist_OPC
      double complex VP,simparray
      double complex answer
      dimension RecAmp1(:,:),RecAmp2(:,:)
      dimension xgrid(:),ygrid(:),VP(:,:,:,:),simparray(:,:)
      allocatable xgrid,ygrid,VP,simparray,RecAmp1,RecAmp2
      end module OPCmodes
c**********************************************************************
      module storage
      implicit double precision(a-h,o-z)
      save
      integer nzlen,nthhstep,nrmdim
      dimension zwrite(:),zhwrite(:),Bwork(:,:),Ewrite(:),Ework(:)
      dimension Pfinal(:),Phfinal(:,:),Pwrite(:,:),Pwork(:,:)
      allocatable zwrite,zhwrite,Pfinal,Bwork,Pwrite,Pwork
      allocatable Phfinal,Ewrite,Ework
      end module storage
c**********************************************************************
      module s2e_out
      implicit double precision(a-h,o-z)
      save ns2e,x_out,y_out,px_out,py_out,pz_out,psi_out,ns2e_w,xout_w,
     1     yout_w,pxout_w,pyout_w,pzout_w,psiout_w,xfin,yfin,pxfin,
     2     pyfin,pzfin,psifin
      dimension ns2e(:,:),x_out(:,:),y_out(:,:),px_out(:,:),
     1          py_out(:,:),pz_out(:,:),psi_out(:,:),ns2e_w(:,:),
     2          xout_w(:,:),yout_w(:,:),pxout_w(:,:),pyout_w(:,:),
     3          pzout_w(:,:),psiout_w(:,:),xfin(:),yfin(:),pxfin(:),
     4          pyfin(:),pzfin(:),psifin(:)
      allocatable ns2e,x_out,y_out,px_out,py_out,pz_out,psi_out,
     1            ns2e_w,xout_w,yout_w,pxout_w,pyout_w,pzout_w,
     2            psiout_w,xfin,yfin,pxfin,pyfin,pzfin,psifin
      end module s2e_out
c**********************************************************************
c      This module should be used (uncommented) for the MPI version
c
       module mpi_parameters
       use mpi
       implicit double precision(a-h,o-z)
       integer myid,numprocs,istatus(mpi_status_size)
       integer (kind=MPI_ADDRESS_KIND) :: IO_recsize
       integer IO_filetype  
       save myid,numprocs,IO_recsize,IO_filetype
       end module mpi_parameters
c**********************************************************************
c     This module should be used (uncommented) for the single-cpu
c     version
c
c     module mpi_parameters
c     implicit double precision(a-h,o-z)
c     integer myid,numprocs
c     save myid,numprocs
c
c     interface mpi_bcast
c        module procedure mpi_bcasti
c        module procedure mpi_bcastc
c        module procedure mpi_bcastr
c        module procedure mpi_bcasta
c     end interface
c
c     CONTAINS
c     subroutine MPI_INIT(i1)
c     integer i1
c     return
c     end subroutine MPI_INIT
c     subroutine MPI_FINALIZE(i1)
c     integer i1
c     return
c     end subroutine MPI_FINALIZE
c     subroutine MPI_COMM_RANK(i1,i2,i3)
c     integer i1,i2,i3
c     i2 = 0 ! set rank
c     return
c     end subroutine MPI_COMM_RANK
c     subroutine MPI_COMM_SIZE(i1,i2,i3)
c     integer i1,i2,i3
c     i2 = 1 ! set number of processes
c     return
c     end subroutine MPI_COMM_SIZE
c     subroutine MPI_REDUCE(c,d,i1,i2,i3,i4,i5,i6)
c     double precision, dimension(1) :: c,d
c     integer i1,i2,i3,i4,i5,i6
c     return
c     end subroutine MPI_REDUCE
c     subroutine MPI_ALLREDUCE(c,d,i1,i2,i3,i4,i5)
c     double precision, dimension(1) :: c,d
c     integer i1,i2,i3,i4,i5
c     return
c     end subroutine MPI_ALLREDUCE
c     subroutine MPI_SEND(c,i1,i2,i3,i4,i5,i6)
c     double precision, dimension(1) :: c
c     integer i1,i2,i3,i4,i5,i6
c     return
c     end subroutine MPI_SEND
c     subroutine MPI_RECV(c,i1,i2,i3,i4,i5,i6,i7)
c     double precision, dimension(1) :: c
c     integer i1,i2,i3,i4,i5,i6,i7
c     return
c     end subroutine MPI_RECV
c     double precision function MPI_Wtime()
c     MPI_Wtime=0
c     return
c     end function MPI_Wtime
c     subroutine MPI_BCASTI(c,i1,i2,i3,i4,i5)
c     integer c,i1,i2,i3,i4,i5
c     return
c     end subroutine MPI_BCASTI
c     subroutine MPI_BCASTC(c,i1,i2,i3,i4,i5)
c     character(LEN=i1) :: c
c     integer i1,i2,i3,i4,i5
c     return
c     end subroutine MPI_BCASTC
c     subroutine MPI_BCASTR(c,i1,i2,i3,i4,i5)
c     double precision c
c     integer i1,i2,i3,i4,i5
c     return
c     end subroutine MPI_BCASTR
c     subroutine MPI_BCASTA(c,i1,i2,i3,i4,i5)
c     double precision, dimension(1,1) :: c
c     integer i1,i2,i3,i4,i5
c     return
c     end subroutine MPI_BCASTA
c
c     subroutine input1_bcast
c     return
c     end subroutine input1_bcast
c     subroutine input2_bcast
c     return
c     end subroutine input2_bcast
c     subroutine input3_bcast
c     return
c     end subroutine input3_bcast
c     subroutine input4_bcast
c     return
c     end subroutine input4_bcast
c     subroutine input5_bcast
c     return
c     end subroutine input5_bcast
c     subroutine input6_bcast
c     return
c     end subroutine input6_bcast
c     subroutine input7_bcast
c     return
c     end subroutine input7_bcast
c     subroutine input8_bcast
c     return
c     end subroutine input8_bcast
c     subroutine input9_bcast
c     return
c     end subroutine input9_bcast
c     subroutine input10_bcast
c     return
c     end subroutine input10_bcast
c     subroutine beams_bcast
c     return
c     end subroutine beams_bcast
c     subroutine slip_bcast
c     return
c     end subroutine slip_bcast
c     end subroutine slip_bcast
c     subroutine optics_bcast
c     return
c     end subroutine optics_bcast
c
c     end module mpi_parameters
c**********************************************************************
      module wakes
      implicit double precision(a-h,o-z)
      character*2 acdc
      character*6 material
      save wake,efield,energy_loss,range,material,acdc,npoints
      dimension wake(:),efield(:),energy_loss(:)
      allocatable wake,efield,energy_loss
      data npoints/13/,range/100.d0/,acdc/'ac'/,material/'Cu'/
      end module wakes
c*********************************************************************
      module diagnstc
      implicit double precision(a-h,o-z)
      save ndiags,zdiags
      dimension zdiags(:)
      allocatable zdiags
      data ndiags/0/
      end module diagnstc
c**********************************************************************
      module the_namelists
      use diagnstc
      use wakes
      use waveguides
      use modes
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,fourth=.25d0,half=.5d0,one=1.d0,two=2.d0,
     1          ten=10.d0,amicro=1.d-6,deci=1.d-10,emass=510.999d0,
     2          awfac=.09337d0,plasma1=.0153244d0,plasma2=.0271618d0,
     3          radfac=1.08874d9,hundred=100.d0,hundredth=.01d0,
     4          pi=3.141592653589793d0,twopi=6.283185307179586d0,
     5          halfpi=1.570796326794897d0,tenfour=1.d4,ninety=90.d0,
     6          quadfac=1.48603d-2,tricent=300.d0,three=3.d0,four=4.d0,
     7          degtorad=1.745329252d-2,thou=1.d3,tenth=.1d0,giga=1.d9,
     8          cvac=2.99792458d10,watts=1.08874d9,tenthou=1.d4)
      character*8 beamtype,wigtype,foctype,wigerrs,source,enrgycon,
     1            dcfield,direction,RFfield,prebunch,RFmode,
     2            wigplane,matchbeam,slippage,beam_diags,trans_mode,
     3            FFTchk,main_out,Msqr,Shot_Noise,trans_cor,
     4            newfield,OPC,Power_vs_z,SpentBeam,S2E,Gauss_Quad,
     5            Fluence_only
      character*9 Runge_Kutta
      character*3 Wakefield,dump,restart
      character*2 modetype
      character*5 eof
      character*10 Bmapfilei,interpolation
      character*11 drifttube,S2E_method
      character*12 tprofile,beamloss,teprofile
      character*17 dinput_file
      character*35 filename,filename4
      character*39 filename3
      character*57 filename5,filename7,nameRF1,nameRF2
      character*42 filename2,filename8
      character*49 filename6
      allocatable P_work(:,:,:),FFT1_work(:,:,:),FFT2_work(:,:,:),
     1            a1_opc(:,:),a2_opc(:,:),
     2            asde1_opc(:,:),asde2_opc(:,:),P00_work(:,:,:)
      allocatable sumw_wk(:),P_spent_wk(:,:)
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
      common/spread/dp,n1,n2,npsi,nphi,ndv,npx,npy,ngam
      common/taper/zsat,dbw,dbw_sqr,dzw,nwig_harms
      common/self/rb2,xavgs,yavgs,rbavg2,sfact,rb0avg2,iself
      common/RFacc/RFfield,agkwrf,bgkwrf,hagkwrf,hbgkwrf,n1stpass,
     3             drifttube
      common/profile/akwwaist
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      common/Bmap/nbx_max,nby_max,nbz_max
c     dimension bdat(100,1000),ibdat(100,2)
c
      namelist/inp1/nsegbw,nsegfoc,energy,current,rbmin,rbmax,beamtype,
     1             xbeam,ybeam,epsx,epsy,dgam,dgamz,nwrite,iprop,sde,
     2             emmodes,nbeams,prebunch,psiwidth,B_ratio,matchbeam,
     3             twissalphx,twissalphy,x_center,y_center,slippage,
     4             beam_diags,trans_mode,FFTchk,main_out,drifttube,
     5             Msqr,Shot_Noise,noise_seed,trans_cor,newfield,
     6             OPC,Power_vs_z,SpentBeam,S2E,S2E_method,
     7             nparts_out,dither,nbin,Wakefield,acdc,material,
     8             npoints,range,nwig_harms,dump,restart,
     9             px_in,py_in,Fluence_only,Runge_Kutta
      namelist/slip/tprofile,teprofile,nslices,tpulse,tlight,tslices,
     1              tshift,nslip,nparts_g,dinput_file,interpolation
      namelist/beams/x0i,y0i,dEi,dIi
      namelist/inp2/bw,zw,Nw,zfirst,zlast,nup,ndown,wigtype,ax,m,nwh,
     1           wigplane,RFfield,Edc,z1,z2,Prfin,lrf,nrf,freqrf,
     2           RFmode,n1stpass,nbxi,nbyi,nbzi,xmaxi,ymaxi,Bmapfilei,
     3           ztapersegi,dbwsegi,dzwsegi
      namelist/wigh/bratio,nwh
      namelist/inp3/Qk,bquad,bdip,zLen,angle,z1st,z2nd,zentry,ndfocus,
     1              Qx0i,Qy0i,foctype
      namelist/inp4/rg,rg_x,rg_y,wavelnth,nmodes,mselect,wx,wy,power,
     1              cutoff,polar,freq_wg
      namelist/inp5/nr,ntheta,nx,ny,npsi,nphi,ndv,ngam,Gauss_Quad
      namelist/inp6/zmax,dz,ndz,ztaper,zsat,dbwdz,dbw,dbw_sqr,dzw,
     1              nstep,ndiags
      namelist/inp7/enrgycon,dcfield
      namelist/inp8/wigerrs,source,nerr,error,iseed
      namelist/inp9/nharm,l,n,pin,phase_in,waist,zwaist,direction,imode
      namelist/inp10/zdiag
c
      data l/0/,n/0/,m/2/,nr/10/,ntheta/10/,nx/10/,ny/10/,iseed/-1/,
     1     ndz/-1/,nbeams/1/,mselect/-1/,nstep/1/,ndfocus/10/,
     2     matchbeam/'on'/,wigtype/'ppf'/,beamtype/'gaussian'/,
     3     wigerrs/'off'/,source/'random'/,enrgycon/'off'/,
     4     dcfield/'off'/,OPC/'off'/,
     5     beamloss/'off'/,beam_diags/'off'/,Fluence_only/'no'/,
     6     trans_mode/'off'/,FFTchk/'off'/,main_out/'on'/,Msqr/'off'/,
     7     Shot_Noise/'off'/,trans_cor/'off'/,newfield/'on'/,
     8     Bmapfilei/'none'/,RFmode/'TE'/,Power_vs_z/'on'/,imode/0/
      data SpentBeam/'off'/,S2E/'off'/,S2E_method/'Gaussian'/,
     1     nparts_out/10000/,nbin/41/,dither/0.021d0/,Wakefield/'off'/,
     2     dump/'off'/,restart/'off'/,Gauss_Quad/'yes'/,
     3     Runge_Kutta/'4th_order'/,interpolation/'high_order'/
c
      data bw/5.1d0/,zw/.96d0/,energy/470.d0/,beami/10.d0/,
     1     dgamz/0.d0/,rg/.32d0/,dz/.1d0/,rbmin/0.d0/,rbmax/.4d0/,
     2     zmax/1309.d0/,pin/1000.d0/,ax/2.5d0/,x0i/0.d0/,y0i/0.d0/,
     3     xbeam/1.d0/,ybeam/1.d0/,error/.01d0/,prebunch/'off'/,
     4     cutoff/.01d0/,dgam/0.d0/,dEi/0.d0/,twissalphx/0.d0/,
     5     ztaper/-1.d6/,dbwdz/0.d0/,twissalphy/0.d0/,
     6     B_ratio/1.d0/,freq_wg/0.d0/,x_center/0.d0/,y_center/0.d0/,
     7     px_in/0.d0/,py_in/0.d0/
      end module the_namelists
c**********************************************************************
      module PARMELA
      implicit double precision(a-h,o-z)
      save energies,charges,currents,x_maxs,y_maxs,Twiss_alph_x,
     1     Twiss_alph_y,eps_xs,eps_ys,dgams,sigma_pxs,sigma_pys,
     2     x_avgs,px_avgs,y_avgs,py_avgs
      dimension energies(:),charges(:),currents(:),x_maxs(:),y_maxs(:),
     1          Twiss_alph_x(:),Twiss_alph_y(:),eps_xs(:),eps_ys(:),
     2          dgams(:),sigma_pxs(:),sigma_pys(:),x_avgs(:),
     3          px_avgs(:),y_avgs(:),py_avgs(:) 
      allocatable energies,charges,currents,x_maxs,y_maxs,Twiss_alph_x,
     1            Twiss_alph_y,eps_xs,eps_ys,dgams,sigma_pxs,sigma_pys,
     2            x_avgs,px_avgs,y_avgs,py_avgs
      end module PARMELA
c**********************************************************************
      module canted_poles
      implicit double precision(a-h,o-z)
      save a_cant
      dimension a_cant(:)
      allocatable a_cant
      end module canted_poles

c**********************************************************************
      module direct_import
      implicit double precision(a-h,o-z)
      save Qtot,Ntot,nfrst,q_scale,q_macro,nslices_d
      save current_d,energy_d,dgam_d,epslonx_d,epslony_d
      dimension current_d(:),energy_d(:),dgam_d(:),epslonx_d(:),
     1          epslony_d(:)
      allocatable current_d,energy_d,dgam_d,epslonx_d,epslony_d
      end module direct_import
c**********************************************************************
      module distfile_import
      implicit double precision(a-h,o-z)
      save current_g,nslices_g,nfirst_g,
     1     dtslice_g,dzslice_g,sumI_g,sigmapx_D,sigmapy_D,
     2     xmax_D,ymax_D,twissbetax_g,twissbetay_g
      dimension current_g(:,:),twissbetax_g(:),twissbetay_g(:)
      allocatable current_g,twissbetax_g,twissbetay_g
      end module distfile_import
c**********************************************************************
      program minerva
      use version
      use waveguides
      use particles
      use particles_ode
      use particles_func
      use wiggler
      use wigg_errors
      use focus
      use modes
      use modes_init
      use harmonics
      use multi_beams
      use bessel
      use rf_acc
      use FFT
      use Head_Power
      use Slice_Diags
      use Spec_Diags
      use M_sqr
      use RFaccel
      use magmap
      use bmapmod
      use opticsmod
      use OPCmodes
      use storage
      use mpi_parameters
      use mpi
      use the_namelists
      use PARMELA
      use s2e_out
      use canted_poles
      use wakes
      use direct_import
      use distfile_import
      implicit double precision(a-h,o-z)
c
c----------------------------------------------------------------------
c      nmodes= # of modes in simulation
c           l= mode number in x-direction
c           n= mode number in y-direction
c          rg= radius of drift tube
c       rbmin= minimum beam radius in cm
c       rbmax= maximum beam radius in cm
c       xbeam= x-dimension of beam in cm
c       ybeam= y-dimension of beam in cm
c    x_center= x-offset of the electron beam in cm
c    y_center= y-offset of the electron beam in cm
c       px_in= px offset ofthe electron beam in px/mc
c       py_in= py offset ofthe electron beam in py/mc
c     current= beam current in Amperes
c          xi= beam plasma frequency/(c*kw)
c    wavelnth= wavelength in meters
c       waist= radius(cm) of the Gaussian spot size at beam waist
c          bw= wiggler field in kg
c          zw= wiggler period in cm
c      energy= beam energy in MeV
c       betaw= wiggler cyclotron frequency/(c*kw)
c         pin= input power in watts
c         dp0= spread in parallel momentum/pz0
c       dgamz= spread in axial energy/gamma(initial)
c          dp= spread in parallel momentum/c
c        dgam= total enegy spread (dgamma/gamma)
c          nr= # of electrons in r integration
c      ntheta= # of electrons in theta integration
c        npsi= # of electrons in psi integration
c        nphi= # of electrons in phi integration
c         ndv= # of electrons in dpz integration
c        ngam= # of electrons in gamma integration - Gaussian &
c              Waterbag Models
c      nwrite= 0 - don't write information on higher order modes
c            = 1 - do write information on higher order modes
c        zmax= length of system in meters
c         sde= 'off' - DISABLE Source-Dependent Expansion
c            = 'on'  - ENABLE Source-Dependent Expansion (DEFAULT)
c       iprop= switch which will turn off the Electromagnetic waves.
c                 iprop = 0 - electron beam propagation only
c                 iprop = 1 - full interaction with EM waves (DEFAULT)
c
c  ELECTRON BEAM MODEL
c
c      The program can treat either (1) a solid or an annular beam
c      with a flat-top profile, (2) a solid beam with a parabolic
c      profile, (3) a sheet beam with a flat-top profile, (4) an
c      elliptical beam profile, (5) a Gaussian beam, and (5) a Waterbag
c      beam model. These choices are made as follows:
c
c           beamtype = 'circular' - flat top beam profile
c                    = 'parabolc' - parabolic beam profile
c                    = 'sheet'    - sheet beam profile
c                    = 'elliptic' - elliptical beam profile
c                    = 'gaussian' - Gaussian Beam Model
c                    = 'waterbag' - Waterbag Beam Model
c
c      The quantities RBMIN and RBMAX must be supplied upon input
c      in units of cm for the round beams, and XBEAM and YBEAM must
c      be input for the sheet beam or elliptical beam in units of cm.
c
c      Energy spread is included in the Gaussian and Waterbag beam models
c      ONLY. In order to activate this option choose DGAM in NAMELIST
c      input INP1 and choose NGAM in NAMELIST INP5
c
c  MULTI-BEAM GAUSSIAN MODEL
c
c      MINERVA can treat multiple Gaussian beams under the assumption that
c      beam is a Gaussian with a shift in position of the centroid (in x
c      and y) and/or energy. To ngage this option choose
c
c           nbeams = number of Gaussian beams (default = 1)
c
c      in NAMELIST INP1. MINERVA will then read NAMELIST BEAMS nbeams times.
c      The input paameters for NAMELIST BEAMS are:
c
c           x0i = x-position (in cm) of the centroid of the ith beam
c           y0i = y-position (in cm) of the centroid of the ith beam
c           dEi = energy shift of the ith in MeV
c
c      Note that the default values of these parameters are zero, so that
c      the user can deal with multiple beams that are shifted in centroid
c      but at the same energy, or beams that have the same centroid (on-axis)
c      but are shifted in energy.
c
c  PREBUNCHED ELECTRON BEAM MODEL
c
c      MINERVA can initialize the beam with a prebunched initial pondero-
c      motive phase space distribution. The distribution used is:
c
c                (1-B_ratio)   2*B_ratio      pi*psi
c       F(psi) = ----------- + --------- sin(--------)**2 ; 0 < psi < psiwidth
c                   2*pi        psiwidth     psiwidth
c      and
c
c                (1-B_ratio)
c       F(psi) = -----------                              ; psiwidth < psi < 2*pi
c                   2*pi
c             
c      and zero elsewhere. The input parameters for this option are:
c
c          prebunch = 'on'  - engages prebunched beam option
c                   = 'off' - disengages prebunched beam option (default)
c          psiwidth = width of prebunched beam in radians
c           B_ratio = Bunch ratio (must be less than or equal to 1) that
c                     represents the ratio of the area under the bunch to
c                     the total area
c
c      in NAMELIST INP1
c
c
c  MODE SELECTION
c
c       The program uses either a Gauss-Hermite or a Gauss-Laguerre mode
c       decomposition. The input parameters are:       
c
c            emmodes = 'hermite' for the Gauss-Hermite modes
c                    = 'laguerre' for the Gauss-Laguerre modes 
c                pin = input power
c                  l = mode number in x-direction (Hermite) or azimuthal
c                      direction (Laguerre)
c                  n = mode number in y-direction (Hermite) or radial
c                      direction (Laguerre)
c              polar = � 1 for LHC & RHC polarizations. This is operative
c                      only for the Gauss-Laguerre modes
c
c       The initial beam waist for all of the modes is specified at the
c       same point as the frequency.
c
c  WIGGLER FIELD MODELS
c
c       The program will also utilize 4 different wiggler field
c       models chosen by NWIG. These choices are as follows:
c    
c               wigtype = 'ppf'  - Parabolic-Pole Face Wiggler
c                       = 'cpf'  - Polynomial Wiggler Focusing
c                       = 'fpf'  - Flat-Pole Face Wiggler
c                       = 'cap'  - Canted Pole Face
c                       = 'hel'  - Helical Wiggler Model
c                       = 'hppf' - Harmonic Parabolic-Pole Face Wiggler
c                       = 'hfpf' - Harmonic Flat-Pole face Wiggler
c                       = 'aple' - APPLE_II Wiggler
c                       = 'map'  - Field Map
c
c       The PARABOLIC-POLE-FACE WIGGLER model is as follows:
c
c            Bwx = [cos(kwz) + sin(kwz) (d/kwdz)] Bw(z) sinh(kwx/√2) sinh(kwy/√2)
c            Bwy = [cos(kwz) + sin(kwz) (d/kwdz)] Bw(z) cosh(kwx/√2) cosh(kwy/√2)
c            Bwz = - √2 Bw(z) sin(kwz) cosh(kwx/√2) sinh(kwy/√2)
c
c       The Polynomial focusing model in the x-direction is as follows:
c
c            Bwx = [sin(kwz) - cos(kwz) (d/kwdz)] Bw(z)
c                        * [sinh(kwy) - .5 Y(kwy) (d^2/kw^2dx^2)](d/kwdx) X(x)
c
c            Bwy = [sin(kwz) - cos(kwz) (d/kwdz)] Bw(z)
c                              * [cosh(kwy) - .5 kwy sinh(kwy) (d^2/dx^2)] X(x)
c
c            Bwz = Bw(z) cos(kwz) [sinh(kwz) - .5 Y(kwy) [1 + (d^2/kw^2dx^2)]
c                                                       *(d^2/kw^2dx^2)] X(x)
c                           
c       where
c                      Y(kwy) = kwy cosh(kwy) - sinh(kwy)
c                      X(x) = Bw(z)*[1 + .5*(x/ax)^2m]
c
c       m = 1 or 2, and ax is a real constant.
c
c       The FLAT-POLE FACE WIGGLER model is as follows:
c
c            Bwx = 0
c            Bwy = [sin(kwz) - cos(kwz) (d/kwdz)] Bw(z) cosh(kwy)
c            Bwz = Bw(z) cos(kwz) sinh(kwy)
c
c       The CANTED POLE FACE WIGGLER model is taken from Robinson et al. [IEEE j.
c       Quantum Electron. vol. QE-23, p, 1497 (1987)]. It uses the FLAT-POLE-FACE
c       wiggler model with the addition of focusing elements as follows:
c
c            Bx = -Bw(z)*a*y
c            By = -Bw(z)*a*x
c
c       where the coefficient "a" is determined from the cant angle of the pole
c       faces. The optimal cant angle depends upon the beam energy in such a way
c       that
c
c             a = Krms*kw/(sqrt(2)*gamma)
c
c       NOTE THAT THIS WIGGLER OPTION REQUIRES THAT matchbeam='off'
c
c       The HELICAL WIGGLER model is as follows:
c
c            Bwr = 2I'_1(kwr) [cos(theta-kwz) - sin(theta-kwz) d/d(kwz)] Bw(z)
c
c                         2
c            Bwtheta = - --- I_1(kwr) [sin(theta-kwz) + cos(theta-kwz) d/d(kwz)] Bw(z)
c                        kwr
c
c            Bwz = 2 Bw I_1(kwr) sin(theta-kwz)
c
c       where I_1 is the modified Bessel function of the first kind of
c       order 1, and I'_1 is its derivative.
c
c       The APPLE-II wiggler model is a superposition of two planar fpf fields
c       that are oriented at right angles to each other and shifted in phase by.
c       phi. The two fields are as follows:
c
c          0 < phi < pi/2
c
c            Bwx1 = [1/sqrt(2)]*[sin(kwz) - cos(kwz)*(d/kwdz)]*Bw(z)*cosh(kwy)
c            Bwy1 = [1/sqrt(2)]*[sin(kwz) - cos(kwz)*(d/kwdz)]*Bw(z)*cosh(kwy)
c            Bwz1 = Bw(z)*cos(kwz)*sinh(kwy)
c
c            Bwx2 = [1/sqrt(2)]*[sin(kwz+phi) - cos(kwz+phi)*(d/kwdz)]*Bw(z)*cosh(kwy)
c            Bwy2 = -[1/sqrt(2)]*[sin(kwz+phi) - cos(kwz+phi)*(d/kwdz)]*Bw(z)*cosh(kwy)
c            Bwz2 = Bw(z)*cos(kwz+phi)*sinh(kwx)
c
c          pi/2 < phi < pi
c
c            Bwx1 = [1/sqrt(2)]*[sin(kwz) - cos(kwz)*(d/kwdz)]*Bw(z)*cosh(kwy)
c            Bwy1 = [1/sqrt(2)]*[sin(kwz) - cos(kwz)*(d/kwdz)]*Bw(z)*cosh(kwy)
c            Bwz1 = Bw(z)*cos(kwz)*sinh(kwy)
c
c            Bwx2 = -[1/sqrt(2)]*[sin(kwz+phi) - cos(kwz+phi)*(d/kwdz)]*Bw(z)*cosh(kwy)
c            Bwy2 = [1/sqrt(2)]*[sin(kwz+phi) - cos(kwz+phi)*(d/kwdz)]*Bw(z)*cosh(kwy)
c            Bwz2 = Bw(z)*cos(kwz+phi)*sinh(kwx)
c
c       N.B. The APPLE-II field model requires the use of Gauss-Laguere modes where
c            "polar" is one of the input variables
c
c       In order to use the FIELD MAP for the wiggler, the user must input the
c       parameters FOR EACH WIGGLER SEGMENT in NAMELIST INP2:
c
c                    nbx = number of points in the x-direction
c                    nby = number of points in the y-direction
c                    nbz = number of points in the z-direction
c                  xmaxi = maximum extent of the field map in the x-direction in
c                          units of meters
c                  ymaxi = maximum extent of the field map in the y-direction in
c                          units of meters
c              Bmapfilei = character*10 format input for the name of the input
c                          file for the field map for the ith segment
c
c       The input procedure for the field map is as follows:
c
c                  do k=1,nbz(i)
c                     do i=1,nbx(i)
c                        do j=1,nby(i)
c                           read(71,600) bxarr(i,j,k),byarr(i,j,k),
c    1                                   bzarr(i,j,k)
c 600                       format(1p3e15.8)
c                        end do
c                     end do
c                  end do
c
c       Note that:
c                 (1) the field should be in units of kG
c                 (2) the sequence for the data where i,j,k denotes the x-, y-,
c                     and z-directions
c                 (3) that the format is e15.8
c
c       The wiggler can have multiple segments as selected by NSEGBW. The
c       default is to nsegbw = 1 however. If the choice of nsegbw > 1 is
c       made, then MINERVA will read from namelist inp2 nsegbw times. The
c       parameters in inp2 describe the wiggler in each segment as
c       follows:
c
c                  bw = wiggler amplitude in kG
c                  zw = wiggler period in cm
c              zstart = start point of wiggler segment in meters
c               zstop = end point of wiggler segment in meters
c                 nup = # of wiggler periods in up-taper of wiggler
c               ndown = # of wiggler periods in down-taper of wiggler
c
c  BI-HARMONIC WIGGLERS
c
c       MINERVA can simulate bi-harmonic wigglers. This is chosen in NAMELIST
c       INP2 by choosing:
c
c                wigtype = 'hppf' - harmonic parabolic-pole-face wiggler
c                        = 'hfpf' - harmonic flat-pole-face wiggler
c                    nwh = harmonic number
c
c       The orientation of the harmonic wiggler field is perpendicular to the
c       fundamental component.
c
c  HARMONIC SERIES WIGGLER
c
c       MINERVA can also model a FLAT-POLE-FACE wiggler withan arbitrary number of
c       harmonic terms. To enable this select wigtype='fpf', and set
c
c                nwig_harms = # of wiggler harmonics (not counting the fundamental)
c
c       in NAMELIST/INP2/. There will then be an additional NAMELIST/WIGH/ that
c       will be read in nwig_harms time for each harmonic component. The input
c       variables for NAMELIST/WIGH/ are:
c
c              bratio = ratio of the harmonic amplitude to that of the fundamental
c                 nwh = harmonic number
c
c  TAPERED WIGGLER MODEL
c
c       The program can also treat the case of tapered wigglers. When a single-segment
c       wiggler is chosen [i.e., NSEGBW=1], then the taper is specified as follows:
c
c             Bw(z) = Bw*[1 + dbw*kw*(z-ztaper)
c                               +dbw_sqr*(z-ztaper)**2/(zmax-ztaper)**2]
c
c       so that the taper can have linear and parabolic components. Here
c
c                ztaper = the start-taper point in units of meters
c                  zsat = alternate entry for the start-taper point
c                       = kw*ztaper (using zsat overrides ztaper)
c                   dbw = normalized slope of the wiggler field for the
c                         linear taper [=(1/Bw*kw)*dBw/dz]
c                       = 0 (default)
c               dbw_sqr = total relative magnitude of the parabolic field change,
c                         dBw/Bw, over the total distance of the taper
c                       = 0 (default)
c
c       N.B.: the parabolic taper is not available when NSEGBW > 1
c
c       The program can also treat a tapered wiggler period. In this
c       case, we adopt the following model of the wiggler wavenumber:
c
c                   kw(z) = kw*[1 + dzw*(kw*z-kw*zsat)]
c
c      Here kw denotes the initial value of the wavenumber in the
c      uniform region. The parameter dzw denotes the slope of the taper
c      as normalized to kw.
c
c      If NSEGBW = 1, then zsat, dbw, and dzw are entered in NAMELIST INP6.
c      If it is so desired, then the rate of taper can be entered in
c      units of kG/m and the start taper point can be entered in units
c      of m. To do so, enter the following parameters:
c
c                       ztaper = start-taper point in m
c                        dbwdz = rate of taper in kG/m
c
c      If NSEGBW > 1, the code will also allow for an arbitrary taper of
c      each wiggler segment. To engage this, set the following parameters in
c      NAMELIST INP2:
c
c           ztapersegi = start taper point in meters as measured from z = 0
c              dbwsegi = normalized slope of the amplitude (as defined in dbw
c                        in NAMELIST INP6)
c              dzwsegi = normalized slope of the wiggler period (as defined in
c                        dzw in NAMELIST INP6)
c
c      The code will then convert these parameters to the appropriate parameters
c      for the taper field model.
c
c  WIGGLER FIELD ERRORS
c
c       The program also includes wiggler field errors in the analysis. To enable
c       this option set WIGERR = 'on'. The model assumes that the wiggler ampli-
c       tude changes on a scale length of zw/NERR. The field errors are chosen
c       on this interval either with a random number generator or via a hard-
c       wired array. Once the field amplitude is chosen by either method, the
c       field is mapped between these discrete points using a sin**2 function.
c       NO WIGGLER ERROR IS USED IN THE ENTRY/EXIT TAPER REGION IN ORDER TO
c       ENSURE THAT THE WIGGLER AMPLITUDE REMAINS POSITIVE.
c
c       To enable the random number generator, set SOURCE = 'random'. Once this
c       option is chosen, the user must also input an integer ISEED to initialize
c       the random number generator, and ERROR which specifies the rms magnitude of
c       the field imperfections.
c
c       To enable the hard-wired error model, select SOURCE = 'fixed'. To use this
c       option, the user must also specify IBDAT(nsegbw,2) and BDAT(nsegbw,npts).
c       Here, IBDAT(nsegbw,1) = NERR for each wiggler segment, and
c       IBDAT(nsegbw,2) = npts (# of points where the amplitude error is specified.
c       In addition, BDAT(nsegbw,npts) is the magnitude of the amplitude fluctua-
c       tion corresponding to each point for each wiggler segment. 
c
c  MAGNETIC FOCUSING
c
c       The program can also handle quadrupole and/or dipole focusing, with
c       multiple segments. The number of segments is selected via nsegfoc.
c       The DEFAULT is to nsegfoc = 0, which chooses no quadrupole or dipole
c       fields.The field model used is controlled by nfocus via
c
c                 foctype = 'quad'   - Quadrupole Field
c                         = 'dipole' - Dipole Field
c
c       The QUADRUPOLE field model used is:
c
c                  Bx = Bq(z) * (y - y0)/aq
c                  By = Bq(z) * (x - x0)/aq
c
c       where Bq(z) is the amplitude, and aq is a transverse scale
c       length, and (x0,y0) is the offset of the centroid. NOTE THAT THIS
c       FIELD IS DIVERGENCE-FREE FOR ANY CHOICE OF Bq(z). This field is
c       also specified by segments, and a choice of nsegfoc > 1 causes
c       the namelist inp3 to be read nsegfoc times.
c
c       The input parameters for each segment correspond to the focal
c       length in meters, and the initial and final positions of each
c       segment in meters. MINERVA then calculates B0/aq in normalized
c       units. It uses the formula
c
c                     Bq            10**(-6)
c                    ____ [kG/cm] = ________ Qk[1/m**2]*Eb[keV]
c                     aq              3
c
c       where Eb = beam energy in keV, Qk is the harmonic periodicity
c       parameter in the dynamical equation x" + Qk*x = 0. As a result, 
c       the normalized amplitude which is used in the particle push is
c       given by
c
c             1      eBq       0.09337 10**(-6)  
c           _____ _________  = _______ ________ Qk[1/m**2]*Eb[keV]*zw[cm]**2
c           kw*aq m(ckw)**2      2*pi     3
c
c       where 0.09337/(2*pi) = 0.0148603. The input parameters are:       
c
c             bquad = quadrupole field gradient in kG/cm ----|
c                   or                                       |-use only one of these
c                Qk = quadrupole parameter in 1/meters**2 ---|
c              Qx0i = position of centroid in x (meters)
c              Qy0i = position of cnetroid in y (meters)
c              z1st = start point in meters
c              z2nd = end point in meters
c
c       The focal length of the quad, f, is given by:
c
c              f = 33.3565 [kG-m] * Energy [GeV]/(bquad [kG/m] * L [m])
c
c       where L = length of the quad
c
c       The DIPOLE Field model is as follows:
c
c                Bx = 0
c                By = Bd
c                Bz = 0
c
c       where Bd is the field value in kG.
c
c       NOTE THAT AT THE PRESENT TIME, BOTH FIELD MODELS EXHIBIT A SHARP
c       STEP INCREASE/DECREASE AT THE ENDS OF THE SEGMENTS. 
c
c  SELF-FIELD & ENERGY CONSERVATION OPTIONS
c
c       The program will also perform an energy conservation check if
c       the parameter ENRGYCON is set to 'on' on input.
c
c       In order to enable the self-field option set DCFIELD to 'on'
c       on input. The program will then calculate the average beam
c       radius at each step and compute the effect of the self-fields
c       on the electron trajectories. NOTE, however, that the
c       algorithm used implicitly assumes a solid pencil beam;
c       hence, there may be difficulties in treating an annular beam.
c
c  DC AND AC ACCELERATING FIELDS
c
c       Both DC and AC accelerating potentials may be applied in each 
c       wiggler segment. The inputs for these options are in NAMELIST
c       INP2 where, for each wiggler segment, the user must select:
c
c             1. DC Accelerating Field:
c
c                RFfield = 'dc' or 'on'
c                    Edc = Accelerating field in MV/m                     
c                     z1 = start point for the field in meters
c                     z2 = stop point for the field in meters
c
c             2. RF Accelerating Field: TE or TM modes in a cylindrical
c                                       waveguide
c                RFfield = 'rf'
c                  Prfin = Injected power in Watts
c                 RFmode = 'TE' or 'TM'
c                    lrf = azimuthal mode number
c                    nrf = transverse mode number
c                 freqrf = RF frequency in GHz
c                     z1 = start point for the field in meters
c                     z2 = stop point for the field in meters
c
c  SLIPPAGE EXTENSION
c
c       Slippage can be included in the simulation by the simualtion of
c       multiple slices over some pulse time. The procedure allows for
c       a pulse time greater than the actual current pulse to allow for
c       the slippage of the light relative to the electron beam.
c
c       Slippage is engaged by defining the character variable
c       "slippage" to be 'on' in Namelist inp1. The default value is
c       'off'. In addition, the user can select an FFT analysis of the
c       time dependence. This is also done in NAMELIST inp1 by setting
c       the charatcer variable "FFTchk" = 'on'. The default value is
c       FFTchk = 'off'.
c
c       When slippage = 'on', MINERVA reads an additional NAMELIST
c       designated by "slip' immediately after reading NAMELIST inp1.
c       The user has a choice of three current profiles: top-hat,
c       parabolic, and Gaussian. These are set by the character variable
c
c             tprofile = 'tophat' (default)
c                      = 'parabolic'
c                      = 'gaussian'
c
c       Additional variables that must be set in NAMELIST slip are:
c
c              nslices = number of slices
c                nslip = interval between the number of steps for which
c                        slippage is applied
c               tpulse = pulse time for current pulse
c               tlight = pulse time for the seed laser pulse
c              tslices = total pulse time for all slices
c               tshift = time shift of the light pulse relative to the
c                        e-beam pulse
c        interpolation = character variable that determined the algorithm
c                        used to interpolate the field when slippage is
c                        applied
c                           = 'high_order' (default)- high order algorithm
c                             that impicitly conserves energy when the field
c                             is advanced, however significantly distorts 
c                             pulse shape when short optical spikes are present.
c                           = 'linear' - linear interpolation algorithm
c                             This conserves energy rigorously only when
c                             slippage is applied at intervals corresponding
c                             to the separation between slices
c
c       All times are in units of seconds, and tslices MUST BE GREATER
c       THAN OR EQUAL TO tpulse.
c
c       The variable "nslip" controls when the communication between
c       the slices is applied. Thus, if nslip = 1, then the time
c       derivative linking the different slices calcuated on every
c       integration step.
c
c       N.B.: the only variable that can change between the e-beam
c             slices in this version is the current.
c
c
c  INCLUSION OF SHOT-NOISE
c
c       MINERVA will impose a shot noise distribution by adding a random
c       shift in the initial phase of the electrons. The input para-
c       meters in NAMELIST/inp1/ to engage and control this option are:
c
c            Shot_Noise = Character control parameter to turn this
c                         option on or off.
c                            = 'on'  --> turns shot-noise on
c                            = 'off' --> turns shot noise off (default)
c            noise_seed = Integer parameter used to initialize the
c                         random number generator. This must be set to
c                         a negative integer.
c             trans_cor = Character control parameter that sets an
c                         for the generation of the random phase shifts.
c                            = 'on'  --> turns on the option
c                            = 'off' --> turns off the option (default)
c                         When turned off, the magnitude of the phase
c                         shift is set by the total number of electrons
c                         over the entire beam cross-setion. When turned
c                         on, the magnitude of the phase shift depends
c                         the fraction of the current carried by each
c                         line of electrons at each given radial posi-
c                         tion.
c
c   COMPILER OPTIONS
c
c       In order to read/write binary data for use with OPC requires or various
c       beam production codes such as ELEGANT, DIMAD, or etc., the compilation
c       with the INTEL Fortran compiler must invoke the option:
c
c                     -assume:byterecl   WINDOWS
c                     -assume byterecl   LINUX
c
c       to read/write the appropriate record length. This option is not necessary
c       or available using GFORTRAN
c
c      ***********************************************************
c
c      START-TO-END SIMULATION SUPPORT
c
c      INPUT: INTERFACE WITH PARMELA, DIMAD, or ELEGANT
c
c       The interface with these beamcodes involves the first running the
c       translator (ELEGANT2MINERVA). ELEGANT2MINERVA will write a binary
c       file called Minerva_Input.dat that MINERVA will then read and use
c       to generate the initial particle distribution.
c
c       This feature is only accessible when slippage = 'on' and is
c       engaged by choosing any of the following:
c
c                    tprofile = 'PARMELA'
c                             = 'DIMAD'
c                             = 'ELEGANT'
c
c       in NAMELIST &slip. When this is done, the tpulse variable in
c       this namelist is ignored, and this value is obtained from the
c       Minerva_Input.dat file.
c
c       THIS VERSION USES ONLY ONE BEAM PER SLICE FROM ELEGANT2MINERVA, AND SO
c       WILL WORK WITH
c
c
c      OUTPUT: DIMAD FILE FORMAT
c
c      MINERVA will output the spent beam phase space in DIMAD format
c      when the character variable "S2E" is set to 'on' in NAMELIST
c      INP1. There are three algorithms to choose from. One is a random
c      particle sampling, and the others use statistical sampling. The two
c      statistical algorithms are as follows:
c
c               Random: Random particle dump
c
c             Gaussian: Determines the Twiss parameters for the global bunch
c                       and fills in the phase space with random
c                       Gaussian deviates.
c
c             Waterbag: Determines the Twiss parameters for the global bunch
c                       and simply fills in the phase space ellipse.
c
c      The input variables in INP1 for this feature are:
c
c                 S2E  = Character variable to enable this feature. To
c                        enable set S2E = 'on'. The default is 'off'
c           S2E_method = Character variable that selects the sampling
c                        algorithm
c                           = 'random' for the random sampling
c                           = 'Gaussian' (default)
c                           = 'Waterbag'
c           nparts_out = The number of particles that will be written
c                        to the output file.
c               dither = The amount of a random shift in the phase space
c                        of each of the output particles for the choice
c                        S2E_method = 'random'
c                 nbin = Integer variable that sets the number of bins
c                        to be used in the statistical sampling of data
c                        for the choice S2E_method = 'statistics3'
c
c      The output file is called "Spent_Beam_Out.txt" and is formatted
c      as an ACSII file and written via format(6(1x,1pe11.4)). The
c      columns represent:
c
c                   column           variable (units)
c                  -------------------------------------
c                     1                 x  (meters)
c                     2                 x' (radians)
c                     3                 y  (meters)
c                     4                 y' (radians)
c                     5                 z-z_avg (meters)
c                     6                 dpz/pz_avg
c                  -------------------------------------
c
c      **************************************************************
c
c      RESTART CAPABILITY
c
c      Jobs can be restarted by the following process. There are two
c      new character*3 parameters to be entered in NAMELIST INP1:
c
c             dump = 'yes' - dump the particle and field data at the
c                            end of the run
c                     'no' - DEFAULT - do not dump the particle and
c                            field data - This is normal operation
c          restart = 'off' - DEFAULT - no restart desired
c                    'on'  - restart job
c
c      The procedure to use restart is as follows:
c
c            1. Set the appropriate zmax in NAMELIST INP6 as well as
c               setting dump='yes' in NAMELIST INP1.
c
c      This will cause MINERVA to write a binary file corresponding to 
c      each slice called:
c
c                  Data_Dump_slice=XXXXX.dat
c
c      which contain the particle and field data at z = zmax
c
c            2. Run MINERVA again using the same input file which has
c               been modified by setting a new zmax in NAMELIST inp6
c               and by setting restart='on' in NAMELIST INP1 (Note:
c               change dump='no')
c
c      ***************************************************************
c
c      GENESIS DISTFILE INPUT
c
c      jobs can be run using a specfic particle load that is derived
c      from a GENESIS-produced particle distribution file. This option
c      is engaged by setting
c
c          tprofile='DISTFILE'
c
c      in NAMELIST slip. Additional parameters that must be included
c      in this namelist are:
c
c             nparts_g = number of partciles in each slice
c          dinput_file = character*17 file that specifies the name of
c                        the distfile
c
c      There must also be an ASCII file named "current.dat" in the run
c      directory that contains the temporal profile of the current on
c      a slice by slice basis.This file contains two columns of data.
c      The first column specifies the position of the slice within the
c      bunch in units of nm. The second column contains the current in
c      Amperes
c
c      ***************************************************************
c
c      OUTPUT UNITS
c
c      FIXED NUMBER OUTPUT UNITS
c
c      unit=10 --> Output Spent Beam Energy Distribution
c                  opened/closed in Spent_Beam
c
c      unit=11 --> Beam Diagnostic Output
c                  opened in Head_diag/closed in ODE
c
c      unit=12 --> Mode Diagnostic Output
c                  opened in Head_diag/closed in ODE
c
c      unit=13 --> Magnetic Field Map
c                  opened in ODE
c
c      unit=14 --> Output of 'On-Axis_Wiggler_Field.txt'
c                  opened/closed in Main Program
c
c      unit=15 --> Output 'Energy_Loss.txt' when wakefields are included
c                  opened/closed in subroutine Wakefields
c
c      unit=16 --> writes dfl file when OPC='on'
c                  opened/closed in subroutine write_dfl
c
c      unit=17 --> scratch file written when nwrite > 0
c                  writes HOM data
c                  opened/closed in Main Program
c                  written in subroutine ODE
c
c      unit=18 --> scratch file written when beam_diags = 'on'
c                  opened/closed in Main Program
c                  written in subroutine ODE
c
c      unit=19 --> writes 'Energy Conservation.txt'
c                  opened/closed in Main Program
c
c      unit=20 --> writes 'Beam_Evolution_slice=XXXX.txt'
c                  or 'Beam_Evolution.txt' files in subroutine
c                  Beam_Evol
c                  opened/closed in Beam_Evol
c
c      unit=21 --> writes file 'S2E_Beam_Out.txt'
c                  opened/closed in Main Program
c
c      unit=22 --> used when OPC = 'on'
c                  opened/closed in subroutine Mode_Out
c                  writes '1st_Harmonic_Power.dat' when slippage='off'
c                  writes '1st_Harmonic_Energy.dat' when slippage='on'
c
c      unit=23 --> writes power at different wavelengths
c                  opened/closed in subroutine Spec_Out
c
c      unit=24 --> used when restart = 'on'
c                  opened/closed in subroutine INIT
c                  writes file 'Data_Dump_slice=XXXXX.txt'
c
c      unit=25 --> writes 'Power_vs_z.txt' files
c                  opened/closed in subroutine Mode_Out
c
c      unit=26 --> writes 'Energy_vs_z.txt' files
c                  opened/closed in subroutine Mode_Out
c
c      unit=27 --> writes 'Fluence_z(m)=X.XXX.txt' files
c                  opened/closed in subroutine ODE
c
c      unit=28 --> Input file (binary) only. Contains detailed particle
c                  phase space. Used when tprofile='DIRECT'
c
c      unit=29 --> writes 'Slice_Statistics.txt' when tprofile='DIRECT'
c                  opened/closed in Main Program
c
c      VARIABLE NUMBER OUTPUT UNITS
c
c      unit=30+ns --> scratch files for Power vs z Data for slice ns
c                     opened/closed in Main Program
c                     written in Main Program and subroutine ODE
c                     read in subroutine Mode_Out
c----------------------------------------------------------------------
c------- MPI Calls
c
      myid=0
      numprocs=1
      call mpi_init(ierr)
      call mpi_comm_rank(mpi_comm_world,myid,ierr)
      call mpi_comm_size(mpi_comm_world,numprocs,ierr)
      if(numprocs.gt.1) starttime=mpi_wtime()
c
c------- Write header line
c
      if(myid.eq.0) write(6,200) Minerva_Version, Version_Date
  200 format(/22x,'****** CODE MINERVA: Version ',f5.2,' - MPI ******',
     1       /,22x,'   Source-Dependent Expansion/Multi-Frequency',
     2       /,22x,' Created by: H.P. Freund & P.J.M. van der Slot',
     3       /,22x,'             Date: ',A18,/)
c
c------- input section
c
c     if(myid.eq.0) write(6,*) ' input inp1' !-------------------------!
      if(myid.eq.0) write(6,*) 'Initialising program ...'              !
      if(myid.eq.0) read(5,inp1)                                       !
      if(numprocs.gt.1) call input1_bcast                              !
      if(myid.eq.0.and.OPC.eq.'on') then                               !
         open(unit=IOPC,file=Config_Mercury,form='formatted',          !
     1        status='replace')                                        !
         write(IOPC,*) ' &inp1'                                        !
         write(IOPC,1111) nsegbw,nsegfoc,iprop,nwrite,nbeams,          !
     1                    noise_seed,nparts_out,nbin,nwig_harms        !
 1111    format('   nsegbw=',i0,/,'   nsegfoc=',i0,/,'   iprop=',i0,/, !
     1          '   nwrite=',i0,/,'   nbeams=',i0,/,                   !
     2          '   noise_seed=',i0,/,'   nparts_out=',i0,/,           !
     3          '   nbin=',i0,/,'   nwig_harms=',i0)                   !
         write(IOPC,1131) energy,current,rbmin,rbmax,xbeam,ybeam,epsx, !
     1                    epsy,dgam,dgamz,psiwidth,B_ratio,twissalphx, !
     2                    twissalphy,dither,x_center,y_center,px_in,   !
     3                    py_in                                        !
 1131    format(SP,'   energy=',1pe17.10,/,'   current=',e17.10,/,     !
     1          '   rbmin=',e17.10,/,'   rbmax=',e17.10,/,'   xbeam=', !--Input INP1
     2          e17.10,/,'   ybeam=',e17.10,/,'   epsx=',e17.10,/,     !
     3          '   epsy=',e17.10,/,'   dgam=',e17.10,/,'   dgamz=',   !
     4          e17.10,/,'   psiwidth=',e17.10,/,'   B_ratio=',        !
     5          e17.10,/,'   twissalphx=',e17.10,/,'   twissalphy=',   !
     6          e17.10,/,'   dither=',                                 !
     7          e17.10,/,'   x_center=',e17.10,/,'   y_center=',e17.10 !
     8          ,/,'   px_in=',e17.10,/,'   py_in=',e17.10)            !
         write(IOPC,8000) trim(beamtype),trim(sde),trim(emmodes),      !
     1                    trim(prebunch),trim(matchbeam),              !
     2                    trim(slippage),trim(beam_diags),             !
     3                    trim(trans_mode),trim(FFTchk),trim(main_out),!
     4                    trim(drifttube),trim(Msqr),trim(Shot_Noise), !
     5                    trim(trans_cor),trim(newfield),trim(OPC),    !
     6                    trim(Power_vs_z),trim(SpentBeam)             !
 8000    format("   beamtype='",a,"'",/,"   sde='",a,"'",/,            !
     1          "   emmodes='",a,"'",/,"   prebunch='",a,"'",/,        !
     2          "   matchbeam='",a,"'",/,                              !
     3          "   slippage='",a,"'",/,"   beam_diags='",a,"'",/,     !
     4          "   trans_mode='",a,"'",/,"   FFTchk='",a,"'",/,       !
     5          "   main_out='",a,"'",/,"   drifttube='",a,"'",/,      !
     6          "   Msqr='",a,"'",/,"   Shot_Noise='",a,"'",/,         !
     7          "   trans_cor='",a,"'",/,"   newfield='",a,"'",/,      !
     8          "   OPC='",a,"'",/,"   Power_vs_z='",a,"'",/,          !
     9          "   SpentBeam='",a,"'")                                !
         write(IOPC,7999) trim(S2E),trim(S2E_method),trim(Wakefield),  !
     1                    trim(acdc),trim(material)                    !
 7999    format("   S2E='",a,"'",/,"   S2E_method='",a,"'",/,          !
     1          "   Wakefield='",a,"'",/,"   acdc='",a,"'",/,          !
     2          "   material='",a,"'")                                 !
         write(IOPC,*) ' /'                                            !
      endif  !---------------------------------------------------------!
c
      if(nwrite.gt.0) open(unit=17,form='unformatted',status='scratch')
      if(beam_diags.eq.'on') then
         open(unit=18,form='unformatted',status='scratch')
      endif
      if(Shot_Noise.eq.'on') noise_seed_1=noise_seed
      if(slippage.ne.'on') Power_vs_z='on'
      if((beamtype.ne.'gaussian').and.(slippage.ne.'off')) then
         write(6,*) ' '
         write(6,*) '**********************************************'
         write(6,*) '* SLIPPAGE ONLY AVAILABLE FOR GAUSSIAN BEAMS *'
         write(6,*) '*            TERMINATE MINERVA               *'
         write(6,*) '**********************************************'
         write(6,*) ' '
         call mpi_finalize(ierr)
         stop
      endif
      if((slippage.ne.'on').and.(FFTchk.ne.'off')) then
         write(6,*) ' '
         write(6,*) '**********************************************'
         write(6,*) "* FFTchk only available when SLIPPAGE = 'on' *"
         write(6,*) '*           FFTchk reset to off              *'
         write(6,*) '**********************************************'
         write(6,*) ' '
         FFTchk='off'
      endif
      ns_frst=1
      ns_last=nslices
      nrmdim=1
      if(slippage.eq.'on') then
         nslip=1
         tlight=-one
         if(myid.eq.0) write(6,*) 
     1                 ' settings for time dependent run ...'
         if(myid.eq.0) then
            read(5,slip)
            if((tprofile.eq.'PARMELA').or.(tprofile.eq.'ELEGANT')                 
     1                  .or.(tprofile.eq.'DIMAD')) tprofile='BEAMCODE'
            if(tprofile.eq.'BEAMCODE') then
               open(unit=1200,file='Minerva_Input.dat',
     1              form='unformatted',status='old')
               read(1200) energy,current,tpulse,epsx,epsy,dgam,x_max,
     1                    twiss_alpha_x,y_max,twiss_alpha_y,sigmapx_P,
     2                    sigmapy_P,x_avg,px_avg,y_avg,py_avg,nslices_P
            endif
         endif
         if(tlight.lt.zero) tlight=tpulse
c        if(teprofile.ne.'variable') then
c           if(tprofile.ne.'BEAMCODE') teprofile=tprofile
c           if(tprofile.eq.'BEAMCODE') teprofile='parabolic'  
c        endif
         if(numprocs.gt.1) call slip_bcast
         if(tprofile.eq.'BEAMCODE') then
            if(numprocs.gt.1) then
               call mpi_bcast(nslices_P,1,mpi_integer,0,mpi_comm_world,
     1                        ier)
               call mpi_bcast(energy,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(current,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)     
               call mpi_bcast(epsx,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(epsy,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(dgam,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(x_max,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)     
               call mpi_bcast(twiss_alpha_x,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(y_max,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(twiss_alpha_y,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(sigmapx_P,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(sigmapy_P,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(x_avg,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(px_avg,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(y_avg,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
               call mpi_bcast(py_avg,1,mpi_double_precision,0,
     1                        mpi_comm_world,ier)
            endif
            allocate(energies(nslices_P),currents(nslices_P),
     1            dgams(nslices_P),eps_xs(nslices_P),eps_ys(nslices_P),
     2            x_maxs(nslices_P),y_maxs(nslices_P),
     3            Twiss_alph_x(nslices_P),Twiss_alph_y(nslices_P),
     4            charges(nslices_P),sigma_pxs(nslices_P),
     5            sigma_pys(nslices_P),x_avgs(nslices_P),
     6            y_avgs(nslices_P),px_avgs(nslices_P),
     7            py_avgs(nslices_P))
            if(myid.eq.0) then
               do ns=1,nslices_P
                  read(1200) energyi,currenti,dgami,eps_xi,eps_yi,
     1                       x_maxi,y_maxi,Twiss_alph_xi,Twiss_alph_yi,
     2                       chargei,sigma_pxsi,sigma_pysi,x_avgsi,
     3                       px_avgsi,y_avgsi,py_avgsi
                  energies(ns)=energyi
                  currents(ns)=currenti
                  dgams(ns)=dgami
                  eps_xs(ns)=eps_xi
                  eps_ys(ns)=eps_yi
                  x_maxs(ns)=x_maxi
                  y_maxs(ns)=y_maxi
                  Twiss_alph_x(ns)=Twiss_alph_xi
                  Twiss_alph_y(ns)=Twiss_alph_yi
                  charges(ns)=chargei
                  sigma_pxs(ns)=sigma_pxsi
                  sigma_pys(ns)=sigma_pysi
                  x_avgs(ns)=x_avgsi
                  px_avgs(ns)=px_avgsi
                  y_avgs(ns)=y_avgsi
                  py_avgs(ns)=py_avgsi
               end do
               close(unit=1200) 
            endif
            if(numprocs.gt.1) then !-----------------------------------!
               call mpi_bcast(energies,nslices_P,mpi_double_precision, !
     1                        0,mpi_comm_world,ier)                    !
               call mpi_bcast(currents,nslices_P,mpi_double_precision, !
     1                        0,mpi_comm_world,ier)                    !
               call mpi_bcast(charges,nslices_P,mpi_double_precision,0,!
     1                        mpi_comm_world,ier)                      !
               call mpi_bcast(x_maxs,nslices_P,mpi_double_precision,0, !
     1                        mpi_comm_world,ier)                      !
               call mpi_bcast(y_maxs,nslices_P,mpi_double_precision,0, !
     1                        mpi_comm_world,ier)                      !
               call mpi_bcast(eps_xs,nslices_P,mpi_double_precision,0, !
     1                        mpi_comm_world,ier)                      !
               call mpi_bcast(eps_ys,nslices_P,mpi_double_precision,0, !
     1                        mpi_comm_world,ier)                      !-- Comment this
               call mpi_bcast(dgams,nslices_P,mpi_double_precision,0,  !-- out in the
     1                        mpi_comm_world,ier)                      !-- single-CPU
               call mpi_bcast(Twiss_alph_x,nslices_P,                  !-- version
     1                       mpi_double_precision,0,mpi_comm_world,ier)!
               call mpi_bcast(Twiss_alph_y,nslices_P,                  !
     1                       mpi_double_precision,0,mpi_comm_world,ier)!
               call mpi_bcast(sigma_pxs,nslices_P,mpi_double_precision,!
     1                        0,mpi_comm_world,ier)                    !
               call mpi_bcast(sigma_pys,nslices_P,mpi_double_precision,!
     1                        0,mpi_comm_world,ier)                    !
               call mpi_bcast(x_avgs,nslices_P,mpi_double_precision,   !
     1                        0,mpi_comm_world,ier)                    !
               call mpi_bcast(px_avgs,nslices_P,mpi_double_precision,  !
     1                        0,mpi_comm_world,ier)                    !
               call mpi_bcast(y_avgs,nslices_P,mpi_double_precision,   !
     1                        0,mpi_comm_world,ier)                    !
               call mpi_bcast(py_avgs,nslices_P,mpi_double_precision,  !
     1                        0,mpi_comm_world,ier)                    !
            endif !----------------------------------------------------!
         endif   ! tprofile = 'BEAMCODE'
c
         if(tprofile.eq.'DIRECT') then  !------------------------------!
            if(myid.eq.0) then                                         !
               Shot_Noise='off'                                        !
               open(unit=28,file='MINERVA_particles.in',               !
     1              form='unformatted',status='old')                   !
               read(28) tpulse,tslices,rnfrst,rnslices,rnslices_d,     !
     1                  Qtot,rNtot                                     !                    
               read(28) q_macro,q_scale,temp1,temp2,temp3,temp4,temp5  !
               nfrst=int(rnfrst)                                       !
               nslices=int(rnslices)                                   !
               nslices_d=int(rnslices_d)                               !
               Ntot=int(rNtot)                                         !
               iparts=0                                                !
               do ns=1,nslices                                         !
                  read(28)ripartns,sumwns,currentns,E_avg,dgam,epslonx,!
     1                    epslony                                      !--Input for
                  iparts=max(iparts,int(ripartns))                     !--Direct
               end do                                                  !--One-to-One
               rewind(28)                                              !--Particle Input
c              write(6,*) ' '
c              write(6,*) 'nslices=',nslices,' nslices_d=',nslices_d
c              write(6,*) 'nfrst=',nfrst
c              write(6,*) 'Ntot=',Ntot,' iparts=',iparts
c              write(6,*) ' '
            endif                                                      !
            if(numprocs.gt.1) then                                     !
              call mpi_bcast(iparts,1,mpi_integer,0,mpi_comm_world,ier)!
              call mpi_bcast(Shot_Noise,8,mpi_character,0,             !
     1                                              mpi_comm_world,ier)!
              call mpi_bcast(nfrst,1,mpi_integer,0,mpi_comm_world,ier) !
              call mpi_bcast(nslices_d,1,mpi_integer,0,mpi_comm_world, !
     1                                                             ier)!
              call mpi_bcast(Ntot,1,mpi_integer,0,mpi_comm_world,ier)  !
              call mpi_bcast(Qtot,1,mpi_double_precision,0,            !
     1                                              mpi_comm_world,ier)!                                         !
              call mpi_bcast(q_macro,1,mpi_double_precision,0,         !
     1                    mpi_comm_world,ier)                          !
              call mpi_bcast(q_scale,1,mpi_double_precision,0,         !
     1                    mpi_comm_world,ier)                          !
              call mpi_bcast(tpulse,1,mpi_double_precision,0,          !
     1                    mpi_comm_world,ier)                          !
              call mpi_bcast(tslices,1,mpi_double_precision,0,         !
     1                    mpi_comm_world,ier)                          !              
            endif                                                      !
         endif  !------------------------------------------------------!
c
         if(tprofile.eq.'DISTFILE') then  !----------------------------!
            if(Shot_Noise.eq.'on') then                                !
               Shot_Noise='off'                                        !
               write(6,*) ' '                                          !
               write(6,*) '***************************************'    !
               write(6,*) '* Shot Noise not needed for DISTFILE  *'    !
               write(6,*) '*        Turn Shot_Noise off          *'    !
               write(6,*) '***************************************'    !
               write(6,*) ' '                                          !
            endif                                                      !
            if(myid.eq.0) then                                         !
               open(unit=28,file='current.dat',form='formatted',       !
     1              status='old')                                      !
               eof='no'                                                !
               nslices_g=0                                             !
               do while (eof.eq.'no')                                  !
                  read(28,*,END=1001) s,current_i                      !
                  nslices_g=nslices_g+1                                !
                  go to 1002                                           !
 1001             eof='yes'                                            !
 1002             continue                                             !
               end do                                                  !
            endif                                                      !
            if(numprocs.gt.1) call mpi_bcast(nslices_g,1,mpi_integer,  !
     1                                            0,mpi_comm_world,ier)!
            allocate(current_g(2,nslices_g),                           !
     1               twissbetax_g(nslices_g),twissbetay_g(nslices_g))  !
            if(myid.eq.0) then                                         !--Input Current
               rewind(28)                                              !--Profile for 
               eof='no'                                                !--DISTFILE Input
               i=0                                                     !
               do while (eof.eq.'no')                                  !
                  read(28,*,END=1003) s,current_i                      !
                  i=i+1                                                !
                  current_g(1,i)=1.d-4*s/cvac                          !
                  current_g(2,i)=current_i                             !
                  go to 1004                                           !
 1003             eof='yes'                                            !
 1004             continue                                             !
               end do                                                  !
               close(unit=28)                                          !
               do nsg=1,nslices_g                                      !
                  if(current_g(2,nsg).lt.half) current_g(2,nsg)=zero   !
               end do                                                  !
            endif                                                      !
            if(numprocs.gt.1) then                                     !
               call mpi_bcast(nparts_g,1,mpi_integer,0,mpi_comm_world, !
     1                                                             ier)!
               call mpi_bcast(current_g,2*nslices_g,                   !
     1                       mpi_double_precision,0,mpi_comm_world,ier)!
               call mpi_bcast(dinput_file,17,mpi_character,0,          !
     1                       mpi_comm_world,ier)                       !
            endif                                                      !
            dtslice_g=current_g(1,2)-current_g(1,1)                    !
            dzslice_g=hundredth*dtslice_g*cvac                         !
            tpulse=current_g(1,nslices_g)-current_g(1,1)               !
            iparts=nparts_g                                            !
            if(tpulse.ge.tslices) then                                 !
               write(6,*) ' '                                          !
               write(6,*) '***************************************'    !
               write(6,*) '*        tpulse >/= tslices           *'    !
               write(6,*) '*        TERMINATE  MINERVA           *'    !
               write(6,*) '***************************************'    !
               write(6,*) ' '                                          !
               call mpi_finalize(ierr)                                 !
               stop                                                    !
            endif                                                      !
            nslices=1+int(half+tslices/dtslice_g)                      !
            tslices=dtslice_g*dble(nslices-1)                          !
            allocate(energies(nslices_g),currents(nslices_g),          !
     1            dgams(nslices_g),eps_xs(nslices_g),eps_ys(nslices_g),!
     2            x_maxs(nslices_g),y_maxs(nslices_g),                 !
     3            Twiss_alph_x(nslices_g),Twiss_alph_y(nslices_g),     !
     4            charges(nslices_g),sigma_pxs(nslices_g),             !
     5            sigma_pys(nslices_g),x_avgs(nslices_g),              !
     6            y_avgs(nslices_g),px_avgs(nslices_g),                !
     7            py_avgs(nslices_g))                                  !
            energies=zero                                              !
            currents=zero                                              !
            dgams=zero                                                 !
            eps_xs=zero                                                !
            eps_ys=zero                                                !
            x_maxs=zero                                                !
            y_maxs=zero                                                !
            Twiss_alph_x=zero                                          !
            Twiss_alph_y=zero                                          !
            charges=zero                                               !
            sigma_pxs=zero                                             !
            sigma_pys=zero                                             !
            x_avgs=zero                                                !
            y_avgs=zero                                                !
            px_avgs=zero                                               !
            py_avgs=zero                                               !
            Q_bunch=zero                                               !
            sumI_g=zero                                                !
            Ntotg=0                                                    !
            current_max=zero                                           !
            do ns=1,nslices_g                                          !
               if(current_g(2,ns).gt.zero) Ntotg=Ntotg+1               !
               sumI_g=sumI_g+current_g(2,ns)                           !
               charges(ns)=dtslice_g*current_g(2,ns)                   !
               currents(ns)=current_g(2,ns)                            !
               current_max=max(current_max,current_g(2,ns))            !
               Q_bunch=Q_bunch+charges(ns)                             !
            end do                                                     !
            current=zero !-----------Obtain rms Current----------------!
            do ns=1,nslices_g                                          !
               current=current+(current_g(2,ns)-current_max)**2        !
            end do                                                     !
            current=sqrt(current/dble(Ntotg))                          !
         endif  !------------------------------------------------------!
c
         ns_frst=nslices
         ns_last=1
         dtpulse=tslices/dble(nslices-1)
         t_min=half*(tslices-tpulse)
         t_max=tpulse+t_min
         if(tprofile.ne.'DISTFILE') then
            do ns=1,nslices
                t_slice=dble(ns-1)*dtpulse
                if(t_slice.ge.t_min) ns_frst=min(ns_frst,ns)
                if(t_slice.le.t_max) ns_last=max(ns_last,ns)
            end do
         endif
         if(tprofile.eq.'DISTFILE') then
            ns_frst=1+(nslices-nslices_g)/2
            ns_last=ns_frst+nslices_g-1
         endif
         if(tprofile.eq.'gaussian') then
            ns_frst=1
            ns_last=nslices
         endif
c     write(6,*) 'tpulse=',tpulse
c     write(6,*) 'tslices=',tslices
c     write(6,*) 'nslices_g=',nslices_g
c     write(6,*) 'nslices=',nslices
c     write(6,*) 'ns_frst=',ns_frst
c     write(6,*) 'ns_last=',ns_last
c
c------- Load Balanced
c
         if(numprocs.eq.1) then
            ns_begin=1
            ns_end=nslices
         endif
         if(numprocs.gt.1) then
            if(numprocs.le.ns_last-ns_frst+1) then
               ns_begin=ns_frst+nint(dble(myid*(ns_last-ns_frst+1))
     1                                                 /dble(numprocs))
               ns_end=ns_frst-1+nint(dble((myid+1)*(ns_last-ns_frst+1))
     1                                                 /dble(numprocs))
               if(myid.eq.0) ns_begin=1
               if(myid.eq.numprocs-1) ns_end=nslices
              else    ! No Load Balancing
               ns_begin=1+(nslices*myid)/numprocs
               ns_end=min(nslices*(myid+1)/numprocs,nslices)
            endif
         endif
         nrmdim=ns_end-ns_begin+1 ! Reduced memory dimension
         if(FFTchk.eq.'on') then
             m=0
    1        m=m+1
             if(nslices.gt.2**m) go to 1
             nFFT=2**m
         endif
      endif    ! if slippage = 'on'
      if(numprocs.gt.nslices) then
            write(6,*) ' '
            write(6,*) '**********************************'
            write(6,*) '*       numprocs > nslices       *'
            write(6,*) '*        TERMINATE MINERVA       *'
            write(6,*) '**********************************'
            write(6,*) ' '
            call mpi_finalize(ierr)
            stop            
      endif
c
      if((myid.eq.0).and.(OPC.eq.'on').and.(slippage.eq.'on')) then  !-!
         write(IOPC,*) ' &slip'                                        !
         write(IOPC,8001) trim(tprofile),trim(teprofile),              !
     1                    trim(interpolation)                          !
 8001    format("   tprofile='",a,"'",/,"   teprofile='",a,"'",/,      !
     1          "   interpolation='",a,"'")                            !
         write(IOPC,1112) nslices,nslip                                !
 1112    format('   nslices=',i0,/,'   nslip=',i0)                     !--Input INP SLIP
         write(IOPC,1132) tpulse,tlight,tslices,tshift                 !
 1132    format(SP,'   tpulse=',1pe17.10,/,'   tlight=',e17.10,/,      !
     1             '   tslices=',e17.10,/,'   tshift=',e17.10)         !
         write(IOPC,*) ' /'                                            !
      endif  !---------------------------------------------------------!
c
      if(Power_vs_z.eq.'on') then
         do ns=ns_begin,ns_end
            open(unit=30+ns,form='unformatted',status='scratch')
         end do
      endif
      allocate(betaw(nsegbw),bwi(nsegbw),zwi(nsegbw),zstart(nsegbw),
     1         zstop(nsegbw),akw(nsegbw),rkw(nsegbw),axkw(nsegbw),
     2         wfac1(nsegbw),wfac2(nsegbw),wfac3(nsegbw),wfac4(nsegbw),
     3         wigangle(nsegbw),zsatseg(nsegbw),dbwseg(nsegbw),
     4         dzwseg(nsegbw),ntype(nsegbw),m1(nsegbw),m2(nsegbw),
     5         m3(nsegbw),nwigup(nsegbw),nwigdn(nsegbw),nwigh(nsegbw))
      if(nsegfoc.gt.0) allocate(bfocus(nsegfoc),zstt(nsegfoc),
     1                 zstp(nsegfoc),snangle(nsegfoc),csangle(nsegfoc),
     2                 zupdown(nsegfoc),bqfocus(nsegfoc),
     3                 Qks(nsegfoc),Qx0(nsegfoc),Qy0(nsegfoc),
     4                 ndfoc(nsegfoc),nfoc(nsegfoc))
      if(nsegfoc.eq.0) allocate(zstt(1))
      allocate(x0(nbeams,nslices),y0(nbeams,nslices),dE(nbeams,nslices),
     1         gammab(nbeams,nslices),sigmax(nbeams,nslices),
     2         sigmay(nbeams,nslices),sigmapx(nbeams,nslices),
     3         sigmapy(nbeams,nslices),dI(nbeams,nslices),
     4         xi(nbeams,nslices),beam_i(nbeams,nslices),
     5         Bwork(nbeams,nslices),costhx(nbeams,nslices),
     6         costhy(nbeams,nslices),sinthx(nbeams,nslices),
     7         sinthy(nbeams,nslices),dgamG(nbeams,nslices),
     8         px0(nbeams,nslices),py0(nbeams,nslices),
     9         epsxi(nbeams,nslices),epsyi(nbeams,nslices))
      allocate(twissalphxi(nbeams,nslices),twissalphyi(nbeams,nslices))
      allocate(twissgamxi(nbeams,nslices),twissgamyi(nbeams,nslices))
      allocate(betatxi(nbeams,nslices),betatyi(nbeams,nslices))
      t=thou*energy
      beami=current
      px0=zero
      py0=zero
      if(nbeams.eq.1) then
             x0(1,1)=x_center
             y0(1,1)=y_center
             px0(1,1)=px_in
             py0(1,1)=py_in
             do ns=2,nslices
                x0(1,ns)=x_center
                y0(1,ns)=y_center
                px0(1,ns)=px_in
                py0(1,ns)=py_in
             end do
             dE(1,1)=zero
             dI(1,1)=zero
             gammab(1,1)=one+thou*energy/emass
             gammaavg=gammab(1,1)
             pbeam=thou*t*beami
         else
            gammaavg=zero
            pbeam=zero
            do i=1,nbeams
               dIi=zero
               if(myid.eq.0) write(6,*) 
     1                   ' settings for multi beam configufation...'
               if(myid.eq.0) read(5,beams)
               if(numprocs.gt.1) call beams_bcast
               if(OPC.eq.'on') then
                  write(IOPC,*) '  &beams'
                  write(IOPC,1133) x0i,y0i,dEi,dIi
 1133             format(SP,'   x0i=',1pe17.10,/,'   y0i=',e17.10,/,
     1                   '   dEi=',e17.10,/,'   dIi=',e17.10)
                  write(IOPC,*) '  /'
               endif
               x0(i,1)=x0i
               y0(i,1)=y0i
               dE(i,1)=dEi
               dI(i,1)=dIi
               gammab(i,1)=one+thou*(energy+dEi)/emass
               gammaavg=gammaavg+gammab(i,1)
               pbeam=pbeam+thou*(t+dEi)*(beami+dIi)
            end do
            gammaavg=gammaavg/dble(nbeams)
      endif
      if(beamtype.eq.'circular') iprofile=0
      if(beamtype.eq.'parabolc') iprofile=1
      if(beamtype.eq.'sheet')    iprofile=2
      if(beamtype.eq.'elliptic') iprofile=3
      if(beamtype.eq.'gaussian') iprofile=4
      if(beamtype.eq.'waterbag') iprofile=5
      if(tprofile.eq.'BEAMCODE') iprofile=4
      if(tprofile.eq.'DIRECT')   iprofile=7
      if(tprofile.eq.'DISTFILE') iprofile=8
      if(emmodes.eq.'hermite') mtype=1
      if(emmodes.eq.'laguerre') mtype=2
      if(sde.eq.'on') killsde=1
      if((sde.eq.'off').or.(sde.eq.'CWA')) killsde=0
      nbx_max=0
      nby_max=0
      nbz_max=0
      allocate(a_cant(nsegbw))
      do i=1,nsegbw
        nwh=3
        wigplane='x'
        Nw=-1
        n1stpass=1
        ztapersegi=200.d0
        dbwsegi=zero
        dzwsegi=zero
c       if(myid.eq.0) write(6,*) ' input inp2 for wiggler segment=',i
        if(myid.eq.0) write(6,'(A,i4.0)') 
     1                    '  settings for wiggler segment ',i
        if(myid.eq.0) read(5,inp2)
        if(numprocs.gt.1) call input2_bcast
        if(myid.eq.0.and.OPC.eq.'on') then
           write(IOPC,*) ' &inp2'
           write(IOPC,1113) Nw,nup,ndown,m,nwh,lrf,nrf,n1stpass,
     1                      nbxi,nbyi,nbzi
 1113      format('   Nw=',i0,/,'   nup=',i0,/,'   ndown=',i0,/,
     1            '   m=',i0,/,'   nwh=',i0,/,'   lrf=',i0,/,
     2            '   nrf=',i0,/,'   n1stpass=',i0,/,'   nbxi=',i0,/,
     3            '   nbyi=',i0,/,'   nbzi=',i0)
           write(IOPC,1134) bw,zw,zfirst,zlast,ax,Edc,z1,z2,Prfin,
     1                    freqrf,xmaxi,ymaxi,ztapersegi,dbwsegi,dzwsegi
 1134      format(SP,'   bw=',1pe17.10,/,'   zw=',e17.10,/,
     1            '   zfirst=',e17.10,/,'   zlast=',e17.10,/,
     2            '   ax=',e17.10,/,'   Edc=',e17.10,/,
     3            '   z1=',e17.10,/,'   z2=',e17.10,/,
     4            '   Prfin=',e17.10,/,'   freqrf=',e17.10,/,
     5            '   xmaxi=',e17.10,/,'   ymaxi=',e17.10,/,
     6            '   ztapersegi=',e17.10,/,'   dbwsegi=',e17.10,/,
     7            '   dzwsegi=',e17.10)
           write(IOPC,8002) trim(wigtype),trim(wigplane),trim(RFfield),
     1                      trim(RFmode),trim(Bmapfilei)
 8002      format("   wigtype='",a,"'",/,"   wigplane='",a,"'",/,
     1            "   RFfield='",a,"'",/,"   RFmode='",a,"'",/,
     2            "   Bmapfilei='",a,"'")
           write(IOPC,*) ' /'
        endif
        if(myid.eq.0.and.OPC.eq.'on') nslcs=nslices ! OPC
        if(RFfield.eq.'rf') then
         allocate(Eacc(nsegbw,nslices),z1acc(nsegbw),z2acc(nsegbw),
     1            Pfacrf(nsegbw),omegarf(nsegbw),aklnrf(nsegbw),
     2            akzrf(nsegbw),xlnrf(nsegbw),Pinrf(nsegbw,nslices),
     3            rgkwrf(nsegbw),Prf(nsegbw,nslices),
     4            dgamdt(nsegbw,nslices),Gloss(nsegbw,nslices),
     5            lnumrf(nsegbw),nnumrf(nsegbw),moderf(nsegbw))
        endif
        if(wigtype.eq.'ppf') ntype(i)=1
        if(wigtype.eq.'cpf') ntype(i)=2
        if(wigtype.eq.'fpf') ntype(i)=3
        if(wigtype.eq.'hel') ntype(i)=4
        if(wigtype.eq.'hppf') ntype(i)=5
        if(wigtype.eq.'hfpf') ntype(i)=6
        if(wigtype.eq.'map') ntype(i)=7
        if(wigtype.eq.'cap') ntype(i)=8
        if(wigtype.eq.'aple') ntype(i)=9
        if((wigtype.eq.'hppf').or.(wigtype.eq.'hfpf')) nwigh(i)=nwh
        if((wigtype.eq.'cap').and.(matchbeam.ne.'off')) then
           write(6,*) 'The Canted-Pole_Face Wiggler requires that:'
           write(6,*) ' '
           write(6,*) "        matchbeam='off'"
           write(6,*) '       TERMINATE MINERVA'
           call mpi_finalize(ierr)
           stop
        endif
        bwi(i)=bw
        zwi(i)=zw
        betaw(i)=awfac*bw*zw
        akw(i)=twopi/zw
        rkw(i)=akw(i)/akw(1)
        a_cant(i)=betaw(i)/(two*sqrt(two)*gammab(1,1))
        zstart(i)=hundred*akw(1)*zfirst
        zsatseg(i)=hundred*akw(1)*ztapersegi
        dbwseg(i)=dbwsegi
        dzwseg(i)=dzwsegi
        if(Nw.gt.0) zlast=zfirst+dble(Nw)*zw/hundred
        zstop(i)=hundred*akw(1)*zlast
        if(wigtype.eq.'map') then
           allocate(delz(nsegbw),delx(nsegbw),dely(nsegbw),
     1            Bmapfile(nsegbw),nbx(nsegbw),nby(nsegbw),nbz(nsegbw))
           xfmax(i)=hundred*akw(1)*xmaxi
           yfmax(i)=hundred*akw(1)*ymaxi
           delz(i)=(zstop(i)-zstart(i))/dble(nbzi-1)
           delx(i)=xfmax(i)/dble(nbxi-1)
           dely(i)=yfmax(i)/dble(nbyi-1)
           nbx(i)=nbxi
           nby(i)=nbyi
           nbz(i)=nbzi
           nbx_max=max(nbx_max,nbxi)
           nby_max=max(nby_max,nbyi)
           nbz_max=max(nbz_max,nbzi)
           Bmapfile(i)=Bmapfilei
        endif
        nwigup(i)=nup
        nwigdn(i)=ndown
        if(wigplane.eq.'x') wigangle(i)=zero
        if(wigplane.eq.'y') wigangle(i)=halfpi
c
        if((RFfield.eq.'dc').or.(RFfield.eq.'on')) then
           do ns=ns_begin,ns_end
              Eacc(i,ns)=Edc*zw/(twopi*tenth*emass)
           end do
           escale=twopi*tenth*emass/zw
           z1acc(i)=twopi*hundred*z1/zw
           z2acc(i)=twopi*hundred*z2/zw
        endif
        if(RFfield.eq.'rf') then
           do ns=ns_begin,ns_end
              nRF1=6000+ns
              nRF2=5001+nslices+ns
              if(nslices.eq.1) then
                 open(unit=nRF1,file='RF_Phase_Diagnostics.txt',
     1                               form='formatted',status='replace')
                 open(unit=nRF2,file='RF_Drive_Power_vs_z.txt',
     1                               form='formatted',status='replace')
                 write(nRF1,999)
  999            format(10x,'RF PHASE DIAGNOSTICS',//,4x,
     1                  'z(m)',7x,'psirf',2x,'vx*cos(kz-wt)')
                 write(nRF2,998)
  998            format(10x,'RF DRIVE POWER DIAGNOSTIC',//,4x,'z(m)',
     1                  6x,'Power(W)',2x,'dPower(W)',3x,'dgam/dt')
              endif
              if(nslices.gt.1) then       
                 write(nameRF1,800) ns
  800            format('RF_Phase_Diagnostics_slice=',i5.5,'.txt')
                 open(unit=nRF1,file=nameRF1,form='formatted',
     1                status='replace')
                 write(nameRF2,801) ns
  801            format('RF_Drive_Power_vs_z_slice=',i5.5,'.txt')
                 open(unit=nRF2,file=nameRF2,form='formatted',
     1                status='replace')
                 write(nRF1,802) ns
  802            format(10x,'RF PHASE DIAGNOSTICS',//,
     1                  20x,'Slice No.:',i5,//,
     2                  4x,'z(m)',7x,'psirf',2x,'vx*cos(kz-wt)')
                 write(nRF2,803) ns
  803            format(10x,'RF DRIVE POWER DIAGNOSTIC',//,
     1                  20x,'Slice No.:',i5,//,4x,'z(m)',6x,
     2                  'Power(W)',2x,'dPower(W)',3x,'dgam/dt')
              endif
           end do
           do ns=ns_begin,ns_end
              Pinrf(i,ns)=Prfin
           end do
           omegarf(i)=giga*zwi(1)*freqrf/cvac
           fscale=cvac/(giga*zwi(1))
           z1acc(i)=akw(1)*hundred*z1
           z2acc(i)=akw(1)*hundred*z2
           lnumrf(i)=lrf
           nnumrf(i)=nrf
           if(RFmode.eq.'TE') moderf(i)=0
           if(RFmode.eq.'TM') moderf(i)=1
           if((RFmode.ne.'TE').and.(RFmode.ne.'TM')) moderf(i)=-1
           if(drifttube.eq.'cylindrical') then
              if(RFmode.eq.'TE') xlnrf(i)=xln(1,lrf+1,nrf)
              if(RFmode.eq.'TM') xlnrf(i)=xln(2,lrf+1,nrf)
           endif
        endif
        if(ntype(i).eq.2) then
           axkw(i)=akw(1)*ax
c          p=dble(m)
           m1(i)=2*m
           m2(i)=2*m-1
           m3(i)=2*m-2
           wfac1(i)=dble(m)/axkw(1)
           wfac2(i)=dble(m2(i)*(m-1))/axkw(1)**2
           wfac3(i)=two*dble(m-1)*dble(2*m-3)/axkw(1)**2
           wfac4(i)=half*dble(m*m2(i))/axkw(1)**2
        endif
      end do  ! end loop over wiggler segments
      if(nwig_harms.ge.1) then  !--------------------------------------!
         allocate(wigharms(nwig_harms,2))                              !
         do nhw=1,nwig_harms                                           !
c             if(myid.eq.0) write(6,*) 'input wigh:bratio,nwh'         !
              if(myid.eq.0) write(6,*)                                 !
     1                       '  -- including wiggler harmonics ...'    !
              if(myid.eq.0) read(5,wigh)                               !--Input Wiggler
              if(numprocs.gt.1) call wigh_bcast                        !--Harmonic Info
              wigharms(nhw,1)=bratio                                   !--For FPF Wiggler     
              wigharms(nhw,2)=dble(nwh)                                !
         end do                                                        !
      endif  !---------------------------------------------------------!
      if(nbx_max.gt.0) then
         allocate(bxarr(nbx_max+1,nby_max+1,nbz_max+1),
     1            byarr(nbx_max+1,nby_max+1,nbz_max+1),
     2            bzarr(nbx_max+1,nby_max+1,nbz_max+1))
      endif
c     if((RFfield.ne.'off').and.(slippage.ne.'off')) then
c        write(6,9998)
c9998    format(/,' SLIPPAGE NOT AVAILABLE FOR RF ACCELERATION',
c    1          /,'******** TERMINATE MINERVA ********')
c        call mpi_finalize(ierr)
c        stop
c     endif
      if(nsegfoc.eq.0) zstt(1)=zstart(1)
      if(nsegfoc.gt.0) then
         Qx0=zero
         Qy0=zero
      endif
      do i=1,nsegfoc  !------------------------------------------------!
        Qk=zero                                                        !
        Qx0i=zero                                                      !
        Qy0i=zero                                                      !
        nfocus=1                                                       !
        angle=ninety                                                   !
        zentry=zero                                                    !
        zLen=-one                                                      !
c       if(myid.eq.0) write(6,*) ' input inp3 for foc element=',i      !
        if(myid.eq.0) write(6,*) ' settings for focussing element ',i  !
        if(myid.eq.0) read(5,inp3)                                     !
        if(numprocs.gt.1) call input3_bcast                            !
        if(myid.eq.0.and.OPC.eq.'on') then                             !
           write(IOPC,*) ' &inp3'                                      !
           write(IOPC,8003) trim(foctype)                              !
 8003      format("   foctype='",a,"'")                                !
           write(IOPC,1135) Qk,bquad,bdip,zLen,angle,z1st,z2nd,zentry, !
     1                      Qx0i,Qy0i                                  !
 1135      format(SP,'   Qk=',1pe17.10,/,'   bquad=',e17.10,/,         !
     1            '   bdip=',e17.10,/,'   zLen=',e17.10,/,             !
     2            '   angle=',e17.10,/,'  z1st=',e17.10,/,             !
     3            '   z2nd=',e17.10,/,'   zentry=',e17.10,/,           !
     4            '   Qx0i=',e17.10,/,'    Qy0i=',e17.10)              !
           write(IOPC,1114) ndfocus                                    !
 1114      format('   ndfocus=',i0)                                    !--Input Focusing
           write(IOPC,*) ' /'                                          !--Magnetic Fields
        endif                                                          !
        if(foctype.eq.'quad')   nfoc(i)=1                              !
        if(foctype.eq.'dipole') nfoc(i)=2                              !
        ndfoc(i)=ndfocus                                               !
        zstt(i)=hundred*akw(1)*z1st                                    !
        if(zLen.gt.zero) z2nd=z1st+zLen                                !
        zstp(i)=hundred*akw(1)*z2nd                                    !
        zupdown(i)=hundred*akw(1)*zentry                               !
        Qx0(i)=hundred*akw(1)*Qx0i                                     !--Qx0i in meters
        Qy0(i)=hundred*akw(1)*Qy0i                                     !--Qy0i in meters
        if(nfoc(i).eq.1) then                                          !
           if(Qk.ne.zero) then                                         !
                 Qks(i)=Qk                                             !
                 bqfocus(i)=-amicro*t*Qk/three                         !
              else                                                     !
                 bqfocus(i)=-bquad                                     !
                 Qks(i)=-three*bqfocus(i)/(amicro*t)                   !
           endif                                                       !
           bfocus(i)=quadfac*bqfocus(i)*zwi(1)**2                      !
        endif                                                          !
        if(nfoc(i).eq.2) then                                          !
           bqfocus(i)=bdip                                             !
           bfocus(i)=awfac*bdip*zwi(1)                                 !
           snangle(i)=sin(degtorad*angle)                              !
           csangle(i)=cos(degtorad*angle)                              !
        endif                                                          !
      end do  !--------------------------------------------------------!
c
c     if(myid.eq.0) write(6,*) ' input inp4'  !------------------------!
      if(myid.eq.0) write(6,*) 
     1             ' settings for optical configuration ...' !---------!
      if(myid.eq.0) then                                               !
         read(5,inp4)                                                  !
         if((wigtype.eq.'aple').and.(emmodes.ne.'laguerre')) then      !
            write(6,*) '******************************************'    !
            write(6,*) '* Use of the aple wiggler model requires *'    !
            write(6,*) '*       using Gauss-Laguerre modes       *'    !
            write(6,*) '*           Terminate MINERVA            *'    !
            write(6,*) '******************************************'    !
            call mpi_finalize(ierr)                                    !
            stop                                                       !
         endif                                                         !
      endif                                                            !
      if(numprocs.gt.1) call input4_bcast                              !
      if(wigtype.eq.'aple') then                                       !
         if(polar.ge.0) then                                           !
            cosinephi=(one-polar)/(one+polar)                          !
            sinephi=two*sqrt(polar)/(one+polar)                        !
         endif                                                         !
         if(polar.lt.zero) then                                        !
            cosinephi=-(one-abs(polar))/(one+abs(polar))               !
            sinephi=-two*sqrt(abs(polar))/(one+abs(polar))             !
         endif                                                         !
      endif                                                            !
      if(myid.eq.0.and.OPC.eq.'on') then                               !
         write(IOPC,*) ' &inp4'                                        !
         write(IOPC,1136) rg,rg_x,rg_y,wavelnth,wx,wy,power,cutoff,    !--Input INP4
     1                    polar,freq_wg                                !
 1136    format(SP,'   rg=',1pe17.10,/,'   rg_x=',e17.10,/,            !
     1          '   rg_y=',e17.10,/,'   wavelnth=',e17.10,/,           !
     2          '   wx=',e17.10,/,'   wy=',e17.10,/,                   !
     3          '   power=',e17.10,/,'   cutoff=',e17.10,/,            !
     4          '   polar=',e17.10,/,'   freq_wg=',e17.10)             !
         write(IOPC,1115) nmodes,mselect                               !
 1115    format('   nmodes=',i0,/,'   mselect=',i0)                    !
         write(IOPC,*) ' /'                                            !
      endif  !---------------------------------------------------------!
      frequency=freq_wg
c
      allocate(P_in(nslices),P_beam(nslices))
      nsize=nmodes 
      allocate(a1(nmodes+1,nslices),a2(nmodes,nslices),xdir(nmodes),
     1        asde1(nmodes,nslices),asde2(nmodes,nslices),ydir(nmodes),
     2        fa1(nmodes+1,nslices),fa2(nmodes,nslices),omega(nmodes),
     3        fsde1(nmodes,nslices),fsde2(nmodes,nslices),
     4        aki1(nmodes,nslices),aki2(nmodes,nslices),
     5        w0(nmodes,nslices),w02(nmodes,nslices),
     6        factor(nmodes,nslices),z0(nmodes,nslices),
     7        zwaste(nmodes,nslices),pfac(nmodes,nslices),
     8        phase_a(nmodes,nslices),lnum(nmodes,nslices),
     9        nnum(nmodes,nslices))
      allocate(Phead(nmodes,nslices),lhead(nmodes,nslices),
     1            nhead(nmodes,nslices))
      allocate(plnmax(nslices))
      allocate(da(nmodes,nslices),pln(nmodes,nslices),
     1            aki(nmodes,nslices),akz(nmodes,nslices))
c
c     if(myid.eq.0) write(6,*) ' input inp5'  !------------------------!
      if(myid.eq.0) write(6,*) ' settings for particle loading ...'    !
      if(myid.eq.0) read(5,inp5)                                       !
      if(numprocs.gt.1) call input5_bcast                              !
      if(myid.eq.0.and.OPC.eq.'on') then                               !
         write(IOPC,*) ' &inp5'                                        !--Input INP5
         write(IOPC,1116) nr,ntheta,nx,ny,npsi,nphi,ndv,ngam           !
 1116    format('   nr=',i0,/,'   ntheta=',i0,/,'   nx=',i0,/,         !
     1          '   ny=',i0,/,'   npsi=',i0,/,'   nphi=',i0,/,         !
     2          '   ndv=',i0,/,'   ngam=',i0)                          !
           write(IOPC,6999) trim(Gauss_Quad)                           !
 6999      format("   Gauss_Quad='",a,"'")                             !
         write(IOPC,*) ' /'                                            !
      endif  !---------------------------------------------------------!
c
c     if(myid.eq.0) write(6,*) ' input inp6'  !------------------------!
      if(myid.eq.0) write(6,*) ' settings for numeric integration ...' !
      if(myid.eq.0) read(5,inp6)                                       !
      if(numprocs.gt.1) call input6_bcast                              !
      if(ndiags.gt.0) allocate(zdiags(ndiags))                         !
      if(myid.eq.0.and.OPC.eq.'on') then                               !
         write(IOPC,*) ' &inp6'                                        !
         write(IOPC,1137) zmax,dz,ztaper,zsat,dbwdz,dbw,dbw_sqr,dzw    !--Input INP6
 1137    format(SP,'   zmax=',1pe17.10,/,'   dz=',e17.10,/,            !
     1          '   ztaper=',e17.10,/,'   zsat=',e17.10,/,             !
     2          '   dbwdz=',e17.10,/,'   dbw=',e17.10,/,               !
     3          '  dbw_sqr=',e17.10,/,'   dzw=',e17.10)                !
         write(IOPC,1117) ndz,nstep,ndiags                             !
 1117    format('   ndz=',i0,/,'   nstep=',i0,/,'   ndiags=',i0)       !
         write(IOPC,*) ' /'                                            !
      endif  !---------------------------------------------------------!
c
      if(ztaper.gt.zero) zsat=hundred*akw(1)*ztaper
      if(dbwdz.ne.zero) dbw=hundredth*dbwdz/(akw(1)*bwi(1))
      if(ndz.gt.0) dz=twopi/dble(ndz)
c
c     if(myid.eq.0) write(6,*) ' input inp7'  !------------------------!
      if(myid.eq.0) read(5,inp7)                                       !
      if(numprocs.gt.1) call input7_bcast                              !
      if(enrgycon.eq.'on')  iecon=1                                    !
      if(enrgycon.eq.'off') iecon=0                                    !
      if(dcfield.eq.'on')   iself=1                                    !--Input INP7
      if(dcfield.eq.'off')  iself=0                                    !
      if((myid.eq.0).and.(iecon.eq.1)) write(6,*)                      !
     1                ' -- energy conservation check turned on ...'    !
      if((myid.eq.0).and.(iecon.eq.0)) write(6,*)                      !
     1                ' -- energy conservation check turned off ...'   !
      if(myid.eq.0.and.OPC.eq.'on') then                               !
         write(IOPC,*) ' &inp7'                                        !
         write(IOPC,8007) trim(enrgycon),trim(dcfield)                 !
 8007    format("   enrgycon='",a,"'",/,"   dcfield='",a,"'")          !
         write(IOPC,*) ' /'                                            !
      endif  !---------------------------------------------------------!
c
      if((beamtype.ne.'circular').or.(beamtype.ne.'parabolic')) iself=0
      if ((myid.eq.0).and.(iself.eq.1)) 
     1                write(6,*) ' -- self fields turned on ...'
      if ((myid.eq.0).and.(iself.eq.0)) 
     1                write(6,*) ' -- self fields turned off ...'
c
c---------------------- handle wiggler errors -------------------------!
      if(myid.eq.0) read(5,inp8)                                       !
      if((myid.eq.0).and.(wigerrs.eq.'on')) write(6,*)                 !
     1 	                            ' settings for wiggler errors ...' !--Input INP8
      if((myid.eq.0).and.(wigerrs.ne.'on'))                            !
     1                   write(6,*) ' -- no wiggler errors applied ...'!
      if(numprocs.gt.1) call input8_bcast                              !
      if(myid.eq.0.and.OPC.eq.'on') then !-----------------------------!
         write(IOPC,*) ' &inp8'                                        !
         write(IOPC,8008) trim(wigerrs),trim(source)                   !
 8008    format("   wigerrs='",a,"'",/,"   source='",a,"'")            !
         write(IOPC,1118) nerr,iseed                                   !--Output for OPC
 1118    format('   nerr=',i0,/,'   iseed=',i0)                        !
         write(IOPC,1138) error                                        !
 1138    format(SP,'   error=',1pe17.10)                               !
         write(IOPC,*) ' /'                                            !
      endif  !---------------------------------------------------------!
      if((wigerrs.eq.'on').and.(source.eq.'random')) then  !-----------!
         maxnsteps=1                                                   !
         do ns=1,nsegbw                                                !
            nst=int(one                                                !
     1             +dble(nerr)*rkw(ns)*(zstop(ns)-zstart(ns))/twopi)   !
            maxnsteps=max(maxnsteps,nst)                               !
         end do                                                        !
         allocate (bwerr(nsegbw+10,maxnsteps+10),dzerr(nsegbw+10))     !
         first_call=ran3(iseed)                                        !
         do ns=1,nsegbw                                                !
              sumb=zero                                                !
              dzerr(ns)=twopi/(dble(nerr)*rkw(ns))                     !
              zlength=zstop(ns)-zstart(ns)                             !
              zup=zstart(ns)+twopi*dble(nwigup(ns))/rkw(ns)            !--Random Errors
              zdown=zstop(ns)-twopi*dble(nwigdn(ns))/rkw(ns)           !
              nsteps=int(one+zlength/dzerr(ns))                        !
              do i=1,nsteps                                            !
                 zerr=zstart(ns)+dble(i-1)*dzerr(ns)                   !
                 bwerr(ns,i)=zero                                      !
                 if((zerr.gt.zup).and.(zerr.lt.zdown)) then            !
                    bwerr(ns,i)=error*(-one+two*ran3(ns))              !
                    sumb=sumb+(bwerr(ns,i)*rkw(ns))**2                 !
                 endif                                                 !
              end do                                                   !
              points=(zdown-zup)/dzerr(ns)                             !
              dbrms=error*sqrt(points/sumb)                            !
              do i=1,nsteps                                            !
                 bwerr(ns,i)=betaw(ns)*rkw(ns)*dbrms*bwerr(ns,i)       !
              end do                                                   !
         end do                                                        !
      endif  !---------------------------------------------------------!
c     if((wigerrs.eq.'on').and.(source.eq.'fixed')) then  !------------!
c        maxnsteps=1                                                   !
c        do ns=1,nsegbw                                                !
c           maxnsteps=max(maxnsteps,ibdat(ns,2))                       !
c        end do                                                        !
c        allocate (bwerr(nsegbw+1,maxnsteps),dzerr(nsegbw+1))          !
c        do ns=1,nsegbw                                                !
c             nerr=ibdat(ns,1)                                         !--Import Errors
c             npts=ibdat(ns,2)                                         !
c             dzerr(ns)=twopi/(dble(nerr)*rkw(ns))                     !
c             do i=1,npts                                              !
c                 bwerr(ns,i)=bdat(ns,i)*rkw(ns)                       !
c             end do                                                   !
c        end do                                                        !
c     endif  !---------------------------------------------------------!
      if((wigerrs.eq.'on').and.(myid.eq.0)) then  !--------------------!
         open(unit=14,file='On-Axis_Wiggler_Field.txt',                !
     1        form='formatted',status='replace')                       !
         write(14,212)                                                 !
  212    format(3x,'ON-AXIS WIGGLER FIELD',//,4x,'z(m)',9x,'aw')       !
         zstep0=twopi/dble(ndz)                                        !
         z=-zstep0                                                     !
   52    z=z+zstep0                                                    !
         do i=1,nsegbw                                                 !
            if((z.ge.zstart(i)).and.(z.le.zstop(i))) then              !
                betawi=betaw(i)*rkw(i)                                 !
                bwz=betawi                                             !
                zup=zstart(i)+twopi*dble(nwigup(i))/rkw(i)             !
                zdown=zstop(i)-twopi*dble(nwigdn(i))/rkw(i)            !--Write Errors
                if((z.ge.zup).and.(z.le.zdown)) then                   !
                   is=int(one+(z-zstart(i))/dzerr(i))                  !
                   deltaz=z-zstart(i)-dble(is-1)*dzerr(i)              !
                   db=bwerr(i,is+1)-bwerr(i,is)                        !
                   dbwz=bwerr(i,is)+db*sin(halfpi*deltaz/dzerr(i))**2  !
                   bwz=bwz+dbwz                                        !
                   zstep=zstep0/(dble(nerr)*rkw(i))                    !
                   write(14,213) z/(hundred*akw(1)),bwz                !
  213              format(2(1x,1pe11.4))                               !
                endif                                                  !
            endif                                                      !
         end do                                                        !
         if(z.le.hundred*akw(1)*zmax) go to 52                         !
         close(unit=14)                                                !
      endif  !---------------------------------------------------------!
c
c-------- set beam,wiggler & wavemode quantities
c
      if((emmodes.eq.'hermite').or.(emmodes.eq.'laguerre')) then
         domega=hundredth*zwi(1)/wavelnth
      endif
      scale=one/(hundred*akw(1))
      ztotal=hundred*akw(1)*zmax
      gamma=one+t/emass
      vz0=sqrt(one-one/gamma**2)
      dp0=(one/(one-dgamz)**2-one)/(two*(gamma**2-one))
      dp=dp0*gamma*vz0
      if(iprofile.le.1) then  !----------------------------------------!
             rmin=akw(1)*rbmin                                         !
             rmax=akw(1)*rbmax                                         !
             rb2=rmax**2                                               !
             xi(1,1)=plasma1*sqrt(beami/(vz0*(rmax**2-rmin**2)))       !
             beam_i(1,1)=beami                                         !
             sfac=twopi*(rmax-rmin)                                    !
      endif                                                            !
      if(iprofile.eq.2) then                                           !
             xb=akw(1)*xbeam                                           !--Legacy
             yb=akw(1)*ybeam                                           !--Distributions
             xi(1,1)=plasma2*sqrt(beami/(vz0*xb*yb))                   !
             beam_i(1,1)=beami                                         !
             sfac=xb*yb                                                !
      endif                                                            !
      if(iprofile.eq.3) then                                           !
             xb=akw(1)*xbeam                                           !
             yb=akw(1)*ybeam                                           !
             xi(1,1)=plasma1*sqrt(beami/(fourth*vz0*xb*yb))            !
             beam_i(1,1)=beami                                         !
             sfac=xb*yb                                                !
      endif  !---------------------------------------------------------!
      if((iprofile.eq.4).and.(matchbeam.eq.'on')) then !---------------!
         sfac=half/twopi**2                                            !
         costhx(1,1)=one                                               !
         costhy(1,1)=one                                               !
         sinthx(1,1)=zero                                              !
         sinthy(1,1)=zero                                              !
         dgamG(1,1)=dgam                                               !
         betaavg=zero                                                  !
         do nb=1,nbeams                                                !
             if(ntype(1).eq.1) akbeta=two*gammab(nb,1)/betaw(1)        !
             if(ntype(1).eq.5) then                                    !
               akbeta=two*gammab(nb,1)                                 !
     1                        /(sqrt(one+dble(nwigh(1)**2))*betaw(1))  !
             endif                                                     !
             if((ntype(1).ne.1).and.(ntype(1).ne.5)) then              !
                   akbeta=sqrt(two)*gammab(nb,1)/betaw(1)              !
             endif                                                     !--Gaussian Beam
             betaavg=betaavg+akbeta                                    !--matchbeam='on'
             sigmax(nb,1)=hundredth                                    !
     1                           *sqrt(akw(1)*epsx*akbeta/gammab(nb,1))!
             sigmapx(nb,1)=hundredth                                   !
     1                           *sqrt(akw(1)*gammab(nb,1)*epsx/akbeta)!
             if(ntype(1).lt.6) then                                    !
                sigmay(nb,1)=hundredth                                 !
     1                           *sqrt(akw(1)*epsy*akbeta/gammab(nb,1))!
                sigmapy(nb,1)=hundredth                                !
     1                           *sqrt(akw(1)*gammab(nb,1)*epsy/akbeta)!
               else                                                    !
                bfunction=akbeta/dble(nwigh(1))                        !
                sigmay(nb,1)=hundredth                                 !
     1                        *sqrt(akw(1)*epsy*bfunction/gammab(nb,1))!
                sigmapy(nb,1)=hundredth                                !
     1                        *sqrt(akw(1)*gammab(nb,1)*epsy/bfunction)!
             endif                                                     !
         end do                                                        !
         betaavg=betaavg/dble(nbeams)                                  !
         area=zero                                                     !
         do nb=1,nbeams                                                !
            area=area+sigmax(nb,1)**2+sigmay(nb,1)**2                  !
         end do                                                        !
         do nb=1,nbeams                                                !
            xi(nb,1)=plasma1*sqrt((beami+dI(nb,1))/(vz0*area))         !
            beam_i(nb,1)=beami+dI(nb,1)                                !
         end do                                                        !
         iself=0                                                       !
      endif  !---------------------------------------------------------!
      if((iprofile.eq.4).and.(matchbeam.ne.'on').and.  !---------------!
     1                                   (tprofile.ne.'BEAMCODE')) then!
c                                                                      !
c      This procedure is valid for angles of rotation of less than     !
c      or equal to 90 degrees                                          !
c                                                                      !
            dgamG(1,1)=dgam                                            !
            xmax=akw(1)*xbeam                                          !
            ymax=akw(1)*ybeam                                          !
            betaxkw=tenthou*gammab(1,1)*xmax**2/(akw(1)*epsx)          !
            betaykw=tenthou*gammab(1,1)*ymax**2/(akw(1)*epsy)          !
            betax=hundredth*betaxkw/akw(1)                             !
            betay=hundredth*betaykw/akw(1)                             !
            twissgamx=(one+twissalphx**2)/betaxkw                      !
            twissgamy=(one+twissalphy**2)/betaykw                      !
            denomx=gammab(1,1)*twissgamx-betaxkw/gammab(1,1)           !
            denomy=gammab(1,1)*twissgamy-betaykw/gammab(1,1)           !
            cos2thx=one/sqrt(one+(two*twissalphx)**2/denomx**2)        !
            if(denomx.lt.zero) cos2thx=-cos2thx                        !
            cos2thy=one/sqrt(one+(two*twissalphy)**2/denomy**2)        !
            if(denomy.lt.zero) cos2thy=-cos2thy                        !--Gaussian Beam
            costhx(1,1)=sqrt((one+cos2thx)/two)                        !--matchbeam='off'
            costhy(1,1)=sqrt((one+cos2thy)/two)                        !
            sinthx(1,1)=sqrt((one-cos2thx)/two)                        !
            sinthy(1,1)=sqrt((one-cos2thy)/two)                        !
            if(twissalphx.lt.zero) sinthx(1,1)=-sinthx(1,1)            !
            if(twissalphy.lt.zero) sinthy(1,1)=-sinthy(1,1)            !
            tempx=one/cos2thx                                          !
            tempy=one/cos2thy                                          !
            t1x=half*twissgamx*akw(1)*epsx*gammab(1,1)                 !
            t1y=half*twissgamy*akw(1)*epsy*gammab(1,1)                 !
            t2x=half*betaxkw*akw(1)*epsx/gammab(1,1)                   !
            t2y=half*betaykw*akw(1)*epsy/gammab(1,1)                   !
            sigmax2=t1x*(one-tempx)+t2x*(one+tempx)                    !
            sigmapx2=t1x*(one+tempx)+t2x*(one-tempx)                   !
            sigmay2=t1y*(one-tempy)+t2y*(one+tempy)                    !
            sigmapy2=t1y*(one+tempy)+t2y*(one-tempy)                   !
            sigmax(1,1)=hundredth*sqrt(sigmax2)                        !
            sigmay(1,1)=hundredth*sqrt(sigmay2)                        !
            sigmapx(1,1)=hundredth*sqrt(sigmapx2)                      !
            sigmapy(1,1)=hundredth*sqrt(sigmapy2)                      !
c           if(ntype(1).eq.4) area=xmax**2+ymax**2                     !
            area=sigmax(1,1)**2+sigmay(1,1)**2                         !
            xi(1,1)=plasma1*sqrt(beami/(vz0*area))                     !
            beam_i(1,1)=beami                                          !
            iself=0                                                    !
      endif  !---------------------------------------------------------!
      if(iprofile.eq.5) then  !----------------------------------------!
            rmax=akw(1)*rbmax                                          !
            rb2=rmax**2                                                !
            xi(1,1)=plasma1*sqrt(beami/(vz0*rmax**2))                  !--Waterbag
            beam_i(1,1)=beami                                          !--Distribution
            if(ntype(1).eq.1) akbeta=two*gamma/betaw(1)                !
            if(ntype(1).gt.1) akbeta=sqrt(two)*gamma/betaw(1)          !
            iself=0                                                    !
      endif  !---------------------------------------------------------!
      if(tprofile.eq.'DISTFILE') then  !-------------------------------!
            beam_i=zero                                                !
            do ns=ns_frst,ns_last                                      !
               beam_i(1,ns)=current_g(2,ns-ns_frst+1)                  !
            end do                                                     !
      endif  !---------------------------------------------------------!
c
      if(slippage.eq.'on') then  !-------------------------------------!
          dtpulse=tslices/dble(nslices-1)                              !
          dt_norm=hundredth*cvac*dtpulse/wavelnth                      !
          z_slip=dble(nslip)*dz/(twopi*dt_norm)                        !
          slip_fac=one+betaw(1)**2/two                                 !--set fractional
          if(ntype(1).eq.4) then                                       !--slippage
             	slip_fac=one+betaw(1)**2                                 !--parameter
          endif                                                        !
          if(ntype(1).eq.9) then                                       !
          	  slip_fac=one+(one+polar**2)*betaw(1)**2/two              !
          endif                                                        !
      endif !----------------------------------------------------------!
c
      P_beam(1)=thou*t*beami
      if(slippage.eq.'on') then
       if((tprofile.ne.'BEAMCODE')
     1                 .or.(tprofile.ne.'DIRECT')
     2                             .or.(tprofile.ne.'DISTFILE')) then
          t_min=half*(tslices-tpulse)
          t_max=tpulse+t_min
          t_center=half*tpulse+t_min
          pbeam=zero
          do nb=1,nbeams
             xitemp=xi(nb,1)
             beam_temp=beam_i(nb,1)
             do ns=max(2,ns_begin),ns_end
                gammab(nb,ns)=gammab(nb,1)
                dgamG(nb,ns)=dgamG(nb,1)
                sigmax(nb,ns)=sigmax(nb,1)
                sigmay(nb,ns)=sigmay(nb,1)
                sigmapx(nb,ns)=sigmapx(nb,1)
                sigmapy(nb,ns)=sigmapy(nb,1)
                x0(nb,ns)=x0(nb,1)
                y0(nb,ns)=y0(nb,1)
                costhx(nb,ns)=costhx(nb,1)
                costhy(nb,ns)=costhy(nb,1)
                sinthx(nb,ns)=sinthx(nb,1)
                sinthy(nb,ns)=sinthy(nb,1)
             end do
             xi(nb,1)=zero
             if(tprofile.eq.'tophat') then
                Q_bunch=beami*tpulse
                do ns=1,nslices
                   t_slice=dble(ns-1)*dtpulse
                   if((t_slice.ge.t_min).and.(t_slice.le.t_max)) then
                        xi(nb,ns)=xitemp
                        beam_i(nb,ns)=beam_temp
                        P_beam(ns)=thou*t*beami
                     else
                        xi(nb,ns)=zero
                        beam_i(nb,ns)=zero
                        P_beam(ns)=zero
                   endif
                   pbeam=pbeam+P_beam(ns)
                end do
             endif
             if(tprofile.eq.'parabolic') then
                Q_bunch=two*beami*tpulse/three
                do ns=1,nslices
                   t_slice=dble(ns-1)*dtpulse
                   if((t_slice.gt.t_min).and.(t_slice.lt.t_max)) then
                     para=abs(one-four*(t_slice-t_center)**2/tpulse**2)
                     xi(nb,ns)=sqrt(para)*xitemp
                     beam_i(nb,ns)=max(zero,para)*beam_temp
                     P_beam(ns)=para*thou*t*beami
                     else
                       xi(nb,ns)=zero
                       beam_i(nb,ns)=zero
                       P_beam(ns)=zero
                   endif
                   pbeam=pbeam+P_beam(ns)
                end do
             endif
             if(tprofile.eq.'gaussian') then
                dtau=half*tpulse
                Q_bunch=fourth*sqrt(pi)*beami*tpulse
     1               *(erf(t_center/dtau)+erf((tslices-t_center)/dtau))
                do ns=1,nslices
                   t_slice=dble(ns-1)*dtpulse
                   argt=(t_slice-t_center)**2/dtau**2
                   xi(nb,ns)=sqrt(exp(-argt))*xitemp
                   beam_i(nb,ns)=exp(-argt)*beam_temp
                   P_beam(ns)=exp(-argt)*thou*t*beami
                end do
                pbeam=pbeam+P_beam(ns)
             endif
          end do
        endif
      endif
c
c------- Set up Temporal Slices for Input from PARMELA, DIMAD, or ELEGANT
c
      if((slippage.eq.'on').and.(tprofile.eq.'BEAMCODE')) then
          epsx_t=epsx
          epsy_t=epsy
          dtpulse=tslices/dble(nslices-1)
c         dtpulse=tslices/dble(nslices)
          dt_norm=hundredth*cvac*dtpulse/wavelnth
          z_slip=dble(nslip)*dz/(twopi*dt_norm)
          slip_fac=one+betaw(1)**2/two
          if(ntype(1).eq.4) then
             	slip_fac=one+betaw(1)**2
          endif
          if(ntype(1).eq.9) then
          	  slip_fac=one+(one+polar**2)*betaw(1)**2/two
          endif
          t_min=half*(tslices-tpulse)
          t_max=tpulse+t_min
          t_center=half*tpulse+t_min
          pbeam=zero
          Q_bunch=zero  !----------------------------------------------!
          do ns=1,nslices_P                                            !--Find total
            Q_bunch=Q_bunch+charges(ns)                                !--bunch charge
          end do        !----------------------------------------------!
          do ns=1,nslices
             x0(1,ns)=x0(1,1)
             y0(1,ns)=y0(1,1)
          end do
c
c
c      This procedure is valid for angles of rotation of less than
c      or equal to 90 degrees
c
c      Only one beam available for BEAMCODE input
c
         do ns=1,nslices
            t_ns=dtpulse*dble(ns-1) !----------------------------------!
            if((t_ns.lt.t_min).or.(t_ns.gt.t_max)) then                !
               beam_i(1,ns)=zero                                       !
               P_beam(ns)=zero                                         !
               if(t_ns.lt.t_min) ns_P=1                                !
               if(t_ns.gt.t_max) ns_P=nslices_P                        !
               slope_P=zero                                            !
            else                                                       !
               dtpulse_P=tpulse/dble(nslices_P)                        !
               t_P=t_ns-t_min                                          !--Find equivalent
               do nsp=1,nslices_P                                      !--BEAMCODE slice
                  tmin_P=dtpulse_P*dble(nsp-1)                         !
                  tmax_P=dtpulse_P*dble(nsp)                           !
                  if((t_P.ge.tmin_P).and.(t_P.lt.tmax_P)) then         !
                      ns_P=nsp                                         !
                      slope_P=(t_P-tmin_P)/dtpulse_P                   !
                   endif                                               !
               end do                                                  !
               beam_i(1,ns)=currents(ns_P)                             !
               P_beam(ns)=1.e6*energies(ns_P)*currents(ns_P)           !
            endif !----------------------------------------------------!
            gammab(1,ns)=one+thou*energies(ns_P)/emass
            dgamG(1,ns)=dgams(ns_P)
c           sigmax(1,ns)=akw(1)*x_maxs(ns_P)
c           sigmay(1,ns)=akw(1)*y_maxs(ns_P)
            sigmax(1,ns)=x_maxs(ns_P)
            sigmay(1,ns)=y_maxs(ns_P)
            sigmapx(1,ns)=sigma_pxs(ns_P)
            sigmapy(1,ns)=sigma_pys(ns_P)
            xmax=akw(1)*x_maxs(ns_P)
            ymax=akw(1)*y_maxs(ns_P)
            epsx=eps_xs(ns_P)
            epsy=eps_ys(ns_P)
            x0(1,ns)=x_avgs(ns_P)
            y0(1,ns)=y_avgs(ns_P)
            px0(1,ns)=px_avgs(ns_P)
            py0(1,ns)=py_avgs(ns_P)
            twissalphxi(1,ns)=Twiss_alph_x(ns_P)
            twissalphyi(1,ns)=Twiss_alph_y(ns_P)
            if(ns_P.lt.nslices_P) then  !------------------------------!
               beam_i(1,ns)=beam_i(1,ns)                               !
     1                       +slope_P*(currents(ns_P+1)-currents(ns_P))!
               P_beam(ns)=P_beam(ns)                                   !
     1                       +slope_P*(energies(ns_P+1)-energies(ns_P))!
               gammab(1,ns)=gammab(1,ns)                               !
     1            +thou*slope_P*(energies(ns_P+1)-energies(ns_P))/emass!
               dgamG(1,ns)=dgamG(1,ns)                                 !
     1                             +slope_P*(dgams(ns_P+1)-dgams(ns_P))!
               sigmax(1,ns)=(sigmax(1,ns)                              !
     1                          +slope_P*(x_maxs(ns_P+1)-x_maxs(ns_P)))!
               sigmay(1,ns)=(sigmay(1,ns)                              !
     1                          +slope_P*(y_maxs(ns_P+1)-y_maxs(ns_P)))!
               sigmapx(1,ns)=sigmapx(1,ns)                             !
     1                     +slope_P*(sigma_pxs(ns_P+1)-sigma_pxs(ns_P))!--Interpolate
               sigmapy(1,ns)=sigmapy(1,ns)                             !--between slices
     1                     +slope_P*(sigma_pys(ns_P+1)-sigma_pys(ns_P))!
c              xmax=akw(1)*xmax+slope_P*(x_maxs(ns_P+1)-x_maxs(ns_P))  !
c              ymax=akw(1)*ymax+slope_P*(y_maxs(ns_P+1)-y_maxs(ns_P))  !
               xmax=akw(1)*(x_maxs(ns_P)                               !
     1                         +slope_P*(x_maxs(ns_P+1)-x_maxs(ns_P))) !
               ymax=akw(1)*(y_maxs(ns_P)                               !
     1                        +slope_P*(y_maxs(ns_P+1)-y_maxs(ns_P)))  !
               epsx=epsx+slope_P*(eps_xs(ns_P+1)-eps_xs(ns_P))         !
               epsxi(1,ns)=epsx                                        !
               epsy=epsy+slope_P*(eps_ys(ns_P+1)-eps_ys(ns_P))         !
               epsyi(1,ns)=epsy                                        !
               x0(1,ns)=x0(1,ns)+slope_P*(x_avgs(ns_P+1)-x_avgs(ns_P)) !
               y0(1,ns)=y0(1,ns)+slope_P*(y_avgs(ns_P+1)-y_avgs(ns_P)) !
               px0(1,ns)=px0(1,ns)                                     !
     1                         +slope_P*(px_avgs(ns_P+1)-px_avgs(ns_P))!
               py0(1,ns)=py0(1,ns)                                     !
     1                         +slope_P*(py_avgs(ns_P+1)-py_avgs(ns_P))!
               twissalphxi(1,ns)=twissalphxi(1,ns)                     !
     1               +slope_P*(Twiss_alph_x(ns_P+1)-Twiss_alph_x(ns_P))!
               twissalphyi(1,ns)=twissalphyi(1,ns)                     !
     1               +slope_P*(Twiss_alph_y(ns_P+1)-Twiss_alph_y(ns_P))!
            endif  !---------------------------------------------------!
            betaxkw=tenthou*gammab(1,ns)*xmax**2/(akw(1)*epsx)
            betaykw=tenthou*gammab(1,ns)*ymax**2/(akw(1)*epsy)
            betax=hundredth*betaxkw/akw(1)
            betay=hundredth*betaykw/akw(1)
            betatxi(1,ns)=betax
            betatyi(1,ns)=betay
            twissgamx=(one+twissalphxi(1,ns)**2)/betaxkw
            twissgamy=(one+twissalphyi(1,ns)**2)/betaykw
            twissgamxi(1,ns)=twissgamx
            twissgamyi(1,ns)=twissgamy
            denomx=gammab(1,ns)*twissgamx-betaxkw/gammab(1,ns)
            denomy=gammab(1,ns)*twissgamy-betaykw/gammab(1,ns)
            cos2thx=one/sqrt(one+(two*twissalphxi(1,ns))**2/denomx**2)
            if(denomx.lt.zero) cos2thx=-cos2thx
            cos2thy=one/sqrt(one+(two*twissalphyi(1,ns))**2/denomy**2)
            if(denomy.lt.zero) cos2thy=-cos2thy
            costhx(1,ns)=sqrt((one+cos2thx)/two)
            costhy(1,ns)=sqrt((one+cos2thy)/two)
            sinthx(1,ns)=sqrt((one-cos2thx)/two)
            sinthy(1,ns)=sqrt((one-cos2thy)/two)
            if(Twiss_alph_x(ns_P).lt.zero) sinthx(1,ns)=-sinthx(1,ns)
            if(Twiss_alph_y(ns_P).lt.zero) sinthy(1,ns)=-sinthy(1,ns)           
            area=sigmax(1,ns)**2+sigmay(1,ns)**2
            xi(1,ns)=plasma1*sqrt(beam_i(1,ns)/area)
            iself=0
            pbeam=pbeam+P_beam(ns)
         end do
         epsx=epsx_t
         epsy=epsy_t
         twissalphx=twiss_alpha_x
         twissalphy=twiss_alpha_y
      endif   !   End BEAMCODE Profile
c
c------- Call mpi_reduce
c
      if(numprocs.gt.1) then
         Bwork=zero
         call mpi_reduce(xi,Bwork,nslices*nbeams,
     1                   mpi_double_precision,mpi_sum,0,
     2                   mpi_comm_world,ier)
         call mpi_bcast(Bwork,nslices*nbeams,mpi_double_precision,0,
     1             mpi_comm_world,ier)
      endif
c
      rgkw=akw(1)*rg
      rgkw2=rgkw**2
      agkwrf=akw(1)*rg_x
      bgkwrf=akw(1)*rg_y
      hagkwrf=half*agkwrf
      hbgkwrf=half*bgkwrf
      akw_wg=rg_x*akw(1)
      bkw_wg=rg_y*akw(1)
      hakw=half*akw_wg
      hbkw=half*bkw_wg
c
c------- set mode parameters
c
      if(iprop.eq.0) nmodes=0
      if((iprop.ne.0).and.(mselect.le.0)) then
         if(teprofile.eq.'variable') then
               nsmax=nslices
            else
               nsmax=1
         endif
         lmax=0
         lmin=0
         nmax=0
         maxh=0
c         if(myid.eq.0) write(6,*) ' input inp9'
         if(myid.eq.0) write(6,*) ' settings for optical modes ...'
c
         if(myid.eq.0.and.OPC.eq.'on'.and.numprocs.gt.1) then
            if(teprofile.eq.'variable') then
             open(unit=2019,file='inp9_minerva.dat',form='unformatted',
     1              status='old')
            endif
            open(unit=2020,file='inp9_mercury.dat',form='unformatted',
     1           status='replace')
         endif
         do ns=1,nsmax  ! do loop to read in mode info for each slice
         plnmax(ns)=zero
         nharms=0
         nlast=0
         do nm=1,nmodes
           nharm=1
           zwaist=zero
           direction='x'
           phase_in=zero
           if(myid.eq.0.and.OPC.eq.'off') read(5,inp9)
           if(myid.eq.0.and.OPC.eq.'on') then
              if(numprocs.eq.1) read(5,inp9)
              if(numprocs.gt.1) then
                 if(teprofile.ne.'variable') read(5,inp9)
                 if(teprofile.eq.'variable') then
                    read(2019) nharm,l,n,pin,phase_in,waist,zwaist,
     1                         direction
                 endif          
              endif
           endif
           if(numprocs.gt.1) call input9_bcast
           maxh=max(maxh,nharm)
           if(myid.eq.0.and.OPC.eq.'on'.and.numprocs.eq.1) then
              write(IOPC,*) ' &inp9'
              write(IOPC,1119) nharm,l,n,imode
 1119         format('   nharm=',i0,/,'   l=',i0,/,'   n=',i0,/,
     1               '   imode=',i0)
              write(IOPC,1139) pin,phase_in,waist,zwaist
 1139         format(SP,'   pin=',1pe17.10,/,'   phase_in=',e17.10,/,
     1               '   waist=',e17.10,/,'   zwaist=',e17.10)
              write(IOPC,8009) trim(direction)
 8009         format("   direction='",a,"'")
              write(IOPC,*) ' /'
           endif
           if(myid.eq.0.and.OPC.eq.'on'.and.numprocs.gt.1) then
              write(2020) nharm,l,n,pin,phase_in,waist,zwaist,
     1                    direction,imode
           endif
           if(nharm.ne.nlast) then
                  nharms=nharms+1
                  nlast=nharm
           endif
           if(emmodes.eq.'hermite') then  !----------------------------!
               lnum(nm,ns)=l                                           !
               lmax=max(lmax,abs(l))                                   !
               lmin=min(lmin,l)                                        !
               nnum(nm,ns)=n                                           !
               nmax=max(nmax,n)                                        !
               phase_a(nm,ns)=phase_in                                 !
               omega(nm)=dble(nharm)*domega                            !
               w0(nm,ns)=akw(1)*waist                                  !
               akwwaist=akw(1)*waist                                   !
               w02(nm,ns)=w0(nm,ns)**2                                 !
               zwaste(nm,ns)=hundred*akw(1)*zwaist                     !
               z0(nm,ns)=half*omega(nm)*w02(nm,ns)                     !
               facln=one/(factorial(l)*factorial(n)*two**(l+n))        !--Set Up
               factor(nm,ns)=facln*vz0/(two*omega(nm))                 !--Gauss-Hermite
               pfac(nm,ns)=two*facln/(radfac*(omega(nm)*w0(nm,ns))**2) !--Modes
               da(nm,ns)=sqrt(pfac(nm,ns)*pin)                         !
c    1                             /(one+(zwaste(nm,ns)/z0(nm,ns))**2))!
               pln(nm,ns)=pin                                          !
               plnmax(ns)=plnmax(ns)+pin                               !
               if(direction.eq.'x') then                               !
                  xdir(nm)=one                                         !
                  ydir(nm)=zero                                        !
               endif                                                   !
               if(direction.eq.'y') then                               !
                  xdir(nm)=zero                                        !
                  ydir(nm)=one                                         !
               endif                                                   !
           endif  !----------------------------------------------------!
c
           if(emmodes.eq.'laguerre') then  !---------------------------!
               polarfac=half*(one+polar**2)                            !
               lnum(nm,ns)=l                                           !
               lmax=max(lmax,abs(l))                                   !
               lmin=min(lmin,l)                                        !
               nnum(nm,ns)=n                                           !
               nmax=max(nmax,n)                                        !
               phase_a(nm,ns)=phase_in                                 !
               omega(nm)=dble(nharm)*domega                            !
               w0(nm,ns)=akw(1)*waist                                  !
               akwwaist=akw(1)*waist                                   !
               w02(nm,ns)=w0(nm,ns)**2                                 !--Set Up              
               zwaste(nm,ns)=hundred*akw(1)*zwaist                     !--Gauss-Laguerre
               z0(nm,ns)=half*omega(nm)*w02(nm,ns)                     !--Modes
               facln=factorial(n)/factorial(abs(l)+n)                  !
               factor(nm,ns)=facln*vz0/(two*two*polarfac*omega(nm))    !
               pfac(nm,ns)=facln                                       !
     1                      /(polarfac*radfac*(omega(nm)*w0(nm,ns))**2)!
               da(nm,ns)=sqrt(pfac(nm,ns)*pin)                         !
c    1                             /(one+(zwaste(nm,ns)/z0(nm,ns))**2))!
               pln(nm,ns)=pin                                          !
               plnmax(ns)=plnmax(ns)+pin                               !
               xdir(nm)=one                                            !
               ydir(nm)=zero                                           !
           endif  !----------------------------------------------------!
         end do   !  do loop over nmodes
         end do   !  do loop over nsmax
c
         if(myid.eq.0.and.OPC.eq.'on'.and.numprocs.gt.1) then
            close(unit=2019)
            close(unit=2020)
         endif
      endif
c
      omtest=-omega(1)
      dirtest=xdir(1)
      nharm=0
      do nm=1,nmodes
        if((omega(nm).ne.omtest).or.(xdir(nm).ne.dirtest)) then
           omtest=omega(nm)
           dirtest=xdir(nm)
           nharm=nharm+1
        endif
      end do
      nharms=nharm
      allocate(Psum(nharms+1,nslices),wharm(nharms+1,nslices),
     1         alphah(nharms+1,nslices),nh1(nharms+1,nslices),
     2         nh2(nharms+1,nslices))
      omtest=-omega(1)
      dirtest=xdir(1)
      nharm=0
      do nm=1,nmodes
        if((omega(nm).ne.omtest).or.(xdir(nm).ne.dirtest)) then
           omtest=omega(nm)
           dirtest=xdir(nm)
           nharm=nharm+1
           nh1(nharm,1)=nm
        endif
      end do
      if(nharms.eq.1) then
           nh2(1,1)=nmodes
        else
           nh2(nharms,1)=nmodes
           do nh=1,nharms-1
               nh2(nh,1)=nh1(nh+1,1)-1
           end do
      endif
      do ns=2,nslices
         do nh=1,nharms
            nh1(nh,ns)=nh1(nh,1)
            nh2(nh,ns)=nh2(nh,1)
         end do
      end do
      if((slippage.eq.'on').and.(teprofile.ne.'variable')) then
         t_min=half*(tslices-tlight)+tshift
         t_max=tlight+t_min
         t_center=half*tlight+t_min
         do nh=1,nharms
            do ns=2,nslices
               nh1(nh,ns)=nh1(nh,1)
               nh2(nh,ns)=nh2(nh,1)
            end do
         end do
         do nm=1,nmodes
            datemp=da(nm,1)
            plntemp=pln(nm,1)
            do ns=2,nslices
                lnum(nm,ns)=lnum(nm,1)
                nnum(nm,ns)=nnum(nm,1) 
                w0(nm,ns)=w0(nm,1)
                w02(nm,ns)=w02(nm,1)
                zwaste(nm,ns)=zwaste(nm,1)
                z0(nm,ns)=z0(nm,1)
                factor(nm,ns)=factor(nm,1)
                pfac(nm,ns)=pfac(nm,1)
            end do
            do ns=ns_begin,ns_end
               t_slice=dble(ns-1)*dtpulse
               if(teprofile.eq.'tophat') then
                  if((t_slice.ge.t_min).and.(t_slice.le.t_max)) then
                        da(nm,ns)=datemp
                        pln(nm,ns)=plntemp
                     else
                        da(nm,ns)=zero
                        pln(nm,ns)=zero
                  endif
               endif
               if(teprofile.eq.'parabolic') then
                  if((t_slice.gt.t_min).and.(t_slice.lt.t_max)) then
                     para=abs(one-four*(t_slice-t_center)**2/tlight**2)
                     da(nm,ns)=sqrt(para)*datemp
                     pln(nm,ns)=para*plntemp
                     else
                        da(nm,ns)=zero
                        pln(nm,ns)=zero
                  endif
               endif
               if(teprofile.eq.'gaussian') then
                  dtau=half*tlight
                  argt=(t_slice-t_center)**2/dtau**2
                  da(nm,ns)=sqrt(exp(-argt))*datemp
                  pln(nm,ns)=exp(-argt)*plntemp
               endif
            end do
         end do
         pmaxtemp=plnmax(1)
         do ns=ns_begin,ns_end
            t_slice=dble(ns-1)*dtpulse
            if(teprofile.eq.'tophat') then
               if((t_slice.ge.t_min).and.(t_slice.le.t_max)) then
                     plnmax(ns)=pmaxtemp
                  else
                     plnmax(ns)=zero
               endif
            endif
            if(teprofile.eq.'parabolic') then
               if((t_slice.ge.t_min).and.(t_slice.le.t_max)) then
                     para=one-four*(t_slice-t_center)**2/tlight**2
                     plnmax(ns)=para*pmaxtemp
                  else
                     plnmax(ns)=zero
               endif
            endif
            if(teprofile.eq.'gaussian') then
                  dtau=half*tlight
                  argt=(t_slice-t_center)**2/dtau**2
                  plnmax(ns)=exp(-argt)*pmaxtemp               
            endif
         end do
      endif
      do ns=ns_begin,ns_end
         P_in(ns)=zero
         do nh=1,nharms
            Psum(nh,ns)=zero
            do nm=nh1(nh,ns),nh2(nh,ns)
               Psum(nh,ns)=Psum(nh,ns)+pln(nm,ns)
               P_in(ns)=P_in(ns)+pln(nm,ns)
            end do
         end do
      end do
c
      if((slippage.eq.'on').and.(FFTchk.eq.'on')
     1                       .and.(ndiags.ge.1)) then
          allocate(A1FFT(nslices,nsize,ndiags),
     1             A2FFT(nslices,nsize,ndiags))
          A1FFT=zero
          A2FFT=zero
      endif
      if(SpentBeam.eq.'on') then
         allocate(gmn(nslices),gmx(nslices))
         allocate(E_spent(n_spent),P_spent(n_spent,nslices))
      endif
      if((slippage.eq.'on').and.(ndiags.ge.1)) then
          allocate (P_slice(nslices,nharms,ndiags),
     1              P00_slice(nslices,nharms,ndiags))
          P_slice=zero
          P00_slice=zero
      endif
      if((slippage.ne.'on').and.(ndiags.ge.1).and.(nharms.gt.1)) then
         allocate (P_spec(nharms,ndiags))
      endif
c
      if((myid.eq.0).AND.(ndiags.gt.0)) write(6,*) 
     1                                ' settings for diagnostics ...'
      do id=1,ndiags  !------------------------------------------------!
c         if(myid.eq.0) write(6,*) ' input inp10'                      !
          if(myid.eq.0) read(5,inp10)                                  !
          if(numprocs.gt.1) call input10_bcast                         !
          if(myid.eq.0.and.OPC.eq.'on') then                           !
             write(IOPC,*) ' &inp10'                                   !
             write(IOPC,1141) zdiag                                    !
 1141        format(SP,'   zdiag=',1pe17.10)                           !
             write(IOPC,*) ' /'                                        !
          endif                                                        !--Input INP10
          zdiags(id)=zdiag                                             !--Diagnostic Check
          if((zdiag.eq.zero).and.(slippage.eq.'on')) then              !--Points
             do nh=1,nharms                                            !
                do ns=ns_begin,ns_end                                  !
                   P_slice(ns,nh,id)=zero                              !
                   do nm=nh1(nh,ns),nh2(nh,ns)                         !
                      P_slice(ns,nh,id)=P_slice(ns,nh,id)+pln(nm,ns)   !
                   end do                                              !
                   P00_slice(ns,nh,id)=pln(nh1(nh,ns),ns)              !
                end do                                                 !
             end do                                                    !
          endif                                                        !
      end do  !--------------------------------------------------------!
c
      allocate(A1_M2(nmodes,nslices,ndiags),
     1         A2_M2(nmodes,nslices,ndiags),
     2         asde1_M2(nmodes,nslices,ndiags))
c
c------- set initial electron distribution & weights
c
      nflag=0
      if((iprop.ne.0).and.(npsi.gt.10)) then
           nten=10*(npsi/10)
           neight=8*(npsi/8)
           if((npsi.ne.nten).and.(npsi.ne.neight)) then
                 npsi=10
                 nflag=10
           endif
      endif    
      if(iprofile.le.3) then
         ngam=1
         if(dgamz.gt.deci) nr=4
         if(dgamz.gt.deci) ny=4
         if(iprofile.le.1) then
             n1=nr
             n2=ntheta
         endif
         if(iprofile.eq.2) then
             n1=nx
             n2=ny
         endif
         if(iprofile.eq.3) then
             n1=ntheta
             n2=nr
         endif
         if(iprop.eq.0) npsi=1
         iparts=n1*n2*npsi
         if(dgamz.gt.deci) iparts=iparts*nphi*ndv
      endif
      if((iprofile.eq.4).or.(iprofile.eq.5)) then
         if(iprop.eq.0) npsi=1
         if(dgam.lt.deci) ngam=1
         n1=ntheta
         n2=nr
         iparts=npsi*n1*n2*nphi*ndv*ngam*nbeams
      endif
      inmax=max(lmax,nmax)+3
      allocate(x(iparts,nrmdim),y(iparts,nrmdim),psi(iparts,nrmdim),
     1     px(iparts,nrmdim),py(iparts,nrmdim),pz(iparts,nrmdim),
     1     fx(iparts,nrmdim),fy(iparts,nrmdim),fpsi(iparts,nrmdim),
     2     fpx(iparts,nrmdim),fpy(iparts,nrmdim),fpz(iparts,nrmdim),
     3     weights(iparts,nrmdim),sumw(nslices),al(iparts,inmax,inmax),
     4     Rbrms(nslices),xavg(nslices),yavg(nslices),
     5     gamma0(iparts,nrmdim),ipart(nslices))
      x=zero
      y=zero
      psi=zero
      px=zero
      py=zero
      pz=zero
      weights=zero
      allocate(qx(iparts,nrmdim),qy(iparts,nrmdim),
     1         qpsi(iparts,nrmdim),qpx(iparts,nrmdim),
     2         qpy(iparts,nrmdim),qpz(iparts,nrmdim),
     3         Prob(iparts,nrmdim),r2(iparts,nrmdim),
     4         mkill(iparts+nmodes,nslices))
      if(emmodes.eq.'hermite') then
         do i=1,2
            do k=1,iparts
               al(k,1,i)=one
            end do
         end do
      endif
      allocate(arg(iparts),gama(iparts),r(iparts),snphi(iparts),
     1         csphi(iparts),wfacy(iparts),rsqr(iparts),elnfac(iparts),
     2         bx(iparts),by(iparts),wfac(iparts),bz(iparts))
      ipart=0
      sumw=zero
      if(iprofile.le.6) then
         do ns=ns_begin,ns_end
            if((ns.ge.ns_frst).and.(ns.le.ns_last)) then
                   ipart(ns)=iparts
                else
                   ipart(ns)=0
            endif
         end do
      endif
c
      if(tprofile.eq.'DISTFILE') then
         do ns=1,nslices
            if((ns.ge.ns_frst).and.(ns.le.ns_last)) then
               if(current_g(2,ns-ns_frst+1).gt.zero) ipart(ns)=nparts_g
            endif
c     write(6,*) 'ns=',ns,' ipart=',ipart(ns)
         end do
      endif
c
      if(tprofile.eq.'DIRECT') then  !---------------------------------!
         if((myid.eq.0).and.(nslices.le.10000)) then                   !
            allocate(current_d(nslices),energy_d(nslices))             !
            allocate(dgam_d(nslices))                                  !
            allocate(epslonx_d(nslices),epslony_d(nslices))            !
            current_d=zero                                             !
            energy_d=zero                                              !
            dgam_d=zero                                                !
            epslonx_d=zero                                             !
            epslony_d=zero                                             !
         endif                                                         !
         if(myid.eq.0) then                                            !
         	  read(28) tm1,tm2,tm3,tm4,tm5,tm6,tm7                 !
         	  read(28) tn1,tn2,tn3,tn4,tn5,tn6,tn7                 !
         endif                                                         !
         do nss=1,nslices                                              !
            if(myid.eq.0) then                                         !
               read(28) ripartsns,sumwns,currentns,energyns,dgamns,    !
     1                  epslonxns,epslonyns                            !
               if(nslices.le.10000) then                               !
                  current_d(nss)=currentns                             !
                  energy_d(nss)=energyns                               !
                  dgam_d(nss)=dgamns                                   !
                  epslonx_d(nss)=epslonxns                             !
                  epslony_d(nss)=epslonyns                             !
               endif                                                   !
            endif                                                      !
            call mpi_bcast(ripartsns,1,mpi_double_precision,0,         !
     1                                              mpi_comm_world,ier)!                                             
            call mpi_bcast(sumwns,1,mpi_double_precision,0,            !--Input for
     1                                              mpi_comm_world,ier)!--DIRECT Injection
            ipart(nss)=int(ripartsns)                                  !--of particles
            sumw(nss)=sumwns                                           !
         end do                                                        !
         if((myid.eq.0).and.(nslices.le.10000)) then                   !
            open(unit=29,file='Slice_Statistics.txt',form='formatted', !
     1           status='replace')                                     !
            write(29,5678)                                             !
 5678       format(24x,'Statistical Distribution by Slice',//,         !
     1      1x,'slice',2x,'time(sec)',3x,'Current(A)',3x,              !
     2         'Energy(MeV)',3x,'dgam(rms)',2x,'epsx(micron)',1x,      !
     3            'epsy(micron)')                                      !
            do nss=1,nslices                                           !
               time=tslices*dble(nss-1)/dble(nslices-1)                !
               write(29,5679) nss,time,current_d(nss),energy_d(nss),   !
     1                        dgam_d(nss),epslonx_d(nss),epslony_d(nss)!
 5679          format(1x,i4,6(1x,1pe12.5))                             !
            end do                                                     !
            close(unit=29)                                             !
            deallocate(current_d,energy_d,dgam_d,epslonx_d,epslony_d)  !
         endif                                                         !
         close(unit=28)                                                !
      endif  !---------------------------------------------------------!
      if(RFfield.eq.'rf') then
          allocate(xbes(iparts),besjl(iparts),besjp(iparts),
     1             fm1(iparts),alphab(iparts),rx(iparts),fm(iparts),
     2             fmk(iparts))
          allocate(sinrf(iparts),cosrf(iparts),stheta(iparts),
     1             ctheta(iparts),sinrfm(iparts),cosrfm(iparts),
     2             sinrfp(iparts),cosrfp(iparts),besjm(iparts))
c
          do i=1,nsegbw
            if(drifttube.eq.'cylindrical') then
                rgkwrf(i)=akw(1)*rg
                rgkw2=rgkw**2
                aklnrf(i)=xlnrf(i)/(akw(1)*rg)
            endif
            if(drifttube.eq.'rectangular') then
                aklnrf(i)=pi*sqrt((lnumrf(i)/agkwrf)**2
     1                                       +(nnumrf(i)/bgkwrf)**2)
            endif
            if(omegarf(i).lt.aklnrf(i)) then
               write(6,901) i
  901          format(/,' RF drive frequency below cutoff in segment',
     1                i3,' TERMINATE MINERVA')
               call mpi_finalize(ierr)
               stop
            endif
            akzrf(i)=sqrt(omegarf(i)**2-aklnrf(i)**2)
            if(RFmode.eq.'TE') Pfac2=one
            if(RFmode.eq.'TM') Pfac2=one+(aklnrf(i)/akzrf(i))**2
            if(drifttube.eq.'cylindrical') then
                rl=dble(lnumrf(i))
                n=nnumrf(i)
                xbes(1)=xlnrf(i)
                call bessels(lnumrf(i),1,xlnrf(i))
                if(RFmode.eq.'TE') then
                    denom=rgkw2*(one-rl**2/xlnrf(i)**2)*(besjl(1)**2)
                endif
                if(RFmode.eq.'TM') then
                    denom=rgkw2*besjp(1)**2  
                endif
            endif
            if(drifttube.eq.'rectangular') then
                denom=agkwrf*bgkwrf/twopi
                if(RFmode.eq.'TE') then
                   if(lnumrf(i)*nnumrf(i).ne.0) denom=half*denom
                endif
                if(RFmode.eq.'TM') denom=half*denom
            endif
            Pfacrf(i)=one/(watts*omegarf(i)*akzrf(i)*denom*Pfac2)
          end do
      endif
      call init(gamma,vz0,rmax,rmin,xb,yb,vz0*gamma,iprofile,dgam,
     1          nbeams,prebunch,psiwidth,beam_diags,Shot_Noise,
     2          noise_seed,B_ratio,current,trans_cor,maxh,
     3          dump,restart,zbegin,Gauss_Quad,
     4          dinput_file,nparts_g,ns_frst,ns_last,epsx,epsy,
     5          twissalphx,twissalphy,pbeam,twissbetax,twissbetay)
c
      allocate(efield(nslices))  !-------------------------------------!
      efield=zero                                                      !
      if(Wakefield.eq.'on') then                                       !--Engage Wakefield
         call wakefields(energy,current,hundredth*rbmax,zmax,zwi(1),   !--Routines
     1       hundredth*rg,hundredth*rg_y,ns_frst,ns_last,dinput_file)  !
      endif  !---------------------------------------------------------!
c
c
c------ Allocate arrays for Energy diagnostic
c
      if((nsegbw.eq.1).and.(nsegfoc.eq.0)) then
             nzlen=int(ztotal/dz)/nstep+1
         else
             call array_dim(zero,dz,ztotal,nstep)
      endif
      allocate(Ewrite(nzlen),Ework(nzlen),Pfinal(nzlen),zwrite(nzlen))
      Ewrite=zero
      Ework=zero
      if(nharms.gt.1) then
         allocate(Pwrite(nzlen,nharms),Pwork(nzlen,nharms),
     1            Phfinal(nzlen,nharms),zhwrite(nzlen))
         Phfinal=zero
         Pwrite=zero
         Pwork=zero
      endif
      zwrite(1)=zero
c
      rewind(22)
      if(RFfield.eq.'rf') then
              do ns=ns_begin,ns_end
                 do i=1,nsegbw
                   Eacc(i,ns)=sqrt(Pfacrf(i)*Pinrf(i,ns))
                 end do
              end do
      endif
      do ns=ns_begin,ns_end
            nss=ns-ns_begin+1
            if(nss.gt.nrmdim) print *,"nss out of bounds 2121"
            do nm=nh1(1,ns),nh2(1,ns)
                alnsq=a1(nm,ns)**2+a2(nm,ns)**2
                Phead(nm,ns)=alnsq/pfac(nm,ns)
                lhead(nm,ns)=lnum(nm,ns)
                nhead(nm,ns)=nnum(nm,ns)
            end do
            alpha=-zwaste(1,ns)/z0(1,ns)
            w=w0(1,ns)*sqrt(one+alpha**2)
            W_Spot=Spot_Size(1,ns)
c           Pwrite(ns,1)=Psum(1,ns)
            Ewrite(1)=Ewrite(1)+dtpulse*Psum(1,ns)
            if(Power_vs_z.eq.'on') then
               write(30+ns) zero,Psum(1,ns),w/akw(1),W_Spot,alpha,
     1                      Rbrms(ns),ipart(ns)
            endif
      end do   ! End Loop over nslices
c
c------- Perform Integration
c
      pin=plnmax(1)
      if(slippage.eq.'on') then
            allocate(beam_ns(nslices))
            do ns=1,nslices
               beam_ns(ns)=beam_i(1,ns)
            end do
      endif
      if(S2E.eq.'on') then
            allocate(ns2e(2,numprocs))
            allocate(x_out(nparts_out+1,numprocs),
     1               y_out(nparts_out+1,numprocs),
     2               px_out(nparts_out+1,numprocs),
     3               py_out(nparts_out+1,numprocs),
     4               pz_out(nparts_out+1,numprocs),
     5               psi_out(nparts_out+1,numprocs))
            ns2e=0
            x_out=zero
            y_out=zero
            px_out=zero
            py_out=zero
            pz_out=zero
            psi_out=zero
      endif
      if(myid.eq.0) write(6,*) 'Begin Integration'
      if(numprocs.gt.1) startode=mpi_wtime()
      if(restart.eq.'off') then
               zinitial=zero
c              if(tprofile.eq.'DISTFILE') zinitial=twopi
            else
               zinitial=zbegin/scale
      endif
c
      call ode(zinitial,dz,ztotal,iecon,pin,pbeam,beami,
     1        nstep,itermax,beamloss,trans_mode,beam_diags,
     2        FFTchk,ns_frst,ns_last,Power_vs_z,SpentBeam,S2E,
     3        S2E_method,dither,nparts_out,nbin,wavelnth,x_avg,
     4        y_avg,z_avg,px_avg,py_avg,pz_avg,dump,restart,
     5        Wakefield,Fluence_only,Runge_Kutta,interpolation)
c
      if(numprocs.gt.1) then  !----------------------------------------!
              call mpi_reduce(Ewrite,Ework,nzlen,mpi_double_precision, !
     1                        mpi_sum,0,mpi_comm_world,ier)            !--Fundamental Energy
            else                                                       !--Reduction when
              Ework=Ewrite                                             !--nslices > 1
      endif                                                            !
      if(myid.eq.0) Pfinal=Ework  !------------------------------------!
      if(main_out.eq.'on') then  !-------------------------------------!
          if(myid.eq.0) write(6,*) 'Write Principal Output Files'      !
          call Mode_Out(itermax,pin,t,beami,nstep,ztotal,wigerrs,      !
     1         prebunch,iprofile,matchbeam,escale,fscale,energy,ndz,   !
     2         rbmin,rbmax,xbeam,ybeam,gamma,dgam,dgamz,dp0,epsx,epsy, !
     3         akbeta,gammaavg,betaavg,twissgamx,twissgamy,twissalphx, !
     4         twissalphy,betax,betay,nbeams,psiwidth,source,iseed0,   !--Output Power &
     5         error,waist,rg,rg_x,rg_y,dz,zmax,vz0,nflag,             !--Energy vs z Files
     6         t_sep,t_rndtrp,tlight,Shot_Noise,noise_seed_1,trans_cor,!
     7         OPC,Power_vs_z,nslices_P,current,x_max,y_max,sigmapx_P, !
     8         sigmapy_P,B_ratio,restart,Gauss_Quad,dinput_file,       !
     9         twissbetax,twissbetay,Runge_Kutta,interpolation)        !
          if(myid.eq.0) write(6,*) 'Output Complete'                   !
      endif  !---------------------------------------------------------!
      if(nharms.gt.1) then  !------------------------------------------!
           if(numprocs.gt.1) then                                      !
                if(myid.eq.0) write(6,*) 'Begin Harmonic Collection'   !
                start_reduce=mpi_wtime()                               !
                call mpi_reduce(Pwrite,Pwork,nharms*nzlen,             !
     1              mpi_double_precision,mpi_sum,0,mpi_comm_world,ier) !
                stopreduce=mpi_wtime()                                 !--Harmonic Energy
                if(myid.eq.numprocs/2) then                            !--Reduction when
                    write(6,*) ' Integration Complete, time=',         !--nslices > 1
     1                       stopreduce-startode,' seconds'            !
                endif                                                  !
              else                                                     !
                Pwork=Pwrite                                           !
            endif                                                      !
      endif  !---------------------------------------------------------!
      if((myid.eq.0).and.(nslices.gt.1)) then                          !
            do nh=2,nharms                                             !
               do i=1,itermax                                          !
                  Phfinal(i,nh)=Phfinal(i,nh)+Pwork(i,nh)              !
               end do                                                  !
            end do                                                     !
            if(nharms.gt.1) write(6,*) 'Begin Harmonic Output'         !--Harmonic Output
            do i=1,itermax-1                                           !--when nslices > 1
               do nh=2,nharms                                          !
                  write(nehout(nh),4000) zhwrite(i),Phfinal(i,nh)      !
 4000             format(2(1x,1pe10.3))                                !
               end do                                                  !
            end do                                                     !
            if(nharms.gt.1) write(6,*) 'Output Complete'               !
      endif  !---------------------------------------------------------!
      do nh=2,nharms  !-------------Close Files------------------------!
            close(unit=nehout(nh))                                     !
      end do                                                           !
      do ns=1,nslices                                                  !
            do nh=2,nharms                                             !
               close(unit=nhout(nh,ns))                                !
            end do                                                     !
      end do  !--------------------------------------------------------!
      if(FFTchk.eq.'on') then  !---------------------------------------!
         if(myid.eq.0) write(6,*) 'Begin FFT'                          !
         if(numprocs.gt.1) then                                        !
             allocate(FFT1_work(nslices,nsize,ndiags),                 !
     1                FFT2_work(nslices,nsize,ndiags))                 !
             FFT1_work=zero                                            !
             FFT2_work=zero                                            !
             call mpi_reduce(A1FFT,FFT1_work,nslices*nsize*ndiags,     !
     1                       mpi_double_precision,mpi_sum,0,           !
     2                       mpi_comm_world,ier)                       !
             call mpi_reduce(A2FFT,FFT2_work,nslices*nsize*ndiags,     !
     1                       mpi_double_precision,mpi_sum,0,           !--Perform FFT
     2                       mpi_comm_world,ier)                       !
         endif                                                         !
         if(myid.eq.0) then                                            !
             if(numprocs.gt.1) then                                    !
                  A1FFT=FFT1_work                                      !
                  A2FFT=FFT2_work                                      !
             endif                                                     !
             call Power_FFT(nFFT)                                      !
         endif                                                         !
         if(numprocs.gt.1) deallocate(FFT1_work,FFT2_work)             !
         if(myid.eq.0) write(6,*) 'FFT Complete'                       !
      endif  !---------------------------------------------------------!
      if((ndiags.ge.1).and.(slippage.eq.'on')) then  !-----------------!
         if(myid.eq.0) write(6,*) 'Output Power vs time Files'         !
         if(numprocs.gt.1) then                                        !
            allocate(P_work(nslices,nharms,ndiags),                    !
     1                  P00_work(nslices,nharms,ndiags))               !
            call mpi_reduce(P_slice,P_work,nslices*nharms*ndiags,      !
     1                      mpi_double_precision,mpi_sum,0,            !
     2                      mpi_comm_world,ier)                        !--Output
            call mpi_reduce(P00_slice,P00_work,                        !--Power vs time
     1                    nslices*nharms*ndiags,mpi_double_precision,  !--Files
     2                    mpi_sum,0,mpi_comm_world,ier)                !
            P_slice=P_work                                             !
            P00_slice=P00_work                                         !
            deallocate(P_work,P00_work)                                !
         endif                                                         !
         if(myid.eq.0) call Slip_Out                                   !
         if(myid.eq.0) write(6,*) 'Output Complete'                    !
      endif  !---------------------------------------------------------!
      if(beam_diags.eq.'on') then  !-----------------------------------!
         if(myid.eq.0) write(6,*) 'Output Beam Evolution'              !
         call Beam_Evol(itermax-1,ns_frst,ns_last)                                     !--Output Beam
         if(myid.eq.0) write(6,*) 'Output Complete'                    !--Evolution
      endif  !---------------------------------------------------------!
      if(SpentBeam.eq.'on') then  !------------------------------------!
         if(myid.eq.0) write(6,*) 'Output Spent Beam'                  !
         if(numprocs.gt.1) then                                        !
            allocate(sumw_wk(nslices),P_spent_wk(n_spent,nslices))     !
            sumw_wk=zero                                               !
            P_Spent_wk=zero                                            !
            call mpi_reduce(sumw,sumw_wk,nslices,mpi_double_precision, !
     1                      mpi_sum,0,mpi_comm_world,ier)              !
            call mpi_reduce(P_spent,P_spent_wk,n_spent*nslices,        !--Output Spent Beam
     1                      mpi_double_precision,mpi_sum,0,            !--Distribution
     2                      mpi_comm_world,ier)                        !
            if(myid.eq.0) then                                         !
               sumw=sumw_wk                                            !
               P_spent=P_spent_wk                                      !
            endif                                                      !
         endif                                                         !
         if(myid.eq.0) call Spent_Beam(beamloss)                       !
         if(numprocs.gt.1) deallocate(sumw_wk,P_spent_wk)              !
         if(myid.eq.0) write(6,*) 'Output Complete'                    !
      endif  !---------------------------------------------------------!
c
      if(S2E.eq.'on') then
            if(myid.eq.0) write(6,*) 'Output S2E Spent Beam'
            if(numprocs.gt.1) then
               allocate(ns2e_w(2,numprocs))
               ns2e_w=0
               call mpi_reduce(ns2e,ns2e_w,2*numprocs,mpi_integer,
     1                         mpi_sum,0,mpi_comm_world,ier)
               ns2e=ns2e_w
               deallocate(ns2e_w)
            endif
c
            if(numprocs.gt.1) then
               allocate(xout_w(nparts_out+1,numprocs))              
               xout_w=zero
               call mpi_reduce(x_out,xout_w,(nparts_out+1)*numprocs,
     1               mpi_double_precision,mpi_sum,0,mpi_comm_world,ier)
               x_out=xout_w
               deallocate(xout_w)
             endif
             if(myid.eq.0) then
               allocate(xfin(nparts_out))
               if(S2E_method.eq.'random') then
                  kp=0
                  do np=1,numprocs
                     do k=1,ns2e(1,np)
                        kp=kp+1
                        xfin(kp)=x_out(k,np)
                     end do
                  end do
               endif
               if((S2E_method.eq.'Waterbag').or.
     1                 (S2E_method.eq.'Gaussian')) then
                  do k=1,nparts_out
                      xfin(k)=x_out(k,1)                   
                  end do
               endif
            endif
            deallocate(x_out)
            if(numprocs.gt.1) then
               allocate(yout_w(nparts_out+1,numprocs))
               yout_w=zero
               call mpi_reduce(y_out,yout_w,(nparts_out+1)*numprocs,
     1               mpi_double_precision,mpi_sum,0,mpi_comm_world,ier)
               y_out=yout_w
               deallocate(yout_w)
            endif
            if(myid.eq.0) then
               allocate(yfin(nparts_out))
               if(S2E_method.eq.'random') then
                  kp=0
                  do np=1,numprocs
                     do k=1,ns2e(1,np)
                        kp=kp+1
                        yfin(kp)=y_out(k,np)
                     end do
                  end do
               endif
               if((S2E_method.eq.'Waterbag').or.
     1                 (S2E_method.eq.'Gaussian')) then
                  do k=1,nparts_out
                      yfin(k)=y_out(k,1)                  
                  end do
               endif
            endif
            deallocate(y_out)
            if(numprocs.gt.1) then
               allocate(pxout_w(nparts_out+1,numprocs))
               pxout_w=zero
               call mpi_reduce(px_out,pxout_w,(nparts_out+1)*numprocs,
     2              mpi_double_precision,mpi_sum,0, mpi_comm_world,ier)
               px_out=pxout_w
               deallocate(pxout_w)
               endif
               if(myid.eq.0) then
               allocate(pxfin(nparts_out))
               if(S2E_method.eq.'random') then
                  kp=0
                  do np=1,numprocs
                     do k=1,ns2e(1,np)
                        kp=kp+1
                        pxfin(kp)=px_out(k,np)
                     end do
                  end do
               endif
               if((S2E_method.eq.'Waterbag').or.
     1                 (S2E_method.eq.'Gaussian')) then
                  do k=1,nparts_out
                      pxfin(k)=px_out(k,1)                 
                  end do
               endif
            endif
            deallocate(px_out)
c
            if(numprocs.gt.1) then
               allocate(pyout_w(nparts_out+1,numprocs))
               pyout_w=zero
               call mpi_reduce(py_out,pyout_w,(nparts_out+1)*numprocs,
     2               mpi_double_precision,mpi_sum,0,mpi_comm_world,ier)
               py_out=pyout_w
               deallocate(pyout_w)
               endif
               if(myid.eq.0) then
               allocate(pyfin(nparts_out))
               if(S2E_method.eq.'random') then
                  kp=0
                  do np=1,numprocs
                     do k=1,ns2e(1,np)
                        kp=kp+1
                        pyfin(kp)=py_out(k,np)
                     end do
                  end do
               endif
               if((S2E_method.eq.'Waterbag').or.
     1                 (S2E_method.eq.'Gaussian')) then
                  do k=1,nparts_out
                      pyfin(k)=py_out(k,1)                     
                  end do
               endif
            endif
            deallocate(py_out)
            if(numprocs.gt.1) then
               allocate(pzout_w(nparts_out+1,numprocs))
               pzout_w=zero 
               call mpi_reduce(pz_out,pzout_w,(nparts_out+1)*numprocs,
     2               mpi_double_precision,mpi_sum,0,mpi_comm_world,ier)   
               pz_out=pzout_w       
               deallocate(pzout_w)    
            endif     
            if(myid.eq.0) then
               allocate(pzfin(nparts_out))
               if(S2E_method.eq.'random') then
                  kp=0
                  do np=1,numprocs
                     do k=1,ns2e(1,np)
                        kp=kp+1
                        pzfin(kp)=pz_out(k,np)
                     end do
                  end do
               endif
               if((S2E_method.eq.'Waterbag').or.
     1                 (S2E_method.eq.'Gaussian')) then
                  kp=0
                  do np=1,numprocs
                     do k=1,ns2e(1,np)
                        kp=kp+1
                        pzfin(kp)=pz_out(k,np)
                     end do
                  end do
               endif
            endif
            deallocate(pz_out)
            
            if(numprocs.gt.1) then
               allocate(psiout_w(nparts_out+1,numprocs))
               
               psiout_w=zero
              call mpi_reduce(psi_out,psiout_w,(nparts_out+1)*numprocs,
     2               mpi_double_precision,mpi_sum,0,mpi_comm_world,ier)
               psi_out=psiout_w
               deallocate(psiout_w)
            endif
            if(myid.eq.0) then
               allocate(psifin(nparts_out))
               if(S2E_method.eq.'random') then
                  kp=0
                  do np=1,numprocs
                     do k=1,ns2e(1,np)
                        kp=kp+1
                        psifin(kp)=psi_out(k,np)
                     end do
                  end do
               endif
               if((S2E_method.eq.'Waterbag').or.
     1                 (S2E_method.eq.'Gaussian')) then
                  kp=0
                  do np=1,numprocs
                     do k=1,ns2e(1,np)
                        kp=kp+1
                        psifin(kp)=psi_out(k,np)
                     end do
                  end do
               endif
            endif
            deallocate(psi_out)
c
            if(myid.eq.0) then
               gam_avg=zero !----------------------------------------!
               xx=zero                                               !
               yy=zero                                               !
               xpxp=zero                                             !
               ypyp=zero                                             !
               xxp=zero                                              !
               yyp=zero                                              !
               do k=1,nparts_out                                     !
                  gam_avg=gam_avg+sqrt(one+pxfin(k)**2+pyfin(k)**2   !
     1                                    +pzfin(k)**2)              !
                  xx=xx+(xfin(k)-x_avg)**2                           !
                  yy=yy+(yfin(k)-y_avg)**2                           !
                  xpxp=xpxp+((pxfin(k)-px_avg)/pzfin(k))**2          !--- Calculate S2E
                  ypyp=ypyp+((pyfin(k)-py_avg)/pzfin(k))**2          !--- emittances
                  xxp=xxp+(xfin(k)-x_avg)*(pxfin(k)-px_avg)/pzfin(k) !
                  yyp=yyp+(yfin(k)-y_avg)*(pyfin(k)-py_avg)/pzfin(k) !
               end do                                                !
               gam_avg=gam_avg/dble(nparts_out)                      !
               xx=xx/dble(nparts_out)                                !
               yy=yy/dble(nparts_out)                                !
               xpxp=xpxp/dble(nparts_out)                            !
               ypyp=ypyp/dble(nparts_out)                            !
               xxp=xxp/dble(nparts_out)                              !
               yyp=yyp/dble(nparts_out)                              !
               eps_x=1.d6*gam_avg*sqrt(xx*xpxp-xxp**2)               !
               eps_y=1.d6*gam_avg*sqrt(yy*ypyp-yyp**2) !-------------!
               open(unit=21,file='S2E_Beam_Out.txt',form='formatted',
     1              status='replace')
               write(21,9993) S2E_method,nparts_out,eps_x,eps_y
 9993          format(30x,'Spent Beam',/,27x,'S2E_method=',a11,/,
     1                25x,i8,' particles',/,
     2                20x,'x emittance=',1pe12.5,' microns',/,
     3                20x,'y emittance=',e12.5,' microns',/)
               write(21,9992)
 9992          format(5x,'x(m)',7x,"x'(rad)",6x,'y(m)',7x,"y'(rad)",4x,
     1                'phase(m)',5x,'dpz/pz')
               do npt=1,nparts_out
                  write(21,9991) xfin(npt),pxfin(npt)/pzfin(npt),
     1                           yfin(npt),pyfin(npt)/pzfin(npt),
     2                           (psifin(npt)-z_avg),
     3                           (pzfin(npt)-pz_avg)/pz_avg
 9991             format(1x,1pe11.4,5(1x,e11.4))
               end do
               close(unit=21)
            endif
            if(myid.eq.0) write(6,*) 'Output Complete'
      endif
      if((ndiags.ge.1).and.(Msqr.eq.'on')) call M_Squared
      if((slippage.ne.'on').and.(ndiags.ge.1)
     1                              .and.(nharms.gt.1)) call Spec_Out
c
      if(beamloss.eq.'total') then
          write(6,*)' Total Beam Loss'
          write(6,*)' TERMINATE MINERVA'
          call mpi_finalize(ierr)
          stop
      endif
c
      if(OPC.eq.'on') then ! read optics namelist and distribute
      	MercuryDFLfile='MercuryInput.dfl'
        MinervaDFLfile='OpticsInput.dfl'
    	  if(myid .eq. 0) read(5,optics)
        if(numprocs.gt.1) call optics_bcast
      endif
      if((OPC.eq.'on').and.(numprocs.gt.1).and.(MpiIO.eq.'no')) then
         allocate(a1_opc(nsize+1,nslices),a2_opc(nsize,nslices),
     1            asde1_opc(nsize,nslices),asde2_opc(nsize,nslices))
         call mpi_reduce(a1,a1_opc,(nsize+1)*nslices,
     1                   mpi_double_precision,mpi_sum,0,
     2                   mpi_comm_world,ier)
         call mpi_reduce(a2,a2_opc,nsize*nslices,
     1                   mpi_double_precision,mpi_sum,0,
     2                   mpi_comm_world,ier)
         call mpi_reduce(asde1,asde1_opc,nslices*nsize,
     1                   mpi_double_precision,mpi_sum,0,
     2                   mpi_comm_world,ier)
         call mpi_reduce(asde2,asde2_opc,nslices*nsize,
     1                   mpi_double_precision,mpi_sum,0,
     2                   mpi_comm_world,ier)
         a1=a1_opc
         a2=a2_opc
         asde1=asde1_opc
         asde2=asde2_opc
      endif
      if(OPC.eq.'on') then ! each core calls these routines
      	if (myid.eq.0) write(6,*) 'Configuring for output to OPC'
        call setupOPC(waist,wavelnth,zwaist,zmax)
        if (myid.eq.0) write(6,*) 'Calculating field for output to OPC'
        call fields
      endif
c
      if((OPC.eq.'on').and.(myid.eq.0)) close(unit=IOPC)
      if(nwrite.gt.0) close(unit=17)
      if(beam_diags.eq.'on') close(unit=18)
      if(enrgycon.ne.'off') close(unit=19)
      if(tprofile.eq.'DIRECT') close(unit=28)
      if(Power_vs_z.eq.'on') then
         do ns=ns_begin,ns_end
            close(unit=30+ns)
         end do
      endif
      if(numprocs.gt.1) stoptime=mpi_wtime()
      if((myid.eq.0).and.(numprocs.gt.1)) then
         print *,'Run time was ',stoptime-starttime,' seconds'
      endif
      call mpi_finalize(ierr)
      stop
      end
c**********************************************************************
      subroutine init(gamma,vz0,rmax,rmin,xb,yb,pz0,iprofile,dgam,
     1                nbeams,prebunch,psiwidth,beam_diags,Shot_Noise,
     2                noise_seed,B_ratio,current,trans_cor,maxh,dump,
     3                restart,zbegin,Gauss_Quad,dinput_file,
     4                nparts_g,ns_frst,ns_last,epsx,epsy,twissalphx,
     5                twissalphy,pbeam,twissbetax,twissbetay)
      use waveguides
      use numbers
      use particles
      use wiggler
      use modes
      use modes_init
      use harmonics
      use gauss_weights
      use arrays_init
      use multi_beams
      use noise_array
      use storage
      use diagnstc
      use PARMELA
      use direct_import
      use distfile_import
      use mpi_parameters
      use mpi
      implicit double precision(a-h,o-z)
      character*3 dump,restart
      character*12 tprofile,teprofile
      character*17 dinput_file
      character*8 prebunch,slippage,beam_diags,Shot_Noise,trans_cor,
     1            Gauss_Quad
      character*29 file1
      character*57 filename5
c     integer(kind=MPI_ADDRESS_KIND):: IO_recsize,lb
c     integer(kind=MPI_OFFSET_KIND):: ioffset
c     integer                      :: istatus(MPI_STATUS_SIZE)
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/spread/dp,n1,n2,npsi,nphi,ndv,npx,npy,ngam
      common/self/rb2,xavgs,yavgs,rbavg2,sfact,rb0avg2,iself
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      allocatable phis(:),temp_mpi(:)
      allocatable gamd(:),psid(:),xd(:),yd(:),pxd(:),pyd(:),pzd(:)
c---------------------------------------------------------------------!
      if(restart.eq.'on') then  !-------------------------------------!
         do ns=ns_begin,ns_end                                        !
            write(file1,601) ns                                       !
  601       format('Data_Dump_slice=',i5.5,'.dat')                    !
            open(unit=24,file=file1,form='unformatted',status='old')  !
            read(24) ipart(ns)                                        !
            if(ipart(ns).gt.0) then                                   !
               nss=ns-ns_begin+1                                      !
c              sumt=zero                                              !
               do k=1,ipart(ns)                                       !
                  read(24) x(k,nss),y(k,nss),psi(k,nss),px(k,nss),    !
     1                    py(k,nss),pz(k,nss),pz(k,nss),weights(k,nss)!
                  sumw(ns)=sumw(ns)+weights(npart,nss)                !
                  gamma0(k,nss)=sqrt(one+px(k,nss)**2+py(k,nss)**2    !
     1                                  +pz(k,nss)**2)                !
               end do                                                 !--Load Particles
               Rbrms(ns)=zero                                         !--and Fields
               xavg(ns)=zero                                          !--for Restart
               yavg(ns)=zero                                          !
               do k=1,ipart(ns)                                       !
                  xavg(ns)=xavg(ns)+weights(k,nss)*x(k,nss)           !
                  yavg(ns)=yavg(ns)+weights(k,nss)*y(k,nss)           !
               end do                                                 !
               xavg(ns)=xavg(ns)/sumw(ns)                             !
               yavg(ns)=yavg(ns)/sumw(ns)                             !
               do k=1,ipart(ns)                                       !
                  rsq=(x(k,nss)-xavg(ns))**2+(y(k,nss)-yavg(ns))**2   !
                  Rbrms(ns)=Rbrms(ns)+weights(k,nss)*rsq              !
               end do                                                 !
               Rbrms(ns)=sqrt(Rbrms(ns)/sumw(ns))/akw(1)              !
            endif                                                     !
            read(24) zbegin  !----------------------------------------!
            do nh=1,nharms                                            !
               read(24) asde1(nh,ns),asde2(nh,ns)                     !
               do nm=nh1(nh,ns),nh2(nh,ns)                            !
                  read(24) a1(nm,ns),a2(nm,ns),aki1(nm,ns),aki2(nm,ns)!--Load Fields
               end do                                                 !--for Restart
            end do                                                    !
            close(unit=24,status='delete')                            !
         end do                                                       !
         return                                                       !
      endif  !--------------------------------------------------------!
c
      if(iprofile.le.5) then
      nwmax=max(ngam,n1,n2,ndv,nphi,npsi)+10  !-----------------------!
      allocate(xw(nwmax,nwmax),w(nwmax,nwmax))                        !
      allocate(wdv(nwmax),xdv(nwmax),wphi(nwmax),xphi(nwmax),         !
     1         wpsi(nwmax),xpsi(nwmax))                               !
      if(npsi.gt.1) then                                              !
        if(prebunch.ne.'on') then                                     !
            if(npsi.eq.8*(npsi/8)) call gauleg(8)                     !
            if(npsi.eq.10*(npsi/10)) call gauleg(10)                  !
            if((npsi.ne.8*(npsi/8))                                   !
     1               .and.(npsi.ne.10*(npsi/10))) call gauleg(npsi)   !
        endif                                                         !
        if(prebunch.eq.'on') then                                     !--Set up
           if(B_ratio.eq.one) call gauleg(npsi)                       !--Gaussian
           if(B_ratio.lt.one) then                                    !--weights
              if(npsi.eq.8*(npsi/8)) call gauleg(8)                   !
              if(npsi.eq.10*(npsi/10)) call gauleg(10)                !
              if((npsi.ne.8*(npsi/8))                                 !
     1               .and.(npsi.ne.10*(npsi/10))) call gauleg(npsi)   !
           endif                                                      !
        endif                                                         !
      endif                                                           !
      if(ngam.gt.1) call gauleg(ngam)                                 !
      call gauleg(n1)                                                 !
      if(n2.ne.n1) call gauleg(n2)                                    !
      if((ndv.gt.1).and.((ndv.ne.n1).or.(ndv.ne.n2))) call gauleg(ndv)!
      if((nphi.gt.1).and.((nphi.ne.n1).or.                            !
     1           (nphi.ne.n2).or.(nphi.ne.ndv))) call gauleg(nphi) !--!
c
c------- handle # of electrons in entry phase psi if npsi > 10
c
      if(prebunch.ne.'on') then
          nnpsi=npsi      
          if(npsi.eq.8*(npsi/8)) nnpsi=8
          if(npsi.eq.10*(npsi/10)) nnpsi=10      
          nsegs=npsi/nnpsi
          segs=dble(nsegs)
          ncount=0
          do i=1,nsegs
              do j=1,nnpsi
                 ncount=ncount+1
                 wpsi(ncount)=w(nnpsi,j)/segs
                 xpsi(ncount)=(xw(nnpsi,j)+dble(2*i-1))/segs-one
              end do    
          end do
      endif
      if(prebunch.eq.'on') then
         if(B_ratio.eq.one) then
            do i=1,npsi
               xpsi(i)=half*psiwidth*(xw(npsi,i)+one)/pi
               wpsi(i)=two*w(npsi,i)*sin(pi*pi*xpsi(i)/psiwidth)**2 
            end do
         endif
         if(B_ratio.lt.one) then
            nnpsi=npsi      
            if(npsi.eq.8*(npsi/8)) nnpsi=8
            if(npsi.eq.10*(npsi/10)) nnpsi=10      
            nsegs=npsi/nnpsi
            segs=dble(nsegs)
            ncount=0
            do i=1,nsegs
                do j=1,nnpsi
                   ncount=ncount+1
                   xpsi(ncount)=(xw(nnpsi,j)+dble(2*i-1))/segs
                   distr=(one-B_ratio)/twopi
                   if(pi*xpsi(ncount).le.psiwidth) then
                      distr=distr+two*B_ratio
     1                    *sin(pi*pi*xpsi(ncount)/psiwidth)**2/psiwidth
                   endif
                   wpsi(ncount)=twopi*distr*w(nnpsi,j)/segs
                end do    
            end do            
         endif
      endif
      endif  !--- if(iprofile.le.5)
c
      if(iprofile.le.3) then  !----------------------------------------!
        if(abs(dp).le.deci) then                                       !
            rnorm=one                                                  !
            ndv=1                                                      !
            nphi=1                                                     !
            wphi(1)=one                                                !
            xphi(1)=zero                                               !
            wdv(1)=one                                                 !
            xdv(1)=pz0                                                 !
            edenom=one                                                 !
          else                                                         !
            do iphi=1,nphi                                             !
               wphi(iphi)=w(nphi,iphi)                                 !
               xphi(iphi)=xw(nphi,iphi)                                !
            end do                                                     !
            do idv=1,ndv                                               !
               wdv(idv)=thrhalf*dp*w(ndv,idv)                          !
               xdv(idv)=half*(three*dp*(xw(ndv,idv)-one)+two*pz0)      !
            end do                                                     !
            s1=zero                                                    !
            s2=zero                                                    !
            do i=1,ndv                                                 !
               s2=s2+wdv(i)*xdv(i)*exp(-(xdv(i)-pz0)**2/dp**2)/pz0     !
               s1=s1+wdv(i)*exp(-(xdv(i)-pz0)**2/dp**2)                !
            end do                                                     !
            rnorm=one/(two*s1)                                         !
            xi(1,1)=xi(1,1)*sqrt(s1/s2)                                !
            edenom=dp**2                                               !
        endif                                                          !
        npart=0                                                        !
        pnorm=one                                                      !--Legacy
        rb0avg2=zero                                                   !--Distributions
        if(iprofile.eq.1) then                                         !
           sr=zero                                                     !
           do j=1,n1                                                   !
               r=half*((rmax-rmin)*xw(n1,j)+(rmax+rmin))               !
               sr=sr+w(n1,j)*r*(one-r**2/rmax**2)                      !
           end do                                                      !
           sr=half*rmax*sr                                             !
           pnorm=half*rb2/sr                                           !
        endif                                                          !
        sumw=zero                                                      !
        do i=1,npsi                                                    !
          if(iprop.eq.0) then                                          !
                psii=zero                                              !
                psiwght=two                                            !
             else                                                      !
                psii=pi*xpsi(i)                                        !
                psiwght=wpsi(i)                                        !
          endif                                                        !
        do j=1,n1                                                      !
        do k=1,n2                                                      !
        if(iprofile.le.1) then                                         !
             r=half*((rmax-rmin)*xw(n1,j)+(rmax+rmin))                 !
             theta=pi*(xw(n2,k)+one)                                   !
             xkw=r*cos(theta)                                          !
             ykw=r*sin(theta)                                          !
             rweight=r*(rmax-rmin)                                     !
             if(iprofile.eq.1) rweight=rweight*(one-r**2/rmax**2)      !
        endif                                                          !
        if(iprofile.eq.2) then                                         !
             xkw=half*xb*xw(n1,j)                                      !
             ykw=half*yb*xw(n2,k)                                      !
             rweight=xb*yb/twopi                                       !
        endif                                                          !
        if(iprofile.eq.3) then                                         !
             theta=pi*(xw(n1,j)+one)                                   !
             rm=half*xb*yb/sqrt(yb**2+(xb**2-yb**2)*sin(theta)**2)     !
             r=half*rm*(xw(n2,k)+one)                                  !
             xkw=r*cos(theta)                                          !
             ykw=r*sin(theta)                                          !
             rweight=r*rm                                              !
        endif                                                          !
        do m=1,nphi                                                    !
          phi=pi*(xphi(m)+one)                                         !
          do nn=1,ndv                                                  !
            pzi=xdv(nn)                                                !
            npart=npart+1                                              !
            facnew=xi(1,1)*xi(1,1)*rweight*pnorm*exp(-(pz0-pzi)**2     !
     1                                          /edenom)*rnorm*pzi/pz0 !
            weights(npart,1)=facnew*psiwght*w(n1,j)*w(n2,k)*wphi(m)    !
     1                                                         *wdv(nn)!
            sumw(1)=sumw(1)+weights(npart,1)                           !
            rb0avg2=rb0avg2+weights(npart,1)*r**2                      !
            x(npart,1)=xkw                                             !
            y(npart,1)=ykw                                             !
            psi(npart,1)=psii                                          !
            if((iself.eq.0).or.(iprofile.gt.1)) then                   !
              sfac=one                                                 !
            endif                                                      !
            if((iself.ne.0).and.(iprofile.le.1)) then                  !
              dgam=xi(1,1)*xi(1,1)*(r**2-rb2)/eight                    !
              sfac=sqrt(one+dgam*(dgam+two*gamma)/pz0**2)              !
            endif                                                      !
            pperp=sfac*sqrt(pz0**2-pzi**2)                             !
            px(npart,1)=pperp*cos(phi)                                 !
            py(npart,1)=pperp*sin(phi)                                 !
            pz(npart,1)=pzi*sfac                                       !
          end do                                                       !
        end do                                                         !
        end do
        end do
        end do
        rb0avg2=rb0avg2/sumw(ns)                                       !
      endif  !---------------------------------------------------------!
c
c------- Gaussian Beam Distribution for Gaussian Optical Modes
c
      if(iprofile.eq.4) then  !----------------------------------------!
       do ns=ns_begin,ns_end                                           !
          nss=ns-ns_begin+1                                            !
          npart=0                                                      !
          sumt=zero                                                    !
          do nb=1,nbeams                                               !
            dgamm=dgamG(nb,ns)*gammab(nb,ns)                           !
            do i=1,ngam                                                !
              if(ngam.gt.1) then                                       !
                 gi=three*dgamm*xw(ngam,i)+gammab(nb,ns)               !
                 gweight=three*dgamm*w(ngam,i)                         !
     1                         *exp(-((gi-gammab(nb,ns))/dgamm)**2/two)!
               else                                                    !
                 gweight=one                                           !
               endif                                                   !
              do j=1,n1                                                !
                if(Gauss_Quad.eq.'yes') then
                    theta=pi*(xw(n1,j)+one)                            !
                    wtheta=w(n1,j)
                  else
                    theta=twopi*dble(j-1)/dble(n1)
                    wtheta=twopi/dble(n1)
                endif
                do k=1,n2                                              !
                  rm=three*sigmax(nb,ns)*sigmay(nb,ns)                 !
     1               /sqrt(sigmay(nb,ns)**2+(sigmax(nb,ns)**2!
     2                                -sigmay(nb,ns)**2)*sin(theta)**2)!
                  r=half*rm*(xw(n2,k)+one)                             !
                  xkw=r*cos(theta)                                     !
                  ykw=r*sin(theta)                                     !
                  cweight=halfpi*rm*r*wtheta*w(n2,k)*                  !
     1                 exp(-(xkw/sigmax(nb,ns))**2/two)                !
     2                        *exp(-(ykw/sigmay(nb,ns))**2/two)        !
     3                             /(twopi*sigmax(nb,ns)*sigmay(nb,ns))!
                  do l=1,nphi                                          !
                    if(Gauss_Quad.eq.'yes') then
                        phi=pi*(xw(nphi,l)+one)                        !
                        wphil=w(nphi,l)
                      else
                        phi=twopi*dble(l-1)/dble(nphi)
                        wphil=twopi/dble(nphi)
                    endif
                    do m=1,ndv                                         !
                      pm=three*sigmapx(nb,ns)*sigmapy(nb,ns)           !
     1                   /sqrt(sigmapy(nb,ns)**2+(sigmapx(nb,ns)**2    !
     2                                 -sigmapy(nb,ns)**2)*sin(phi)**2)!
                      p=half*pm*(xw(ndv,m)+one)                        !
                      pxi=p*cos(phi)                                   !
                      pyi=p*sin(phi)                                   !
                      pweight=halfpi*pm*p*wphil*w(ndv,m)               !
     1                 *exp(-(pxi/sigmapx(nb,ns))**2/two)              !
     2                       *exp(-(pyi/sigmapy(nb,ns))**2/two)        !
     3                           /(twopi*sigmapx(nb,ns)*sigmapy(nb,ns))!
                      sumt=sumt+cweight*pweight*gweight                !
                    end do
                  end do
                end do
              end do
            end do
          end do
          gnorm=dble(nbeams)/sumt                                      !
          area=zero                                                    !
          do nb=1,nbeams                                               !
             area=area+sigmax(nb,ns)**2+sigmay(nb,ns)**2               !
          end do                                                       !
          sumw(ns)=zero                                                !
          do nb=1,nbeams                                               !
            do ii=1,ngam                                               !
              if(ngam.gt.1) then                                       !
                gi=three*dgamm*xw(ngam,ii)+gammab(nb,ns)               !
                p0sqr=gi**2-one                                        !
                gweight=three*dgamm*w(ngam,ii)                         !
     1                         *exp(-((gi-gammab(nb,ns))/dgamm)**2/two)!
               else                                                    !
                gi=gammab(nb,ns)                                       !
                p0sqr=gi**2-one                                        !
                gweight=one                                            !
              endif                                                    !
              do i=1,npsi                                              !
                if(iprop.eq.0) then                                    !
                     psii=zero                                         !
                     psiwght=one                                       !
                   else                                                !
                     psii=pi*xpsi(i)                                   !
                     psiwght=wpsi(i)/two                               !
                endif                                                  !
                do j=1,n1                                              !
                  if(Gauss_Quad.eq.'yes') then
                       theta=pi*(xw(n1,j)+one)                         !
                       wtheta=w(n1,j)
                     else
                       theta=twopi*dble(j-1)/dble(n1)
                       wtheta=twopi/dble(n1)
                  endif
                  do k=1,n2                                            !
                    rm=three*sigmax(nb,ns)*sigmay(nb,ns)               !
     1                  /sqrt(sigmay(nb,ns)**2+(sigmax(nb,ns)**2       !
     2                                -sigmay(nb,ns)**2)*sin(theta)**2)!
                    r=half*rm*(xw(n2,k)+one)                           !
                    xkw=r*cos(theta)                                   !
                    ykw=r*sin(theta)                                   !
                    cweight=halfpi*rm*r*wtheta*w(n2,k)*                !
     1                  exp(-(xkw/sigmax(nb,ns))**2/two)               !
     2                        *exp(-(ykw/sigmay(nb,ns))**2/two)        !
     3                             /(twopi*sigmax(nb,ns)*sigmay(nb,ns))!
                    do l=1,nphi                                        !
                      if(Gauss_Quad.eq.'yes') then
                         phi=pi*(xw(nphi,l)+one)                       !
                         wphil=w(nphi,l)
                       else
                         phi=twopi*dble(l-1)/dble(nphi)
                         wphil=twopi/dble(nphi)
                      endif
                      do m=1,ndv                                       !
                        pm=three*sigmapx(nb,ns)*sigmapy(nb,ns)         !
     1                    /sqrt(sigmapy(nb,ns)**2+(sigmapx(nb,ns)**2   !
     2                                 -sigmapy(nb,ns)**2)*sin(phi)**2)!
                        p=half*pm*(xw(ndv,m)+one)                      !
                        pxi=p*cos(phi)                                 !
                        pyi=p*sin(phi)                                 !
                        pweight=halfpi*pm*p*wphil*w(ndv,m)             !
     1                     *exp(-(pxi/sigmapx(nb,ns))**2/two)          !
     2                       *exp(-(pyi/sigmapy(nb,ns))**2/two)        !
     3                           /(twopi*sigmapx(nb,ns)*sigmapy(nb,ns))!
                        npart=npart+1                                  !
                        weights(npart,nss)=four*(xi(nb,ns)**2)*area
     1                                                           *gnorm!
     1                    *psiwght*cweight*pweight*gweight/dble(nbeams)!
                        sumw(ns)=sumw(ns)+weights(npart,nss)           !
                        x(npart,nss)=akw(1)*x0(nb,ns)                  !
     1                             +xkw*costhx(nb,ns)-pxi*sinthx(nb,ns)!
                        px(npart,nss)=px0(nb,ns)                       !
     1                             +xkw*sinthx(nb,ns)+pxi*costhx(nb,ns)!
                        y(npart,nss)=akw(1)*y0(nb,ns)                  !
     1                             +ykw*costhy(nb,ns)-pyi*sinthy(nb,ns)!
                        py(npart,nss)=py0(nb,ns)                       !
     1                             +ykw*sinthy(nb,ns)+pyi*costhy(nb,ns)!
                        psi(npart,nss)=psii                            !
                        pz(npart,nss)=sqrt(p0sqr-px(npart,nss)**2
     1                                               -py(npart,nss)**2)!
                      end do
                    end do
                  end do
                end do
              end do
            end do
          end do  !----------------------------------------------------!
       end do   ! end sum over nslices
      endif     ! endif for Gaussian Beam Distribution
c
      if(iprofile.eq.5) then  !----------------------------------------!
          if(ntype(1).eq.1) akbeta=betaw(1)/(two*gamma*vz0)            !
          if(ntype(1).gt.1) akbeta=betaw(1)/(sqrt(two)*gamma*vz0)      !
          akbeta2=akbeta**2                                            !
          akrb2=(akbeta*rmax)**2                                       !
          rmax2=rmax**2                                                !
          denom=zero                                                   !
          dgamm=dgam*gamma                                             !
          span=three*dgamm                                             !
          do i=1,ngam                                                  !
          if(ngam.gt.1) then                                           !
             gi=span*xw(ngam,i)+gamma                                  !
             p0sqr=gi**2-one                                           !
             gweight=span*w(ngam,i)*exp(-((gi-gamma)/dgamm)**2/two)    !
           else                                                        !
             p0sqr=pz0**2                                              !
             gweight=one                                               !
          endif                                                        !
          do j=1,n1                                                    !
            r2=half*rmax2*(xw(n1,j)+one)                               !
            temp=akbeta2*(rmax2-r2)                                    !
            Pmax2=temp*p0sqr/(one+temp)                                !
            do k=1,ndv                                                 !
              denom=denom+quarter*rmax2*Pmax2*w(n1,j)*w(ndv,k)*gweight !
            end do                                                     !
          end do                                                       !
          end do                                                       !
          gnorm=one/(pi*pi*denom)                                      !
          xifac=four*rmax2*(quarter*pi*xi(1,1))**2                     !
          npart=0                                                      !
          sumw=zero                                                    !
          do ii=1,ngam                                              !
            if(ngam.gt.1) then                                         !
              gi=span*xw(ngam,ii)+gamma                                !
              p0sqr=gi**2-one                                          !
              gweight=span*w(ngam,ii)*exp(-((gi-gamma)/dgamm)**2/two)  !
             else                                                      !
              gi=gamma                                                 !
              p0sqr=pz0**2                                             !
              gweight=one                                              !
            endif                                                      !
          do i=1,npsi                                               !
             if(iprop.eq.0) then                                       !--Waterbag
                 psii=zero                                             !--Distribution
                 psiwght=one                                           !
               else                                                    !
                 psii=pi*xpsi(i)                                       !
                 psiwght=wpsi(i)/two                                   !
             endif                                                     !
          do j=1,n1                                                    !
           r2=half*rmax2*(xw(n1,j)+one)                                !
           r=sqrt(r2)                                                  !
           temp=akbeta2*(rmax2-r2)                                     !
           Pmax2=temp*p0sqr/(one+temp)                                 !
           do k=1,n2                                                   !
             theta=pi*(xw(n2,k)+one)                                   !
             xkw=r*cos(theta)                                          !
             ykw=r*sin(theta)                                          !
             do m=1,nphi                                               !
               phi=pi*(xw(nphi,m)+one)                                 !
               do nn=1,ndv                                             !
                 pperp2=half*Pmax2*(xw(ndv,nn)+one)                    !
                 pperp=sqrt(pperp2)                                    !
                 npart=npart+1                                         !
                 x(npart,1)=xkw                                        !
                 y(npart,1)=ykw                                        !
                 psi(npart,1)=psii                                     !
                 px(npart,1)=pperp*cos(phi)                            !
                 py(npart,1)=pperp*sin(phi)                            !
                 pzi=sqrt(p0sqr-pperp2)                                !
                 pz(npart,1)=pzi                                       !
                 weights(npart,1)=xifac*gnorm*rmax2*Pmax2*pzi*gweight  !  
     1               *psiwght*w(n1,j)*w(n2,k)*w(nphi,m)*w(ndv,nn)      !
     2               /sqrt(p0sqr)                                      !
                 sumw(1)=sumw(1)+weights(npart,1)                      !
               end do                                                  !
             end do                                                    !
           end do                                                      !
         end do                                                        !
       end do
       end do
      endif  !---------------------------------------------------------!
c
c     if(tprofile.eq.'DIRECT') then  !---------------------------------!
c     	imode=MPI_MODE_RDONLY                                          !
c     	allocate(temp_mpi(7))                                          !
c     	call MPI_FILE_OPEN(MPI_COMM_SELF,'MINERVA_particles.in',       !
c    1                     imode,MPI_INFO_NULL,ifh,mpi_err)            !
c       ! setup MPI-IO to access file
c       call MPI_TYPE_Contiguous(7,MPI_DOUBLE_PRECISION,IO_filetype,   !
c    1                            mpi_err)                             !
c       call MPI_TYPE_COMMIT(IO_filetype,mpi_err)                      !
c       call MPI_TYPE_GET_EXTENT(IO_filetype,lb,IO_recsize,mpi_err)    !
c       ioffset=(2+nslices+sum(ipart(1:ns_begin-1)))*IO_recsize        !--DIRECT particle
c       call MPI_FILE_SET_VIEW(ifh,ioffset,MPI_DOUBLE_PRECISION,       !--Import
c    1                IO_filetype,'native',MPI_INFO_NULL,mpi_err)      !
c       ! end setup MPI-IO
c       ! start reading data
c       sumw=zero                                                      !
c       do ns=ns_begin,ns_end                                          !
c          do k=1,ipart(ns)                                            !
c             call MPI_FILE_READ(ifh,temp_mpi,7,MPI_DOUBLE_PRECISION,  !
c    1                           istatus,mpi_err)                      !
c             x(k,ns)=temp_mpi(1)                                      !
c             y(k,ns)=temp_mpi(2)                                      !
c             px(k,ns)=temp_mpi(3)                                     !
c             py(k,ns)=temp_mpi(4)                                     !
c             psi(k,ns)=temp_mpi(5)                                    !
c             pz(k,ns)=temp_mpi(6)                                     !
c             weights(k,ns)=temp_mpi(7)                                !
c             sumw(ns)=sumw(ns)+weights(k,ns)                          !
c          end do                                                      !
c       end do                                                         !
c       call MPI_FILE_CLOSE(ifh,mpi_err)                               !
c       deallocate(temp_mpi)                                           !
c     endif  !---------------------------------------------------------!
c
      if(tprofile.eq.'DISTFILE') then  !-------------------------------!
        if(myid.eq.0) write(6,*) 'Begin DISTFILE input'                !
        if(myid.eq.0) open(unit=1001,file=dinput_file,access='direct', !
     1             form='unformatted',status='unknown',recl=8*nparts_g)!
        sumw=zero                                                      !
        dgams=zero
        allocate(gamd(nparts_g),psid(nparts_g),xd(nparts_g),           !
     1           yd(nparts_g),pxd(nparts_g),pyd(nparts_g),             !
     2           pzd(nparts_g))                                        !
        do nsg=1,nslices_g                                             !
           ns=ns_frst+nsg-1                                            !
           j=6*(nsg-1)+1                                               !
           if(myid.eq.0) then                                          !
              read(1001,rec=j)   (gamd(i),i=1,nparts_g)                !
              read(1001,rec=j+1) (psid(i),i=1,nparts_g)                !
              read(1001,rec=j+2) (xd(i),i=1,nparts_g)                  !
              read(1001,rec=j+3) (yd(i),i=1,nparts_g)                  !
              read(1001,rec=j+4) (pxd(i),i=1,nparts_g)                 !
              read(1001,rec=j+5) (pyd(i),i=1,nparts_g)                 !
           endif                                                       !
           if(numprocs.gt.1) then                                      !
              call mpi_bcast(gamd,nparts_g,mpi_double_precision,0,     !
     1                                              mpi_comm_world,ier)!
              call mpi_bcast(psid,nparts_g,mpi_double_precision,0,     !
     1                                              mpi_comm_world,ier)!
              call mpi_bcast(xd,nparts_g,mpi_double_precision,0,       !
     1                                              mpi_comm_world,ier)!
              call mpi_bcast(yd,nparts_g,mpi_double_precision,0,       !
     1                                              mpi_comm_world,ier)!
              call mpi_bcast(pxd,nparts_g,mpi_double_precision,0,      !
     1                                              mpi_comm_world,ier)!
              call mpi_bcast(pyd,nparts_g,mpi_double_precision,0,      !
     1                                              mpi_comm_world,ier)!
           endif                                                       !
           if(ipart(ns).gt.0) then  !---Obtain SLICE Properties--------!
              gamd_avg=zero                                            !
              do k=1,nparts_g                                          !
                 pzd(k)=sqrt(gamd(k)**2-one-pxd(k)**2-pyd(k)**2)       !
                 gamd_avg=gamd_avg+gamd(k)                             !
                 x_avgs(nsg)=x_avgs(nsg)+xd(k)                         !
                 y_avgs(nsg)=y_avgs(nsg)+yd(k)                         !--PARTICLE
                 px_avgs(nsg)=px_avgs(nsg)+pxd(k)                      !--input from
                 py_avgs(nsg)=py_avgs(nsg)+pyd(k)                      !--DISTFILE
              end do                                                   !
              gamd_avg=gamd_avg/dble(nparts_g)                         !
              energies(nsg)=(gamd_avg-one)*emass                       !
              x_avgs(nsg)=hundred*x_avgs(nsg)/dble(nparts_g)           !
              y_avgs(nsg)=hundred*y_avgs(nsg)/dble(nparts_g)           !
              px_avgs(nsg)=px_avgs(nsg)/dble(nparts_g)                 !
              py_avgs(nsg)=py_avgs(nsg)/dble(nparts_g)                 !
              do k=1,nparts_g                                          !
                 dgams(nsg)=dgams(nsg)+(gamd(k)-gamd_avg)**2           !
                 x_maxs(nsg)=x_maxs(nsg)+(hundred*xd(k)-x_avgs(nsg))**2!
                 y_maxs(nsg)=y_maxs(nsg)+(hundred*yd(k)-y_avgs(nsg))**2!
                 sigma_pxs(nsg)=sigma_pxs(nsg)+(pxd(k)-px_avgs(nsg))**2!
                 sigma_pys(nsg)=sigma_pys(nsg)+(pyd(k)-py_avgs(nsg))**2!
              end do                                                   !
              dgams(nsg)=sqrt(dgams(nsg)/dble(nparts_g))/gamd_avg      !
              x_maxs(nsg)=sqrt(x_maxs(nsg)/dble(nparts_g))             !
              y_maxs(nsg)=sqrt(y_maxs(nsg)/dble(nparts_g))             !
              sigma_pxs(nsg)=sqrt(sigma_pxs(nsg)/dble(nparts_g))       !
              sigma_pys(nsg)=sqrt(sigma_pys(nsg)/dble(nparts_g))       !
              xx=zero                                                  !
              yy=zero                                                  !
              xpxp=zero                                                !
              ypyp=zero                                                !
              xxp=zero                                                 !
              yyp=zero                                                 !
              do k=1,nparts_g                                          !
                 xx=xx+(hundred*xd(k)-x_avgs(nsg))**2                  !
                 yy=yy+(hundred*yd(k)-y_avgs(nsg))**2                  !
                 xpxp=xpxp+((pxd(k)-px_avgs(nsg))/pzd(k))**2           !
                 ypyp=ypyp+((pyd(k)-py_avgs(nsg))/pzd(k))**2           !
                 xxp=xxp+(hundred*xd(k)-x_avgs(nsg))                   !
     1                                   *(pxd(k)-px_avgs(nsg))/pzd(k) !
                 yyp=yyp+(hundred*yd(k)-y_avgs(nsg))                   !
     1                                   *(pyd(k)-py_avgs(nsg))/pzd(k) !
              end do                                                   !
              xx=xx/dble(nparts_g)                                     !
              yy=yy/dble(nparts_g)                                     !
              xpxp=xpxp/dble(nparts_g)                                 !
              ypyp=ypyp/dble(nparts_g)                                 !
              xxp=xxp/dble(nparts_g)                                   !
              yyp=yyp/dble(nparts_g)                                   !
              epsx_rms=1.d4*sqrt(xx*xpxp-xxp**2)                       !
              epsy_rms=1.d4*sqrt(yy*ypyp-yyp**2)                       !
              betagamma=sqrt(gamma**2-one)                             !
              eps_xs(nsg)=betagamma*epsx_rms                           !
              eps_ys(nsg)=betagamma*epsy_rms                           !
              Twiss_alph_x(nsg)=-1.d4*xxp/epsx_rms                     !
              Twiss_alph_y(nsg)=-1.d4*yyp/epsy_rms                     !
              twissbetax_g(nsg)=hundredth*xx/sqrt(xx*xpxp-xxp**2)      !
              twissbetay_g(nsg)=hundredth*yy/sqrt(yy*ypyp-yyp**2)      !
           endif                                                       !
c------------------------Fill Particle Arrays--------------------------!                                 
           if((ns.ge.ns_begin).and.(ns.le.ns_end)                      !
     1                                     .and.(ipart(ns).gt.0)) then !
              nss=ns-ns_begin+1                                        !
              do k=1,nparts_g                                          !
                 x(k,nss)=twopi*hundred*xd(k)/zwi(1)                   !              
                 y(k,nss)=twopi*hundred*yd(k)/zwi(1)                   !
                 px(k,nss)=pxd(k)                                      !
                 py(k,nss)=pyd(k)                                      !
                 psi(k,nss)=psid(k)                                    !
                 pz(k,nss)=pzd(k)                                      !
                 weights(k,nss)=sixteen*current_g(2,nsg)               !
     1                                         /(Alfven*dble(nparts_g))!
                 sumw(ns)=sumw(ns)+weights(k,nss)                      !
              end do                                                   !
           endif                                                       !
        end do                                                         !
c----------------------------------------------------------------------!
        pbeam=zero                                                     !
        do nsg=1,nslices_g                                             !
           pbeam=pbeam+1.e6*energies(nsg)*current_g(2,nsg)             !
        end do                                                         !
c---------------------------------Obtain Global Properties-------------!
        sum_gam=zero                                                   !
        avg_x=zero                                                     !
        avg_y=zero                                                     !
        avg_px=zero                                                    !
        avg_py=zero                                                    !
        do ns=ns_begin,ns_end                                          !
           if(ipart(ns).gt.0) then                                     !
              sum_ns=zero                                              !
              avg_xns=zero                                             !
              avg_yns=zero                                             !
              avg_pxns=zero                                            !
              avg_pyns=zero                                            !
              nss=ns-ns_begin+1                                        !
              do k=1,nparts_g                                          !
                 gam_d=sqrt(one+px(k,nss)**2+py(k,nss)**2+pz(k,nss)**2)!
                 sum_ns=sum_ns+gam_d                                   !
                 avg_xns=avg_xns+x(k,nss)*zwi(1)/(twopi*hundred)       !
                 avg_yns=avg_yns+y(k,nss)*zwi(1)/(twopi*hundred)       !
                 avg_pxns=avg_pxns+px(k,nss)                           !
                 avg_pyns=avg_pyns+py(k,nss)                           !
              end do                                                   !
              sum_gam=sum_gam+sum_ns*current_g(2,ns-ns_frst-1)/sumI_g  !
              avg_x=avg_x+avg_xns*current_g(2,ns-ns_frst+1)/sumI_g     !
              avg_y=avg_y+avg_yns*current_g(2,ns-ns_frst+1)/sumI_g     !
              avg_px=avg_px+avg_pxns*current_g(2,ns-ns_frst+1)/sumI_g  !
              avg_py=avg_py+avg_pyns*current_g(2,ns-ns_frst+1)/sumI_g  !
           endif                                                       !
        end do                                                         !
        gam_tot=sum_gam/dble(nparts_g)                                 !
        x_tot=avg_x/dble(nparts_g)                                     !
        y_tot=avg_y/dble(nparts_g)                                     !
        px_tot=avg_px/dble(nparts_g)                                   !
        py_tot=avg_py/dble(nparts_g)                                   !
        gam_g=zero                                                     !
        x_avg=zero                                                     !
        y_avg=zero                                                     !
        px_avg=zero                                                    !
        py_avg=zero                                                    !
        if(numprocs.gt.1) then                                         !
              call mpi_reduce(gam_tot,gam_g,1,mpi_double_precision,    !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(gam_g,1,mpi_double_precision,0,           !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(x_tot,x_avg,1,mpi_double_precision,      !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(x_avg,1,mpi_double_precision,0,           !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(y_tot,y_avg,1,mpi_double_precision,      !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(y_avg,1,mpi_double_precision,0,           !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(px_tot,px_avg,1,mpi_double_precision,    !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(px_avg,1,mpi_double_precision,0,          !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(py_tot,py_avg,1,mpi_double_precision,    !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(py_avg,1,mpi_double_precision,0,          !
     1                                              mpi_comm_world,ier)!
           else                                                        !
              gam_g=gam_tot                                            !
              x_avg=x_tot                                              !
              y_avg=y_tot                                              !
              px_avg=px_tot                                            !
              py_avg=py_tot                                            !
        endif                                                          !
        energy=emass*(gam_g-one)                                       !
        sum_dgam=zero                                                  !
        rms_x=zero                                                     !
        rms_y=zero                                                     !
        rms_px=zero                                                    !
        rms_py=zero                                                    !
        xx_tot=zero                                                    !
        yy_tot=zero                                                    !
        xpxp_tot=zero                                                  !
        ypyp_tot=zero                                                  !
        xxp_tot=zero                                                   !
        yyp_tot=zero                                                   !
        do ns=ns_begin,ns_end                                          !
           if(ipart(ns).gt.0) then                                     !
              sum_ns=zero                                              !
              rms_xns=zero                                             !
              rms_yns=zero                                             !
              rms_pxns=zero                                            !
              rms_pyns=zero                                            !
              xx_totns=zero                                            !
              yy_totns=zero                                            !
              xpxp_totns=zero                                          !
              ypyp_totns=zero                                          !
              xxp_totns=zero                                           !
              yyp_totns=zero                                           !
              nss=ns-ns_begin+1                                        !
              do k=1,nparts_g                                          !
                 gam_d=sqrt(one+px(k,nss)**2+py(k,nss)**2+pz(k,nss)**2)!
                 sum_ns=sum_ns+(gam_d-gam_g)**2/gam_g**2               !
                 rms_xns=rms_xns+(x(k,nss)*zwi(1)/(twopi*hundred)      !
     1                                                      -x_avg)**2 !
                 rms_yns=rms_yns+(y(k,nss)*zwi(1)/(twopi*hundred)      !
     1                                                      -y_avg)**2 !
                 rms_pxns=rms_pxns+(px(k,nss)-px_avg)**2               !
                 rms_pyns=rms_pyns+(py(k,nss)-py_avg)**2               !
                 xx_totns=xx_totns+(x(k,nss)-x_avg)**2                 !
                 yy_totns=yy_totns+(y(k,nss)-y_avg)**2                 !
                 xpxp_totns=xpxp_totns                                 !
     1                               +((px(k,nss)-px_avg)/pz(k,nss))**2!
                 ypyp_totns=ypyp_totns                                 !
     1                               +((py(k,nss)-py_avg)/pz(k,nss))**2!
                 xxp_totns=xxp_totns+(x(k,nss)-x_avg)                  !
     1                                    *(px(k,nss)-px_avg)/pz(k,nss)!
                 yyp_totns=yyp_totns+(y(k,nss)-y_avg)                  !
     1                                    *(py(k,nss)-py_avg)/pz(k,nss)!
              end do                                                   !
              sum_dgam=sum_dgam+sum_ns*current_g(2,ns-ns_frst+1)/sumI_g!
              rms_x=rms_x+rms_xns*current_g(2,ns-ns_frst+1)/sumI_g     !
              rms_y=rms_y+rms_yns*current_g(2,ns-ns_frst+1)/sumI_g     !
              rms_px=rms_px+rms_pxns*current_g(2,ns-ns_frst+1)/sumI_g  !
              rms_py=rms_py+rms_pyns*current_g(2,ns-ns_frst+1)/sumI_g  !
              xx_tot=xx_tot+xx_totns*current_g(2,ns-ns_frst+1)/sumI_g  !
              yy_tot=yy_tot+yy_totns*current_g(2,ns-ns_frst+1)/sumI_g  !
              xpxp_tot=xpxp_tot                                        !
     1                     +xpxp_totns*current_g(2,ns-ns_frst+1)/sumI_g!
              ypyp_tot=ypyp_tot                                        !
     1                     +ypyp_totns*current_g(2,ns-ns_frst+1)/sumI_g!
              xxp_tot=xxp_tot                                          !
     1                      +xxp_totns*current_g(2,ns-ns_frst+1)/sumI_g!
              yyp_tot=yyp_tot                                          !
     1                      +yyp_totns*current_g(2,ns-ns_frst+1)/sumI_g!
           endif                                                       !
        end do                                                         !
        dgam_tot=zero                                                  !
        xmax_D=zero                                                    !
        ymax_D=zero                                                    !
        sigmapx_D=zero                                                 !
        sigmapy_D=zero                                                 !
        xx=zero                                                        !
        yy=zero                                                        !
        xpxp=zero                                                      !
        ypyp=zero                                                      !
        xxp=zero                                                       !
        yyp=zero                                                       !
        if(numprocs.gt.1) then                                         !
              call mpi_reduce(sum_dgam,dgam_tot,1,mpi_double_precision,!
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(dgam_tot,1,mpi_double_precision,0,        !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(rms_x,xmax_D,1,mpi_double_precision,     !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(xmax_D,1,mpi_double_precision,0,          !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(rms_y,ymax_D,1,mpi_double_precision,     !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(ymax_D,1,mpi_double_precision,0,          !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(rms_px,sigmapx_D,1,mpi_double_precision, !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(sigmapx_D,1,mpi_double_precision,0,       !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(rms_py,sigmapy_D,1,mpi_double_precision, !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(sigmapy_D,1,mpi_double_precision,0,       !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(xx_tot,xx,1,mpi_double_precision,        !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(xx,1,mpi_double_precision,0,              !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(yy_tot,yy,1,mpi_double_precision,        !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(yy,1,mpi_double_precision,0,              !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(xpxp_tot,xpxp,1,mpi_double_precision,    !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(xpxp,1,mpi_double_precision,0,            !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(ypyp_tot,ypyp,1,mpi_double_precision,    !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(ypyp,1,mpi_double_precision,0,            !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(xxp_tot,xxp,1,mpi_double_precision,      !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(xxp,1,mpi_double_precision,0,             !
     1                                              mpi_comm_world,ier)!
              call mpi_reduce(yyp_tot,yyp,1,mpi_double_precision,      !
     1                                    mpi_sum,0,mpi_comm_world,ier)!
              call mpi_bcast(yyp,1,mpi_double_precision,0,             !
     1                                              mpi_comm_world,ier)!
           else                                                        !
              dgam_tot=sum_dgam                                        !
              xmax_D=rms_x                                             !
              ymax_D=rms_y                                             !
              sigmapx_D=px_rms                                         !
              sigmapy_D=py_rms                                         !
              xx=xx_tot                                                !
              yy=yy_tot                                                !
              xpxp=xpxp_tot                                            !
              ypyp=ypyp_tot                                            !
              xxp=xxp_tot                                              !
              yyp=yyp_tot                                              !
        endif                                                          !
        dgam=sqrt(dgam_tot/dble(nparts_g))                             !
        xmax_D=sqrt(xmax_D/dble(nparts_g))                             !
        ymax_D=sqrt(ymax_D/dble(nparts_g))                             !
        sigmapx_D=sqrt(sigmapx_D/dble(nparts_g))                       !
        sigmapy_D=sqrt(sigmapy_D/dble(nparts_g))                       !
        xx=xx/dble(nparts_g)                                           !
        yy=yy/dble(nparts_g)                                           !
        xpxp=xpxp/dble(nparts_g)                                       !
        ypyp=ypyp/dble(nparts_g)                                       !
        xxp=xxp/dble(nparts_g)                                         !
        yyp=yyp/dble(nparts_g)                                         !
        epsx_rms=1.d4*sqrt(xx*xpxp-xxp**2)                             !
        epsy_rms=1.d4*sqrt(yy*ypyp-yyp**2)                             !
        betagamma=sqrt(gam_g**2-one)                                   !
        epsx=betagamma*epsx_rms                                        !
        epsy=betagamma*epsy_rms                                        !
        twissbetax=hundredth*xx/sqrt(xx*xpxp-xxp**2)                   !
        twissbetay=hundredth*yy/sqrt(yy*ypyp-yyp**2)                   !
        twissalphx=-1.d4*xxp/epsx_rms                                  !
        twissalphy=-1.d4*yyp/epsy_rms                                  !
        if(myid.eq.0) write(6,*) '  End DISTFILE input'                !
        close(unit=1001)                                               !
      endif  !---------------------------------------------------------!
c
      do ns=ns_begin,ns_end
         nss=ns-ns_begin+1
         if(nss.gt.nrmdim) print *,"nss out of bounds 2841"
         Rbrms(ns)=zero !----------------------------------------------!
         xavg(ns)=zero                                                 !
         yavg(ns)=zero                                                 !
         if(sumw(ns).gt.zero) then                                     !
            do k=1,ipart(ns)                                           !
               xavg(ns)=xavg(ns)+weights(k,nss)*x(k,nss)               !
               yavg(ns)=yavg(ns)+weights(k,nss)*y(k,nss)               !--Calculate
            end do                                                     !--rms Beam
            xavg(ns)=xavg(ns)/sumw(ns)                                 !--Radius
            yavg(ns)=yavg(ns)/sumw(ns)                                 !
            do k=1,ipart(ns)                                           !
               rsq=(x(k,nss)-xavg(ns))**2+(y(k,nss)-yavg(ns))**2       !
               Rbrms(ns)=Rbrms(ns)+weights(k,nss)*rsq                  !
            end do                                                     !
            Rbrms(ns)=sqrt(Rbrms(ns)/sumw(ns))/akw(1)                  !
         endif  !------------------------------------------------------!
c
         do k=1,ipart(ns)  !-------------------------------------------!
            gamma0(k,nss)=sqrt(one+px(k,nss)**2+py(k,nss)**2           !--Calculate
     1                            +pz(k,nss)**2)                       !--gamma0
         end do  !-----------------------------------------------------!
c
         if((ndiags.ge.1).and.(beam_diags.eq.'on')  !------------------!
     1                                      .and.(ipart(ns).gt.0)) then!  
            nunit5=998+9*nslices+ns                                    !
            if(nslices.eq.1) write(filename5,300)                      !
  300       format('Beam_Diagnostics_z(m)= 0.000E+00.txt')             !
            if(nslices.gt.1) write(filename5,301) ns                   !
  301       format('Beam_Diagnostics_z(m)= 0.000E+00_slice=',i5.5,     !
     1             '.txt')                                             !
            open(unit=nunit5,file=filename5,form='formatted',          !
     1        status='replace')                                        !
            write(nunit5,302)                                          !
  302       format(29x,'BEAM DIAGNOSTIC OUTPUT')                       !
            if(nslices.gt.1) write(nunit5,303) ns                      !--Output Initial
  303       format(/,32x,'Slice No.:',i6)                              !--Phase Space
            write(nunit5,304) zero                                     !--if ndiags>1
  304       format(/,' z(m)=',1pe11.4,//,6x,'kwx',10x,'kwy',9x,'px/mc',!
     1             8x,'py/mc',8x,'psi',10x,'pz/mc',7x,'dpsidz')        !
            do k=1,ipart(ns)                                           !
              vz=pz(k,nss)/sqrt(one+px(k,nss)**2+py(k,nss)**2          !
     1                             +pz(k,nss)**2)                      !
              dpsidz=one+omega(1)-omega(1)/vz                          !
              write(nunit5,305) x(k,nss),y(k,nss),px(k,nss),py(k,nss), !
     1                      psi(k,nss),pz(k,nss),dpsidz                !
  305         format(7(1x,1pe12.5))                                    !
            end do                                                     !
            close(unit=nunit5)                                         !
         endif  !------------------------------------------------------!
      end do    ! end loop over nslices
c
      if(iprop.eq.0) return
      if((emmodes.eq.'hermite').or.(emmodes.eq.'laguerre')) then !-----!
          do ns=ns_begin,ns_end                                        !
             do nm=1,nmodes                                            !
                a1(nm,ns)=da(nm,ns)*cos(phase_a(nm,ns))                !
                a2(nm,ns)=da(nm,ns)*sin(phase_a(nm,ns))                !
                aki1(nm,ns)=zero                                       !--Initialize
                aki2(nm,ns)=zero                                       !--Gaussian
             end do                                                    !--Optical Modes
             do nh=1,nharms                                            !
               asde2(nh,ns)=-zwaste(nh1(nh,ns),ns)/z0(nh1(nh,ns),ns)   !
               asde1(nh,ns)=w0(nh1(nh,ns),ns)*sqrt(one+asde2(nh,ns)**2)!
             end do                                                    !
          end do                                                       !
      endif  !---------------------------------------------------------!
c
      if(Shot_Noise.eq.'on') then !------------------------------------!
         allocate (phis(nslices))                                      !
         phis(1)=pi*(-one+two*ran3(noise_seed))                        !
         do ns=2,nslices                                               !
            phis(ns)=pi*(-one+two*ran3(ns))                            !
         end do                                                        !
         if(ntype(1).ne.4) rmsK=betaw(1)/sqrt(two)                     !
         if(ntype(1).eq.4) rmsK=betaw(1)                               !
         wave_noise=hundredth*zwi(1)*(one+rmsK**2)/(two*gamma**2)      !
         if(trans_cor.eq.'on') then                                    !
            allocate (cfac(iparts,nrmdim))                             !
            call current_trans                                         !
         endif                                                         !
         do ns=ns_begin,ns_end                                         !
            nss=ns-ns_begin+1                                          !
            rN_e=beam_i(1,ns)*wave_noise/(echarge*clight*vz0) !--------!-> # of electrons/slice
            if(ntype(1).ne.4) then                                     !
                   aw=betaw(1)/sqrt(two)                               !
                   arg=aw**2/(two*(one+aw**2))                         !
                   call bsJ_n(0,arg,bj_0,bj_1)                         !
                   rJJ=aw*(bj_0-bj_1)                                  !
                   akbeta=two*gamma/betaw(1)                           !
            endif                                                      !
            if(ntype(1).eq.4) then                                     !--Calculate # of
                   aw=betaw(1)                                         !--correlated electrons
                   rJJ=aw                                              !
                   akbeta=sqrt(two)*gamma/betaw(1)                     !
            endif                                                      !
            sig=hundredth*sqrt(akw(1)*epsx*akbeta/gamma)               !
            rho_P=half*(current*(rJJ/sig)**2/Alfven)**(one/three)/gamma!
c           corLength=wave_noise/(four*pi*rho_P)                       !
            if(nslices.eq.1)rN_e=4.3d0*rN_e/(four*pi*sqrt(three)*rho_P)!
            if(nslices.gt.1) rN_e=dt_norm*rN_e !-----------------------!
c           dt_new=corLength*rN_e/(clight*dtpulse)
c           if(nslices.gt.1) rN_e= dt_new*rN_e !-----------------------!
            if(rN_e.gt.zero) then                                      !
                 dpsi=two/sqrt(rN_e)                                   !
              else                                                     !
                 dpsi=zero                                             !
            endif                                                      !
            phi=phis(ns)                                               !
            do nh=1,maxh                                               !
               Rnh=dble(nh)                                            !
               if(trans_cor.eq.'off') then                             !
                  do k=1,ipart(ns)                                     !
                     psi(k,nss)=psi(k,nss)                             !
     1                              +dpsi*sin(Rnh*(psi(k,nss)-phi))/Rnh!
                  end do                                               !
               endif                                                   !
                  if(trans_cor.eq.'on') then                           !
                     do k=1,ipart(ns)                                  !
                        psi(k,nss)=psi(k,nss)                          !
     1                  +cfac(k,nss)*dpsi*sin(Rnh*(psi(k,nss)-phi))/Rnh!
                     end do                                            !
                  endif                                                !
            end do                                                     !
         end do                                                        !
         if(trans_cor.eq.'on') deallocate (cfac)                       !
      endif  !---------------------------------------------------------!
      return
      end
c***********************************************************************
      subroutine current_trans
      use particles
      use multi_beams
      use noise_array
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,tiny=1.d-7)
      character*12 tprofile,teprofile
      character*8 slippage
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/spread/dp,n1,n2,npsi,nphi,ndv,npx,npy,ngam
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      dimension temp(n1,n2),temp2(n1,n2),r2(n1,n2)
c----------------------------------------------------------------------
c
c     This subroutine is used to adjust the phase shifts in setting
c     start-up from noise to account for the amount of charge at
c     each radial position. It is called when trans_cor = 'on'
c
c     N.B.: This algorithm does not work for multi-beam cases
c
c----------------------------------------------------------------------
      do ns=ns_begin,ns_end
         nss=ns-ns_begin+1
         do i=1,n1
            do j=1,n2
               temp(i,j)=zero
               temp2(i,j)=zero
            end do
         end do
c
         npart=0
         do m=1,ngam
           do n=1,npsi
              do i=1,n1
                 do j=1,n2
                    do k=1,nphi
                       do l=1,ndv
                         npart=npart+1
                         temp(i,j)=temp(i,j)+weights(npart,nss)
                         r2(i,j)=x(npart,nss)**2+y(npart,nss)**2
                       end do
                    end do
                 end do
              end do
           end do
         end do
         total=zero
         do i=1,n1
            do j=1,n2
               rtemp=r2(i,j)
               do i2=1,n1
                  do j2=1,n2
                     if(abs(r2(i2,j2)-rtemp).lt.tiny) then
                        temp2(i,j)=temp2(i,j)+temp(i2,j2)
                     endif
                  end do
               end do
               total=total+temp2(i,j)
            end do
         end do
c
         npart=0
         do m=1,ngam
           do n=1,npsi
              do i=1,n1
                 do j=1,n2
                    do k=1,nphi
                       do l=1,ndv
                         npart=npart+1
                         cfac(npart,nss)=sqrt(temp2(i,j)/total)
                       end do
                    end do
                 end do
              end do
           end do
         end do

      end do   ! end loop over nslices
      return
      end
c**********************************************************************
      subroutine ode(x0,dz,xmax,iecon,pin,pbeam,beami,nstep,
     1           itermax,beamloss,trans_mode,beam_diags,FFTchk,
     2           ns_frst,ns_last,Power_vs_z,SpentBeam,S2E,S2E_method,
     3           dither,nparts_out,nbin,wavelnth,x_avg,y_avg,z_avg,
     4           px_avg,py_avg,pz_avg,dump,restart,Wakefield,
     5           Fluence_only,Runge_Kutta,interpolation)
      use waveguides
      use numbers
      use particles
      use particles_ode
      use wiggler
      use wigg_errors
      use focus
      use modes
      use harmonics
      use FFT
      use Slice_Diags
      use Spec_Diags
      use M_sqr
      use RFaccel
      use magmap
      use bmapmod
      use storage
      use mpi_parameters
      use mpi
      use s2e_out
      use diagnstc
      implicit double precision(a-h,o-z)
      character*3 dump,restart,Wakefield
      character*8 RFfield,slippage,trans_mode,beam_diags,FFTchk,
     1            Power_vs_z,SpentBeam,S2E,Fluence_only
      character*9 Runge_Kutta
      character*10 interpolation
      character*11 drifttube,S2E_method
      character*12 tprofile,beamloss,teprofile
      character*29 file1
      character*57 file10
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
      common/self/rb2,xavgs,yavgs,rbavg2,sfact,rb0avg2,iself
      common/profile/akwwaist
      common/RFacc/RFfield,agkwrf,bgkwrf,hagkwrf,hbgkwrf,n1stpass,
     3             drifttube
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      common/Bmap/nbx_max,nby_max,nbz_max
      dimension a(4),b(4),c(4),ptot(nslices),deltapsi(nslices)
      allocatable qa1(:,:),qa2(:,:),qsde1(:,:),qsde2(:,:),dfa1(:,:),
     1            dfa2(:,:),dsde1(:,:),dsde2(:,:),akz(:,:),aki(:,:),
     2            pln(:,:),xpw(:),ypw(:),Poynting(:,:),Fluence(:,:,:),
     3            Fluence_work(:,:,:),zpos(:),zneg(:)
      allocatable Q_slice(:),t_slice(:),n_slice(:),
     1            x_min(:),x_max(:),y_min(:),y_max(:),F_perp(:),
     2            px_max(:),px_min(:),py_max(:),py_min(:),F_pz(:),
     3            pz_max(:),pz_min(:),ntotperp(:),ntotpz(:),
     4            nbin_perp(:),nbin_pz(:),diffx(:),diffy(:),
     5            diffpx(:),diffpy(:),diffpz(:),gmx_wk(:),gmn_wk(:)
      data (a(i),i=1,4)/.5d0,.2928932188d0,1.7071067812d0,
     1                  .1666666667d0/
      data (b(i),i=1,4)/2.d0,1.d0,1.d0,2.d0/
      data (c(i),i=1,4)/.5d0,.2928932188d0,1.7071067812d0,.5d0/      
c----------------------------------------------------------------------
      allocate(qa1(nmodes+1,nslices),qa2(nmodes,nslices),
     1         qsde1(nmodes,nslices),qsde2(nmodes,nslices),
     2         dfa1(nmodes,nslices),dfa2(nmodes,nslices),
     3         dsde1(nharms,nslices),dsde2(nharms,nslices),
     4         akz(nmodes,nslices),aki(nmodes,nslices),
     5         pln(nmodes,nslices))
      allocate(zpos(3*(nsegbw+nsegfoc)),zneg(3*(nsegbw+nsegfoc)))
      if(emmodes.eq.'laguerre') then
         allocate(snltheta(iparts,lmax+1),csltheta(iparts,lmax+1))
      endif
      if(iecon.eq.1) then  !-------------------------------------------!
         if(myid.eq.0) then                                            !
            open(unit=19,file='Energy Conservation.txt',               !
     1           form='formatted',status='replace')                    !
            write(19,1060)                                             !
 1060       format(12x,' ENERGY CONSERVATION DIAGNOSTIC',//,4x,'z(m)', !
     1             6x,'etapart',4x,'etarad')                           !
         endif                                                         !
         sum_e=zero                                                    !
         do ns=ns_begin,ns_end                                         !
            nss=ns-ns_begin+1                                          !
            if(sumw(ns).gt.zero) then                                  !
               do k=1,ipart(ns)                                        !
                  sum_e=sum_e+weights(k,nss)*(gamma0(k,nss)-one)       !
               end do                                                  !
            endif                                                      !
         end do                                                        !
         if(numprocs.gt.1) then                                        !
               call mpi_reduce(sum_e,esum,1,mpi_double_precision,      !
     1                         mpi_sum,0,mpi_comm_world,ier)           !
               call mpi_bcast(esum,1,mpi_double_precision,0,           !
     1                        mpi_comm_world,ier)                      !
            else                                                       !
               esum=sum_e                                              !
         endif                                                         !
      endif  !---------------------------------------------------------!
c
c------- distributed arrays used in this subroutine
c
c        x,y,px,py,pz,psi,fx,fy,fpx,fpy,fpz,fpsi,
c        qx,qy,qpsi,qpx,qpy,qpz,Prob,r2
c
      if(RFfield.eq.'rf') then
         powernorm=pwfac*akw(1)
         do ns=ns_begin,ns_end
            do i=1,nsegbw
               Prf(i,ns)=Pinrf(i,ns)
               Gloss(i,ns)=zero
               dgamdt(i,ns)=zero
            end do
         end do
      endif
c
c------- Initialization for status monitor
c
      zlength=scale*(xmax-x0)           ! total integration length, used for progress monitor
      dlength=min(zlength/10.0d0,1.0d0) ! progress in steps of 10% of total length or 1 m
c
c------- Initialize over nslices
c
      if(numprocs.gt.1) then
         do i=ns_begin,ns_end
            call Head_Harm(i,Power_vs_z,myid)
         end do
      end if
      do ns=ns_begin,ns_end
         nss=ns-ns_begin+1
         if(nss.gt.nrmdim) print *,"nss out of bounds 3121"
         if(numprocs.eq.1) call Head_Harm(ns,Power_vs_z,myid)
         a1(nmodes+1,ns)=x0
         do k=1,ipart(ns)
             qx(k,nss)=zero
             qy(k,nss)=zero
             qpsi(k,nss)=zero
             qpx(k,nss)=zero
             qpy(k,nss)=zero
             qpz(k,nss)=zero
         end do
         do nm=1,nmodes
             qa1(nm,ns)=zero
             qa2(nm,ns)=zero
         end do
         qa1(nmodes+1,ns)=zero
         do nh=1,nharms
            qsde1(nh,ns)=zero
            qsde2(nh,ns)=zero
         end do
         deltapsi(ns)=zero
      end do   ! End Loop over nslices
      iteration=0
      itermax=1
      dx=dz
      if(nsegfoc.eq.0) zstt(1)=xmax
c
c------- start integration loop & test for last step
c
    2 if(a1(nmodes+1,ns_begin)+dx.gt.xmax) then
         if(numprocs.gt.1.and.myid.eq.0) then
            do ns=1,nslices
               do nh=2,nharms
                  close(unit=nhout(nh,ns))
c                 These units must be closed after the call to ode
c                  if((nslices.gt.1).and.(ns.eq.1)) then
c                     close(unit=nehout(nh))
c                  endif
               end do
            end do
            return
         else
             do ns=ns_begin,ns_end
               do nh=2,nharms
                  close(unit=nhout(nh,ns))
                  if((nslices.gt.1).and.(ns.eq.1)) then
                     if(numprocs.eq.1) then
                        close(unit=nehout(nh))
                     end if
                  endif
               end do
             end do
             return
         end if
      endif
      if((nslices.gt.1).and.(nharms.gt.1)) then  !--------------------!
         zhwrite(itermax)=scale*a1(nmodes+1,1)                        !
         do ns=ns_begin,ns_end                                        !
            do nh=2,nharms                                            !
              do nm=nh1(nh,ns),nh2(nh,ns)                             !--Accumulate Harmonic
                 alnsq=a1(nm,ns)**2+a2(nm,ns)**2                      !--Energies when
                 Pwrite(itermax,nh)=Pwrite(itermax,nh)+               !--nslices > 1
     1                                    dtpulse*alnsq/pfac(nm,ns)   !
              end do                                                  !
            end do                                                    !
         end do                                                       !
      endif  !--------------------------------------------------------!
c
c------- adjust step size
c
      z=a1(nmodes+1,ns_begin)
      isegbw=0
      isegb0=0
      iwcount=0
      dzdt=z_slip
      do i=1,nsegbw
           if((z.ge.zstart(i)).and.(z.le.zstop(i))) then
              iwcount=iwcount+1
              if(iwcount.le.1) then
                 isegbw=i
                 dx=zwi(isegbw)*dz/zwi(1)
                 if(ntype(isegbw).eq.7) then
                    do ii=1,nbx_max
                       do j=1,nby_max
                          do k=1,nbz_max
                             bxarr(ii,j,k)=zero
                             byarr(ii,j,k)=zero
                             bzarr(ii,j,k)=zero
                          end do
                       end do
                    end do
                    open(unit=13,file=Bmapfile(i),status='old')
                    write(6,*) 'Begin Reading Magnetic Field Map'
                    do k=1,nbz(i)
                       do ii=1,nbx(i)
                          do j=1,nby(i)  
                             read(13,600) bxarr(ii,j,k),byarr(ii,j,k),
     1                                    bzarr(ii,j,k)
  600                        format(1p3e15.8)
                          end do
                       end do
                    end do
                    do ii=1,nbx(i)
                       do j=1,nby(i)
                          do k=1,nbz(i)
                             bxarr(ii,j,k)=awfac*zwi(i)*bxarr(ii,j,k)
                             byarr(ii,j,k)=awfac*zwi(i)*byarr(ii,j,k)
                             bzarr(ii,j,k)=awfac*zwi(i)*bzarr(ii,j,k)
                          end do
                       end do
                    end do
                    write(6,*) 'Finished Reading Magnetic Field Map'
                 endif
              endif
           endif
      end do
c
      if((isegbw.eq.0).and.(nsegfoc.gt.0)) then
        do i=1,nsegfoc
           if((z+amicro.ge.zstt(i)).and.(z-amicro.lt.zstp(i))) then
                isegb0=i
                dx=(zstp(i)-zstt(i))/dble(ndfoc(i))
                nwj=1
                do j=1,nsegbw-1
                   if((z.ge.zstop(j)).and.(z.le.zstart(j+1))) nwj=j
                end do
                dzdt=z_slip*(zstp(i)-zstt(i))/slip_fac
     1                                       /(zstop(nwj)-zstart(nwj))
           endif
        end do
      endif
c
      if((isegbw.eq.0).and.(isegb0.eq.0)) then
           if((z.lt.zstart(1)).and.(z.lt.zstt(1))) then
                 deltaz=min(zstart(1),zstt(1))
                 dx=deltaz/int(amicro+deltaz/dz)
              else
                 npos=0
                 nneg=0
                 do i=1,nsegbw
                     if(z.gt.zstart(i)) then
                           npos=npos+1
                           zpos(npos)=z-zstart(i)
                        else
                           nneg=nneg+1
                           zneg(nneg)=z-zstart(i)
                     endif
                     if(z.gt.zstop(i)-amicro) then
                           npos=npos+1
                           zpos(npos)=z-zstop(i)
                        else
                           nneg=nneg+1
                           zneg(nneg)=z-zstop(i)
                     endif
                 end do
                 do i=1,nsegfoc
                     if(z.gt.zstt(i)) then
                           npos=npos+1
                           zpos(npos)=z-zstt(i)
                         else
                           nneg=nneg+1
                           zneg(nneg)=z-zstt(i)
                     endif
                     if(z.gt.zstp(i)-amicro) then
                           npos=npos+1
                           zpos(npos)=z-zstp(i)
                         else
                           nneg=nneg+1
                           zneg(nneg)=z-zstp(i)
                    endif
                 end do
                 if(nneg.gt.0) then
                        zless=zpos(1)
                        do i=2,npos
                           zless=min(zless,zpos(i))
                        end do
                        zmore=zneg(1)
                        do i=2,nneg
                           zmore=max(zmore,zneg(i))
                        end do
                        deltaz=zless-zmore
                        dx=deltaz/int(amicro+deltaz/dz)
                   else
                        dx=dz
                 endif
           endif
           dzdt=z_slip*deltaz/slip_fac/(zstop(1)-zstart(1))
      endif
c
c------- perform integration over step
c
      do ns=ns_begin,ns_end
         nss=ns-ns_begin+1
         if(nss.gt.nrmdim) print *,"nss out of bounds 3402"
         if(Runge_Kutta.eq.'4th_order') n_iters=4
         if(Runge_Kutta.eq.'2nd_order') n_iters=2
         do j=1,n_iters
           xj=a1(nmodes+1,ns)
           call func(xj,j,ns,nss,xmax,Wakefield)
           fa1(nmodes+1,ns)=one
           if(Runge_Kutta.eq.'4th_order') then
              do k=1,ipart(ns)
                tx=a(j)*(fx(k,nss)-b(j)*qx(k,nss))
                x(k,nss)=x(k,nss)+dx*tx
                qx(k,nss)=qx(k,nss)+three*tx-c(j)*fx(k,nss)
                ty=a(j)*(fy(k,nss)-b(j)*qy(k,nss))
                y(k,nss)=y(k,nss)+dx*ty
                qy(k,nss)=qy(k,nss)+three*ty-c(j)*fy(k,nss)
                tpsi=a(j)*(fpsi(k,nss)-b(j)*qpsi(k,nss))
                psi(k,nss)=psi(k,nss)+dx*tpsi
                qpsi(k,nss)=qpsi(k,nss)+three*tpsi-c(j)*fpsi(k,nss)
                tpx=a(j)*(fpx(k,nss)-b(j)*qpx(k,nss))
                px(k,nss)=px(k,nss)+dx*tpx
                qpx(k,nss)=qpx(k,nss)+three*tpx-c(j)*fpx(k,nss)
                tpy=a(j)*(fpy(k,nss)-b(j)*qpy(k,nss))
                py(k,nss)=py(k,nss)+dx*tpy
                qpy(k,nss)=qpy(k,nss)+three*tpy-c(j)*fpy(k,nss)
                tpz=a(j)*(fpz(k,nss)-b(j)*qpz(k,nss))
                pz(k,nss)=pz(k,nss)+dx*tpz
                qpz(k,nss)=qpz(k,nss)+three*tpz-c(j)*fpz(k,nss)          
              end do
              do nm=1,nmodes
                ta1=a(j)*(fa1(nm,ns)-b(j)*qa1(nm,ns))
                a1(nm,ns)=a1(nm,ns)+dx*ta1
                qa1(nm,ns)=qa1(nm,ns)+three*ta1-c(j)*fa1(nm,ns)
                ta2=a(j)*(fa2(nm,ns)-b(j)*qa2(nm,ns))
                a2(nm,ns)=a2(nm,ns)+dx*ta2
                qa2(nm,ns)=qa2(nm,ns)+three*ta2-c(j)*fa2(nm,ns)        
              end do
              ta1=a(j)*(fa1(nmodes+1,ns)-b(j)*qa1(nmodes+1,ns))
              a1(nmodes+1,ns)=a1(nmodes+1,ns)+dx*ta1
              qa1(nmodes+1,ns)=qa1(nmodes+1,ns)+three*ta1
     1                                           -c(j)*fa1(nmodes+1,ns)
              do nh=1,nharms
                 tsde1=a(j)*(fsde1(nh,ns)-b(j)*qsde1(nh,ns))
                 asde1(nh,ns)=asde1(nh,ns)+dx*tsde1
                 qsde1(nh,ns)=qsde1(nh,ns)+three*tsde1
     1                                              -c(j)*fsde1(nh,ns)
                 tsde2=a(j)*(fsde2(nh,ns)-b(j)*qsde2(nh,ns))
                 asde2(nh,ns)=asde2(nh,ns)+dx*tsde2
                 qsde2(nh,ns)=qsde2(nh,ns)+three*tsde2
     1                                              -c(j)*fsde2(nh,ns)
              end do
           endif
           if(Runge_Kutta.eq.'2nd_order') then
              halfdx=half*dx
              do k=1,ipart(ns)
                 x(k,nss)=x(k,nss)+halfdx*fx(k,nss)
                 y(k,nss)=y(k,nss)+halfdx*fy(k,nss)
                 psi(k,nss)=psi(k,nss)+halfdx*fpsi(k,nss)
                 px(k,nss)=px(k,nss)+halfdx*fpx(k,nss)
                 py(k,nss)=py(k,nss)+halfdx*fpy(k,nss)
                 pz(k,nss)=pz(k,nss)+halfdx*fpz(k,nss)                   
              end do
              do nm=1,nmodes
                a1(nm,ns)=a1(nm,ns)+halfdx*fa1(nm,ns)
                a2(nm,ns)=a2(nm,ns)+halfdx*fa2(nm,ns)        
              end do
              a1(nmodes+1,ns)=a1(nmodes+1,ns)+halfdx*fa1(nmodes+1,ns)
              do nh=1,nharms
                 asde1(nh,ns)=asde1(nh,ns)+halfdx*fsde1(nh,ns)
                 asde2(nh,ns)=asde2(nh,ns)+halfdx*fsde2(nh,ns)
              end do
           endif
c
c------- Calculate Pump Depletion Rate when RFfield = 'rf'
c
           if((RFfield.eq.'rf').and.(isegbw.gt.0).and.(j.eq.4)) then
            if(moderf(isegbw).ge.0) then
             sumIb=zero
             current=zero
             do k=1,ipart(ns)
                sumIb=sumIb+weights(k,nss)
             end do
             if(ipart(ns).gt.0) current=beami*sumIb/sumw(ns)
             dPower=-restmass*current*dz*omegarf(isegbw)
     1                                               *dgamdt(isegbw,ns)
             Gloss(isegbw,ns)=half*dPower
     1                                  /((Prf(isegbw,ns)+smallest)*dz)       
             Prf(isegbw,ns)=Prf(isegbw,ns)+dPower
             if(Prf(isegbw,ns).gt.zero) then
                 Eacc(isegbw,ns)=sqrt(Pfacrf(isegbw)*Prf(isegbw,ns))
               else
                 dPower=zero
                 Eacc(isegbw,ns)=zero
             endif
             if((z.ge.z1acc(isegbw)).and.(z.le.z2acc(isegbw))) then
                nRF2=5001+nslices+ns
                if(ipart(ns).gt.0) then
                      write(nRF2,999) scale*z,max(Prf(isegbw,ns),zero),
     1                          dPower,dgamdt(isegbw,ns)
  999                 format(4(1x,1pe10.3))
                endif
             endif
            endif
           endif
c
           nkill=1  !--------------------------------------------------!
           mkill(1,ns)=0                                               !
           if(drifttube.eq.'cylindrical') then                         !
              do k=1,ipart(ns)                                         !
                 r2(k,nss)=x(k,nss)**2+y(k,nss)**2                     !
              end do                                                   !
              do k=1,ipart(ns)                                         !
                if(r2(k,nss).ge.rgkw2) then                            !
                     nkill=nkill+1                                     !
                     mkill(nkill,ns)=k                                 !
                endif                                                  !
              end do                                                   !
           endif                                                       !
           if(drifttube.eq.'rectangular') then                         !
              do k=1,ipart(ns)                                         !
                 if((abs(x(k,nss)).ge.hagkwrf)                         !
     1                             .or.(abs(y(k,nss)).ge.hbgkwrf)) then!
                     nkill=nkill+1                                     !
                     mkill(nkill,ns)=k                                 !
                 endif                                                 !
              end do                                                   !
           endif                                                       !
           if((nkill-1.ge.ipart(ns)).and.(ipart(ns).gt.0)              !
     1                  .and.(ns.ge.ns_frst).and.(ns.le.ns_last)) then !
              beamloss='total'                                         !
              return                                                   !
           endif                                                       !--Test & Eject
           if(nkill.gt.1) then                                         !--Electrons
              nkill=nkill+1                                            !--if Necessary
              mkill(nkill,ns)=ipart(ns)+1                              !
              iq=0                                                     !
   20         iq=iq+1                                                  !
              if(iq.ge.nkill) go to 128                                !
              mm1=mkill(iq,ns)+1                                       !
              mm2=mkill(iq+1,ns)-1                                     !
              if(mm2.lt.mm1) go to 20                                  !
              iq2=iq-1                                                 !
              do ip=mm1,mm2                                            !
                 weights(ip-iq2,nss)=weights(ip,nss)                   !
                 gamma0(ip-iq2,nss)=gamma0(ip,nss)                     !
                 x(ip-iq2,nss)=x(ip,nss)                               !
                 y(ip-iq2,nss)=y(ip,nss)                               !
                 psi(ip-iq2,nss)=psi(ip,nss)                           !
                 px(ip-iq2,nss)=px(ip,nss)                             !
                 py(ip-iq2,nss)=py(ip,nss)                             !
                 pz(ip-iq2,nss)=pz(ip,nss)                             !
                 qx(ip-iq2,nss)=qx(ip,nss)                             !
                 qy(ip-iq2,nss)=qy(ip,nss)                             !
                 qpsi(ip-iq2,nss)=qpsi(ip,nss)                         !
                 qpx(ip-iq2,nss)=qpx(ip,nss)                           !
                 qpy(ip-iq2,nss)=qpy(ip,nss)                           !
                 qpz(ip-iq2,nss)=qpz(ip,nss)                           !
              end do                                                   !
              go to 20                                                 !
  128         continue                                                 !
              ipart(ns)=2+ipart(ns)-nkill                              !
           endif  !----------------------------------------------------!
c
        end do  ! end Runge-Kutta Loop
      end do    ! end loop over nslices
c
      if((slippage.eq.'on').and.(z.le.zstop(nsegbw)).and.  !-----------!
     1                   (iteration.eq.nslip*(iteration/nslip))) then  !
         if(mod(myid,2).eq.0) then                                     !
             if(myid.lt.numprocs-1) then                               !
                if((emmodes.eq.'hermite')                              !
     1                        .or.(emmodes.eq.'laguerre')) then        !
                   call mpi_send(asde1(1,ns_end),nharms,               !
     1                mpi_double_precision,myid+1,1,mpi_comm_world,ier)!
                   call mpi_send(asde2(1,ns_end),nharms,               !
     1                mpi_double_precision,myid+1,2,mpi_comm_world,ier)!
                endif                                                  !
                call mpi_send(a1(1,ns_end),nmodes,                     !
     1                mpi_double_precision,myid+1,3,mpi_comm_world,ier)!
                call mpi_send(a2(1,ns_end),nmodes,                     !
     1                mpi_double_precision,myid+1,4,mpi_comm_world,ier)!
             endif                                                     !
             if(myid.gt.0) then                                        !
                if((emmodes.eq.'hermite')                              !
     1                        .or.(emmodes.eq.'laguerre')) then        !
                   call mpi_recv(asde1(1,ns_begin-1),nharms,           !
     1                   mpi_double_precision,myid-1,5,mpi_comm_world, !
     2                   istatus,ier)                                  !
                   call mpi_recv(asde2(1,ns_begin-1),nharms,           !
     1                   mpi_double_precision,myid-1,6,mpi_comm_world, !
     2                   istatus,ier)                                  !
                endif                                                  !
                call mpi_recv(a1(1,ns_begin-1),nmodes,                 !
     1                mpi_double_precision,myid-1,7,mpi_comm_world,    !
     2                istatus,ier)                                     !
                call mpi_recv(a2(1,ns_begin-1),nmodes,                 !
     1                mpi_double_precision,myid-1,8,mpi_comm_world,    !
     2                istatus,ier)                                     !
             endif                                                     !
          endif                                                        !--Transfer slice
          if(mod(myid,2).eq.1) then                                    !--Info between
             if(myid.gt.0) then                                        !--CPUs
                if((emmodes.eq.'hermite')                              !
     1                        .or.(emmodes.eq.'laguerre')) then        !
                   call mpi_recv(asde1(1,ns_begin-1),nharms,           !
     1                   mpi_double_precision,myid-1,1,mpi_comm_world, !
     2                   istatus,ier)                                  !
                   call mpi_recv(asde2(1,ns_begin-1),nharms,           !
     1                   mpi_double_precision,myid-1,2,mpi_comm_world, !
     2                   istatus,ier)                                  !
                endif                                                  !
                call mpi_recv(a1(1,ns_begin-1),nmodes,                 !
     1                mpi_double_precision,myid-1,3,mpi_comm_world,    !
     2                istatus,ier)                                     !
                call mpi_recv(a2(1,ns_begin-1),nmodes,                 !
     1                mpi_double_precision,myid-1,4,mpi_comm_world,    !
     2                istatus,ier)                                     !
             endif                                                     !
             if(myid.lt.numprocs-1) then                               !
                if((emmodes.eq.'hermite')                              !
     1                        .or.(emmodes.eq.'laguerre')) then        !
                   call mpi_send(asde1(1,ns_end),nharms,               !
     1                mpi_double_precision,myid+1,5,mpi_comm_world,ier)!
                   call mpi_send(asde2(1,ns_end),nharms,               !
     1                mpi_double_precision,myid+1,6,mpi_comm_world,ier)!
                endif                                                  !
                call mpi_send(a1(1,ns_end),nmodes,                     !
     1                mpi_double_precision,myid+1,7,mpi_comm_world,ier)!
                call mpi_send(a2(1,ns_end),nmodes,                     !
     1                mpi_double_precision,myid+1,8,mpi_comm_world,ier)!
             endif                                                     !
          endif !------------------------------------------------------!
          if(interpolation.eq.'high_order') then                       !
             do nh=1,nharms                                            !
                dfa1=zero                                              !
                dfa2=zero                                              !                                                    ! 
                do ns=max(2,ns_begin),ns_end                           !
                   do nm=nh1(nh,ns),nh2(nh,ns)                         !
                      dphasea1=(one-dzdt)*a1(nm,ns)+dzdt*a1(nm,ns-1)   !                 
                      dphasea2=(one-dzdt)*a2(nm,ns)+dzdt*a2(nm,ns-1)   !
                      dphase=sqrt(dphasea1**2+dphasea2**2)             !
                      amod=sqrt((one-dzdt)*(a1(nm,ns)**2+a2(nm,ns)**2) !
     2                           +dzdt*(a1(nm,ns-1)**2+a2(nm,ns-1)**2))!
                      dfa1(nm,ns)=dphasea1*amod/(dphase+1.d-30)        !
                      dfa2(nm,ns)=dphasea2*amod/(dphase+1.d-30)        !
                   end do                                              !
                end do                                                 !
                do ns=max(2,ns_begin),ns_end                           !
                   do nm=nh1(nh,ns),nh2(nh,ns)                         !
                      a1(nm,ns)=dfa1(nm,ns)                            !--Apply 
                      a2(nm,ns)=dfa2(nm,ns)                            !--Slippage
                   end do                                              !
                end do                                                 !
             end do                                                    !
          endif                                                        !
          if(interpolation.eq.'linear') then                           !
             do nm=1,nmodes                                            !
                do ns=max(2,ns_begin),ns_end                           !
                   if(abs(a1(nm,ns)-a1(nm,ns-1)).gt.smallest)then      !
                         dfa1(nm,ns)=a1(nm,ns)-a1(nm,ns-1)             !
                      else                                             !
                          dfa1(nm,ns)=zero                             !
                   endif                                               !
                   if(abs(a2(nm,ns)-a2(nm,ns-1)).gt.smallest)then      !
                          dfa2(nm,ns)=a2(nm,ns)-a2(nm,ns-1)            !
                      else                                             !
                          dfa2(nm,ns)=zero                             !
                   endif                                               !
                end do                                                 !
             end do                                                    !
             do nm=1,nmodes                                            !
                do ns=max(2,ns_begin),ns_end                           !
                   a1(nm,ns)=a1(nm,ns)-dzdt*dfa1(nm,ns)                !
                   a2(nm,ns)=a2(nm,ns)-dzdt*dfa2(nm,ns)                !
                end do                                                 !
             end do                                                    !
          endif                                                        !
          do nh=1,nharms                                               !
             if((emmodes.eq.'hermite').or.(emmodes.eq.'laguerre'))then !
                do ns=max(2,ns_begin),ns_end                           !
                   dsde1(nh,ns)=asde1(nh,ns)-asde1(nh,ns-1)            !
                   dsde2(nh,ns)=asde2(nh,ns)-asde2(nh,ns-1)            !
                end do                                                 !
                do ns=max(2,ns_begin),ns_end                           !
                   asde1(nh,ns)=asde1(nh,ns)-dzdt*dsde1(nh,ns)         !
                   asde2(nh,ns)=asde2(nh,ns)-dzdt*dsde2(nh,ns)         !
                end do                                                 !
             endif                                                     !
          end do                                                       !
c----------------------------------------------------------------------!
          do ns=1,ns_begin-1                                           !
             do nm=1,nmodes                                            !
                a1(nm,ns)=zero                                         !
                a2(nm,ns)=zero                                         !
             end do                                                    !
             do nh=1,nharms                                            !
                asde1(nh,ns)=zero                                      !
                asde2(nh,ns)=zero                                      !
             end do                                                    !
         end do                                                        !
         do ns=ns_end+1,nslices                                        !
             do nm=1,nmodes                                            !
                a1(nm,ns)=zero                                         !
                a2(nm,ns)=zero                                         !
             end do                                                    !
             do nh=1,nharms                                            !
                asde1(nh,ns)=zero                                      !
                asde2(nh,ns)=zero                                      !
             end do                                                    !
         end do                                                        !
      endif  !---------------------------------------------------------!
c
c------- find mode quantities
c
      allocate(Fluence(n_grid,n_grid,nharms))
      Fluence=zero
      do ns=ns_begin,ns_end
         z=a1(nmodes+1,ns)
         ptot(ns)=zero
         if((emmodes.eq.'hermite').or.(emmodes.eq.'laguerre')) then  !-!
            do nh=1,nharms                                             !
              do nm=nh1(nh,ns),nh2(nh,ns)                              !
               alnsq=a1(nm,ns)**2+a2(nm,ns)**2                         !
               if(alnsq.gt.zero) then                                  !
                    aki(nm,ns)=(aki1(nm,ns)*a1(nm,ns)                  !
     1                                  +aki2(nm,ns)*a2(nm,ns))/alnsq  !
                    akz(nm,ns)=omega(nm)+(aki2(nm,ns)*a1(nm,ns)        !
     1                                  -aki1(nm,ns)*a2(nm,ns))/alnsq  !--Gaussian
                  else                                                 !--Optical
                    aki(nm,ns)=zero                                    !--Modes
                    akz(nm,ns)=omega(nm)                               !
               endif                                                   !
               pln(nm,ns)=alnsq/pfac(nm,ns)                            !
               ptot(ns)=ptot(ns)+pln(nm,ns)                            !
              end do                                                   !
            end do                                                     !
            deltapsi(ns)=deltapsi(ns)+dz*(akz(1,ns)-omega(1))          !
         endif  !------------------------------------------------------!
         if(ptot(ns).gt.plnmax(ns)) plnmax(ns)=ptot(ns)
c
c------- Output Beam & Mode States
c
         nflag=0
         do id=1,ndiags
            zmeters=scale*z
            if((abs(zmeters-zdiags(id)).le.scale*dx).and.
     1                                  (zmeters.le.zdiags(id))) then
               nflag=1
               z_diag=zdiags(id)
               call Head_Diag(id,ns,11,12,trans_mode,beam_diags,
     1                        Fluence_only)
               if(beam_diags.eq.'on') then  !--------------------------!
                  write(11,401) zmeters                                !
  401             format(/,' z(m)=',1pe11.4,//,6x,'kwx',10x,'kwy',9x,  !
     1             'px/mc',8x,'py/mc',8x,'psi',10x,'pz/mc',7x,'dpsidz')!
                  do k=1,ipart(ns)                                     !
                     vz=pz(k,nss)/sqrt(one+px(k,nss)**2                !--write
     1                                      +py(k,nss)**2+pz(k,nss)**2)!--Beam_Diagnostics.txt
                     dpsidz=one+akz(1,ns)-omega(1)/vz                  !--files
                     write(11,402) x(k,nss),y(k,nss),px(k,nss),        !
     1                             py(k,nss),z+deltapsi(ns)+psi(k,nss),!
     2                             pz(k,nss),dpsidz                    !
  402                format(7(1x,1pe12.5))                             !
                  end do                                               !
               endif !-------------------------------------------------!
               if(trans_mode.eq.'on') then  !--------------------------!
                  if(Fluence_only.eq.'no') write(12,403) zmeters,ns
  403             format(/,12x,'z(m)=',1pe11.4,/,11x,'slice=',i5)
                  waste=akwwaist/akw(1)
                  do nh=1,nharms
                    allocate(Poynting(n_grid,n_grid))
                    w=asde1(nh,ns)
                    w2=w**2
                    alpha=asde2(nh,ns)
                    if(Fluence_only.eq.'no') then
                       write(12,405) hundredth*zwi(1)/omega(nh1(nh,ns))
  405                  format(/,7x,'Wavelength(m)=',1pe12.5,//,4x,'l',
     1                        4x,'n',6x,'A1',11x,'A2',9x,'Power(W)')
                       do nm=nh1(nh,ns),nh2(nh,ns)
                          alnsq=a1(nm,ns)**2+a2(nm,ns)**2
                          pwr=alnsq/pfac(nm,ns)
                          write(12,406) lnum(nm,ns),nnum(nm,ns),
     1                                  a1(nm,ns),a2(nm,ns),pwr
  406                     format(2(1x,i4),3(1x,1pe12.5))
                       end do
                    endif
                    wlength=zwi(1)/omega(nh1(nh,ns))  !  wavelength in cm
                    pintensity=four*twopi*watts/wlength**2
                    sum_x=zero
                    sum_y=zero
                    sum_total=zero
                    n_offset=1+n_grid/2
                    if(mtype.eq.1) then   ! Gauss-Hermite Modes  !-----!
                       allocate(xpw(1),ypw(1))                         !
                       do nx=1,n_grid                                  !
                          xp=dble(nx-n_offset)*waste/dble(10)          !
                          xpw(1)=rt2*akw(1)*xp/w                       !
                          call hermite(xpw,lmax+2,1,1)                 !
                          do ny=1,n_grid                               !
                             yp=dble(ny-n_offset)*waste/dble(10)       !
                             ypw(1)=rt2*akw(1)*yp/w                    !
                             call hermite(ypw,nmax+2,1,2)              !
                             rsqr=half*(xpw(1)**2+ypw(1)**2)           !
                             sum1=zero                                 !
                             sum2=zero                                 !
                             do nm1=nh1(nh,ns),nh2(nh,ns)              !
                                lnm1=lnum(nm1,ns)+1                    !
                                nnm1=nnum(nm1,ns)+1                    !
                                do nm2=nh1(nh,ns),nh2(nh,ns)           !
                                   lnm2=lnum(nm2,ns)+1                 !
                                   nnm2=nnum(nm2,ns)+1                 !
                                   sum1=sum1+a1(nm1,ns)*a1(nm2,ns)     !
     1                                     *al(1,lnm1,1)*al(1,lnm2,1)  !
     2                                       *al(1,nnm1,2)*al(1,nnm2,2)!
                                   sum2=sum2+a2(nm1,ns)*a2(nm2,ns)     !
     1                                     *al(1,lnm1,1)*al(1,lnm2,1)  !
     2                                       *al(1,nnm1,2)*al(1,nnm2,2)!
                                end do                                 !
                             end do                                    !
                             Poynting(nx,ny)=pintensity                !
     1                                      *exp(-two*rsqr)*(sum1+sum2)!
                             if(Poynting(nx,ny).lt.smallest) then      !
                             	    Poynting(nx,ny)=zero               !    
                             endif                                     !
                             Poynting(nx,ny)=Poynting(nx,ny)/hundred   !
                             sum_x=sum_x+xp*Poynting(nx,ny)/hundred    !
                             sum_y=sum_y+yp*Poynting(nx,ny)/hundred    !
                             sum_total=sum_total                       !
     1                                         +Poynting(nx,ny)/hundred!
                          end do                                       !
                       end do                                          !
                       do nx=1,n_grid                                  !
                          do ny=1,n_grid                               !
                             Fluence(nx,ny,nh)=Fluence(nx,ny,nh)       !
     1                                                 +Poynting(nx,ny)!
                          end do                                       !
                       end do                                          !
                       deallocate(xpw,ypw)                             !
                    endif  !-------------------------------------------!
                    if(mtype.eq.2) then   ! Gauss-Laguerre Modes !-----!
                       do nx=1,n_grid                                  !
c                         xp=akw(1)*dble(nx-n_offset)*waste/dble(10)   !
                          xp=dble(nx-n_offset)*waste/dble(10)          !
                          do ny=1,n_grid                               !
c                            yp=akw(1)*dble(ny-n_offset)*waste/dble(10)!
                             yp=dble(ny-n_offset)*waste/dble(10)       !
                             rp=sqrt(xp**2+yp**2)                      !
                             rsqr=akw(1)**2*(xp**2+yp**2)/w2           !
                             arg=two*rsqr                              !
                             if(arg.gt.zero) then                      !
                                   cs=xp/rp                            !
                                   sn=yp/rp                            !
                                   root=sqrt(arg)                      !
                                else                                   !
                                   root=zero                           !
                                   cs=zero                             !
                                   sn=zero                             !
                             endif                                     !
                             sum1=zero                                 !
                             sum2=zero                                 !
                             do nm1=nh1(nh,ns),nh2(nh,ns)              !
                                l_nm1=lnum(nm1,ns)                     !
                                n_nm1=nnum(nm1,ns)                     !
                               call laguerre_2(arg,t1,abs(l_nm1),n_nm1)!
                                do nm2=nh1(nh,ns),nh2(nh,ns)           !
                                   l_nm2=lnum(nm2,ns)                  !
                                   n_nm2=nnum(nm2,ns)                  !
                                   ldiff=abs(l_nm1-l_nm2)              !
                                   sgn_l=one                           !
                                   if(l_nm1-l_nm2.lt.0) sgn_l=-one     !
                                   if(ldiff.eq.0) then                 !
                                      cs_ldiff=one                     !
                                      sn_ldiff=zero                    !
                                     else                              !
                                      cs_ldiff=cs                      !
                                      sn_ldiff=sn                      !
                                      ll=2                             !
                                      do while (ll.le.ldiff)           !
                                       cs_ldiff=cs*cs_ldiff-sn*sn_ldiff!
                                       sn_ldiff=sn*cs_ldiff+cs*sn_ldiff!
                                       ll=ll+1                         !
                                      end do                           !
                                   endif                               !
                                   sn_ldiff=sn_ldiff*sgn_l             !
                               call laguerre_2(arg,t2,abs(l_nm2),n_nm2)!
                               fact=t1*t2*root**(abs(l_nm1)+abs(l_nm2))!
                                   amp11=fact*a1(nm1,ns)*a1(nm2,ns)    !
                                   amp22=fact*a2(nm1,ns)*a2(nm2,ns)    !
                                   amp12=fact*a1(nm1,ns)*a2(nm2,ns)    !
                                   amp21=fact*a2(nm1,ns)*a1(nm2,ns)    !
                                   sum1=sum1+(amp11+amp22)*cs_ldiff    !
                                   sum2=sum2+(amp12-amp21)*sn_ldiff    !
                                end do                                 !
                             end do                                    !
                             Poynting(nx,ny)=pintensity                !
     1                                          *exp(-arg)*(sum1+sum2) !
                             if(Poynting(nx,ny).lt.smallest) then      !
                             	    Poynting(nx,ny)=zero               !
                             endif                                     !
                             Poynting(nx,ny)=Poynting(nx,ny)/hundred   !
                             sum_x=sum_x+xp*Poynting(nx,ny)            !
                             sum_y=sum_y+yp*Poynting(nx,ny)            !
                             sum_total=sum_total+Poynting(nx,ny)       !
                          end do                                       !
                       end do                                          !
                       do nx=1,n_grid                                  !
                          do ny=1,n_grid                               !
                             Fluence(nx,ny,nh)=Fluence(nx,ny,nh)       !
     1                                                 +Poynting(nx,ny)!
                          end do                                       !
                       end do                                          !
                    endif  !-------------------------------------------!
                    if(sum_total.gt.zero) then                         !
                          x_centroid=sum_x/sum_total                   !
                          y_centroid=sum_y/sum_total                   !
                       else                                            !
                          x_centroid=zero                              !
                          y_centroid=zero                              !
                    endif                                              !
                    sigma_x=zero                                       !
                    sigma_y=zero                                       !
                    do nx=1,n_grid                                     !
                       xp=dble(nx-n_offset)*waste/dble(10)             !
                       do ny=1,n_grid                                  !
                    	  yp=dble(ny-n_offset)*waste/dble(10)            !
                    	  sigma_x=sigma_x                                !
     1                             +Poynting(nx,ny)*(xp-x_centroid)**2 !
                    	  sigma_y=sigma_y                                !
     1                             +Poynting(nx,ny)*(yp-y_centroid)**2 !
                       end do                                          !
                    end do                                             !
                    if(sum_total.gt.zero) then                         !
                       sigma_x=sqrt(sigma_x/sum_total)                 !
                       sigma_y=sqrt(sigma_y/sum_total)                 !
                    endif                                              !
                    if(Fluence_only.ne.'yes') then                     !
                       write(12,407) x_centroid,sigma_x                !
  407                  format(/,'   x_centroid(cm)=',1pe10.3,          !
     1                        ' sigma_x(cm)=',e10.3)                   !
                       write(12,408) y_centroid,sigma_y                !
  408                  format('   y_centroid(cm)=',1pe10.3,            !
     1                        ' sigma_y(cm)=',e10.3)                   !
                       write(12,409)                                   !
  409                  format(/,14x,'Mode Pattern',//,4x,'x(cm)',8x,   !
     1                        'y(cm)',3x,'Intensity(W/cm^2)')          !
                       do nx=1,n_grid                                  !
                          xp=dble(nx-n_offset)*waste/dble(10)          !
                          do ny=1,n_grid                               !
                  	     yp=dble(ny-n_offset)*waste/dble(10)           !
                  	     write(12,410) xp,yp,Poynting(nx,ny)           !
  410                        format(3(1x,1pe12.5))                     !
                          end do                                       !
                       end do                                          !
                    endif                                              !
                    deallocate(Poynting)                               !
                  end do   ! End Loop over Harmonics                   !
               endif  !------------------------------------------------!
               if(slippage.eq.'on') then
                  do nh=1,nharms
                     P_slice(ns,nh,id)=zero
                     if((emmodes.eq.'hermite')
     1                      .or.(emmodes.eq.'laguerre')) then
                        do nm=nh1(nh,ns),nh2(nh,ns)
                           w2=asde1(nh,ns)**2
                           alnsq=a1(nm,ns)**2+a2(nm,ns)**2
                           P_slice(ns,nh,id)=P_slice(ns,nh,id)
     1                                               +alnsq/pfac(nm,ns)
                           if(nm.eq.nh1(nh,nm)) then
                               P00_slice(ns,nh,id)=alnsq/pfac(nm,ns)                           
                           endif
                        end do
                     endif
                  end do
               endif
               if((slippage.ne.'on').and.(nharms.gt.1)) then
                  do nh=1,nharms
                     P_spec(nh,id)=zero
                     if((emmodes.eq.'hermite')
     1                      .or.(emmodes.eq.'laguerre')) then
                        do nm=nh1(nh,ns),nh2(nh,ns)
                          alnsq=a1(nm,ns)**2+a2(nm,ns)**2
                          P_spec(nh,id)=P_spec(nh,id)+alnsq/pfac(nm,ns)
                        end do
                     endif
                  end do
               endif
               if((slippage.eq.'on').and.(FFTchk.eq.'on')) then  !-----!
                  do nm=1,nmodes                                       !
                     A1FFT(ns,nm,id)=a1(nm,ns)                         !--Output Data
                     A2FFT(ns,nm,id)=a2(nm,ns)                         !--for FFTchk
                  end do                                               !
               endif !-------------------------------------------------!
               do nm=1,nmodes !----------------------------------------!
                  A1_M2(nm,ns,id)=a1(nm,ns)                            !
                  A2_M2(nm,ns,id)=a2(nm,ns)                            !--Output Data
               end do                                                  !--for M-Square
               do nh=1,nharms                                          !--Diagnostic
                  asde1_M2(nh,ns,id)=asde1(nh,ns)                      !
               end do                                                  !
            endif  !---------------------------------------------------!
            close(unit=11)
            close(unit=12)
         end do   ! End Loop over ndiags
      end do      ! End Loop over nslices
      if((trans_mode.eq.'on').and.(nflag.gt.0).and.(nslices.gt.1)) then!
         if(myid.eq.0) then                                            !
            write(file10,411) z_diag                                   !
  411       format('Fluence_z(m)=',1pe12.5,'.txt')                     !
            open(unit=27,file=file10,form='formatted',status='replace')!
         endif                                                         !
         if(numprocs.gt.1) then                                        !
            allocate(Fluence_work(n_grid,n_grid,nharms))               !
            Fluence_work=zero                                          !
            call mpi_reduce(Fluence,Fluence_work,n_grid*n_grid*nharms, !
     1               mpi_double_precision,mpi_sum,0,mpi_comm_world,ier)!
            Fluence=Fluence_work                                       !                                     !
            deallocate(Fluence_work)                                   !
         endif                                                         !
         if(myid.eq.0) then                                            !
            do nh=1,nharms                                             !
               sumF=zero                                               !
               sumx=zero                                               !
               sumy=zero                                               !
               do nx=1,n_grid                                          !
                  xp=dble(nx-n_offset)*waste/dble(10)                  !
                  do ny=1,n_grid                                       !
                     yp=dble(ny-n_offset)*waste/dble(10)               !
                     sumF=SumF+Fluence(nx,ny,nh)                       !
                     sumx=sumx+xp*Fluence(nx,ny,nh)                    !--Output
                     sumy=sumy+yp*Fluence(nx,ny,nh)                    !--Fluence
                  end do                                               !--over all
               end do                                                  !--slices
               x_center=sumx/sumF                                      !
               y_center=sumy/sumF                                      !
               sumr=zero                                               !
               do nx=1,n_grid                                          !
                  xp=dble(nx-n_offset)*waste/dble(10)                  !
                  do ny=1,n_grid                                       !
                     yp=dble(ny-n_offset)*waste/dble(10)               !
                     sumr=sumr+((xp-x_center)**2+(yp-y_center)**2)     !
     1                                              *Fluence(nx,ny,nh) !
                  end do                                               !
               end do                                                  !
               sigma_r=sqrt(sumr/sumF)                                 !
               Fscale=dtpulse*waste**2
               write(27,412) z_diag,hundredth*zwi(1)/omega(nh1(nh,ns)),!
     1                       Fscale*sumF,1.d4*sigma_r,1.d4*x_center,   !
     2                       1.d4*y_center                             !
  412          format(14x,'TOTAL FLUENCE',//,                          !
     1                3x,'       z=',1p1e12.5,' m',//,                 !
     2                3x,'   Wavelength=',e12.5,' m',/,                !
     3                3x,' Pulse Energy=',e12.5,' J',/,                !
     4                3x,'rms Spot Size=',e12.5,' microns',/,          !
     5                3x,'     x_center=',e12.5,' microns',/,          !
     6                3x,'     y_center=',e12.5,' microns',//,         !
     7                4x,'x(cm)',8x,'y(cm)',5x,'Fluence(J/cm^2)')      !
               do nx=1,n_grid                                          !
                  xp=dble(nx-n_offset)*waste/dble(10)                  !
                  do ny=1,n_grid                                       !
                     yp=dble(ny-n_offset)*waste/dble(10)               !
                     Pdensity=dtpulse*Fluence(nx,ny,nh)
                     if(abs(Pdensity).lt.1.d-99) Pdensity=zero
                     write(27,413) xp,yp,Pdensity                      !
  413                format(3(1x,1pe12.5))                             !
                  end do                                               !
               end do                                                  !
            end do                                                     !
            close(unit=27)                                             !
         endif                                                         !
      endif  !---------------------------------------------------------!
      deallocate(Fluence)
c
      iteration=iteration+1
      if(iteration.eq.nstep*(iteration/nstep)) then
         itermax=itermax+1
         do ns=ns_begin,ns_end
            nss=ns-ns_begin+1
            if(nss.gt.nrmdim) print *,"nss out of bounds 3720"
c
            if(beam_diags.eq.'on') then  !-----------------------------!
               xavg(ns)=zero                                           !
               yavg(ns)=zero                                           !
               pzavg=zero                                              !
               gamavg=zero                                             !
               psiavg=zero                                             !
               Rbrms(ns)=zero                                          !
               dxrms=zero                                              !
               dyrms=zero                                              !
               if(sumw(ns).gt.zero) then                               !
                  do k=1,ipart(ns)                                     !
                     gamavg=gamavg+weights(k,nss)*                     !
     1                 sqrt(one+px(k,nss)**2+py(k,nss)**2+pz(k,nss)**2)!
                     xavg(ns)=xavg(ns)+weights(k,nss)*x(k,nss)         !
                     yavg(ns)=yavg(ns)+weights(k,nss)*y(k,nss)         !
                     pzavg=pzavg+weights(k,nss)*pz(k,nss)              !
                     psiavg=psiavg+weights(k,nss)*psi(k,nss)           !--Generate Scratch
                  end do                                               !--Arrays for Writing
                  xavg(ns)=xavg(ns)/sumw(ns)                           !--The Beam_Evolution.txt
                  yavg(ns)=yavg(ns)/sumw(ns)                           !--File(s)
                  pzavg=pzavg/sumw(ns)                                 !
                  gamavg=gamavg/sumw(ns)                               !
                  psiavg=psiavg/sumw(ns)                               !
                  do k=1,ipart(ns)                                     !
                     rsq=(x(k,nss)-xavg(ns))**2+(y(k,nss)-yavg(ns))**2 !
                     Rbrms(ns)=Rbrms(ns)+weights(k,nss)*rsq            !
                     dxrms=dxrms+weights(k,nss)*(x(k,nss)-xavg(ns))**2 !
                     dyrms=dyrms+weights(k,nss)*(y(k,nss)-yavg(ns))**2 !
                  end do                                               !
                  Rbrms(ns)=sqrt(Rbrms(ns)/sumw(ns))/akw(1)            !
                  dxrms=sqrt(dxrms/sumw(ns))/akw(1)                    !
                  dyrms=sqrt(dyrms/sumw(ns))/akw(1)                    !
               endif                                                   !
               write(18) scale*z,xavg(ns)/akw(1),yavg(ns)/akw(1),      !
     1                   dxrms,dyrms,Rbrms(ns),pzavg,gamavg,           !
     2                   psiavg/twopi,ipart(ns)                        !
            endif  !---------------------------------------------------!
c
c------- E&M Output Section
c
             if(iprop.ne.0) then
               do nh=1,nharms
                 Psum(nh,ns)=zero
                 do nm=nh1(nh,ns),nh2(nh,ns)
                     Psum(nh,ns)=Psum(nh,ns)+pln(nm,ns)
                 end do
               end do
               zwrite(itermax)=scale*z
               Ewrite(itermax)=Ewrite(itermax)+dtpulse*Psum(1,ns)
               if((emmodes.eq.'hermite')  !----------------------------!
     1                                .or.(emmodes.eq.'laguerre')) then!
                 W_Spot=Spot_Size(1,ns)                                !
                 if(Power_vs_z.eq.'on') then  !------------------------!
                    write(30+ns) scale*z,Psum(1,ns),asde1(1,ns)/akw(1),!--Harmonic Output
     1                    W_Spot,asde2(1,ns),Rbrms(ns),ipart(ns)       !--when nslices=1
                 endif  !----------------------------------------------!
                 if((nh.gt.1).and.(Power_vs_z.eq.'on')) then           !
                    do nh=2,nharms                                     !
                       W_Spot=Spot_Size(nh,ns)                         !
                       write(nhout(nh,ns),201) scale*z,                !
     1                          Psum(nh,ns),W_Spot,asde1(nh,ns)/akw(1),!
     2                          asde2(nh,ns)                           !
  201                  format(5(1x,1pe10.3))                           !--Gaussian Optical
                    end do                                             !--Modes
                 endif                                                 !
                 if(nwrite.gt.0) then                                  !
                    do nm=1,nmodes                                     !
                       write(17) z,pln(nm,ns),aki(nm,ns),akz(nm,ns),   !
     1                         ipart(ns),ptot(ns)                      !
                    end do                                             !
                 endif                                                 !
               endif  !------------------------------------------------!
             endif
         end do   ! End Loop over nslices
c
         if(iecon.eq.1) then  !----------------------------------------!
            etapart=zero                                               !
            etarad=zero                                                !
            do ns=ns_begin,ns_end                                      !
               nss=ns-ns_begin+1                                       !
               if(sumw(ns).gt.zero) then                               !
                  sum1=zero                                            !
                  do k=1,ipart(ns)                                     !
                     gam=sqrt(one+px(k,nss)**2+py(k,nss)**2            !
     1                                                   +pz(k,nss)**2)!
                     sum1=sum1+weights(k,nss)*(gamma0(k,nss)-gam)      !
c    1                                             /(gamma0(k,nss)-one)!
                  end do                                               !
                  etapart=etapart+hundred*sum1/esum                    !
               endif                                                   !
               etarad=etarad+hundred*(ptot(ns)-P_in(ns))/pbeam         !--Energy
            end do                                                     !--Conservation
            if(numprocs.gt.1) then                                     !--Diagnostic
                  call mpi_reduce(etarad,etarad_tot,1,                 !
     1                            mpi_double_precision,mpi_sum,0,      !
     2                            mpi_comm_world,ier)                  !
                  call mpi_bcast(etarad_tot,1,mpi_double_precision,0,  !
     1                           mpi_comm_world,ier)                   !
                  call mpi_reduce(etapart,etapart_tot,1,               !
     1                            mpi_double_precision,mpi_sum,0,      !
     2                            mpi_comm_world,ier)                  !
                  call mpi_bcast(etapart_tot,1,mpi_double_precision,0, !
     1                           mpi_comm_world,ier)                   !
               else                                                    !
                  etarad_tot=etarad                                    !
                  etapart_tot=etapart                                  !
            endif                                                      !
            if(myid.eq.0) write(19,107) scale*z,etapart_tot,etarad_tot !
  107       format(5(1x,1pe10.3))                                      !
         endif  !------------------------------------------------------!
      endif       ! Endif for Output Control
c
      if((abs(z-xmax).lt.dx).and.(SpentBeam.eq.'on')) then  !----------!
         do ns=ns_begin,ns_end                                         !
           if(ipart(ns).gt.0) then                                     !
              nss=ns-ns_begin+1                                        !
              if(nss.gt.nrmdim) print *,"nss out of bounds 3720"       !
              gam=sqrt(one+px(1,nss)**2+py(1,nss)**2+pz(1,nss)**2)     !
              gmn(ns)=gam                                              !
              gmx(ns)=gam                                              !
              do k=1,ipart(ns)                                         !
                 gam=sqrt(one+px(k,nss)**2+py(k,nss)**2+pz(k,nss)**2)  !
                 gmn(ns)=min(gmn(ns),gam)                              !
                 gmx(ns)=max(gmx(ns),gam)                              !
              end do                                                   !
           endif                                                       !
         end do                                                        !
         if(numprocs.gt.1) then                                        !
            allocate(gmx_wk(nslices),gmn_wk(nslices))                  !
            gmx_wk=zero                                                !
            gmn_wk=zero                                                !
            call mpi_reduce(gmx,gmx_wk,nslices,                        !
     1                      mpi_double_precision,mpi_sum,0,            !
     2                      mpi_comm_world,ier)                        !--Generate Arrays
            call mpi_reduce(gmn,gmn_wk,nslices,                        !--For the Output
     1                      mpi_double_precision,mpi_sum,0,            !--of the Spent Beam
     2                      mpi_comm_world,ier)                        !--Energy Distribution
            gmx=gmx_wk                                                 !
            gmn=gmn_wk                                                 !
         endif                                                         !
         if(myid.eq.0) then                                            !
            G_x=one                                                    !
            do ns=1,nslices                                            !
               G_x=max(G_x,gmx(ns))                                    !
            end do                                                     !
            G_n=G_x                                                    !
            do ns=1,nslices                                            !
               if(gmn(ns).gt.one) G_n=min(G_n,gmn(ns))                 !
            end do                                                     !
         endif                                                         !
         if(numprocs.gt.1) then                                        !
            call mpi_bcast(G_x,1,mpi_double_precision,0,               !
     1                     mpi_comm_world,ier)                         !
            call mpi_bcast(G_n,1,mpi_double_precision,0,               !
     1                     mpi_comm_world,ier)                         !
         endif                                                         !
         dg=(G_x-G_n)/dble(n_spent)                                    !
         do ns=ns_begin,ns_end                                         !
             g1=G_n-dg                                                 !
             nss=ns-ns_begin+1                                         !
             do n=1,n_spent                                            !
                g1=g1+dg                                               !
                g2=g1+dg                                               !
                E_spent(n)=(g1-one)*emass                              !
                P_spent(n,ns)=zero                                     !
                do k=1,ipart(ns)                                       !
                   gam=sqrt(one+px(k,nss)**2+py(k,nss)**2+pz(k,nss)**2)!
                   if((gam.ge.g1).and.(gam.lt.g2)) then                !
                       P_spent(n,ns)=P_spent(n,ns)+weights(k,nss)      !
                   endif                                               !
                end do                                                 !
                if(sumw(ns).gt.zero) then                              !
                    P_spent(n,ns)=P_spent(n,ns)/sumw(ns)               !
                endif                                                  !
             end do                                                    !
         end do                                                        !
         if(numprocs.gt.1) deallocate(gmx_wk,gmn_wk)                   !
      endif  !---------------------------------------------------------!
c
      if((abs(z-xmax).lt.dx).and.(dump.eq.'yes')) then  !-------------!
         do ns=ns_begin,ns_end                                        !
            write(file1,601) ns                                       !
  601       format('Data_Dump_slice=',i5.5,'.dat')                    !
            open(unit=24,file=file1,form='unformatted',               !
     1           status='replace')                                    !
            write(24) ipart(ns)                                       !
            if(ipart(ns).gt.0) then                                   !
               nss=ns-ns_begin+1                                      !
               do k=1,ipart(ns)                                       !
                  write(24) x(k,nss),y(k,nss),psi(k,nss),px(k,nss),   !
     1                    py(k,nss),pz(k,nss),pz(k,nss),weights(k,nss)!---Write Data Files
               end do                                                 !---for Restart
            endif                                                     !
            write(24) scale*z                                         !
            do nh=1,nharms                                            !
               write(24) asde1(nh,ns),asde2(nh,ns)                    !
               do nm=nh1(nh,ns),nh2(nh,ns)                            !
                write(24) a1(nm,ns),a2(nm,ns),aki1(nm,ns),aki2(nm,ns) !
               end do                                                 !
            end do                                                    !
            close(unit=24)                                            !
         end do                                                       !
      endif  !--------------------------------------------------------!
c
c------- Output Spent Beam Particle Distribution
c
      if((abs(z-xmax).lt.dx).and.(S2E.eq.'on')) then
         nbh=1+nbin/2
         allocate(Q_slice(nslices),t_slice(nslices),n_slice(nslices))
         dt=tslices/dble(nslices-1)
         Q_sum=zero
         do ns=1,nslices
            Q_slice(ns)=beam_ns(ns)*dt
            Q_sum=Q_sum+Q_slice(ns)
            t_slice(ns)=tslices*dble(ns-1)/dble(nslices)
         end do
         ncount=0 ! # of slices with particles
         nstart=0 ! 1st slice with particles
         ntest=0
         do ns=1,nslices
           if(Q_slice(ns).gt.zero) then
              ncount=ncount+1
              if(ntest.eq.0) nstart=ns
           endif
           if(nstart.gt.0) ntest=1
         end do
         nsum=0  !----------------------------------------------------!
         do ns=1,nslices                                              !
            n_slice(ns)=(nparts_out*Q_slice(ns))/Q_sum                !--Find # of particles
            nsum=nsum+n_slice(ns)                                     !--per slice
         end do  !----------------------------------------------------!
         n_diff=nparts_out-nsum !-------------------------------------!
         nshift=(ncount-n_diff)/2                                     !
         n1st=nstart+nshift                                           !
         n2nd=nstart+nshift+n_diff-1                                  !--Correct for
         do ns=n1st,n2nd                                              !--Truncation Error
            n_slice(ns)=n_slice(ns)+1                                 !
         end do !-----------------------------------------------------!
         b_length=cvac*tpulse/hundred
         ns_tot=b_length/wavelnth  ! total number of wavelengths in the bunch
         dz_slice=wavelnth*dble(ns_tot)/dble(ncount) ! distance between slices
c
         z_avg=zero  !------------------------------------------------!
         do ns=1,nslices                                              !
            z_ns=b_length*dble(ns-1)/dble(ncount)                     !--Find average z(m)
            z_avg=z_avg+z_ns*Q_slice(ns)/Q_sum                        !        
         end do  !----------------------------------------------------!
c
         sumwtotns=zero !---------------------------------------------!
         do ns=ns_begin,ns_end                                        !
            if(sumw(ns).gt.zero) sumwtotns=sumwtotns+sumw(ns)         !
         end do                                                       !
         sumwtot=zero                                                 !
         if(numprocs.gt.1) then                                       !
               call mpi_reduce(sumwtotns,sumwtot,1,                   !
     1                        mpi_double_precision,mpi_sum,           !
     2                        0,mpi_comm_world,ier)                   !
               call mpi_bcast(sumwtot,1,mpi_double_precision,         !
     1                         0,mpi_comm_world,ier)                  !
           else                                                       !
               sumwtot=sumwtotns                                      !
         endif                                                        !
         pz_avgns=zero                                                !
         x_avgns=zero                                                 !
         y_avgns=zero                                                 !
         px_avgns=zero                                                !
         py_avgns=zero                                                !
         do ns=ns_begin,ns_end                                        !
            nss=ns-ns_begin+1                                         !
            if(ipart(ns).gt.0) then                                   !
              do k=1,ipart(ns)                                        !
                x_avgns=x_avgns+weights(k,nss)*scale*x(k,nss)         !
                y_avgns=y_avgns+weights(k,nss)*scale*y(k,nss)         !
                px_avgns=px_avgns+weights(k,nss)*px(k,nss)            !
                py_avgns=py_avgns+weights(k,nss)*py(k,nss)            !
                pz_avgns=pz_avgns+weights(k,nss)*pz(k,nss)            !
              end do                                                  !
            endif                                                     !
         end do                                                       !
         x_avg=zero                                                   !
         yavg=zero                                                    !
         px_avg=zero                                                  !
         py_avg=zero                                                  !
         pz_avg=zero                                                  !
         if(numprocs.gt.1) then                                       !---Find average x,y,
               call mpi_reduce(x_avgns,x_avg,1,                       !---px,py,pz
     1                   mpi_double_precision,mpi_sum,                !
     2                   0,mpi_comm_world,ier)                        !
               call mpi_reduce(y_avgns,y_avg,1,                       !
     1                   mpi_double_precision,mpi_sum,                !
     2                   0,mpi_comm_world,ier)                        !
               call mpi_reduce(px_avgns,px_avg,1,                     !
     1                   mpi_double_precision,mpi_sum,                !
     2                   0,mpi_comm_world,ier)                        !
               call mpi_reduce(py_avgns,py_avg,1,                     !
     1                   mpi_double_precision,mpi_sum,                !
     2                   0,mpi_comm_world,ier)                        !
               call mpi_reduce(pz_avgns,pz_avg,1,                     !
     1                   mpi_double_precision,mpi_sum,                !
     2                   0,mpi_comm_world,ier)                        !
           else                                                       !
               x_avg=x_avgns                                          !
               y_avg=y_avgns                                          !
               px_avg=px_avgns                                        !
               py_avg=py_avgns                                        !
               pz_avg=pz_avgns                                        !
         endif                                                        !
         if(myid.eq.0) then                                           !
            x_avg=x_avg/sumwtot                                       !
            y_avg=y_avg/sumwtot                                       !
            px_avg=px_avg/sumwtot                                     !
            py_avg=py_avg/sumwtot                                     !
            pz_avg=pz_avg/sumwtot                                     !
         endif                                                        !
         if(numprocs.gt.1) then                                       !
            call mpi_bcast(x_avg,1,mpi_double_precision,              !
     1                     0,mpi_comm_world,ier)                      !
            call mpi_bcast(y_avg,1,mpi_double_precision,              !
     1                     0,mpi_comm_world,ier)                      !
            call mpi_bcast(px_avg,1,mpi_double_precision,             !
     1                     0,mpi_comm_world,ier)                      !
            call mpi_bcast(py_avg,1,mpi_double_precision,             !
     1                     0,mpi_comm_world,ier)                      !
            call mpi_bcast(pz_avg,1,mpi_double_precision,             !
     1                     0,mpi_comm_world,ier)                      !
         endif !------------------------------------------------------!
c
         if(S2E_method.eq.'random') then
           idum=-1-2*myid !-------------------------------------------!
           first_call=ran3(idum)                                      !
           ncc=0                                                      !         
           do ns=ns_begin,ns_end                                      !
             nss=ns-ns_begin+1                                        !
             if(beam_ns(ns).gt.zero) then                             !
               do i=1,n_slice(ns)                                     !
                 ncc=ncc+1                                            !
                 k=min(1+int(ipart(ns)*ran3(i)),ipart(ns))            !
                 shift=one+dither*(-one+two*ran3(i))                  !
                 x_out(ncc,myid+1)=shift*scale*x(k,nss)               !--- Load particles
                 y_out(ncc,myid+1)=shift*scale*y(k,nss)               !
                 px_out(ncc,myid+1)=shift*px(k,nss)                   !
                 py_out(ncc,myid+1)=shift*py(k,nss)                   !
                 pz_out(ncc,myid+1)=pz(k,nss)                         !
                 z_ns=b_length*dble(ns-1)/dble(ncount)                !
                 psi_out(ncc,myid+1)=z_ns+dz_slice*ran3(i)            !
               end do                                                 !
             endif                                                    !
           end do !---------------------------------------------------!
           ns2e(1,myid+1)=ncc
         endif
c
         if((S2E_method.eq.'Waterbag').or.  !-------------------------!
     1          (S2E_method.eq.'Gaussian'))then                       !
           gam_avgns=zero                                             !
           xxns=zero                                                  !
           yyns=zero                                                  !
           xpxpns=zero                                                !
           ypypns=zero                                                !
           xxpns=zero                                                 !
           yypns=zero                                                 !
           do ns=ns_begin,ns_end                                      !
             nss=ns-ns_begin+1                                        !
             if(beam_ns(ns).gt.zero) then                             !
               do k=1,ipart(ns)                                       !
                 gam_avgns=gam_avgns+weights(k,nss)*
     1              sqrt(one+px(k,nss)**2+py(k,nss)**2+pz(k,nss)**2)
                 xxns=xxns+weights(k,nss)*(scale*x(k,nss)-x_avg)**2   !
                 yyns=yyns+weights(k,nss)*(scale*y(k,nss)-y_avg)**2   !
                 xpxpns=xpxpns+weights(k,nss)                         !
     1                              *((px(k,nss)-px_avg)/pz(k,nss))**2!
                 ypypns=ypypns+weights(k,nss)                         !
     1                              *((py(k,nss)-py_avg)/pz(k,nss))**2!
                 xxpns=xxpns+weights(k,nss)*(scale*x(k,nss)-x_avg)    !
     1                                   *(px(k,nss)-px_avg)/pz(k,nss)!
                 yypns=yypns+weights(k,nss)*(scale*y(k,nss)-y_avg)    !
     1                                   *(py(k,nss)-py_avg)/pz(k,nss)!
                end do                                                !
             endif                                                    !
           end do                                                     !
           gam_avg=zero                                               !
           xx=zero                                                    !
           yy=zero                                                    !
           xpxp=zero                                                  !
           ypyp=zero                                                  !
           xxp=zero                                                   !
           yyp=zero                                                   ! 
           if(numprocs.gt.1) then                                     !
               call mpi_reduce(gam_avgns,gam_avg,1,                   !
     1              mpi_double_precision,mpi_sum,0,mpi_comm_world,ier)!
               call mpi_reduce(xxns,xx,1,mpi_double_precision,        !
     1                   mpi_sum,0,mpi_comm_world,ier)                !
               call mpi_reduce(yyns,yy,1,mpi_double_precision,        !
     1                   mpi_sum,0,mpi_comm_world,ier)                !
               call mpi_reduce(xxpns,xxp,1,mpi_double_precision,      !
     1                   mpi_sum,0,mpi_comm_world,ier)                !
               call mpi_reduce(xpxpns,xpxp,1,mpi_double_precision,    !
     1                   mpi_sum,0,mpi_comm_world,ier)                !
               call mpi_reduce(ypypns,ypyp,1,mpi_double_precision,    !
     1                   mpi_sum,0,mpi_comm_world,ier)                !
               call mpi_reduce(yypns,yyp,1,mpi_double_precision,      !
     1                   mpi_sum,0,mpi_comm_world,ier)                !
             else                                                     !
               gam_avg=gam_avgns                                      !
               xx=xxns                                                !
               yy=yyns                                                !
               xpxp=xpxpns                                            !
               ypyp=ypypns                                            !
               xxp=xxpns                                              !
               yyp=yypns                                              !
           endif                                                      !
           if(myid.eq.0) then                                         !
              gam_avg=gam_avg/sumwtot                                 !
              xx=xx/sumwtot                                           !
              yy=yy/sumwtot                                           !
              xpxp=xpxp/sumwtot                                       !
              ypyp=ypyp/sumwtot                                       !
              xxp=xxp/sumwtot                                         !
              yyp=yyp/sumwtot                                         !
c                                                                     !
              epsx_geom=sqrt(xx*xpxp-xxp**2)                          !
              epsy_geom=sqrt(yy*ypyp-yyp**2)                          !
              epsx=gam_avg*epsx_geom                                  !
              epsy=gam_avg*epsy_geom                                  !
              Twissax=-xxp/epsx_geom                                  !
              Twissay=-yyp/epsy_geom                                  !
              Twissbx=xx/epsx_geom                                    !
              Twissby=yy/epsy_geom                                    !
              Twissgx=(one+Twissax**2)/Twissbx                        !
              Twissgy=(one+Twissay**2)/Twissby                        !
c                                                                     !
              idum=-1-2*myid                                          !
              first_call=ran3(idum)                                   !
              sinthx=-Twissax/sqrt(one+Twissax**2)                    !
              costhx=one/sqrt(one+Twissax**2)                         !
              sinthy=-Twissay/sqrt(one+Twissay**2)                    !
              costhy=one/sqrt(one+Twissay**2)                         !
              sig_x=sqrt(epsx_geom*Twissbx)                           !
              sig_px=sqrt(epsx_geom*(gam_avg**2)/Twissbx)             !
              sig_y=sqrt(epsy_geom*Twissby)                           !
              sig_py=sqrt(epsy_geom*(gam_avg**2)/Twissby)             !
              if(S2E_method.eq.'Gaussian') then                       !
                 do k=1,nparts_out                                    !
                    xt=sig_x*gaussdev(k)/Twissbx                      !
                    pxt=sig_px*gaussdev(k)                            !
                    x_out(k,1)=x_avg+Twissbx*(costhx*xt+sinthx*pxt)   !
                    px_out(k,1)=px_avg-sinthx*xt+costhx*pxt           !
                    yt=sig_y*gaussdev(k)/Twissby                      !
                    pyt=sig_py*gaussdev(k)                            !
                    y_out(k,1)=y_avg+Twissby*(costhy*yt+sinthy*pyt)   !
                    py_out(k,1)=py_avg-sinthy*yt+costhy*pyt           !
                 end do                                               !
              endif                                                   !
              if(S2E_method.eq.'Waterbag') then                       !
                 do k=1,nparts_out                                    !
                    xran=sig_x*(one-two*ran3(k))                      !
                    xt=two*xran                                       !
                    pxt=two*sig_px*sqrt(one-(xran/sig_x)**2)          !
     1                                        *(one-two*ran3(k))      !
                    x_out(k,1)=x_avg+costhx*xt+sinthx*pxt             !
                    px_out(k,1)=px_avg+(-sinthx*xt+costhx*pxt)        !
                    yran=sig_y*(one-two*ran3(k))                      !
                    yt=two*yran                                       !
                    pyt=two*sig_py*sqrt(one-(yran/sig_y)**2)          !
     1                                        *(one-two*ran3(k))      !                
                    y_out(k,1)=y_avg+costhy*yt+sinthy*pyt             !
                    py_out(k,1)=py_avg+(-sinthy*yt+costhy*pyt)        !
                 end do                                               !
              endif                                                   !
           endif                                                      !
c                                                                     !
           ncc=0                                                      !
           do ns=ns_begin,ns_end                                      !
             nss=ns-ns_begin+1                                        !
             if(beam_ns(ns).gt.zero) then                             !
               do i=1,n_slice(ns)                                     !
                 ncc=ncc+1                                            !
                 k=min(1+int(ipart(ns)*ran3(i)),ipart(ns))            !
                 pz_out(ncc,myid+1)=pz(k,nss)                         !
                 z_ns=b_length*dble(ns-1)/dble(ncount)                !
                 psi_out(ncc,myid+1)=z_ns+dz_slice*ran3(i)            !
               end do                                                 !
             endif                                                    !
           end do                                                     !
           ns2e(1,myid+1)=ncc                                         !
         endif !------------------------------------------------------!
      endif  ! for S2E = 'on'
c
   10 if(myid.eq.0) then  !-------------------------------------------!
   	    zmon=scale*a1(nmodes+1,ns_begin)                              !
        if ( (mod(zmon,dlength)<scale*dx) )                           !
     1              write (6,'(i4," %, z = ",f5.1," m")')             !---  status monitor
     2              int(100.0d0*zmon/zlength), zmon                   !
      endif  !--------------------------------------------------------!
      go to 2
      end
c**********************************************************************
      subroutine func(z,j,ns,nss,xmax,Wakefield)
      use particles
      use particles_func
      use wiggler
      use wigg_errors
      use focus
      use modes
      use harmonics
      use bessel
      use rf_acc
      use multi_beams
      use RFaccel
      use canted_poles
      use wakes
      use waveguides
      use numbers
      implicit double precision(a-h,o-z)
      parameter(biscale=14.0625d0,b01=.0360768d0,b02=.0045813d0,
     1          b03=.2659732d0,b04=1.2067492d0,b05=3.0899424d0,
     2          b06=3.5156229d0,b10=.00301532d0,b12=.00032411d0,
     3          b13=.02658733d0,b14=.15084934d0,b15=.51498869d0,
     4          b16=.87890594d0)
      character*3 Wakefield
      character*8 RFfield,slippage
      character*11 drifttube
      character*12 tprofile,teprofile
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
      common/taper/zsat,dbw,dbw_sqr,dzw,nwig_harms
      common/self/rb2,xavgs,yavgs,rbavg2,sfact,rb0avg2,iself
      common/RFacc/RFfield,agkwrf,bgkwrf,hagkwrf,hbgkwrf,n1stpass,
     3             drifttube
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      dimension s1(nmodes),s2(nmodes),hlnfac(iparts),fxy(iparts)
      allocatable rkl(:,:)
c----------------------------------------------------------------------
c
      bx=zero  !-------------------------------------------------------!
      by=zero                                                          !
      bz=zero                                                          !
      do k=1,ipart(ns)                                                 !
         gama(k)=sqrt(one+px(k,nss)**2+py(k,nss)**2+pz(k,nss)**2)      !
         pzinv=one/pz(k,nss)                                           !
         fx(k,nss)=px(k,nss)*pzinv                                     !
         fy(k,nss)=py(k,nss)*pzinv                                     !
         fpx(k,nss)=zero                                               !
         fpy(k,nss)=zero                                               !
         fpz(k,nss)=zero                                               !
      end do                                                           !--Handle Single-
      if((emmodes.eq.'hermite').or.(emmodes.eq.'laguerre')) then       !--Particle Orbits
         do k=1,ipart(ns)                                              !
            fpsi(k,nss)=domega*(one-gama(k)/pz(k,nss))                 !
         end do                                                        !                                                       !
      endif  !---------------------------------------------------------!
c
c------- handle wiggler dynamics
c
      nwig=0 ! avoid uninitialised error that can happen in some cases
      do i=1,nsegbw
         isegbw=0
         bwz=zero
         bwx=zero
         if((z.ge.zstart(i)).and.(z.le.zstop(i))) then
               isegbw=i
               nwig=ntype(isegbw)
               dkwz=rkw(i)
               dkwz2=dkwz**2
               cs=cos(rkw(i)*(z-zstart(i)))
               sn=sin(rkw(i)*(z-zstart(i)))
c              cs=cos(rkw(i)*z)
c              sn=sin(rkw(i)*z)
               if((nwig.eq.5).or.(nwig.eq.6)) then
                  hnumber=dble(nwigh(i))
                  csh=cos(hnumber*rkw(i)*z)
                  snh=sin(hnumber*rkw(i)*z)
               endif        
               betawi=betaw(i)*rkw(i)
               bwz=betawi
               if((z.le.zstart(i)+twopi*dble(nwigup(i))/rkw(i))
     1               .and.(nwigup(i).gt.0)) then
                  zprime=rkw(i)*(z-zstart(i))
                  r4nwig=dble(4*nwigup(i))
                  bwz=betawi*sin(zprime/r4nwig)**2
                  bwx=betawi*sin(zprime/dble(2*nwigup(i)))/r4nwig
               endif
               if((z.ge.zstop(i)-twopi*dble(nwigdn(i))/rkw(i))
     1               .and.(nwigdn(i).gt.0)) then                  
                  zprime=rkw(i)*(z-zstop(i))+twopi*dble(nwigdn(i))
                  r4nwig=dble(4*nwigdn(i))
                  bwz=betawi*cos(zprime/r4nwig)**2
                  bwx=-betawi*sin(zprime/dble(2*nwigdn(i)))/r4nwig
               endif
               if(nsegbw.eq.1) then
                    if((abs(dbw).gt.deci).and.(z.gt.zsat)) then
                             bwz=betaw(1)*(one+dbw*(z-zsat))
                             if(nwig.eq.2) bwx=betaw(1)*dbw/dkwz
                    endif
                    if((abs(dbw_sqr).gt.deci).and.(z.gt.zsat)) then
                             bwz=betaw(1)*(one+dbw_sqr*(z-zsat)**2
     1                                                 /(xmax-zsat)**2)
                    endif
                    if((abs(dzw).gt.deci).and.(z.gt.zsat)) then
                             zkw=z+half*dzw*(z-zsat)**2
                             dkwz=one+dzw*(z-zsat)
                             dkwz2=dkwz**2
                             cs=cos(zkw)
                             sn=sin(zkw)
                             if(nwig.eq.2) then
                                 bwz=bwz*cosh(rgkw)/cosh(dkwz*rgkw)
                                 bwx=-rgkw*dzw*bwz*tanh(rgkw*dkwz)/dkwz
                             endif
                    endif
               endif
               if(nsegbw.gt.1) then
                    if((abs(dbwseg(i)).gt.deci)
     1                                     .and.(z.gt.zsatseg(i))) then
                            bwz=betaw(i)*(one+dbwseg(i)*(z-zsatseg(i)))
                            if(nwig.eq.2) bwx=betaw(i)*dbwseg(i)/dkwz
                    endif
                    if((abs(dzwseg(i)).gt.deci)
     1                                     .and.(z.gt.zsatseg(i))) then
                             zkw=z+half*dzwseg(i)*(z-zsatseg(i))**2
                             dkwz=one+dzwseg(i)*(z-zsatseg(i))
                             dkwz2=dkwz**2
                             cs=cos(zkw)
                             sn=sin(zkw)
                             if(nwig.eq.2) then
                                 bwz=bwz*cosh(rgkw)/cosh(dkwz*rgkw)
                                 bwx=-rgkw*dzw*bwz*tanh(rgkw*dkwz)/dkwz
                             endif
                    endif
               endif
         endif
         if(isegbw.gt.0) then
c
            if(iberr.ne.0) then  !-------------------------------------!
               istep=int(one+(z-zstart(isegbw))/dzerr(isegbw))         !
               deltaz=z-zstart(isegbw)-dble(istep-1)*dzerr(isegbw)     !
               dberr=bwerr(isegbw,istep+1)-bwerr(isegbw,istep)         !--Add Field Error
               dbwz=bwerr(isegbw,istep)                                !--to Wiggler Amplitude
     1                     +dberr*sin(halfpi*deltaz/dzerr(isegbw))**2  !
               bwz=bwz+dbwz                                            !
            endif  !---------------------------------------------------!
c
            if((nwig.eq.1).or.(nwig.eq.5)) then  !---------------------!
               bperp=quarter*(bwz*cs+bwx*sn)                           !
               bwzt=quarter*rt2*bwz*sn                                 !
               csa=cos(wigangle(isegbw))                               !
               sna=sin(wigangle(isegbw))                               !
               do k=1,ipart(ns)                                        !
                 xt=x(k,nss)*csa+y(k,nss)*sna                          !
                 yt=-x(k,nss)*sna+y(k,nss)*csa                         !
                 argx=dkwz*rti2*xt                                     !
                 argy=dkwz*rti2*yt                                     !
                 explus=exp(argx)                                      !
                 exmin=one/explus                                      !
                 eyplus=exp(argy)                                      !
                 eymin=one/eyplus                                      !
                 chx=explus+exmin                                      !
                 shy=eyplus-eymin                                      !
                 bxt=bperp*shy*(explus-exmin)                          !
                 byt=bperp*chx*(eyplus+eymin)                          !
                 bx(k)=bx(k)+bxt*csa+byt*sna                           !--Determine PPF
                 by(k)=by(k)-bxt*sna+byt*csa                           !--Wiggler Field
                 bz(k)=bz(k)-bwzt*chx*shy                              !
               end do                                                  !
            endif                                                      !
            if(nwig.eq.5) then                                         !
               bperph=quarter*hnumber*(bwz*csh+bwx*snh)                !
               bwzh=hnumber*quarter*rt2*bwz*snh                        !
               do k=1,ipart(ns)                                        !
                 argx=hnumber*dkwz*rti2*x(k,nss)                       !
                 argy=hnumber*dkwz*rti2*y(k,nss)                       !
                 explus=exp(argx)                                      !
                 exmin=one/explus                                      !
                 eyplus=exp(argy)                                      !
                 eymin=one/eyplus                                      !
                 chy=eyplus+eymin                                      !
                shx=explus-exmin                                       !
                 bx(k)=bx(k)+bperph*chy*(explus+exmin)                 !
                 by(k)=by(k)+bperph*shx*(eyplus-eymin)                 !
                 bz(k)=bz(k)-bwzh*chy*shx                              !
               end do                                                  !
            endif  !---------------------------------------------------!
c
            if((nwig.eq.1).and.(nsegbw.eq.1).and.(z.gt.zsat)) then  !--!
               if((abs(dzw).gt.deci).or.(abs(dbw).gt.deci)) then       !
                  bwzn=bwz/(akw(1)*dkwz)                               !
                  do k=1,ipart(ns)                                     !
                    argx=dkwz*rti2*x(k,nss)                            !
                    argy=dkwz*rti2*y(k,nss)                            !
                    explus=exp(argx)                                   !
                    exmin=one/explus                                   !
                    eyplus=exp(argy)                                   !
                    eymin=one/eyplus                                   !--Determine Fringing
                    chx=half*(explus+exmin)                            !--Fields for PPF
                    shx=half*(explus-exmin)                            !--Wiggler
                    chy=half*(eyplus-eymin)                            !
                    shy=half*(eyplus-eymin)                            !
                    bxt=(dbw-dzw)*chx*chy                              !
     1                              +dzw*(argx*shx*chy+argy*chx*shy)   !
                    byt=(dbw-dzw)*shx*shy                              !
     1                              +dzw*(argx*chx*shy+argy*shx*chy)   !
                    bx(k)=bx(k)+bwzn*bxt*sn                            !
                    by(k)=by(k)+bwzn*byt*sn                            !
                  end do                                               !
               endif                                                   !
            endif  !---------------------------------------------------!
c
            if(nwig.eq.2) then  !--------------------------------------!
               tfac1=wfac1(isegbw)/dkwz                                !
               tfac2=wfac2(isegbw)/dkwz2                               !
               tfac3=wfac3(isegbw)/dkwz2                               !
               tfac4=wfac4(isegbw)/dkwz2                               !
               bperp=bwz*sn-bwx*cs                                     !
               dbp=bwz*cs*dzw                                          !
               do k=1,ipart(ns)                                        !
                 xa=x(k,nss)/axkw(isegbw)                              !
                 xam3=xa**m3(isegbw)                                   !
                 argy=dkwz*y(k,nss)                                    !--Determine Polynomial
                 eyplus=exp(argy)                                      !--Focussing Wiggler
                 eyminus=one/eyplus                                    !
                 shy=half*(eyplus-eyminus)                             !
                 chy=half*(eyplus+eyminus)                             !
                 xx=one+half*xa**m1(isegbw)                            !
                 yy=argy*chy-shy                                       !
                 bx(k)=bx(k)+bperp*tfac1*xa*(shy*xam3-tfac2*yy)        !
     1                -dbp*tfac1*xa*(yy*xam3-tfac2*(argy*shy-three*yy))!
                 by(k)=by(k)+bperp*(xx*chy-tfac4*xam3*argy*shy)        !
     1                                 -dbp*argy*(xx*shy-yy*tfac4*xam3)!
                 bz(k)=bz(k)+bwz*cs*(xx*shy-tfac4*yy*(xam3+tfac3))     !
               end do                                                  !
            endif  !---------------------------------------------------!
c
            if((nwig.eq.3).or.(nwig.eq.6)) then  !---------------------!
               bperp=half*(bwz*sn-bwx*cs)                              !
               bpar=half*bwz*cs                                        !
               csa=cos(wigangle(isegbw))                               !
               sna=sin(wigangle(isegbw))                               !
               do k=1,ipart(ns)                                        !
                 argy=dkwz*(-x(k,nss)*sna+y(k,nss)*csa)                !
                 eyplus=exp(argy)                                      !
                 eyminus=one/eyplus                                    !
                 byt=bperp*(eyplus+eyminus)                            !
                 bx(k)=bx(k)+byt*sna                                   !
                 by(k)=by(k)+byt*csa                                   !--Determine FPF
                 bz(k)=bz(k)+bpar*(eyplus-eyminus)                     !--Wiggler Field
               end do                                                  !
            endif                                                      !
            if(nwig.eq.6) then                                         !
               bperph=half*hnumber*(bwz*snh-bwx*csh)                   !
               bwzh=half*hnumber*bwz*csh                               !
               do k=1,ipart(ns)                                        !
                 argx=hnumber*dkwz*x(k,nss)                            !
                 explus=exp(argx)                                      !
                 exminus=one/explus                                    !
                 bx(k)=bx(k)+bperph*(explus+exminus)                   !
                 bz(k)=bz(k)+bwzh*(explus-exminus)                     !
               end do                                                  !
            endif  !---------------------------------------------------!
c
            if(nwig.eq.9) then  !--------------------------------------!
               ellipticK=sqrt(one+polar**2)/rt2                        !
               bperp1=half*ellipticK*(bwz*sn-bwx*cs)                   !
               bpar1=half*ellipticK*bwz*cs                             !
               sine=sn*cosinephi+cs*sinephi                            !
               cosine=cs*cosinephi-sn*sinephi                          !
               bperp2=half*ellipticK*(bwz*sine-bwx*cosine)             !
               bpar2=half*ellipticK*bwz*cosine                         !
               if(polar.ge.zero) then                                  !
                  do k=1,ipart(ns)                                     !
                     argy=dkwz*y(k,nss)                                !
                     eyplus=exp(argy)                                  !
                     coshkwy=eyplus+one/eyplus                         !
                     sinhkwy=eyplus-one/eyplus                         !
                     argx=dkwz*x(k,nss)                                !
                     explus=exp(argx)                                  !
                     coshkwx=explus+one/explus                         !
                     sinhkwx=explus-one/explus                         !
                     bx(k)=bx(k)+(bperp2*coshkwx-bperp1*coshkwy)/rt2   !--Determine APPLE-II
                     by(k)=by(k)+(bperp2*coshkwx+bperp1*coshkwy)/rt2   !--Wiggler Field
                     bz(k)=bz(k)+bpar1*sinhkwy+bpar2*sinhkwx           !
                  end do                                               !
               endif                                                   !
               if(polar.lt.zero) then                                  !
                  do k=1,ipart(ns)                                     !
                     argy=dkwz*y(k,nss)                                !
                     eyplus=exp(argy)                                  !
                     coshkwy=eyplus+one/eyplus                         !
                     sinhkwy=eyplus-one/eyplus                         !
                     argx=dkwz*x(k,nss)                                !
                     explus=exp(argx)                                  !
                     coshkwx=explus+one/explus                         !
                     sinhkwx=explus-one/explus                         !
                     bx(k)=bx(k)+(bperp1*coshkwy+bperp2*coshkwx)/rt2   !
                     by(k)=by(k)+(bperp1*coshkwy-bperp2*coshkwx)/rt2   !
                     bz(k)=bz(k)+bpar1*sinhkwy+bpar2*sinhkwx           !
                  end do                                               !
               endif                                                   !
            endif  !---------------------------------------------------!
c
            if((nwig.eq.3).and.(nwig_harms.ge.1)) then  !--------------!
               do nh=1,nwig_harms                                      !
                  rnwh=wigharms(nh,2)                                  !
                  bratio=wigharms(nh,1)                                !
                  hbwz=bratio*bwz                                      !
                  hbwx=bratio*bwx/rnwh                                 !
                  hcs=cos(rnwh*rkw(isegbw)*(z-zstart(isegbw)))         !
                  hsn=sin(rnwh*rkw(isegbw)*(z-zstart(isegbw)))         !
                  csa=cos(wigangle(isegbw))                            !
                  sna=sin(wigangle(isegbw))                            !--Add Harmonic Terms
                  hbpar=half*hbwz*hcs                                  !--To FPF WIggler Field
                  hbperp=half*(hbwz*hsn-hbwx*hcs)                      !
                  do k=1,ipart(ns)                                     !
                     hargy=rnwh*dkwz*(-x(k,nss)*sna+y(k,nss)*csa)      !
                     heyplus=exp(hargy)                                !
                     heyminus=one/heyplus                              !
                     hbyt=hbperp*(heyplus+heyminus)                    !
                     bx(k)=bx(k)+hbyt*sna                              !
                     by(k)=by(k)+hbyt*csa                              !
                     bz(k)=bz(k)+hbpar*(heyplus-heyminus)              !
                  end do                                               !            
               end do                                                  !
            endif  !---------------------------------------------------!
c
            if((nwig.eq.3).and.(nsegbw.eq.1).and.(z.gt.zsat)) then  !--!
               if((abs(dzw).gt.deci).or.(abs(dbw).gt.deci)) then       !
                  bwzn=bwz/(akw(1)*dkwz)                               !
                  do k=1,ipart(ns)                                     !
                    argy=dkwz*(-x(k,nss)*sna+y(k,nss)*csa)             !
                    eyplus=exp(argy)                                   !--Determine Fringing
                    eyminus=one/eyplus                                 !--Fields for FPF
                    chy=half*(eyplus-eyminus)                          !--Wiggler
                    shy=half*(eyplus-eyminus)                          !
                    byt=-(dbw-dzw)*chy-dzw*argy*shy                    !
                    by(k)=by(k)+bwzn*byt*cs                            !
                  end do                                               !
               endif                                                   !
            endif  !---------------------------------------------------!
c
            if(nwig.eq.8) then  !--------------------------------------!
               bperp=half*(bwz*sn-bwx*cs)                              !
               bpar=half*bwz*cs                                        !
               csa=cos(wigangle(isegbw))                               !
               sna=sin(wigangle(isegbw))                               !
               do k=1,ipart(ns)                                        !
                 argy=dkwz*(-x(k,nss)*sna+y(k,nss)*csa)                !
                 eyplus=exp(argy)                                      !
                 eyminus=one/eyplus                                    !
                 byt=bperp*(eyplus+eyminus)                            !
                 bx(k)=bx(k)+byt*sna-a_cant(i)*bwz*y(k,nss)            !
                 by(k)=by(k)+byt*csa-a_cant(i)*bwz*x(k,nss)            !
                 bz(k)=bz(k)+bpar*(eyplus-eyminus)                     !
               end do                                                  !--Determine Canted
               if((nsegbw.eq.1).and.(z.gt.zsat)) then                  !--Pole Face Wiggler
                  if((abs(dzw).gt.deci).or.(abs(dbw).gt.deci)) then    !
                     bwzn=bwz/(akw(1)*dkwz)                            !
                     do k=1,ipart(ns)                                  !
                       argy=dkwz*(-x(k,nss)*sna+y(k,nss)*csa)          !
                       eyplus=exp(argy)                                !
                       eyminus=one/eyplus                              !
                       chy=half*(eyplus-eyminus)                       !
                       shy=half*(eyplus-eyminus)                       !
                       byt=-(dbw-dzw)*chy-dzw*argy*shy                 !
                       by(k)=by(k)+bwzn*byt*cs                         !
                     end do                                            !
                  endif                                                !
               endif                                                   !
            endif  !---------------------------------------------------!
c
            if(nwig.eq.4) then  !--------------------------------------!
               do k=1,ipart(ns)                                        !
                  sinchi=y(k,nss)*cs-x(k,nss)*sn                       !
                  coschi=x(k,nss)*cs+y(k,nss)*sn                       !
                  r2=x(k,nss)**2+y(k,nss)**2                           !
                  s=r2/biscale                                         !
                  besi0=s*(s*(s*(s*(s*(b01+b02*s)+b03)+b04)+b05)+b06)  !
     1                   +one                                          !
                  besi1=s*(s*(s*(s*(s*(b10+b12*s)+b13)+b14)+b15)+b16)  !--Determine Helical
     1                   +half                                         !--Wiggler Field
                  besi2=(besi0-two*besi1)/(r2+tiny)                    !
                  bx(k)=bx(k)+besi0*(bwz*cs+bwx*sn)                    !
     1                 +besi2*(bwz*(x(k,nss)*coschi-y(k,nss)*sinchi)   !
     2                         -bwx*(x(k,nss)*sinchi+y(k,nss)*coschi)) !
                  by(k)=by(k)+besi0*(bwz*sn-bwx*cs)                    !               
     1                 +besi2*(bwz*(x(k,nss)*sinchi+y(k,nss)*coschi)   !
     2                         +bwx*(x(k,nss)*coschi-y(k,nss)*sinchi)) !
                  bz(k)=bz(k)+two*bwz*besi1*sinchi                     !
               end do                                                  !
            endif  !---------------------------------------------------!
      endif
      
      if(nwig.eq.7) call FieldMap(z,isegbw,ns,nss)  !-------------------------!--Fieldmap
      
      end do   ! End loop over wiggler segments
c
      if(nsegfoc.gt.0) then  !-----------------------------------------!
         do i=1,nsegfoc                                                !
            if((z.ge.zstt(i)).and.(z.le.zstp(i))) then                 !
               bfoc=bfocus(i)                                          !
               if(z.lt.zstt(i)+zupdown(i)) then                        !
                  dzratio=(z-zstt(i))/zupdown(i)                       !
                  bfoc=bfocus(i)*sin(halfpi*dzratio)**2                !
               endif                                                   !
               if(z.gt.zstp(i)-zupdown(i)) then                        !
                  dzratio=(z-zstp(i)+zupdown(i))/zupdown(i)            !
                  bfoc=bfocus(i)*cos(halfpi*dzratio)**2                !
               endif                                                   !
               if((nfoc(i).eq.1).and.(ipart(ns).gt.0)) then            !
                 do k=1,ipart(ns)                                      !--Determine the Dipole
                    bx(k)=bx(k)+bfoc*(y(k,nss)-Qy0(i))                 !--& Quadrupole Fields
                    by(k)=by(k)+bfoc*(x(k,nss)-Qx0(i))                 !
                 end do                                                !
               endif                                                   !
               if(nfoc(i).eq.2) then                                   !
                 do k=1,ipart(ns)                                      !
                    bx(k)=bx(k)+bfoc*csangle(i)                        !
                    by(k)=by(k)+bfoc*snangle(i)                        !
                 end do                                                !
               endif                                                   !
            endif                                                      !
         end do                                                        !
      endif  !---------------------------------------------------------!
c
      do k=1,ipart(ns)  !----------------------------------------------!
           fpx(k,nss)=fpx(k,nss)+by(k)-bz(k)*fy(k,nss)                 !--Include
           fpy(k,nss)=fpy(k,nss)+bz(k)*fx(k,nss)-bx(k)                 !--Magnetostatic
           fpz(k,nss)=fpz(k,nss)+bx(k)*fy(k,nss)-by(k)*fx(k,nss)       !--Fields
      end do  !--------------------------------------------------------!
c
      if(Wakefield.eq.'on') then  !------------------------------------!
         do k=1,ipart(ns)                                              !
              fpz(k,nss)=fpz(k,nss)-efield(nss)                        !--Include Wakefields
         end do                                                        !
      endif  !---------------------------------------------------------!
c
      if(((RFfield.eq.'dc').or.(RFfield.eq.'on'))  !-------------------!
     1                                       .and.(isegbw.gt.0)) then  !
         if((z.ge.z1acc(isegbw)).and.(z.le.z2acc(isegbw))) then        !
            do k=1,ipart(ns)                                           !
               fpz(k,nss)=fpz(k,nss)+Eacc(isegbw,ns)*gama(k)/pz(k,nss) !
            end do                                                     !
         endif                                                         !
      endif                                                            !
      if((RFfield.eq.'rf').and.(isegbw.gt.0)) then                     !
       if(moderf(isegbw).ge.0) then                                    !
         if((z.ge.z1acc(isegbw)).and.(z.le.z2acc(isegbw))) then        !
            lrf=lnumrf(isegbw)                                         !
            nrf=nnumrf(isegbw)                                         !
            if(drifttube.eq.'cylindrical') then                        !
               do k=1,ipart(ns)                                        !
c                 tau=one-psi(k,nss)/domega                            !
                  tau=z-psi(k,nss)/domega                              !
                  psirf=akzrf(isegbw)*z-omegarf(isegbw)*tau            !
                  sinrf(k)=sin(psirf)                                  !
                  cosrf(k)=cos(psirf)                                  !
                  r(k)=sqrt(x(k,nss)**2+y(k,nss)**2)                   !
                  stheta(k)=y(k,nss)/r(k)                              !
                  ctheta(k)=x(k,nss)/r(k)                              !
                  sinrfm(k)=sinrf(k)*ctheta(k)-cosrf(k)*stheta(k)      !
                  cosrfm(k)=cosrf(k)*ctheta(k)+sinrf(k)*stheta(k)      !
                  sinrfp(k)=sinrf(k)*ctheta(k)+cosrf(k)*stheta(k)      !
                  cosrfp(k)=cosrf(k)*ctheta(k)-sinrf(k)*stheta(k)      !
                  xbes(k)=aklnrf(isegbw)*r(k)                          !
               end do                                                  !
               xbesmax=aklnrf(isegbw)*rgkwrf(isegbw)                   !
               call bessels(lrf,ipart(ns),xbesmax)                     !
               do k=1,ipart(ns)                                        !
                  besjm(k)=dble(2*lrf)*besjl(k)/xbes(k)-besjp(k)       !
               end do                                                  !
               do l=1,lrf                                              !
                  do k=1,ipart(ns)                                     !
                     snrf=sinrf(k)                                     !
                     snrfm=sinrfm(k)                                   !
                     snrfp=sinrfp(k)                                   !
                     sinrfm(k)=snrfm*ctheta(k)+cosrfm(k)*stheta(k)     !
                     cosrfm(k)=cosrfm(k)*ctheta(k)-snrfm*stheta(k)     !
                     sinrfp(k)=snrfp*ctheta(k)+cosrfp(k)*stheta(k)     !
                     cosrfp(k)=cosrfp(k)*ctheta(k)-snrfp*stheta(k)     !
                     sinrf(k)=snrf*ctheta(k)+cosrf(k)*stheta(k)        !
                     cosrf(k)=cosrf(k)*ctheta(k)-snrf*stheta(k)        !
                  end do                                               !
               end do                                                  !
               if(j.eq.1) then                                         !
                  nRF1=6000+ns                                         !
                  RFphase=(akzrf(isegbw)+one)*z                        !
     1                           -omegarf(isegbw)*(z-psi(4,nss)/domega)!
                  if(ipart(ns).gt.0) then                              !
                     write(nRF1,905) scale*z,RFphase,fx(4,nss)*cosrf(4)!
  905                format(3(1x,1pe10.3))                             !
                  endif                                                !
               endif                                                   !
               Anorm=half*Eacc(isegbw,ns)                              !
               dgamdt(isegbw,ns)=zero                                  !
               do k=1,ipart(ns)                                        !
                  res=omegarf(isegbw)*gama(k)/pz(k,nss)-akzrf(isegbw)  !
                  t1=besjm(k)*cosrfm(k)+besjp(k)*cosrfp(k)             !
                  t2=besjm(k)*sinrfm(k)-besjp(k)*sinrfp(k)             !
                  t3=besjm(k)*sinrfm(k)+besjp(k)*sinrfp(k)             !
                  t4=besjm(k)*cosrfm(k)-besjp(k)*cosrfp(k)             !
                  t5=aklnrf(isegbw)*besjl(k)*cosrf(k)                  !
                  fpx(k,nss)=fpx(k,nss)-Anorm*(res*t1-t5*fy(k,nss)     !
     1                                            -Gloss(isegbw,ns)*t3)!
                  fpy(k,nss)=fpy(k,nss)+Anorm*(res*t2-t5*fx(k,nss)     !
     1                                            +Gloss(isegbw,ns)*t4)!
                  fpz(k,nss)=fpz(k,nss)                                !
     1              -Anorm*akzrf(isegbw)*(t1*fx(k,nss)-t2*fy(k,nss))   !
     2              -Anorm*Gloss(isegbw,ns)*(t3*fx(k,nss)+t4*fy(k,nss))!
                  dgamdt(isegbw,ns)=dgamdt(isegbw,ns)                  !
     1                               -Anorm*omegarf(isegbw)            !
     2              *weights(k,nss)*(t1*px(k,nss)-t2*py(k,nss))/gama(k)!
               end do                                                  !--Handle AC & DC
               if(sumw(ns).gt.zero) then                               !--Accelerating
                       dgamdt(isegbw,ns)=dgamdt(isegbw,ns)/sumw(ns)    !--Fields
                   else                                                !
                       dgamdt(isegbw,ns)=zero                          !
               endif                                                   !
            endif                                                      !
            if(drifttube.eq.'rectangular') then                        !
               faca=dble(lrf)*pi/agkwrf                                !
               facb=dble(nrf)*pi/bgkwrf                                !
               facakln=faca/aklnrf(isegbw)                             !
               facbkln=facb/aklnrf(isegbw)                             !
               do k=1,ipart(ns)                                        !
                  tau=z-psi(k,nss)/domega                              !
                  psirf=akzrf(isegbw)*z-omegarf(isegbw)*tau            !
                  sinrf(k)=sin(psirf)                                  !
                  cosrf(k)=cos(psirf)                                  !
                  sinrfm(k)=sin(faca*(x(k,nss)+hagkwrf))               !
                  cosrfm(k)=cos(faca*(x(k,nss)+hagkwrf))               !
                  sinrfp(k)=sin(facb*(y(k,nss)+hbgkwrf))               !
                  cosrfp(k)=cos(facb*(y(k,nss)+hbgkwrf))               !
               end do                                                  !
               Anorm=Eacc(isegbw,ns)                                   !
               dgamdt(isegbw,ns)=zero                                  !
               do k=1,ipart(ns)                                        !
                  res=omegarf(isegbw)*gama(k)/pz(k,nss)-akzrf(isegbw)  !
                  t1=facbkln*cosrfm(k)*sinrfp(k)*sinrf(k)              !
                  t2=facakln*sinrfm(k)*cosrfp(k)*sinrf(k)              !
                  t3=facbkln*cosrfm(k)*sinrfp(k)*cosrf(k)              !
                  t4=facakln*sinrfm(k)*cosrfp(k)*cosrf(k)              !
                  t5=aklnrf(isegbw)*cosrfm(k)*cosrfp(k)*cosrf(k)       !
                  fpx(k,nss)=fpx(k,nss)+Anorm*(res*t1+t5*fy(k,nss)     !
     1                                            +Gloss(isegbw,ns)*t3)!
                  fpy(k,nss)=fpy(k,nss)-Anorm*(res*t2+t5*fx(k,nss)     !
     1                                            +Gloss(isegbw,ns)*t4)!
                  fpz(k,nss)=fpz(k,nss)                                !
     1              +Anorm*akzrf(isegbw)*(t1*fx(k,nss)-t2*fy(k,nss))   !
     2              -Anorm*Gloss(isegbw,ns)*(t3*fx(k,nss)-t4*fy(k,nss))!
                  dgamdt(isegbw,ns)=dgamdt(isegbw,ns)                  !
     1                              -Anorm*omegarf(isegbw)             !
     2              *weights(k,nss)*(t1*px(k,nss)-t2*py(k,nss))/gama(k)!
               end do                                                  !
               if(sumw(ns).gt.zero) then                               !
                       dgamdt(isegbw,ns)=dgamdt(isegbw,ns)/sumw(ns)    !
                   else                                                !
                       dgamdt(isegbw,ns)=zero                          !
               endif                                                   !
            endif                                                      !
         endif                                                         !
       endif                                                           !
      endif  !---------------------------------------------------------!
c
      if(iself.ne.0) then  !-------------------------------------------!
       xavgs=zero                                                      !
       yavgs=zero                                                      !
       vzavg=zero                                                      !
       do k=1,ipart(ns)                                                !
         vzavg=vzavg+weights(k,nss)*pz(k,nss)/gama(k)                  !
         xavgs=xavgs+weights(k,nss)*x(k,nss)                           !
         yavgs=yavgs+weights(k,nss)*y(k,nss)                           !
       end do                                                          !
       vzavg=vzavg/sumw(ns)                                            !
       xavgs=xavgs/sumw(ns)                                            !
       yavgs=yavgs/sumw(ns)                                            !
       rbavg2=zero                                                     !
       do k=1,ipart(ns)                                                !
         rbavg2=rbavg2+weights(k,nss)*((x(k,nss)-xavgs)**2             !--Include Self
     1                                         +(y(k,nss)-yavgs)**2)   !--Electric(radial) &
       end do                                                          !--Magnetic(azimuthal)
       rbavg2=rbavg2/sumw(ns)                                          !--Fields
       rbmax2=twoquart*rbavg2                                          !
       sfact=half*xi(1,ns)**2*rb0avg2                                  !
       do k=1,ipart(ns)                                                !
         r2=(x(k,nss)-xavgs)**2+(y(k,nss)-yavgs)**2                    !
         sfacts=sfact/max(r2,rbmax2)                                   !
         fpx(k,nss)=fpx(k,nss)                                         !
     1              -sfacts*(vzavg-gama(k)/pz(k,nss))*(x(k,nss)-xavgs) !
         fpz(k,nss)=fpz(k,nss)+sfacts*vzavg*(px(k,nss)*(x(k,nss)-xavgs)!
     1                           +py(k,nss)*(y(k,nss)-yavgs))/pz(k,nss)!
         fpy(k,nss)=fpy(k,nss)-sfacts*(vzavg-gama(k)/pz(k,nss))        !
     1                                              *(y(k,nss)-yavgs)  !
       end do                                                          !
      endif  !---------------------------------------------------------!
      if(iprop.eq.0) return
c
c------ calculate perturbed trajectory increments, source currents
c------ and update fields
c
      s1=zero
      s2=zero
c
      if((emmodes.eq.'hermite').or.(emmodes.eq.'laguerre')) then
      do nh=1,nharms
        omeganh=omega(nh1(nh,ns))
        fratio=omeganh/domega
        alpha=asde2(nh,ns)
        w=asde1(nh,ns)
        w2=w**2  
        hfac=rt2/w
        wratio=w0(nh1(nh,ns),ns)/w
        fac=one/w0(nh1(nh,ns),ns)**2
c
        if(emmodes.eq.'hermite') then  !-------------------------------!
          modehl=1                                                     !
          do k=1,ipart(ns)                                             !
             arg(k)=hfac*x(k,nss)                                      !
          end do                                                       !
          call hermite(arg,lmax+2,ipart(ns),1)                         !
          do k=1,ipart(ns)                                             !
             arg(k)=hfac*y(k,nss)                                      !
          end do                                                       !
          call hermite(arg,nmax+2,ipart(ns),2)                         !
          do k=1,ipart(ns)                                             !
             r2=(x(k,nss)**2+y(k,nss)**2)/w2                           !
             phi=fratio*psi(k,nss)+r2*alpha                            !
             efac=exp(-r2)                                             !
             snphi(k)=efac*sin(phi)                                    !
             csphi(k)=efac*cos(phi)                                    !
          end do                                                       !
          do nm=nh1(nh,ns),nh2(nh,ns)                                  !  
             do k=1,ipart(ns)                                          !
                hlnfac(k)=al(k,lnum(nm,ns)+1,1)*al(k,nnum(nm,ns)+1,2)  !
             end do                                                    !
             do k=1,ipart(ns)                                          !
                temp=hlnfac(k)*(a1(nm,ns)*csphi(k)-a2(nm,ns)*snphi(k)) !
                fpx(k,nss)=fpx(k,nss)+xdir(nm)*fratio*fpsi(k,nss)*temp !
     1                                                         *wratio !
                fpy(k,nss)=fpy(k,nss)+ydir(nm)*fratio*fpsi(k,nss)*temp !
     1                                                         *wratio !
                fxy(k)=xdir(nm)*fx(k,nss)+ydir(nm)*fy(k,nss)           !
                fpz(k,nss)=fpz(k,nss)-fxy(k)*omeganh*temp*wratio       !
             end do                                                    !
             do k=1,ipart(ns)                                          !
                wfac(k)=weights(k,nss)*fxy(k)                          !
                s1(nm)=s1(nm)+wfac(k)*hlnfac(k)*csphi(k)*wratio        !
                s2(nm)=s2(nm)-wfac(k)*hlnfac(k)*snphi(k)*wratio        !
             end do                                                    !
          end do                                                       !
          if((killsde.ne.0).and.(ipart(ns).gt.0)) then                 !
              l1st=lnum(nh1(nh,ns),ns)                                 !
              lsde=lnum(nh1(nh,ns),ns)+2                               !
              n1st=nnum(nh1(nh,ns),ns)                                 !
              nsde=nnum(nh1(nh,ns),ns)+2                               !
              dfac=factorial(lsde)*factorial(n1st)*two**(1+lsde+n1st)  !
              sum1=zero                                                !
              sum2=zero                                                !
              if((l1st.eq.0).and.(n1st.eq.0)) then                     !
                  am1=a1(nh1(nh,ns),ns)                                !--Handle
                  am2=a2(nh1(nh,ns),ns)                                !--Gauss-Hermite
                  do k=1,ipart(ns)                                     !--Modes
                    hlnfac(k)=al(k,lsde+1,1)*al(k,1,2)                 !
     1                                      +al(k,1,1)*al(k,nsde+1,2)  !
                  end do                                               !
                  do k=1,ipart(ns)                                     !
                    sum1=sum1+wfac(k)*csphi(k)*hlnfac(k)*wratio        !
                    sum2=sum2-wfac(k)*snphi(k)*hlnfac(k)*wratio        !
                  end do                                               !
                else                                                   !
                  am1=a1(nh1(nh,ns),ns)+a1(nh1(nh,ns)+1,ns)            !
                  am2=a2(nh1(nh,ns),ns)+a2(nh1(nh,ns)+1,ns)            !
                  do k=1,ipart(ns)                                     !
                    hlnfac(k)=al(k,lsde+1,1)*al(k,n1st+1,2)            !
     1                           +al(k,l1st+1,1)*al(k,nsde+1,2)        !
                  end do                                               !
                  do k=1,ipart(ns)                                     !
                    hlnfac(k)=hlnfac(k)+al(k,nsde+1,1)*al(k,l1st+1,2)  !
     1                                 +al(k,n1st+1,1)*al(k,lsde+1,2)  !
                  end do                                               !
                  do k=1,ipart(ns)                                     !
                    sum1=sum1+wfac(k)*csphi(k)*hlnfac(k)*wratio        !
                    sum2=sum2-wfac(k)*snphi(k)*hlnfac(k)*wratio        !
                  end do                                               !
             endif                                                     !
             sum1=fac*sum1/(dfac*omeganh)                              !
             sum2=fac*sum2/(dfac*omeganh)                              !
             Asq=max(am1**2+am2**2,tiny2)                              !
             Xsde=two*(sum1*am2-sum2*am1)/Asq                          !
             Ysde=-two*(sum1*am1+sum2*am2)/Asq                         !
            else                                                       !
             Xsde=zero                                                 !
             Ysde=zero                                                 !
          endif                                                        !
        endif  !-------------------------------------------------------!
c
        if(emmodes.eq.'laguerre') then  !------------------------------!
        	allocate(rkl(ipart(ns),lmax+1))                              !
        	rkl=zero                                                     !
          modehl=2                                                     !
          do k=1,ipart(ns)                                             !
             rsqr(k)=x(k,nss)**2+y(k,nss)**2                           !
             r(k)=sqrt(rsqr(k))                                        !
             arg(k)=two*rsqr(k)/w2                                     !
          end do                                                       !
          call laguerre(arg,abs(lmax),nmax+1,ipart(ns))                !
          do k=1,ipart(ns)                                             !
             phi=fratio*psi(k,nss)+alpha*rsqr(k)/w2                    !
             snphi(k)=sin(phi)                                         !
             csphi(k)=cos(phi)                                         !
             elnfac(k)=exp(-rsqr(k)/w2)                                !
             wfac(k)=weights(k,nss)*fx(k,nss)                          !
             wfacy(k)=polar*weights(k,nss)*fy(k,nss)                   !
          end do                                                       !
          do k=1,ipart(ns)
             snltheta(k,1)=zero
             csltheta(k,1)=one
          end do
          do ll=1,lmax                                                 !
             do k=1,ipart(ns)                                          !
                snltheta(k,ll+1)=snltheta(k,ll)*x(k,nss)/r(k)          !
     1                                   +csltheta(k,ll)*y(k,nss)/r(k) !
                csltheta(k,ll+1)=csltheta(k,ll)*x(k,nss)/r(k)          !
     1                                   -snltheta(k,ll)*y(k,nss)/r(k) !
             end do                                                    !
          end do                                                       !
          do k=1,ipart(ns)                                             !
             rkl(k,1)=one                                              !
          end do                                                       !
          do ln=2,lmax+1                                               !
          	do k=1,ipart(ns)                                           !
          	   rkl(k,ln)=(hfac*r(k))*rkl(k,ln-1)	                     !
          	end do                                                     !
          end do                                                       !
          do nm=nh1(nh,ns),nh2(nh,ns)                                  !
            l=lnum(nm,ns)                                              !
            labs=abs(l)                                                !
            if(l.ge.0) then                                            !
                  sgnl=one                                             !
               else                                                    !
                  sgnl=-one                                            !
            endif                                                      !
            l1=labs+1                                                  !
            n1=nnum(nm,ns)+1                                           !
            do k=1,ipart(ns)                                           !--Handle
              eln=elnfac(k)*al(k,l1,n1)*rkl(k,l1)                      !--Gauss-Laguerre
              snphil=snphi(k)*csltheta(k,l1)
     1                                    +sgnl*csphi(k)*snltheta(k,l1)
              csphil=csphi(k)*csltheta(k,l1)
     1                                    -sgnl*snphi(k)*snltheta(k,l1)
              af1=a1(nm,ns)*csphil-a2(nm,ns)*snphil                    !--Modes
              af2=polar*(a1(nm,ns)*snphil+a2(nm,ns)*csphil)            !
              fpx(k,nss)=fpx(k,nss)+eln*fratio*fpsi(k,nss)*af1*wratio  !
              fpy(k,nss)=fpy(k,nss)-eln*fratio*fpsi(k,nss)*af2*wratio  !
              fpz(k,nss)=fpz(k,nss)                                    !
     1                -eln*omeganh*wratio*(fx(k,nss)*af1-fy(k,nss)*af2)!
              s1(nm)=s1(nm)+eln*wratio*(wfac(k)*csphil-wfacy(k)*snphil)!
              s2(nm)=s2(nm)-eln*wratio*(wfac(k)*snphil+wfacy(k)*csphil)!
            end do                                                     !
          end do                                                       !
          if((killsde.ne.0).and.(ipart(ns).gt.0)) then                 !
             lsde=lnum(nh1(nh,ns),ns)                                  !
             lsde1=abs(lsde)+1                                         !
             n1st=nnum(nh1(nh,ns),ns)                                  !
             nsde=nnum(nh1(nh,ns),ns)+1                                !
             dfac=four*dble(nsde)*polarfac*factorial(nsde+abs(lsde))   !
     1                                               /factorial(nsde)  !
             sum1=zero                                                 !
             sum2=zero                                                 !
             if(lsde.ge.0) then                                        !
                   sgnl=one                                            !
                else                                                   !
                   sgnl=-one                                           !
             endif                                                     !
             do k=1,ipart(ns)                                          !
             	 eln=elnfac(k)*al(k,lsde1,nsde+1)*rkl(k,lsde1)           !
               snphil=snphi(k)*csltheta(k,lsde1)
     1                                 +sgnl*csphi(k)*snltheta(k,lsde1)
               csphil=csphi(k)*csltheta(k,lsde1)
     1                                 -sgnl*snphi(k)*snltheta(k,lsde1)
               sum1=sum1+eln*wratio*(wfac(k)*csphil-wfacy(k)*snphil)   !
               sum2=sum2-eln*wratio*(wfac(k)*snphil+wfacy(k)*csphil)   !
             end do                                                    !
             sum1=fac*sum1/(dfac*omeganh)                              !
             sum2=fac*sum2/(dfac*omeganh)                              !
             Asq=max(a1(nh1(nh,ns),ns)**2+a2(nh1(nh,ns),ns)**2,tiny2)  !
             Xsde=-(sum1*a2(nh1(nh,ns),ns)-sum2*a1(nh1(nh,ns),ns))/Asq !
             Ysde=(sum1*a1(nh1(nh,ns),ns)+sum2*a2(nh1(nh,ns),ns))/Asq  !
            else                                                       !
             Xsde=zero                                                 !
             Ysde=zero                                                 !
          endif                                                        !
          deallocate(rkl)                                              !
        endif  !-------------------------------------------------------!
c
        if((emmodes.eq.'hermite').or.(emmodes.eq.'laguerre'))then  !---!
           if(sde.eq.'CWA') then                                       !
              Xsde=(one-alpha**2)/(omeganh*w2)                         !
              Ysde=two*alpha/(omeganh*w2)                              !
           endif                                                       !
           fsde1(nh,ns)=two*alpha/(omeganh*w)-w*Ysde                   !
           dw=fsde1(nh,ns)/w                                           !
           adw=alpha*dw                                                !
           fsde2(nh,ns)=two*(adw+(one-alpha**2)/(omeganh*w2))-two*Xsde !
           aKfac=half*fsde2(nh,ns)+(one+alpha**2)/(omeganh*w2)-adw     !
           do nm=nh1(nh,ns),nh2(nh,ns)                                 !--Gaussian
             aKln=aKfac*dble(abs(lnum(nm,ns))+modehl*nnum(nm,ns)+1)    !--Optical Modes
             fa1(nm,ns)=fac*factor(nm,ns)*s1(nm)+aKln*a2(nm,ns)        !
             fa2(nm,ns)=fac*factor(nm,ns)*s2(nm)-aKln*a1(nm,ns)        !
           end do                                                      !
           if((sde.eq.'CWA').and.(emmodes.eq.'hermite')) then  !-------!
              do nm=nh1(nh,ns),nh2(nh,ns)                              !
                 l=lnum(nm,ns)                                         !
                 n=nnum(nm,ns)                                         !
                 do nm1=nh1(nh,ns),nh2(nh,ns)                          !
                    l1=lnum(nm1,ns)                                    !
                    n1=nnum(nm1,ns)                                    !
                    if((l1.eq.l-2).and.(n1.eq.n)) then                 !
                       fa1(nm,ns)=fa1(nm,ns)-Xsde*a2(nm,ns)/four       !
     2                                      +Ysde*a1(nm,ns)/two        !
                       fa2(nm,ns)=fa2(nm,ns)+Xsde*a2(nm,ns)/four       !
     2                                      +Ysde*a1(nm,ns)/two        !
                    endif                                              !
                    if((l1.eq.l).and.(n1.eq.n-2)) then                 !
                       fa1(nm,ns)=fa1(nm,ns)-Xsde*a2(nm,ns)/four       !
     2                                      +Ysde*a1(nm,ns)/two        !
                       fa2(nm,ns)=fa2(nm,ns)+Xsde*a2(nm,ns)/four       !
     2                                      +Ysde*a1(nm,ns)/two        !
                    endif                                              !
                    if((l1.eq.l+2).and.(n1.eq.n)) then                 !
                       fa1(nm,ns)=fa1(nm,ns)                           !
     1                           -Xsde*dble((l+1)*(l+2))*a2(nm,ns)     !
     2                           +two*Ysde*dble((l+1)*(l+2))*a1(nm,ns) !
                       fa2(nm,ns)=fa2(nm,ns)                           !
     1                           +Xsde*dble((l+1)*(l+2))*a1(nm,ns)     !
     2                           -two*Ysde*dble((l+1)*(l+2))*a2(nm,ns) !
                    endif                                              !
                    if((l1.eq.l).and.(n1.eq.n+2)) then                 !
                       fa1(nm,ns)=fa1(nm,ns)                           !
     1                           -Xsde*dble((n+1)*(n+2))*a2(nm,ns)     !
     2                           +two*Ysde*dble((n+1)*(n+2))*a1(nm,ns) !
                       fa2(nm,ns)=fa2(nm,ns)                           !
     1                           +Xsde*dble((n+1)*(n+2))*a1(nm,ns)     !
     2                           -two*Ysde*dble((n+1)*(n+2))*a2(nm,ns) !
                    endif                                              !
                 end do                                                !
              end do                                                   !
           endif  !----------------------------------------------------!
           if((sde.eq.'CWA').and.(emmodes.eq.'laguerre')) then  !------!
              do nm=nh1(nh,ns),nh2(nh,ns)                              !
                 l=lnum(nm,ns)                                         !
                 n=nnum(nm,ns)                                         !
                 do nm1=nh1(nh,ns),nh2(nh,ns)                          !
                    l1=lnum(nm1,ns)                                    !
                    n1=nnum(nm1,ns)                                    !
                    if((l1.eq.l).and.(n1.eq.n+1)) then                 !
                       ln1=abs(l)+n+1                                  !
                       fa1(nm,ns)=fa1(nm,ns)+Xsde*dble(ln1)*a2(nm1,ns) !
     2                                      +Ysde*dble(ln1)*a1(nm1,ns) !
                       fa2(nm,ns)=fa2(nm,ns)-Xsde*dble(ln1)*a1(nm1,ns) !
     2                                      +Ysde*dble(ln1)*a2(nm1,ns) !
                    endif                                              !
                    if((l1.eq.l).and.(n1.eq.n-1)) then                 !
                       fa1(nm,ns)=fa1(nm,ns)+Xsde*dble(n)*a2(nm1,ns)   !
     2                                      -Ysde*dble(n)*a1(nm1,ns)   !
                       fa2(nm,ns)=fa2(nm,ns)-Xsde*dble(n)*a1(nm1,ns)   !
     2                                      -Ysde*dble(n)*a2(nm1,ns)   !
                    endif                                              !
                 end do                                                !
              end do                                                   !
           endif  !----------------------------------------------------!
           do nm=nh1(nh,ns),nh2(nh,ns)                                 !
              aki1(nm,ns)=fa1(nm,ns)                                   !
              aki2(nm,ns)=fa2(nm,ns)                                   !
           end do                                                      !
        endif  !-------------------------------------------------------!
      end do   ! End Loop over nharms
      endif
      return
      end
c**********************************************************************
      function nhout(nh,ns)
      implicit double precision(a-h,o-z)
      character*8 slippage
      character*12 tprofile,teprofile
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
c----------------------------------------------------------------------
c
c     Determine the unit numbers for the output of the Power_vs_z
c     files for the harmonic output
c
c----------------------------------------------------------------------
      nhout=998+25*nslices+(ns-1)*(nharms-1)
      do nhh=2,nh
         if(nhh.le.nh) nplast=1
         do npp=1,nplast
            nhout=nhout+1
         end do
      end do
      return
      end
c**********************************************************************
      function nehout(nh)
      implicit double precision(a-h,o-z)
      character*8 slippage
      character*12 tprofile,teprofile
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
c----------------------------------------------------------------------
c
c     Determine the unit numbers for the output of the Energy_vs_z
c     files for the harmonic output
c
c----------------------------------------------------------------------
      nehout=998+25*nslices+nslices*nharms+(nharms-1)
      do nhh=2,nh
         if(nhh.le.nh) nplast=1
         do npp=1,nplast
            nehout=nehout+1
         end do
      end do
      return
      end
c**********************************************************************
      subroutine Spent_Beam(beamloss)
      use particles
      use Slice_Diags
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0)
      character*12 tprofile,beamloss,teprofile
      character*8 slippage
      character*35 filename
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
c----------------------------------------------------------------------
c
c     This subroutine handles the output of the spent beam energy
c     distribution
c
c----------------------------------------------------------------------
      write(filename,101)
  101 format('Spent_Beam.txt')
      open(unit=10,file=filename,form='formatted',status='replace')
c
      if(beamloss.ne.'total') then
           if(nslices.eq.1) write(10,102)
  102      format(5x,'SPENT BEAM DISTRIBUTION',/)
           if(nslices.gt.1) write(10,103)
  103      format(15x,'SPENT BEAM DISTRIBUTION',/)
           if(nslices.gt.1) then
              sumw_max=zero
              do ns=1,nslices
                 sumw_max=max(sumw_max,sumw(ns))
              end do
              do ns=1,nslices
                 fac=sumw(ns)/sumw_max
                 do n=1,n_spent
                    P_Spent(n,ns)=fac*P_Spent(n,ns)
                 end do
              end do
              sumP=zero
              do ns=1,nslices
                 do n=1,n_spent
                    sumP=SumP+P_spent(n,ns)
                 end do
              end do              
              do ns=1,nslices
                 do n=1,n_spent
                    P_spent(n,ns)=P_spent(n,ns)/sumP
                 end do
              end do
           endif
           E_avg=zero
           do ns=1,nslices
              do n=1,n_spent
                 E_avg=E_avg+E_spent(n)*P_spent(n,ns)
              end do
           end do
           E_rms=zero
           do ns=1,nslices
              do n=1,n_spent
                 E_rms=E_rms+P_spent(n,ns)*(E_spent(n)-E_avg)**2
              end do
           end do
           E_rms=sqrt(E_rms)
           if(nslices.eq.1) write(10,105) E_avg,E_rms,E_rms/E_avg
  105      format(9x,'E_avg(MeV)=',1pe12.5,/,
     1            3x,'delta-E_rms(MeV)=',e12.5,/,
     2            1x,'(delta-E/E_avg)rms=',e12.5,/)
           if(nslices.gt.1) write(10,106) E_avg,E_rms,E_rms/E_avg
  106      format(16x,'E_avg(MeV)=',1pe12.5,/,
     1            10x,'delta-E_rms(MeV)=',e12.5,/,
     2            8x,'(delta-E/E_avg)rms=',e12.5,/)
           if(nslices.eq.1) then
              write(10,202)
  202         format(6x,'E(MeV)',9x,'P(E)')
              do n=1,n_spent
                 write(10,203) E_spent(n),P_spent(n,1)
  203            format(2(1x,1pe14.7))
              end do
           endif
           if(nslices.gt.1) then
              write(10,204)
  204         format(2x,'slice',5x,'time(s)',8x,'E(MeV)',10x,'P(E)')

              do ns=1,nslices
                 time=dble(ns-1)*dtpulse
                 do n=1,n_spent
                    write(10,205) ns,time,E_spent(n),P_spent(n,ns)
  205               format(1x,i6,3(1x,1pe14.7))
                 end do
              end do
           endif
         else
           write(10,301)
  301      format(/,10x,'TOTAL BEAM LOSS')
      endif
      close(unit=10)
      return
      end
c**********************************************************************
      subroutine Beam_Evol(itermax,ns_frst,ns_last)
      use mpi_parameters
      use mpi
      implicit double precision(a-h,o-z)
      character*12 tprofile,teprofile
      character*8 slippage
      character*39 filename3
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      allocatable zposition(:),z_w(:),dxc_avg(:),dyc_avg(:),dxc_w(:),
     1            dyc_w(:),xc_avg(:),yc_avg(:),xc_w(:),yc_w(:)
c----------------------------------------------------------------------
c
c------- output beam evolution data
c
      allocate(dxc_avg(itermax),dyc_avg(itermax),zposition(itermax),
     1         xc_avg(itermax),yc_avg(itermax))
      nwpart=ns_last-ns_frst+1
      zposition=zero
      dxc_avg=zero
      dyc_avg=zero
      xc_avg=zero
      yc_avg=zero
      do ns=ns_begin,ns_end
         if(slippage.ne.'off') then
             write(filename3,202) ns
  202        format('Beam_Evolution_slice=',i5.5,'.txt')
             open(unit=20,file=filename3,form='formatted',
     1                 status='replace')
             write(20,203) ns
  203        format(38x,'ELECTRON BEAM DIAGNOSTICS',//,43x,
     1            'Slice No.:',i6,//,5x,'z(m)',7x,'xc(cm)',6x,'yc(cm)',
     2            4x,'<x-xc>(cm)',2x,'<y-yc>(cm)',2x,'Rb,rms(cm)',3x,
     3            '<pz/mc>',5x,'<gamma>',5x,'psi/2*pi',4x,'ipart')
         endif
         if(slippage.eq.'off') then
             open(unit=20,file='Beam_Evolution.txt',
     1            form='formatted',status='replace')
             write(20,206)
  206        format(38x,'ELECTRON BEAM DIAGNOSTICS',//,5x,'z(m)',
     1            7x,'xc(cm)',6x,'yc(cm)',4x,'<x-xc>(cm)',2x,
     2            '<y-yc>(cm)',2x,'Rb,rms(cm)',3x,'<pz/mc>',5x,
     3            '<gamma>',5x,'psi/2*pi',4x,'ipart')
         endif
         rewind(18)
         do iters=1,itermax
           do ns2=ns_begin,ns_end
              read(18) zscale,xav,yav,dxrms,dyrms,Rbrm,pzav,gamav,
     1                 psiav,np
              if(ns.eq.ns_frst) zposition(iters)=zscale
              if((ns2.eq.ns).and.(np.gt.0)) then
                 write(20,100) zscale,xav,yav,dxrms,dyrms,Rbrm,
     1                         pzav,gamav,psiav,np
  100            format(9(1x,1pe11.4),1x,i6)
                 xc_avg(iters)=xc_avg(iters)+xav
                 yc_avg(iters)=yc_avg(iters)+yav
                 dxc_avg(iters)=dxc_avg(iters)+dxrms
                 dyc_avg(iters)=dyc_avg(iters)+dyrms
              endif
           end do
         end do
         close(unit=20)
      end do
      if(nslices.gt.1) then
         if(numprocs.gt.1) then
            allocate(dxc_w(itermax),dyc_w(itermax),z_w(itermax),
     1               xc_w(itermax),yc_w(itermax))
            dxc_w=zero
            dyc_w=zero
            xc_w=zero
            yc_w=zero
            z_w=zero
            ndim=itermax
            call mpi_reduce(xc_avg,xc_w,ndim,mpi_double_precision,
     1                      mpi_sum,0,mpi_comm_world,ier)
            call mpi_reduce(yc_avg,yc_w,ndim,mpi_double_precision,
     1                      mpi_sum,0,mpi_comm_world,ier)
            call mpi_reduce(dxc_avg,dxc_w,ndim,mpi_double_precision,
     1                      mpi_sum,0,mpi_comm_world,ier)
            call mpi_reduce(dyc_avg,dyc_w,ndim,mpi_double_precision,
     1                      mpi_sum,0,mpi_comm_world,ier)
            call mpi_reduce(zposition,z_w,itermax,mpi_double_precision,
     1                      mpi_sum,0,mpi_comm_world,ier)
            xc_avg=xc_w
            yc_avg=yc_w
            dxc_avg=dxc_w
            dyc_avg=dyc_w
            zposition=z_w
            deallocate(xc_w,yc_w,dxc_w,dyc_w,z_w)
         endif
         if(myid.eq.0) then
            open(unit=20,file='Average_Beam_Evolution.txt',
     1           form='formatted',status='replace')
            write(20,300)
  300       format(6x,'AVERAGE ENVELOPE EVOLUTION',//,
     1             5x,'z(m)',7x,'xc(cm)',5x,'yc(cm)',5x,'<x-xc>(cm)',
     2             2x,'<y-yc>(cm)')
            avg_dxc=zero
            avg_dyc=zero
            do i=1,itermax
               avg_xc=xc_avg(i)/dble(nwpart)
               avg_yc=yc_avg(i)/dble(nwpart)
               avg_dxc=dxc_avg(i)/dble(nwpart)
               avg_dyc=dyc_avg(i)/dble(nwpart)
               write(20,301) zposition(i),avg_xc,avg_yc,avg_dxc,avg_dyc
  301          format(5(1x,1pe11.4))
            end do
            close(unit=20)
         endif
         deallocate(zposition,dxc_avg,dyc_avg,xc_avg,yc_avg)
      endif
      return
      end
c**********************************************************************
      subroutine headers(wigerrs,prebunch,iprofile,matchbeam,escale,
     1        fscale,energy,beami,current,ndz,ztotal,rbmin,rbmax,
     2        xbeam,ybeam,gamma,dgam,dgamz,dp0,epsx,epsy,akbeta,
     3        gammaavg,betaavg,twissgamx,twissgamy,twissalphx,
     4        twissalphy,betax,betay,nbeams,psiwidth,source,iseed0,
     5        error,waist,rg,rg_x,rg_y,dz,nstep,zmax,vz0,nflag,
     6        ns,nunit,t_sep,t_rndtrp,slicenum,switch,tlight,
     7        Shot_Noise,noise_seed,B_ratio,trans_cor,nslices_P,
     8        x_max,y_max,sigmapx_P,sigmapy_P,Gauss_Quad,dinput_file,
     9        twissbetax,twissbetay,Runge_Kutta,interpolation)
      use particles
      use numbers
      use waveguides
      use particles_ode
      use particles_func
      use wiggler
      use focus
      use modes
      use modes_init
      use harmonics
      use multi_beams
      use bessel
      use rf_acc
      use Head_Power
      use RFaccel
      use storage
      use mpi_parameters
      use mpi
      use PARMELA
      use distfile_import
      use diagnstc
      implicit double precision(a-h,o-z)
      character*8 beamtype,wigtype,foctype,wigerrs,source,enrgycon,
     1            dcfield,direction,RFfield,prebunch,RFmode,
     2            wigplane,matchbeam,slippage,slicenum,switch,
     3            Shot_Noise,trans_cor,Gauss_Quad
      character*9 Runge_Kutta
      character*2 modetype
      character*10 interpolation
      character*11 drifttube
      character*12 tprofile,teprofile
      character*17 dinput_file
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
      common/spread/dp,n1,n2,npsi,nphi,ndv,npx,npy,ngam
      common/taper/zsat,dbw,dbw_sqr,dzw,nwig_harms
      common/self/rb2,xavgs,yavgs,rbavg2,sfact,rb0avg2,iself
      common/RFacc/RFfield,agkwrf,bgkwrf,hagkwrf,hbgkwrf,n1stpass,
     3             drifttube
      common/profile/akwwaist
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
c----------------------------------------------------------------------
c
      write(nunit,201)
  201 format(/,' WIGGLER CHARACTERISTICS',/)
      write(nunit,202)
  202 format(5x,'Segment',2x,'Type',6x,'Bw',10x,'zw',10x,'K',9x,
     1       'zstart',7x,'Lw',6x,'Nup',1x,'Ndown',1x,'wiggle',
     2       /,8x,'No.',13x,'kG',10x,'cm',22x,'m',11x,'m',16x,
     3       'plane')
      hunkw=hundred*akw(1)
      do i=1,nsegbw
         if(ntype(i).eq.1) then
            if(wigangle(i).eq.zero) wigplane='x'
            if(wigangle(i).ne.zero) wigplane='y'
            write(nunit,203) i,'ppf',bwi(i),zwi(i),betaw(i),
     1                   zstart(i)/hunkw,(zstop(i)-zstart(i))/hunkw,
     2                   nwigup(i),nwigdn(i),wigplane              
  203       format(6x,i4,4x,a4,5(1x,1pe11.4),1x,i3,1x,i3,5x,a2)
         endif
         if(ntype(i).eq.2) then
            wigplane='x'
            write(nunit,203) i,'pfm',bwi(i),zwi(i),betaw(i),
     1                   zstart(i)/hunkw,(zstop(i)-zstart(i))/hunkw,
     2                   nwigup(i),nwigdn(i),wigplane
         endif
         if(ntype(i).eq.3) then
            if(wigangle(i).eq.zero) wigplane='x'
            if(wigangle(i).ne.zero) wigplane='y'
            write(nunit,203) i,'fpf',bwi(i),zwi(i),betaw(i),
     1                   zstart(i)/hunkw,(zstop(i)-zstart(i))/hunkw,
     2                   nwigup(i),nwigdn(i),wigplane
         endif
         if(ntype(i).eq.4) then
            wigplane='NA'
            write(nunit,203) i,'hel',bwi(i),zwi(i),betaw(i),
     1                   zstart(i)/hunkw,(zstop(i)-zstart(i))/hunkw,
     2                   nwigup(i),nwigdn(i),wigplane
         endif
         if(ntype(i).eq.9) then
            wigplane='NA'
            write(nunit,203) i,'aple',bwi(i),zwi(i),betaw(i),
     1                   zstart(i)/hunkw,(zstop(i)-zstart(i))/hunkw,
     2                   nwigup(i),nwigdn(i),wigplane
         endif
         if(ntype(i).eq.5) then
            wigplane='x'
            write(nunit,203) i,'hppf',bwi(i),zwi(i),betaw(i),
     1                   zstart(i)/hunkw,(zstop(i)-zstart(i))/hunkw,
     2                   nwigup(i),nwigdn(i),wigplane
         endif
         if(ntype(i).eq.6) then
            wigplane='x'
            write(nunit,203) i,'hfpf',bwi(i),zwi(i),betaw(i),
     1                   zstart(i)/hunkw,(zstop(i)-zstart(i))/hunkw,
     2                   nwigup(i),nwigdn(i),wigplane
         endif
         if(RFfield.ne.'off') then
           if((RFfield.eq.'on').or.(RFfield.eq.'dc')) then
              write(nunit,204) escale*Eacc(i,ns),scale*z1acc(i),
     1                          scale*z2acc(i)
  204         format(/,15x,'DC Accelerating Field Applied:',1pe11.4,
     1          '(MV/m)',/,31x,'z1(m)=',e11.4,' z2(m)=',e11.4)
           endif
           if((RFfield.eq.'rf').and.(moderf(i).ge.0)) then
              if(moderf(i).eq.0) modetype='TE'
              if(moderf(i).eq.1) modetype='TM'
              write(nunit,205) modetype,lnumrf(i),nnumrf(i),drifttube,
     1                  Pinrf(i,ns),fscale*omegarf(i),scale*z1acc(i),
     2                  scale*z2acc(i)
  205         format(/,15x,'RF Accelerating Field Applied: ',a2,
     1                '(',i2,',',i2,') Mode in a ',a11,' waveguide',
     2                /,30x,'Pin(W)=',1pe11.4,' Frequency(GHz)=',e13.6,
     3                /,31x,'z1(m)=',e11.4,10x,'z2(m)=',e11.4)
           endif
         endif
      end do
      if(nwig_harms.ge.1) then
         write(nunit,*) ' '
         write(nunit,*) '          FPF WIGGLER(S) WITH HARMONICS'
         write(nunit,*) ' '
         do i=1,nsegbw
            if(ntype(i).eq.3) then 
            write(nunit,*) '             Segment  Harmonic    Bw ratio'
            do nhw=1,nwig_harms
            write(nunit,222) i,int(wigharms(nhw,2)),wigharms(nhw,1)
  222       format(14x,i3,7x,i3,6x,1p1e12.5)
            end do
         endif
      end do
      endif
      if((abs(dbw).gt.deci).or.(abs(dzw).gt.deci)) then
         write(nunit,206) zsat,dbw,dzw,zsat/(hundred*akw(1)),
     1                    hundred*akw(1)*bwi(1)*dbw
  206     format(/,10x,'LINEAR TAPERED WIGGLER: zsat=',1pe11.4,' dbw=',
     1            e11.4,' dzw=',e11.4,/,27x,'start taper(m)=',e11.4,
     2            ' slope(kG/m)=',e11.4)
      endif
      if(abs(dbw_sqr).gt.deci) then
           write(nunit,220) zsat,zsat/(hundred*akw(1)),dbw_sqr
  220      format(/,10x,'PARABOLIC TAPERED WIGGLER: zsat=',1pe11.4,/,
     1            36x,' start taper(m)=',e11.4,/,
     2            36x,' DBw/Bw=',e11.4)
      endif
      if((abs(dbw).lt.deci).and.(abs(dzw).lt.deci).and.
     1                                    (abs(dbw_sqr).lt.deci)) then
           if(nsegbw.eq.1) write(nunit,207)
  207      format(/,10x,'UNIFORM WIGGLER')
      endif
      if(nsegbw.gt.1) then
         do i=1,nsegbw
            if((abs(dbwseg(i)).gt.deci)
     1                               .or.(abs(dzwseg(i)).gt.deci)) then
              write(nunit,500) i,zsatseg(i),dbwseg(i),dzwseg(i),
     1                         zsatseg(i)/(hundred*akw(1)),
     2                         hundred*akw(1)*bwi(i)*dbwseg(i)
  500       format(/,10x,'TAPERED WIGGLER SEGMENT(',i4,'):',/,
     1            27x,' zsat=',1pe11.4,' dbw=',e11.4,' dzw=',e11.4,/,
     2            27x,'start taper(m)=',e11.4,' slope(kG/m)=',e11.4)
            endif
         end do
      endif
      if(wigerrs.eq.'off') then
            write(nunit,208)
  208       format(/,10x,'WIGGLER ERRORS DISABLED')
      endif
      if(wigerrs.ne.'off') then
            write(nunit,209)
  209       format(10x,'WIGGLER ERRORS ENABLED')
            if(source.eq.'random') write(nunit,210) nerr,error,iseed0
  210       format(10x,'RANDOM ERRORS: nerr=',i3,' (dBw/Bw)rms=',
     1             1pe10.3,' iseed=',i3,/)
            if(source.eq.'fixed') write(nunit,211) nerr
  211       format(10x,'FIXED ERRORS: nerr=',i3,/)      
      endif
c
      if(nsegfoc.gt.0) then
         write(nunit,214)
  214    format(/,' EXTERNAL MAGNETIC FOCUSING ENABLED',/)
         if(nsegfoc.gt.1) write(nunit,215) nsegfoc
  215    format(5x,'Multi-Segmented Focusing Fields: No. of Segments ='
     1          ,i4,/)
         nq=0
         nd=0
         do i=1,nsegfoc
           if(nfoc(i).eq.1) nq=nq+1
           if(nfoc(i).eq.2) nd=nd+1
         end do
         if(nq.gt.0) write(nunit,216)
  216    format(8x,'Quad',2x,'Field Gradient',4x,'K',9x,'zstart',
     1          6x,'zstop',7x,'Ltrans',4x,'ndfocus',/,9x,'No.',
     2          6x,'kG/cm',7x,'1/m**2',8x,'m',11x,'m',11x,'m')
         do i=1,nsegfoc
            if(nfoc(i).eq.1) write(nunit,217) i,bqfocus(i),Qks(i),
     1       zstt(i)/(hundred*akw(1)),zstp(i)/(hundred*akw(1)),
     2       zupdown(i)/(hundred*akw(1)),ndfoc(i)
  217       format(8x,i3,4x,1pe11.4,4(1x,e11.4),1x,i6)
         end do
         if(nd.gt.0) write(nunit,218)
  218    format(7x,'Dipole',5x,'B-Field',6x,'zstart',
     1          6x,'zstop',7x,'Ltrans',4x,'ndfocus',/,9x,'No.',
     2          8x,'kG',11x,'m',11x,'m',11x,'m')
         do i=1,nsegfoc
            if(nfoc(i).eq.2) write(nunit,219) i,bqfocus(i),
     1       zstt(i)/(hundred*akw(1)),zstp(i)/(hundred*akw(1)),
     2       zupdown(i)/(hundred*akw(1)),ndfoc(i)
  219       format(8x,i3,4x,4(1x,1pe11.4),1x,i6)
         end do
      endif
c
      if(iprofile.eq.0) write(nunit,300) energy,beami,rbmin,rbmax
  300 format(/,' CIRCULAR ELECTRON BEAM: FLAT-TOP PROFILE',//,10x,
     1     'Eb(MeV)=',1pe12.5,' Ib(Amp)=',e10.3,'  rbmin(cm)=',e10.3,
     2     ' rbmax(cm)=',e10.3)
      if(iprofile.eq.1) write(nunit,301) energy,beami,rbmin,rbmax
  301 format(/,' CIRCULAR ELECTRON BEAM: PARABOLIC PROFILE',//,10x,
     1     'Eb(MeV)=',1pe12.5,' Ib(Amp)=',e10.3,'  rbmin(cm)=',e10.3,
     2     ' rbmax(cm)=',e10.3)          
      if(iprofile.eq.2) write(nunit,302) energy,beami,xbeam,ybeam
  302 format(/,' RECTANGULAR ELECTRON BEAM: FLAT-TOP PROFILE',//,10x,
     1      'Eb(MeV)=',1pe12.5,' Ib(Amp)=',e10.3,'  xbeam(cm)=',e10.3,
     2     ' ybeam(cm)=',e10.3)      
      if(iprofile.eq.3) write(nunit,303) energy,beami,xbeam,ybeam
  303 format(/,' ELLIPTICAL ELECTRON BEAM: FLAT-TOP PROFILE',//,10x,
     1      'Eb(MeV)=',1pe12.5,' Ib(Amp)=',e10.3,'  xbeam(cm)=',e10.3,
     2     ' ybeam(cm)=',e10.3)
      if(iprofile.le.3) then
           write(nunit,304) gamma,xi(1,ns)
  304      format(10x,' gamma=',1pe10.3,'      xi=',e10.3)
        if((iprofile.le.1).or.(iprofile.eq.3)) then

            write(nunit,305) ipart(ns),n1,n2,npsi
  305       format(11x,'ipart=',i6,10x,'nr=',i3,9x,'ntheta=',i3,9x,
     1             'npsi=',i3)
           else
            write(nunit,306) ipart(ns),n1,n2,npsi
  306       format(11x,'ipart=',i8,10x,'nx=',i3,8x,'    ny=',i3,9x,
     1             'npsi=',i5)
        endif 
        write(nunit,307) dp0,dgamz,nphi,ndv,Gauss_Quad
  307   format(13x,'dp0=',1pe10.3,3x,'dgamz=',e10.3,4x,'nphi=',i3,10x,
     1       'ndv=',i3,3x,'Gauss_Quad=',a8)
        if((nflag.ne.0).and.(iprofile.le.3)) write(nunit,990)
  990                  format(/,' NPSI chosen incorrectly',/
     1                        ' NPSI = 10 was substituted',/)
        if(iself.eq.0) then
             write(nunit,308)
  308        format(/,10x,'SELF-FIELD EFFECTS DISABLED')
          else
             write(nunit,309)
  309        format(/,10x,'SELF-FIELD EFFECTS ENABLED')
        endif
      endif
c
      if((iprofile.eq.4).and.(nbeams.eq.1)  !---------------------------!
     1      .and.(matchbeam.eq.'on').and.(tprofile.ne.'BEAMCODE')) then !
        if(switch.eq.'off') cur=beami                                   !
        if(switch.eq.'on') cur=beam_i(1,ns)                             !
        write(nunit,310) energy,cur,epsx,scale*sigmax(1,ns),            !
     1                   sigmapx(1,ns),epsy,scale*sigmay(1,ns),         !
     2                   sigmapy(1,ns),gamma,dgam,scale*akbeta          !
  310   format(/,' GAUSSIAN BEAM MODEL: MATCHED BEAM',//,10x,           !
     1     'Eb(MeV)=',1pe12.5,' Ib(Amp)=',e10.3,/,10x,                  !---Gaussian Beam
     2     'x emittance(mm-mrad)=',e10.3,' sigmax(m)=',e10.3,           !---matchbeam='on'
     3     ' sigmapx/mc=',e10.3,/,10x,'y emittance(mm-mrad)=',e10.3,    !
     4     ' sigmay(m)=',e10.3,' sigmapy/mc=',e10.3, /,10x,             !
     5     'gamma=',e10.3,4x,'dgam=',e10.3,/,10x,'beta(m)=',e10.3)      !
        if(switch.eq.'on') write(nunit,311) ipart(ns),n1,n2,npsi,nphi,  !
     1                     ndv,ngam,Gauss_Quad                          !
  311   format(10x,'ipart=',i6,' nx=',i3,' ny=',i3,' npsi=',i6,         !
     1         ' nphi=',i3,' ndv=',i3,' ngam=',i3,' Gauss_Quad=',a8)    !
      endif  !----------------------------------------------------------!
c
      if((iprofile.eq.4).and.(nbeams.gt.1)
     1                                   .and.(matchbeam.eq.'on')) then
        write(nunit,312) energy,beami,epsx,scale*sigmax(1,ns),
     1               sigmapx(1,ns),epsy,scale*sigmay(1,ns),
     2               sigmapy(1,ns),gammaavg,dgam,scale*betaavg
  312   format(/,' GAUSSIAN MULTI-BEAM MODEL: MATCHED BEAM',//,10x,
     1     'Eb(MeV)=',1pe12.5,' Ib(Amp)=',e10.3,/,10x,
     2     'x emittance(mm-mrad)=',e10.3,' sigmax(m)=',e10.3,
     3     ' sigmapx/mc=',e10.3,/,10x,'y emittance(mm-mrad)=',e10.3,
     4     ' sigmay(m)=',e10.3,' sigmapy/mc=',e10.3,/,10x,
     5     'gamma=',e10.3,4x,'dgam=',e10.3,/,10x,' beta(m)=',e10.3)  
        write(nunit,313) ipart(ns),n1,n2,npsi,nphi,ndv,ngam,nbeams,
     1                   Gauss_Quad
  313   format(10x,'ipart=',i6,' nx=',i3,' ny=',i3,' npsi=',i6,
     1         ' nphi=',i3,' ndv=',i3,' ngam=',i3,' nbeams=',i3,
     2         10x,'Gauss_Quad=',a8,//,
     3         31x,'BEAM PARAMETERS',//,20x,'x0(cm)',7x,'y0(cm)',3x,
     4         'Energy Shift(MeV)',2x,'Current Shift (A)')
        do nb=1,nbeams
            write(nunit,314) x0(nb,ns),y0(nb,ns),dE(nb,ns),dI(nb,ns)
  314       format(15x,2(1x,1pe12.5),3x,e12.5,6x,e12.5)
        end do
      endif
c
      if((iprofile.eq.4).and.(matchbeam.ne.'on').and.  !----------------!
     1                                    (tprofile.ne.'BEAMCODE')) then!
        write(nunit,315)                                                !
  315   format(/,' GAUSSIAN BEAM MODEL: ARBITRARY TWISS PARAMETERS')    !
        if(switch.eq.'off') write(nunit,707) energy,beami,dgam,gamma,   !
     1     epsx,epsy,                                                   !
     2     hundredth*sigmax(1,ns)/akw(1),hundredth*sigmay(1,ns)/akw(1), !
     3     betax,betay,twissalphx,twissalphy,                           !
     4     hundred*akw(1)*twissgamx,hundred*akw(1)*twissgamy,           !
     5     sigmapx(1,ns),sigmapy(1,ns)                                  !
  707   format(/,10x,'Eb(MeV)=',1pe12.5,2x,'Ib(Amp)=',e12.5,2x,/,       !
     2       11x,'DEb/Eb=',e12.5,4x,'gamma=',e12.5,//,                  !---Gaussian Beam
     3       38x,'x',16x,'y',/,10x, 'emittance(mm-mrad)',5x,e12.5,5x,   !---matchbeam='off'
     4       e12.5,/,20x,'sigma(m)',5x,e12.5,5x,e12.5,                  !
     5       /,21x,'beta(m)',5x,e12.5,5x,e12.5,/,17x,'Twiss alpha',     !
     6       5x,e12.5,5x,e12.5,/,12x,'Twiss gamma(1/m)',5x,e12.5,       !
     7       5x,e12.5,/,22x,'sigmap',5x,e12.5,5x,e12.5,/)               !
        if(switch.eq.'on') then                                         !
           write(nunit,707) energy,beam_i(1,ns),dgam,gamma,epsx,epsy,   !
     1     hundredth*sigmax(1,ns)/akw(1),hundredth*sigmay(1,ns)/akw(1), !
     2     betax,betay,twissalphx,twissalphy,                           !
     3     hundred*akw(1)*twissgamx,hundred*akw(1)*twissgamy,           !
     4     sigmapx(1,ns),sigmapy(1,ns)                                  !
           write(nunit,311)ipart(ns),n1,n2,npsi,nphi,ndv,ngam,Gauss_Quad!
        endif                                                           !
      endif  !----------------------------------------------------------!
        if((nflag.ne.0).and.(iprofile.eq.4)) write(nunit,990)
c
      if(tprofile.eq.'BEAMCODE') then  !--------------------------------!
        write(nunit,700)                                                !
  700   format(/,' GAUSSIAN BEAM MODEL: BEAMCODE INPUT BEAM')           !
        if(switch.eq.'on') then                                         !
         write(nunit,701)                                               !
  701    format(/,9x,'SLICE PROPERTIES')                                !
         enrgy=(gammab(1,ns)-one)*emass                                 !
         write(nunit,707) enrgy,beam_i(1,ns),dgamG(1,ns),gammab(1,ns),  !
     1     xi(1,ns),epsxi(1,ns),epsyi(1,ns),                            !
     2     hundredth*sigmax(1,ns)/akw(1),hundredth*sigmay(1,ns)/akw(1), !
     3     betatxi(1,ns),betatyi(1,ns),twissalphxi(1,ns),               !
     4     twissalphyi(1,ns),hundred*akw(1)*twissgamxi(1,ns),           !
     5     hundred*akw(1)*twissgamyi(1,ns),sigmapx(1,ns),sigmapy(1,ns)  !
         write(nunit,311) n1*n2*npsi*nphi*ndv*ngam,n1,n2,npsi,nphi,ndv, !
     1                    ngam,Gauss_Quad                               !
        endif                                                           !
        write(nunit,702) energy,current,dgam,epsx,epsy,x_max,sigmapx_P, !
     1                   y_max,sigmapy_P,twissalphx,twissalphy          !
  702   format(/,9x,'GLOBAL BEAM PROPERTIES',//,7x,'Eb(MeV)=',1pe12.5,  !
     1         7x,'Ib(Amp)=',e12.5,2x,'DEb/Eb=',e12.5,/,1x,             !
     2         'epsx(mm-mrad)=',e12.5,1x,'epsy(mm-mrad)=',e12.5,/,      !
     3         3x,'sigma_x(cm)=',e12.5,3x,'sigma_px/mc=',e12.5,/,       !
     4         3x,'sigma_y(cm)=',e12.5,3x,'sigma_py/mc=',e12.5,/,       !--BEAMFILE
     5         1x,'twiss_alpha_x=',e12.5,1x,'twiss_alpha_y=',e12.5)     !--Output
        write(nunit,703)                                                !
  703   format(/,9x,'BIN PROPERTIES',//,3x,'slice',                     !
     1         3x,'Energy(MeV)',3x,'Charge(C)',3x,'Current(A)',         !
     2         2x,'eps_x(micron)',1x,'eps_y(micron)',3x,'dE/Eavg')      !
        do nsp=1,nslices_P                                              !
           write(nunit,704) nsp,energies(nsp),charges(nsp),             !
     1           currents(nsp),eps_xs(nsp),eps_ys(nsp),dgams(nsp)       !
  704      format(3x,i3,3x,4(1x,1pe12.5),2(2x,e12.5))                   !
        end do                                                          !
        write(nunit,705)                                                !
  705   format(/,3x,'slice',3x,'sigma_x(cm)',2x,'sigma_px/mc',1x,       !
     1         'Twiss_alpha_x',1x,'sigma_y(cm)',3x,'sigma_py/mc',       !
     2         2x,'Twiss_alpha_y')                                      !
        do nsp=1,nslices_P                                              !
           write(nunit,706) nsp,x_maxs(nsp),sigma_pxs(nsp),             !
     1                      Twiss_alph_x(nsp),y_maxs(nsp),              !
     2                      sigma_pys(nsp),Twiss_alph_y(nsp)            !
  706      format(3x,i3,3x,4(1x,1pe12.5),2(2x,e12.5))                   !
        end do                                                          !
      endif  !----------------------------------------------------------!
c
      if(tprofile.eq.'DISTFILE') then  !--------------------------------!
        write(nunit,800) dinput_file                                    !
  800   format(/,' DISTFILE INPUT: filename=',a17)                      !
        write(nunit,801) energy,current,dgam,epsx,epsy,xmax_D,sigmapx_D,!
     1                   ymax_D,sigmapy_D,twissalphx,twissalphy,        !
     2                   twissbetax,twissbetay                          !
  801   format(/,9x,'GLOBAL BEAM PROPERTIES',//,7x,'Eb(MeV)=',1pe12.5,  !
     1         7x,'Ib(Amp)=',e12.5,2x,'DEb/Eb=',e12.5,/,1x,             !
     2         'epsx(mm-mrad)=',e12.5,1x,'epsy(mm-mrad)=',e12.5,/,      !
     3         3x,'sigma_x(cm)=',e12.5,3x,'sigma_px/mc=',e12.5,/,       !
     4         3x,'sigma_y(cm)=',e12.5,3x,'sigma_py/mc=',e12.5,/,       !
     5         1x,'twiss_alpha_x=',e12.5,1x,'twiss_alpha_y=',e12.5,/,   !
     6         1x,'twi_beta_x(m)=',e12.5,1x,'twi_beta_y(m)=',e12.5  )   !
        write(nunit,802) nslices_g                                      !
  802   format(/,9x,'SLICE PROPERTIES: nslices_g=',i4,//,2x,'slice',    !
     1         4x,'Energy(MeV)',3x,'Charge(C)',3x,'Current(A)',         !--DISTFILE
     2         2x,'eps_x(micron)',1x,'eps_y(micron)',3x,'dE/Eavg')      !--Output
        do nsg=1,nslices_g                                              !
           write(nunit,803) nsg,energies(nsg),charges(nsg),             !
     1              currents(nsg),eps_xs(nsg),eps_ys(nsg),dgams(nsg)    !
  803      format(2x,i5,2x,4(1x,1pe12.5),2(2x,e12.5))                   !
        end do                                                          !
        write(nunit,804)                                                !
  804   format(/,3x,'slice',3x,'sigma_x(cm)',1x,'Twiss_beta_x',1x,      !
     1         'Twiss_alpha_x',1x,'sigma_y(cm)',2x,'Twiss_beta_y',      !
     2         2x,'Twiss_alpha_y')                                      !
        do nsg=1,nslices_g                                              !
           write(nunit,803) nsg,x_maxs(nsg),twissbetax_g(nsg),          !
     1                      Twiss_alph_x(nsg),y_maxs(nsg),              !
     2                      twissbetay_g(nsg),Twiss_alph_y(nsg)         !
        end do                                                          !
      endif  !----------------------------------------------------------!
c
c------ Write Temporal Pulse Information
c
      if(((iprofile.eq.4).or.(tprofile.eq.'DISTFILE'))
     1                                    .and.(slippage.eq.'on')) then
        write(nunit,316) tprofile,Q_bunch,tslices,tpulse,tlight,
     1                   dtpulse,dt_norm,nslices,interpolation,nslip
  316   format(/,10x,'SLIPPAGE ENGAGED: ',a12,' Profile',  
     1          /,27x,'  Bunch Charge(Coul) =',1pe11.4,
     2          /,27x,'  Time Interval(sec) =',e11.4,
     3          /,18x,' Electron Pulse Duration(sec) =',e11.4,
     4          /,18x,'    Light Pulse Duration(sec) =',e11.4,
     5          /,26x,'Slice Separation(sec) =',e11.4,
     6          /,27x,'    Slice Separation =',e11.4,' Wavelengths',
     7          /,28x,'   Number of Slices =',i6,
     8          /,29x,'Slippage Algorithm = ',a10                
     9          /,28x,'Slippage Applied Every',i4,' integration steps')
         if(tprofile.eq.'tophat') write(nunit,317) tpulse,tshift
  317    format(/,15x,'TOP-HAT TEMPORAL PROFILE: tpulse(sec)=',1pe11.4,
     1          /,41x,'tshift(sec)=',e11.4)
      endif
c
c------ Waterbag Beam Model
c
      if(iprofile.eq.5) then
        write(nunit,319) energy,beami,rbmax,gamma,dgam,xi(1,ns),
     1               scale*akbeta,ipart(ns),n1,n2,npsi,ndv,nphi,ngam
  319   format(/,' WATERBAG BEAM MODEL:',//,10x,
     1     'Eb(MeV)=',1pe12.5,' Ib(Amp)=',e10.3,'  Rb(cm)=',e10.3,
     2     /,12x,'gamma=',e10.3,4x,'dgam=',e10.3,/,14x,'xi=',e10.3,
     3     ' beta(m)=',e10.3,/,12x,'ipart=',i6,' nr=',i3,' ntheta=',i3,
     4     ' npsi=',i3,' ndv=',i3,' nphi=',i3,' ngam=',i3)
        if(nflag.ne.0) write(nunit,990)
      endif
      if(prebunch.ne.'off') then
         write(nunit,320) psiwidth,B_ratio
  320    format(/,' PREBUNCHED BEAM MODEL: psiwidth=',1pe12.5,
     1            ' B_ratio=',e12.5)
      endif
      if(Shot_Noise.eq.'on') then
         write(nunit,322) noise_seed,trans_cor
  322    format(/,9x,'SHOT NOISE ENABLED: noise_seed =',i6,
     1          /,30x,'trans_cor =    ',a8)
      endif
c
      write(nunit,321) Runge_Kutta,dz,dz/twopi,scale*dz
  321 format(/,a9,' RUNGE-KUTTA INTEGRATION:',//,10x,'kw*dz=',1pe10.3,
     1       ' dz/zw=',e10.3,' dz(m)=',e10.3)
c
      if(emmodes.eq.'hermite') then  !---------------------------------!
          do nh=1,nharms                                               !
             omegah=omega(nh1(nh,ns))                                  !
             if(drifttube.eq.'cylindrical') then                       !
                write(nunit,400) omegah,hundredth*zwi(1)/omegah,rg,    !
     1                          w0(nh1(nh,ns),ns)/akw(1),              !
     1                          hundredth*zwaste(nh1(nh,ns),ns)/akw(1),!
     2                          hundredth*z0(nh1(nh,ns),ns)/akw(1)     !
  400           format(/,' ELECTROMAGNETIC FIELD: omega/ckw=',1pe12.5, !
     1                 ' wavelength(m)=',e12.5,' Rg(cm)=',e10.3,/,24x, !
     2                 'waist(cm)=',e10.3,' zwaist(m)=',e10.3,         !
     3                 ' Rayleigh Range(m)=',e10.3,/)                  !
             endif                                                     !
             if(drifttube.eq.'rectangular') then                       !
                write(nunit,4001) omegah,hundredth*zwi(1)/omegah,rg_x, !
     1                          rg_y,w0(nh1(nh,ns),ns)/akw(1),         !
     1                          hundredth*zwaste(nh1(nh,ns),ns)/akw(1),!
     2                          hundredth*z0(nh1(nh,ns),ns)/akw(1)     !
 4001           format(/,' ELECTROMAGNETIC FIELD: omega/ckw=',1pe12.5, !
     1                ' wavelength(m)=',e12.5,' ag(cm)=',e10.3,' bg=', !
     2                 e10.3,/,24x,'waist(cm)=',e10.3,' zwaist(m)=',   !
     3                 e10.3,' Rayleigh Range(m)=',e10.3,/)            !
             endif                                                     !
          end do                                                       !
          write(nunit,600) hundredth*zwi(1)/omega(nh1(1,ns))           !
  600     format(21x,'Wavelength(m) =',1pe10.3)                        !
          if(xdir(nh1(1,ns)).gt.zero) then                             !
                   write(nunit,401)                                    !
  401              format(21x,'Polarization = x-direction',/)          !
               else                                                    !--Header for
                   write(nunit,402)                                    !--Gauss-Hermite
  402              format(21x,'Polarization = y-direction',/)          !--Modes
          endif                                                        !
          if(nslices.gt.1) then                                        !
             if(slicenum.eq.'on') write(nunit,602) ns                  !
  602        format(26x,'Slice No.:',i6,/)                             !
          endif                                                        !
          if(switch.eq.'on') then                                      !
             do nm=nh1(1,ns),nh2(1,ns)                                 !
               write(nunit,403) lhead(nm,ns),nhead(nm,ns),Phead(nm,ns) !
  403          format(10x,'Gauss-Hermite(',i3,',',i3,                  !
     1                ') Mode: Power(W)=',1pe10.3)                     !
             end do                                                    !
          endif                                                        !
      endif  !---------------------------------------------------------!
      if(emmodes.eq.'laguerre') then  !--------------------------------!
        do nh=1,nharms                                                 !
          omegah=omega(nh1(nh,ns))                                     !
          write(nunit,404) omegah,hundredth*zwi(1)/omegah,rg,          !
     1                 w0(nh1(nh,ns),ns)/akw(1),                       !
     2                 hundredth*zwaste(nh1(nh,ns),ns)/akw(1),         !
     3                 hundredth*z0(nh1(nh,ns),ns)/akw(1),polar        !
  404     format(/,' ELECTROMAGNETIC FIELD: omega/ckw=',1pe12.5,       !
     1       ' wavelength(m)=',e12.5,' Rg(cm)=',e10.3,/,24x,           !
     2       'waist(cm)=',e10.3,' zwaist(m)=',e10.3,                   !
     3       ' Rayleigh Range(m)=',e10.3,' polar=',e10.3,/)            !
          write(nunit,600) hundredth*zwi(1)/omega(nh1(1,ns))           !
          if(polar.gt.zero) then                                       !--Header for
                   write(nunit,405)                                    !--Gauss-Laguerre
  405              format(21x,'Polarization = RHCP',/)                 !--Modes
               else                                                    !
                   write(nunit,406)                                    !
  406              format(21x,'Polarization = LHCP',/)                 !
          endif                                                        !
          if(switch.eq.'on') then                                      !
             do nm=nh1(1,ns),nh2(1,ns)                                 !
               write(nunit,407) lhead(nm,ns),nhead(nm,ns),Phead(nm,ns) !
  407          format(10x,'Gauss-Laguerre(',i3,',',i3,                 !
     1                ') Mode: Power(W)=',1pe10.3)                     !
             end do                                                    !
          endif                                                        !
        end do                                                         !
      endif  !---------------------------------------------------------!
      return
      end
c**********************************************************************
      subroutine Mode_Out(itermax,pin,t,beami,nstep,ztotal,wigerrs,
     1      prebunch,iprofile,matchbeam,escale,fscale,energy,ndz,
     2      rbmin,rbmax,xbeam,ybeam,gamma,dgam,dgamz,dp0,epsx,
     3      epsy,akbeta,gammaavg,betaavg,twissgamx,twissgamy,
     4      twissalphx,twissalphy,betax,betay,nbeams,psiwidth,
     5      source,iseed0,error,waist,rg,rg_x,rg_y,dz,zmax,
     6      vz0,nflag,t_sep,t_rndtrp,tlight,Shot_Noise,noise_seed,
     7      trans_cor,OPC,Power_vs_z,nslices_P,current,x_max,y_max,
     8      sigmapx_P,sigmapy_P,B_ratio,restart,Gauss_Quad,dinput_file,
     9      twissbetax,twissbetay,Runge_Kutta,interpolation)
      use version
      use modes
      use waveguides
      use storage
      use mpi_parameters
      use multi_beams
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,ten=10.d0,amicro=1.d-6)
      character*2 modetype
      character*3 restart
      character*10 interpolation
      character*12 tprofile,teprofile
      character*17 dinput_file
      character*8 beamtype,wigtype,foctype,wigerrs,source,enrgycon,
     1            dcfield,direction,RFfield,prebunch,RFmode,
     2            wigplane,matchbeam,slippage,slicenum,switch,
     3            Shot_Noise,trans_cor,OPC,Power_vs_z,Gauss_Quad
      character*9 Runge_Kutta
      character*35 filename
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
c----------------------------------------------------------------------
      do ns=ns_begin,ns_end               !--Output of Power_vs_z files
         slicenum='on'
         if(Power_vs_z.eq.'on') rewind(30+ns)
         if(slippage.eq.'off') then  !---------------------------------!--Open Power_vs_z
              open(unit=25,file='Power_vs_z.txt',form='formatted',     !--File when
     1             status='replace')                                   !--when slippage='off'
         endif  !------------------------------------------------------!
         if((slippage.ne.'off').and.(Power_vs_z.eq.'on')) then !-------!
               write(filename,502) ns                                  !--Open Power_vs_z
  502          format('Power_vs_z_slice=',i5.5,'.txt')                 !--File when
               open(unit=25,file=filename,form='formatted',            !--slippage='on'
     1              status='replace')                                  !
         endif  !------------------------------------------------------!
c
         if((slippage.eq.'off').or.(Power_vs_z.eq.'on')) 
     1      write(25,501) Minerva_Version, Version_Date
  501 format(/22x,'****** CODE MINERVA: Version ',f5.2,' - MPI ******',
     1       /,22x,'   Source-Dependent Expansion/Multi-Frequency',/,
     2         22x,' Created by: H.P. Freund & P.J.M. van der Slot',
     3       /,22x,'             Date: ',A18,/)
c
         if(Power_vs_z.eq.'on') then
            switch='on'  !---------------------------------------------!
            call headers(wigerrs,prebunch,iprofile,matchbeam,escale,   !
     1         fscale,energy,beami,current,ndz,ztotal,rbmin,rbmax,     !
     2         xbeam,ybeam,gamma,dgam,dgamz,dp0,epsx,epsy,akbeta,      !
     3         gammaavg,betaavg,twissgamx,twissgamy,twissalphx,        !
     4         twissalphy,betax,betay,nbeams,psiwidth,source,iseed0,   !
     5         error,waist,rg,rg_x,rg_y,dz,nstep,zmax,vz0,     !
     6         nflag,ns,25,t_sep,t_rndtrp,slicenum,switch,tlight,      !
     7         Shot_Noise,noise_seed,B_ratio,trans_cor,nslices_P,      !
     8         x_max,y_max,sigmapx_P,sigmapy_P,Gauss_Quad,dinput_file, !
     9         twissbetax,twissbetay,Runge_Kutta,interpolation)        !
            if(slippage.eq.'off') then                                 !--Write Header for
              eta=(plnmax(ns)-pin)/(ten*t*beami)                       !--Power_vs_z Files
              write(25,604) eta                                        !
  604         format(/,14x,'Overall Maximum Efficiency =',1pe10.3,' %')!
            endif                                                      !
            if((emmodes.eq.'hermite').or.(emmodes.eq.'laguerre')) then !
                write(25,602)                                          !
  602           format(/,4x,'z(m)',6x,'Power(W)',4x,'w(cm)',2x,        !
     1            'spot_size(cm)',2x,'alpha',4x,'Rbrms(cm)',3x,'npart')!
            endif  !---------------------------------------------------!
c
           if((emmodes.eq.'hermite').or.(emmodes.eq.'laguerre'))then !-!
             do iters=1,itermax                                        !
               read(30+ns) zscale,Pwr,wm,W_Spot,alp,Rbrm,np            !
               if(np.gt.0) write(25,603) zscale,Pwr,wm,                !
     1                     W_Spot,alp,Rbrm,np                          !
  603          format(6(1x,1pe10.3),1x,i7)                             !
               if(np.eq.0) write(25,605) zscale,Pwr,wm,                !
     1                     W_spot,alp,np                               !--Power_vs_z Output
  605          format(5(1x,1pe10.3),12x,i7)                            !--for Gaussian Optical
               if((OPC.eq.'on').and.(zscale.eq.zero)                   !--Modes
     1                                 .and.(slippage.eq.'off')) then  !
                   open(unit=22,file='1st_Harmonic_Power.dat',         !
     1                  form='formatted',status='replace')             !
                   write(22,*) Pwr                                     !
                   close(unit=22)                                      !
               endif                                                   !
             end do                                                    !
           endif  !----------------------------------------------------!
         endif  ! endif for Power_vs_z='on'
c
         if((Power_vs_z.eq.'on').and.(nwrite.gt.0)) then  !------------!
            if((emmodes.eq.'hermite').or.(emmodes.eq.'laguerre')) then !
              do nm=1,nmodes                                           !
                rewind(17)                                             !
                write(25,611) lnum(nm,ns),nnum(nm,ns)                  !
  611           format(//,20x,'Gauss-Hermite(',i3,',',i3,') Mode')     !
                write(25,612)                                          !
  612           format(/,4x,'kwz',8x,'power',5x,'imk/kw',5x,'rek/kw',  !
     1              4x,'npart')                                        !--Output Data
    8           continue                                               !--From Other Modes
                do nn=1,nmodes                                         !
                   read(17) z,dans,plnns,akins,akzns,ipartns,ptotns    !
                end do                                                 !
                write(25,613) z,dans,plnns,akins,akzns,ipartns,ptotns  ! 
  613           format(5(1x,1pe10.3),1x,i6,1x,e10.3)                   !
                if(z.lt.(ztotal-dble(nstep-1)+amicro)) go to 8         !
              end do                                                   !
            endif                                                      !
         endif  !------------------------------------------------------!
         if((slippage.ne.'on').or.(Power_vs_z.eq.'on')) close(unit=25)
      end do       ! end loop over nslices for ouput of Power_vs_z files
c
      if(slippage.eq.'on'.and.myid.eq.0) then  !--Output Energy_vs_z when slippage='on'
         slicenum='off'
         if(Power_vs_z.eq.'on') then
            do ns=1,nslices
               rewind(30+ns)
            end do
         endif
         if(restart.ne.'on') then  !-----------------------------------!--Open
               open(unit=26,file='Energy_vs_z.txt',form='formatted',   !--Energy_vs_z
     1              status='replace')                                  !--File for
               write(26,501) Minerva_Version,Version_Date              !--Amplifier Mode
         endif  !------------------------------------------------------!
         if(restart.eq.'on') then  !-----------------------------------!
               open(unit=26,file='Energy_vs_z.txt',form='formatted',   !--Append Data
     1              status='old',access='append')                      !--for Restart
         endif !-------------------------------------------------------!
c
         switch='off'  !-----------------------------------------------!
         iter1st=2                                                     !
         if(restart.ne.'on') then                                      !
            call headers(wigerrs,prebunch,iprofile,matchbeam,escale,   !
     1          fscale,energy,beami,current,ndz,ztotal,rbmin,rbmax,    !
     2          xbeam,ybeam,gamma,dgam,dgamz,dp0,epsx,epsy,akbeta,     !
     3          gammaavg,betaavg,twissgamx,twissgamy,twissalphx,       !
     4          twissalphy,betax,betay,nbeams,psiwidth,source,iseed0,  !--Write Header to
     5          error,waist,rg,rg_x,rg_y,dz,nstep,zmax,vz0,            !--Energy_vs_z file
     6          nflag,1,26,t_sep,t_rndtrp,slicenum,switch,tlight,      !--When Restart='off'
     7          Shot_Noise,noise_seed,B_ratio,trans_cor,nslices_P,     !
     8          x_max,y_max,sigmapx_P,sigmapy_P,Gauss_Quad,dinput_file,!
     9          twissbetax,twissbetay,Runge_Kutta,interpolation)       !
            write(26,700)                                              !
  700       format(/,25x,'z(m)',6x,'Energy(J)')                        !
            iter1st=1                                                  !
         endif  !------------------------------------------------------!
         do iters=iter1st,itermax
            write(26,701) zwrite(iters),Pfinal(iters)
  701       format(21x,1pe12.5,1x,e10.3)
            if((OPC.eq.'on').and.(zwrite(iters).eq.zero)) then
                  open(unit=22,file='1st_Harmonic_Energy.dat',
     1                 form='formatted',status='replace')
                  write(22,*) Pfinal(iters)
                  close(unit=22)
            endif
         end do 
         close(unit=26)         
      endif
      return
      end
c**********************************************************************
      subroutine Slip_Out
      use Slice_Diags
      use modes
      use harmonics
      use diagnstc
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,smallest=1.d-99,tiny=1.d-8)
      character*8 slippage
      character*12 tprofile,teprofile
      character*62 filename
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
c----------------------------------------------------------------------
      do id=1,ndiags
        do nh=1,nharms
           numharm=int(.5d0+omega(nh1(nh,1))/domega)
           if(numharm.eq.1) write(filename,104) zdiags(id)
  104      format('1st_Harmonic_Powers_vs_time_z(m)=',1pe11.4,
     1               '.txt')
           if(numharm.eq.2) write(filename,105) zdiags(id)
  105      format('2nd_Harmonic_Powers_vs_time_z(m)=',1pe11.4,
     1               '.txt')
           if(numharm.eq.3) write(filename,106) zdiags(id)
  106      format('3rd_Harmonic_Powers_vs_time_z(m)=',1pe11.4,
     1               '.txt')
           if(numharm.ge.4) write(filename,107) numharm,zdiags(id)
  107      format(i5,'th_Harmonic_Powers_vs_time_z(m)=',1pe11.4,
     1               '.txt')
c
           open(unit=25,file=filename,form='formatted',
     1          status='replace')
c
           if(numharm.eq.1) write(25,112) zdiags(id)
  112      format('  1st_Harmonic_Powers_vs_time',//,8x,'z(m)=',
     1               1pe12.5,/)
           if(numharm.eq.2) write(25,113) zdiags(id)
  113      format('  2nd_Harmonic_Powers_vs_time',//,8x,'z(m)=',
     1               1pe12.5,/)
           if(numharm.eq.3) write(25,114) zdiags(id)
  114      format('  3rd_Harmonic_Powers_vs_time',//,8x,'z(m)=',
     1               1pe12.5,/)
           if(numharm.ge.4) write(25,115) numharm,zdiags(id)
  115      format(2x,i5,'th_Harmonic_Powers_vs_time',//,8x,'z(m)=',
     1               1pe12.5,/)
           write(25,116)
  116      format('   slice',3x,'time(s)',4x,'Power(W)',4x,'TEM00')
           do ns=1,nslices
              time=dble(ns-1)*dtpulse
              if(P_slice(ns,nh,id).gt.smallest) then
                     write(25,117) ns,time,P_slice(ns,nh,id),
     1                             P00_slice(ns,nh,id)
                  else
                     write(25,117) ns,time,zero,zero
              endif
  117         format(2x,i6,3(1x,1pe10.3))
           end do
           close(unit=25)
        end do
      end do
      return
      end
c**********************************************************************
      subroutine Spec_Out
      use modes
      use harmonics
      use wiggler
      use Spec_Diags
      use diagnstc
      implicit double precision(a-h,o-z)
      parameter(hundredth=0.01d0)
      character*62 filename
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
c----------------------------------------------------------------------
      do id=1,ndiags
         write(filename,101) zdiags(id)
  101    format('Power_Spectrum_z(m)=',1pe11.4,'.txt')
         open(unit=23,file=filename,form='formatted',status='replace')
         write(23,102) zdiags(id)
  102    format(15x,'Power Spectrum',//,15x,'z(m)=',1pe10.3)
         write(23,104)
  104    format(/,3x,'omega/ckw',3x,'Wavelength(m)',4x,'Power(W)')
         do nh=1,nharms
           wavelnth=hundredth*zwi(1)/omega(nh1(nh,1))
           write(23,105) omega(nh1(nh,1)),wavelnth,P_spec(nh,id)
  105      format(3(1x,1pe13.6))
         end do
         close(unit=23)
      end do
      return
      end
c**********************************************************************
      subroutine Head_Harm(ns,Power_vs_z,myid)
      use modes
      use harmonics
      use wiggler
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,one=1.d0,hundredth=0.01d0,tiny=1.d-8)
      character*49 filename,filename6
      character*8 slippage,Power_vs_z
      character*12 tprofile,teprofile
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype      
c----------------------------------------------------------------------
      do nh=2,nharms
         numharm=int(.5d0+omega(nh1(nh,1))/domega)
         nouth=nhout(nh,ns)
         if((nslices.gt.1).and.(ns.eq.ns_begin).and.(myid.eq.0)) then
            nouteh=nehout(nh)
         endif
         if(numharm.eq.2) then  !--------------------------------------!
          if(Power_vs_z.ne.'off') then                                 !
            if(nslices.eq.1) write(filename6,200)                      !
  200       format('2nd_Harmonic_Power_vs_z.txt')                      !
          endif                                                        !
            if(nslices.gt.1) then                                      !
               if(Power_vs_z.ne.'off') write(filename6,202) ns         !
  202          format('2nd_Harmonic_Power_vs_z_slice=',i5.5,'.txt')    !
               if((ns.eq.ns_begin).and.(myid.eq.0)) write(filename,300)!
  300          format('2nd_Harmonic_Energy_vs_z.txt')                  !
            endif                                                      !
            if(Power_vs_z.ne.'off') then                               !--2nd Harmonic
                open(unit=nouth,file=filename6,form='formatted',       !
     1               status='replace')                                 !
                write(nouth,204)                                       !
  204           format(10x,'2nd HARMONIC POWER versus z')              !
            endif                                                      !
            if(ns.eq.ns_begin.and.nslices.gt.1.and.(myid.eq.0)) then   !
               open(unit=nouteh,file=filename,form='formatted',        !
     1              status='replace')                                  !
               write(nouteh,302)                                       !
  302          format(10x,'2nd HARMONIC ENERGY versus z')              !
            endif                                                      !
         endif  !------------------------------------------------------!
         if(numharm.eq.3) then  !--------------------------------------!
           if(Power_vs_z.ne.'off') then                                !
             if(nslices.eq.1) write(filename6,205)                     !
  205        format('3rd_Harmonic_Power_vs_z.txt')                     !
           endif                                                       !
            if(nslices.gt.1) then                                      !
               if(Power_vs_z.ne.'off') write(filename6,207) ns         !
  207          format('3rd_Harmonic_Power_vs_z_slice=',i5.5,'.txt')    !
               if((ns.eq.ns_begin).and.(myid.eq.0)) write(filename,303)!--3rd Harmonic
  303          format('3rd_Harmonic_Energy_vs_z.txt')                  !
            endif                                                      !
            if(Power_vs_z.ne.'off') then                               !
               open(unit=nouth,file=filename6,form='formatted',        !
     1              status='replace')                                  !
               write(nouth,209)                                        !
  209          format(10x,'3rd HARMONIC POWER versus z')               !
            endif                                                      !
            if(ns.eq.ns_begin.and.nslices.gt.1.and.(myid.eq.0)) then   !
               open(unit=nouteh,file=filename,form='formatted',        !
     1              status='replace')                                  !
               write(nouteh,305)                                       !
  305          format(10x,'3rd HARMONIC ENERGY versus z')              !
            endif                                                      !
         endif  !------------------------------------------------------!
         if(numharm.ge.4) then  !--------------------------------------!
            if(Power_vs_z.ne.'off') then                               !
              if(nslices.eq.1) write(filename6,210) numharm            !
  210         format(i5,'th_Harmonic_Power_vs_z.txt')                  !
            endif                                                      !
            if(nslices.gt.1) then                                      !
               if(Power_vs_z.ne.'off') write(filename6,212) numharm,ns !
  212          format(i5,'th_Harmonic_Power_vs_z_slice=',i5.5,'.txt')  !
               if((ns.eq.ns_begin).and.(myid.eq.0))                    !
     1                                      write(filename,306) numharm!
  306          format(i5,'th_Harmonic_Energy_vs_z.txt')                !
            endif                                                      !-->/= 4th Harmonic
            if(Power_vs_z.ne.'off') then                               !
               open(unit=nouth,file=filename6,form='formatted',        !
     1              status='replace')                                  !
               write(nouth,214) numharm                                !
  214          format(10x,i5,'th HARMONIC POWER versus z')             !
            endif                                                      !
            if(ns.eq.ns_begin.and.nslices.gt.1.and.(myid.eq.0)) then   !
               open(unit=nouteh,file=filename,form='formatted',        !
     1              status='replace')                                  !
               write(nouteh,308) numharm                               !
  308          format(10x,i5,'th HARMONIC ENERGY versus z')            !
            endif                                                      !
         endif  !------------------------------------------------------!
         if(Power_vs_z.ne.'off') then
            if(nslices.gt.1) write(nouth,216) ns
  216       format(/,15x,'Slice No.:',i6)
         endif
         if(Power_vs_z.ne.'off') then
            write(nouth,218) hundredth*zwi(1)/omega(nh1(nh,1))
         endif
         if((nslices.gt.1).and.(ns.eq.ns_begin).and.(myid.eq.0)) then
            write(nouteh,218) hundredth*zwi(1)/omega(nh1(nh,1))
         endif
  218    format(/,10x,' Wavelength(m) =',1pe12.5,/)
c
         if(Power_vs_z.ne.'off') then
            do nm=nh1(nh,ns),nh2(nh,ns)
               alnsq=a1(nm,ns)**2+a2(nm,ns)**2
               Power=alnsq/pfac(nm,ns)
               write(nouth,245) lnum(nm,ns),nnum(nm,ns),Power
  245          format('Gauss-Hermite(',i3,',',i3,') Mode: Power(W)=',
     1              1pe10.3)
             end do
             write(nouth,244)
  244        format(/,4x,'z(m)',6x,'Power(W)',4x,'w(cm)',2x,
     1              'spot_size(cm)',2x,'alpha')
          endif
          if((nslices.gt.1).and.(ns.eq.ns_begin).and.(myid.eq.0)) then
             write(nouteh,309)
  309        format(/,4x,'z(m)',6x,'Energy(J)')
          endif
      end do  !---End Loop over Harmonics
      return
      end
c**********************************************************************
      subroutine Head_Diag(id,ns,nunit6,nunit7,trans_mode,beam_diags,
     1                     Fluence_only)
      use diagnstc
      implicit double precision(a-h,o-z)
      character*8 slippage,trans_mode,beam_diags,Fluence_only
      character*12 tprofile,teprofile
      character*57 filename7,filename9
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
c----------------------------------------------------------------------
      if(beam_diags.eq.'on') then
         if(nslices.eq.1) write(filename7,400) zdiags(id)
  400    format('Beam_Diagnostics_z(m)=',1pe10.3,'.txt')
         if(nslices.gt.1) write(filename7,401) zdiags(id),ns
  401    format('Beam_Diagnostics_z(m)=',1pe12.5,'_slice=',i5.5,'.txt')
         open(unit=nunit6,file=filename7,form='formatted',
     1        status='replace')
         write(nunit6,402)
  402    format(29x,'BEAM DIAGNOSTIC OUTPUT')
         if(nslices.gt.1) write(nunit6,403) ns
  403    format(/,32x,'Slice No.:',i6)
      endif
c
      if((trans_mode.eq.'on').and.(Fluence_only.eq.'no')) then
         if(nslices.eq.1) write(filename9,404) zdiags(id)
  404    format('Mode_Diagnostics_z(m)=',1pe10.3,'.txt')
         if(nslices.gt.1) write(filename9,405) zdiags(id),ns
  405    format('Mode_Diagnostics_z(m)=',1pe12.5,'_slice=',i5.5,'.txt')
         open(unit=nunit7,file=filename9,form='formatted',
     1        status='replace')
         write(nunit7,406)
  406    format(8x,'MODE DIAGNOSTIC OUTPUT')
         if(nslices.gt.1) write(nunit7,407) ns
  407    format(/,12x,'Slice No.:',i6)
      endif
      return
      end
c**********************************************************************
c**********************************************************************
c                                                                     *
c     These are utility/special function subroutines and functions    *
c                                                                     *
c**********************************************************************
c**********************************************************************
      subroutine hermite(t,m,npoints,nxy)
      use particles
      use numbers, only : one, two
      implicit double precision(a-h,o-z)
      dimension t(npoints)
c----------------------------------------------------------------------
      if(m.gt.0) then
         do k=1,npoints
             al(k,2,nxy)=two*t(k)
         end do
         do i=1,m-1
           twoi=dble(2*i)
           do k=1,npoints
             al(k,i+2,nxy)=two*t(k)*al(k,i+1,nxy)-twoi*al(k,i,nxy)
           end do
         end do
      endif
      return
      end
c**********************************************************************
      double precision function Hm(x,m)
      use numbers, only : one, two
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      if(m.eq.0) Hm=one
      if(m.eq.1) Hm=two*x
      if(m.gt.1) then
         H1=two*x
         H0=one
         do i=1,m-1
           Hm=two*x*H1-dble(2*i)*H0
           H0=H1
           H1=Hm
         end do
      endif
      return
      end
c**********************************************************************
      subroutine laguerre(t,lmax,mmax,npoints)
      use particles
      use numbers, only : one, two
      implicit double precision(a-h,o-z)
      dimension t(npoints)
c----------------------------------------------------------------------
c
c------- calculate m=0, 0 < l < lmax
c
      do l=1,lmax+1
        do k=1,npoints
            al(k,l,1)=one
        end do
      end do
      if(mmax.eq.0) return
c
c------- calculate m=1, 0 < l < lmax
c
      do l=1,lmax+1
        do k=1,npoints
            al(k,l,2)=dble(l)-t(k)
        end do
      end do
      if(mmax.eq.1) return
c
c------- iterate upwards
c
      do mm=3,mmax+1
         m=mm-1
         do ll=1,lmax+1
            l=ll-1
            do k=1,npoints
               al(k,ll,mm)=((dble(2*m-2+ll)-t(k))*al(k,ll,m)
     1                          -dble(m-1+l)*al(k,ll,m-1))/dble(m)
            end do
         end do
      end do
      return
      end
c**********************************************************************
      subroutine laguerre_2(arg,t,l,m)
      use numbers, only : one, two
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
c
c------- calculate m=0 & l>=0
c
      t=one
      if(m.eq.0) return
c
c------- calculate m=1 & l>=0
c
      t=dble(l+1)-arg
      if(m.eq.1) return
c
c------- iterate upwards
c
      t_0=one
      t_1=t
      do mm=2,m
         t=((dble(2*mm+l-1)-arg)*t_1-dble(mm+l-1)*t_0)/dble(mm)
         t_0=t_1
         t_1=t
      end do
      return
      end
c**********************************************************************
      double precision function factorial(n)
      use numbers, only : one
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      factorial=one
      if(n.le.1) return
      do i=2,n
         factorial=dble(i)*factorial
      end do
      return
      end
c**********************************************************************
      double precision function ran3(idum)
      implicit double precision(a-h,o-z)
      parameter(mbig=1000000000,mseed=161803398,mz=0,fac=1.d0/mbig)
      dimension ma(55)
      save inext,inextp,ma
      data iff/0/
c----------------------------------------------------------------------
c     Returns a uniform deviate between 0.0 and 1.0. Set idum to any
c     negative value to initialize or reinitialize the sequence.
c
c     Taken from Numerical Recipes
c
c----------------------------------------------------------------------
      if((idum.lt.0).or.(iff.eq.0)) then
         iff=1
         mj=mseed-iabs(idum)
         ma(55)=mj
         mj=mod(mj,mbig)
         mk=1
         do i=1,54
            ii=mod(21*i,55)
            ma(ii)=mk
            mk=mj-mk
            if(mk.lt.mz) mk=mk+mbig
            mj=ma(ii)
         end do
         do k=1,4
           do i=1,55
              ma(i)=ma(i)-ma(1+mod(i+30,55))
              if(ma(i).lt.mz) ma(i)=ma(i)+mbig
           end do
         end do
         inext=0
         inextp=31
         idum=1
      endif
      inext=inext+1
      if(inext.eq.56) inext=1
      inextp=inextp+1
      if(inextp.eq.56) inextp=1
      mj=ma(inext)-ma(inextp)
      if(mj.lt.mz) mj=mj+mbig
      ma(inext)=mj
      ran3=mj*fac
      return
      end
c**********************************************************************
      double precision function gaussdev(idum)
      use numbers, only : one, two
      implicit double precision(a-h,o-z)
      save iset,gset
      data iset/0/
c----------------------------------------------------------------------
c     This function calculates random deviates for a Gaussian distribu-
c     tion to be used to load particles randomly based on the statisti-
c     cal properties of the spent beam
c----------------------------------------------------------------------
      if(iset.eq.0) then
    1       v1=two*ran3(idum)-one
            v2=two*ran3(idum)-one
            r=v1**2+v2**2
            if(r.ge.one) go to 1
            fac=sqrt(-two*log(r)/r)
            gset=v1*fac
            gaussdev=v2*fac
            iset=1
         else
            gaussdev=gset
            iset=0
         endif
      return
      end
c**********************************************************************
      double precision function erf(x)
      implicit double precision(a-h,o-z)
      parameter(one=1.d0)
      data a1/.254829592d0/,a2/-.284496736d0/,a3/1.421413741d0/
     1     a4/-1.453152027d0/,a5/1.061405429d0/,p/.3275911d0/
c----------------------------------------------------------------------
      t=one/(one+p*x)
      erf=one-t*exp(-x**2)*(a1+t*(a2+t*(a3+t*(a4+a5*t))))
      return
      end
c**********************************************************************
      subroutine gauleg(n)
      use gauss_weights
      implicit double precision (a-h,o-z)
      parameter(eps=2.d-16,half=.5d0,pi=3.141592653589793d0,one=1.d0,
     1          quarter=.25d0,zero=0.d0,two=2.d0,oneneg=-1.d0)
      data x1/oneneg/,x2/one/
c----------------------------------------------------------------------
      m=(n+1)/2
      xm=half*(x2+x1)
      xl=half*(x2-x1)
      do i=1,m
         z=cos(pi*(i-quarter)/(n+half))
    1    continue
           p1=one
           p2=zero
           do j=1,n
              p3=p2
              p2=p1
              p1=((two*j-one)*z*p2-(j-one)*p3)/j
           end do
           pp=n*(z*p1-p2)/(z*z-one)
           z1=z
           z=z1-p1/pp
         if(abs(z-z1).gt.eps) go to 1
         xw(n,i)=xm-xl*z
         xw(n,n+1-i)=xm+xl*z
         w(n,i)=two*xl/((one-z*z)*pp*pp)
         w(n,n+1-i)=w(n,i)
      end do
      do i=1,n
         if(abs(xw(n,i)).lt.eps) xw(n,i)=zero
      end do
      return
      end
c********************************************************************
      subroutine bessels(n,ipart,xmax)
      use bessel
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,one=1.d0,two=2.d0,deci=1.d-10)
c-------------------------------------------------------------------
      m=max0(2*int(xmax),n)+11
      t=one
       if(m.eq.2*(m/2)) t=-one
      do k=1,ipart
         rx(k)=two/xbes(k)
         fm(k)=zero
         fm1(k)=deci
      alphab(k)=zero
      end do
      do 5 l=1,m-2
      t=-t
      tone=one+t
      mk=m-l
      do k=1,ipart
         fmk(k)=rx(k)*dble(mk)*fm1(k)-fm(k)
         fm(k)=fm1(k)
         fm1(k)=fmk(k)
        alphab(k)=alphab(k)+tone*fmk(k)
      end do
      if(mk.ne.(n+1)) go to 5
      do k=1,ipart
         besjp(k)=fm(k)
         besjl(k)=fmk(k)
      end do
    5 continue
      do k=1,ipart
         fmk(k)=rx(k)*fm1(k)-fm(k)
         alphab(k)=alphab(k)+fmk(k)
      end do
      if(n.gt.0) go to 8
      do k=1,ipart
         besjp(k)=fm1(k)
         besjl(k)=fmk(k)
      end do
    8 continue
      do k=1,ipart
         besjp(k)=besjp(k)/alphab(k)
         besjl(k)=besjl(k)/alphab(k)
      end do
      return
      end
c********************************************************************
      subroutine bsJ_n(n,x,bj_n,bj_p)
      implicit double precision(a-h,o-z)
      parameter(nparts=10000)
      parameter(zero=0.d0,one=1.d0,two=2.d0,deci=1.d-10)
c-------------------------------------------------------------------
      if(x.eq.zero) then
         bj_n=one
         bj_p=zero
         return
      endif
      m=max0(2*int(x),n)+11
      t=one
      if(m.eq.2*(m/2)) t=-one
      rx=two/x
      fm=zero
      fm1=deci
      alpha=zero
      do 5 l=1,m-2
         t=-t
         tone=one+t
         mk=m-l
         fmk=rx*dble(mk)*fm1-fm
         fm=fm1
         fm1=fmk
         alpha=alpha+tone*fmk
         if(mk.ne.(n+1)) go to 5
         bj_p=fm
         bj_n=fmk
    5 continue
      fmk=rx*fm1-fm
      alpha=alpha+fmk
      if(n.gt.0) go to 8
      bj_p=fm1
      bj_n=fmk
    8 bj_p=bj_p/alpha
      bj_n=bj_n/alpha
      return
      end
c**********************************************************************
c**********************************************************************
c                                                                     *
c     These subroutines handle the FFT calculation using the routines *
c     from Numerical Recipes                                          *
c                                                                     *
c**********************************************************************
c**********************************************************************
      subroutine Power_FFT(nFFT)
      use modes
      use harmonics
      use wiggler
      use FFT
      use diagnstc
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,one=1.d0,two=2.d0,four=4.d0,hundredth=0.01d0,
     1          cvac=2.99792458d10,twopi=6.283185307179586d0)
      character*8 slippage
      character*12 tprofile,teprofile
      character*48 filename
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
      double complex fft1(nFFT),fft2(nFFT)
      dimension Pout(2*nFFT+1),wout(2*nFFT+1),datat(2*nFFT),
     1          Ptemp(2*nFFT+1)
c----------------------------------------------------------------------
      df=one/(dble(nFFT)*dtpulse)
      do id=1,ndiags
         do nh=1,nharms
            numharm1=int(0.5d0+omega(nh1(nh,1))/domega)
            omega1=omega(nh1(nh,1))
            f0=cvac*akw(1)*omega(nh1(nh,1))/twopi
            if(numharm1.eq.1) write(filename,100) zdiags(id)
  100            format('1st_Harmonic_FFT_z(m)=',1pe11.4,'.txt')
            if(numharm1.eq.2) write(filename,101) zdiags(id)
  101            format('2nd_Harmonic_FFT_z(m)=',1pe11.4,'.txt')
            if(numharm1.eq.3) write(filename,102) zdiags(id)
  102            format('3rd_Harmonic_FFT_z(m)=',1pe11.4,'.txt')
            if(numharm1.ge.4) write(filename,103) numharm1,zdiags(id)
  103            format(i2,'th_Harmonic_FFT_z(m)=',1pe11.4,'.txt')
            open(unit=23,file=filename,form='formatted',
     1           status='replace')
            if(numharm1.eq.1) write(23,108) zdiags(id)
  108            format(8x,'1st Harmonic FFT',//,8x,'z(m)=',1pe11.4)
            if(numharm1.eq.2) write(23,109) zdiags(id)
  109            format(8x,'2nd Harmonic FFT',//,8x,'z(m)=',1pe11.4)
            if(numharm1.eq.3) write(23,110) zdiags(id)
  110            format(8x,'3rd Harmonic FFT',//,8x,'z(m)=',1pe11.4)
            if(numharm1.ge.4) write(23,111) numharm1,zdiags(id)
  111            format(8x,i2,'th Harmonic FFT',//,8x,'z(m)=',1pe11.4)
c
c           N.B.: this algorithm is valid as long as the number of
c                 modes for each harmonic is the same for each slice
c
            Ptemp=zero
            datat=zero
            do nm=nh1(nh,1),nh2(nh,1)
               do ns=1,nslices
                  j=2*(ns-1)+1
                  Pfc=one/sqrt(pfac(nm,ns))
                  datat(j)=Pfc*A1FFT(ns,nm,id)
                  datat(j+1)=Pfc*A2FFT(ns,nm,id)
               end do
               do nss=2*nslices+1,2*nFFT
                  datat(nss)=zero
               end do
               call four1(datat,nFFT,1)
               do ns=1,nFFT
                  j=2*(ns-1)+1
                  Ptemp(ns)=Ptemp(ns)+datat(j)**2+datat(j+1)**2
               end do
            end do
            Pout(nFFT/2)=Ptemp(1)
            wout(nFFT/2)=cvac/f0
            nFFTh=nFFT/2
            do i=2,nFFTh
               ip=i-1
               im=nFFT-ip
               wout(nFFTh+ip)=cvac/(f0-dble(ip)*df)
               wout(nFFTh-ip)=cvac/(f0+dble(ip)*df)
               Pout(nFFTh+ip)=Ptemp(i)
               Pout(nFFTh-ip)=Ptemp(im)
            end do          
            Poutmax=zero
            do i=1,nFFT-1
               Poutmax=max(Pout(i),Poutmax)
            end do
            avg_wavelength=zero  !-------------------------------------!
            sum_P=zero                                                 !
            do i=1,nFFT-1                                              !
               sum_P=sum_P+Pout(i)                                     !
               avg_wavelength=avg_wavelength+wout(i)*Pout(i)           !
            end do                                                     !--Determine
            avg_wavelength=avg_wavelength/sum_P                        !--Wavelength &
            sum_rms=zero                                               !--Relative
            do i=1,nFFT-1                                              !--Linewidth
               sum_rms=sum_rms+Pout(i)*(wout(i)-avg_wavelength)**2     !
            end do                                                     !
            rel_linewidth=sqrt(sum_rms/sum_P)/avg_wavelength           !
            write(23,201) hundredth*avg_wavelength,rel_linewidth       !
  201       format(/,' Average Wavelength=',1pe13.6,' m'               !
     1             /,' Relative Linewidth=',e13.6) !-------------------!
            write(23,116)
  116       format(/,' Wavelength(m)',2x,'Power(arb_units)')
            do i=1,nFFT-1
               write(23,200) hundredth*wout(i),Pout(i)/Poutmax
  200          format(1x,1pe13.6,3x,e13.6)
            end do
            close(unit=23)  
         end do  ! End loop over nharms  
      end do     ! End loop over idiags
      return
      end
c********************************************************************
      subroutine four1(fftdata,nn,isign)
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,half=0.5d0,one=1.d0,two=2.d0,
     1          twopi=6.283185307179586d0)
      dimension fftdata(2*nn)
c--------------------------------------------------------------------
      n=2*nn
      j=1
      do i=1,n,2
         if(j.gt.i) then
             tempr=fftdata(j)
             tempi=fftdata(j+1)
             fftdata(j)=fftdata(i)
             fftdata(j+1)=fftdata(i+1)
             fftdata(i)=tempr
             fftdata(i+1)=tempi
          endif
          m=n/2
    1     if((m.ge.2).and.(j.gt.m)) then
             j=j-m
             m=m/2
             go to 1
          endif
          j=j+m
      end do
      mmax=2
    2 if(n.gt.mmax) then
          istep=2*mmax
          theta=twopi/(isign*mmax)
          wpr=-two*sin(half*theta)**2
          wpi=sin(theta)
          wr=one
          wi=zero
          do m=1,mmax,2
             do i=m,n,istep
                j=i+mmax
                tempr=wr*fftdata(j)-wi*fftdata(j+1)
                tempi=wr*fftdata(j+1)+wi*fftdata(j)
                fftdata(j)=fftdata(i)-tempr
                fftdata(j+1)=fftdata(i+1)-tempi
                fftdata(i)=fftdata(i)+tempr
                fftdata(i+1)=fftdata(i+1)+tempi
             end do
             wtemp=wr
             wr=wr*wpr-wi*wpi+wr
             wi=wi*wpr+wtemp*wpi+wi
          end do
          mmax=istep
          go to 2
      endif
      return
      end
c**********************************************************************
      double precision function Spot_Size(nh,ns)
      use modes
      use harmonics
      use wiggler
      use numbers
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      Bsq=zero
      rsqBsq=zero
      if(emmodes.eq.'hermite') then
         do nm=nh1(nh,ns),nh2(nh,ns)
            l=lnum(nm,ns)
            n=nnum(nm,ns)
            fac=factorial(l)*factorial(n)*two**(l+n)
            Asq=a1(nm,ns)**2+a2(nm,ns)**2
            Bsq=Bsq+fac*Asq
            rsqBsq=rsqBsq+fac*dble(l+n+1)*Asq
            do nm2=nh1(nh,ns),nh2(nh,ns)
               l2=lnum(nm2,ns)
               n2=nnum(nm2,ns)
               if((l2.eq.l+2).and.(n.eq.n2)) then
                  Asq=a1(nm,ns)*a1(nm2,ns)+a2(nm,ns)*a2(nm2,ns)
                  rsqBsq=rsqBsq+two*fac*dble((l+1)*(l+2))*Asq
               endif
               if((n2.eq.n+2).and.(l.eq.l2)) then
                  Asq=a1(nm,ns)*a1(nm2,ns)+a2(nm,ns)*a2(nm2,ns)
                  rsqBsq=rsqBsq+two*fac*dble((n+1)*(n+2))*Asq
               endif
            end do
         end do
      endif
      if(emmodes.eq.'laguerre') then
         do nm=nh1(nh,ns),nh2(nh,ns)
            l=lnum(nm,ns)
            n=nnum(nm,ns)
            fac=factorial(abs(l)+n)/factorial(n)
            Asq=a1(nm,ns)**2+a2(nm,ns)**2
            Bsq=Bsq+fac*Asq
            rsqBsq=rsqBsq+fac*dble(l+2*n+1)*Asq
            do nm2=nh1(nh,ns),nh2(nh,ns)
               l2=lnum(nm2,ns)
               n2=nnum(nm2,ns)
               if((l2.eq.l).and.(n2.eq.n-1))
     1            rsqBsq=rsqBsq-dble(n)*fac
     2                     *(a1(nm,ns)*a1(nm2,ns)+a2(nm,ns)*a2(nm2,ns))
               if((l2.eq.l).and.(n2.eq.n+1))
     1            rsqBsq=rsqBsq-dble(n+abs(l)+1)*fac
     2                     *(a1(nm,ns)*a1(nm2,ns)+a2(nm,ns)*a2(nm2,ns))
            end do
         end do
      endif
c
      if(Bsq.gt.zero) then
            Spot_Size=asde1(nh,ns)*sqrt(rsqBsq/Bsq)/akw(1)
         else
            Spot_Size=asde1(nh,ns)/akw(1)
      endif
      return
      end
c**********************************************************************
      subroutine M_Squared
      use numbers
      use modes
      use harmonics
      use wiggler
      use M_sqr
      use diagnstc
      implicit double precision(a-h,o-z)
      character*8 slippage
      character*12 tprofile,teprofile
      character*55 filename
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
c----------------------------------------------------------------------
      write(filename,199)
  199 format('M_Squared_Test.txt')
      open(unit=171,file=filename,form='formatted',status='replace')
      write(171,201)
  201 format(/,21x,'M Squared Test',/)
         if(slippage.ne.'on') write(171,203)
  203       format(6x,'z(m)',6x,'Harmonic',5x,'M_Squared')
         if(slippage.eq.'on') write(171,204)
  204       format(6x,'z(m)',6x,'Harmonic',5x,'Slice',5x,
     1             'M_Squared')
      do ns=ns_begin,ns_end
         do nh=1,nharms
            do id=1,ndiags
               w=asde1_M2(nh,ns,id)/akw(1)
               w2=w**2
               Bsq=zero
               rsqBsq=zero
c              Csq=zero
               do nm=nh1(nh,ns),nh2(nh,ns)
                  l=lnum(nm,ns)
                  n=nnum(nm,ns)
                  fac=factorial(abs(l))*factorial(n)*two**(abs(l)+n)
                  Asq=A1_M2(nm,ns,id)**2+A2_M2(nm,ns,id)**2
                  Bsq=Bsq+fac*Asq
                  rsqBsq=rsqBsq+fac*dble(abs(l)+n+1)*Asq
c
c      The off-diagonal terms in the calcxulation of rsqBsq were found to
c      vanish numerically --> deleted from the calculation
c
c      The other off-diagonal terms should not contribute due to the
c      randomness of the phase average in time --> delete Csq, t2, t3, etc.
c      contributions.
c
c                 do nm2=nh1(nh,ns),nh2(nh,ns)
c                    l2=lnum(nm2,ns)
c                    n2=nnum(nm2,ns)
c                    if((l2.eq.l+2).and.(n.eq.n2)) then
c                       Asq=A1_M2(nm,ns,id)*A1_M2(nm2,ns,id)
c    1                                +A2_M2(nm,ns,id)*A2_M2(nm2,ns,id)
c                       Asqm=A1_M2(nm,ns,id)*A2_M2(nm2,ns,id)
c    1                                -A1_M2(nm2,ns,id)*A2_M2(nm,ns,id)
c                       rsqBsq=rsqBsq+two*fac*dble((l+1)*(l+2))*Asq
c                       Csq=Csq+two*fac*dble((l+1)*(l+2))*Asqm
c                    endif
c                    if((n2.eq.n+2).and.(l.eq.l2)) then
c                       Asq=A1_M2(nm,ns,id)*A1_M2(nm2,ns,id)
c    1                                +A2_M2(nm,ns,id)*A2_M2(nm2,ns,id)
c                       Asqm=A1_M2(nm,ns,id)*A2_M2(nm2,ns,id)
c    1                                -A1_M2(nm2,ns,id)*A2_M2(nm,ns,id)
c                       rsqBsq=rsqBsq+two*fac*dble((n+1)*(n+2))*Asq
c                       Csq=Csq+two*fac*dble((n+1)*(n+2))*Asqm
c                    endif
c                end do
               end do
               if(Bsq.gt.zero) then
c                 t2norm=four/(pi*rsqBsq)
                  Bsq=halfpi*Bsq
                  rsqBsq=half*halfpi*rsqBsq
c                 Csq=halfpi*Csq
                  facM=two*rsqBsq/Bsq
c                 t1=two-Bsq/rsqBsq
c                 t2=zero
c                 t3=(half*Csq/rsqBsq)**2
c                 do nm=nh1(nh,ns),nh2(nh,ns)
c                    l1=lnum(nm,ns)
c                    n1=nnum(nm,ns)
c                    do nm2=nh1(nh,ns),nh2(nh,ns)
c                       l2=lnum(nm2,ns)
c                       n2=nnum(nm2,ns)
c                       Asq=A1_M2(nm,ns,id)*A1_M2(nm2,ns,id)
c    1                                +A2_M2(nm,ns,id)*A2_M2(nm2,ns,id)
c                       dt2=simson_r(l1,l2,n1,n2)*Asq
c                       t2=t2+dt2
c                    end do
c                 end do
c                 t2=t2norm*t2
c                 rMsq=facM*sqrt(one-t1+t2-t3)
                  rMsq=facM
                  if(slippage.ne.'on') write(171,100) zdiags(id),nh,
     1                                                rMsq
  100             format(1x,1pe12.5,3x,i5,6x,e12.5,1x,e12.5)
                  if(slippage.eq.'on') write(171,101) zdiags(id),nh,ns,
     1                                                rMsq
  101             format(1x,1pe12.5,3x,i5,4x,i5,6x,e12.5,1x,e12.5)
               endif
            end do      !  End loop over ndiags
         end do      !  End loop over nharms
      end do         !  End loop over nslices
      close(unit=171)
      return
      end
c**********************************************************************
      double precision function simson_r(l1,l2,n1,n2)
      implicit double precision (a-h,o-z)
      parameter(zero=0.d0,half=.5d0,one=1.d0,two=2.d0,three=3.d0,
     1          four=4.d0,acc=1.d-2,x_max=3.d0,maxit=25)
c----------------------------------------------------------------------
      h=half*x_max
      s=f_r(zero,l1,l2,n1,n2)+f_r(x_max,l1,l2,n1,n2)
      s=s+two*f_r(h,l1,l2,n1,n2)
      prev=h*s/three
      n=2
      do j=1,maxit
        h=half*h
        ds=zero
        do i=1,n
           x=zero+h*(one+two*dble(i-1))
           ds=ds+f_r(x,l1,l2,n1,n2)
        end do
        s=s+four*ds
        simson_r=h*s/three
        if(abs(simson_r-prev).le.acc*abs(simson_r)) return
        s=s-two*ds
        prev=simson_r
        n=2*n
      end do
      write(6,100)
  100 format(/,' convergence not achieved after max iterations for',/,
     1         ' radial integral in M^2 routine')
      return
      end
c**********************************************************************
      double precision function f_r(r,l1,l2,n1,n2)
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,half=.5d0,two=2.d0,three=3.d0,four=4.d0,
     1          one=1.d0,acc=1.e-2,maxit=25,pi=3.141592653589793d0,
     2          twopi=6.283185307179586d0)
c----------------------------------------------------------------------
      h=pi
      s=f_t(zero,r,l1,l2,n1,n2)+f_t(twopi,r,l1,l2,n1,n2)
      s=s+two*f_t(h,r,l1,l2,n1,n2)
      prev=h*s/three
      n=2
      do j=1,maxit
        h=half*h
        ds=zero
        do i=1,n
           x=zero+h*(one+two*dble(i-1))
           ds=ds+f_t(x,r,l1,l2,n1,n2)
        end do
        s=s+four*ds
        f_r=h*s/three
        if(abs(f_r-prev).le.acc*abs(f_r)) return
        s=s-two*ds
        prev=f_r
        n=2*n
      end do
      write(6,100)
  100 format(/,' convergence not achieved after max iterations',/,
     1         ' for azimuthal integral in M^2 routine')
      return
      end
c**********************************************************************
      double precision function f_t(x,r,l1,l2,n1,n2)
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,small=1.d-9)
c----------------------------------------------------------------------
      f_t=zero
      if(r.eq.zero) return
      cosx=cos(x)
      sinx=sin(x)
      cr=r*cosx
      sr=r*sinx
      if((l1.gt.0).and.(l2.gt.0)) then
         f_t=f_t+dble(l1*l2)*cosx*cosx
     1            *max(small,Hm(cr,l1-1))*max(small,Hm(cr,l2-1))
     2            *max(small,Hm(sr,n1))*max(small,Hm(sr,n2))
      endif
      if((n1.gt.0).and.(n2.gt.0)) then
         f_t=f_t+dble(n1*n2)*sinx*sinx
     1             *max(small,Hm(cr,l1))*max(small,Hm(cr,l2))
     2             *max(small,Hm(sr,n1-1))*max(small,Hm(sr,n2-1))
      endif
      if((l1.gt.0).and.(n2.gt.0)) then
         f_t=f_t+dble(l1*n2)*sinx*cosx
     1             *max(small,Hm(cr,l1-1))*max(small,Hm(cr,l2))
     2             *max(small,Hm(sr,n1))*max(small,Hm(sr,n2-1))      
      endif
      if((l2.gt.0).and.(n1.gt.0)) then
         f_t=f_t+dble(l2*n1)*sinx*cosx
     1             *max(small,Hm(cr,l1))*max(small,Hm(cr,l2-1))
     2             *max(small,Hm(sr,n1-1))*max(small,Hm(sr,n2))      
      endif
      f_t=r*f_t*exp(-r**2)
      return
      end
c**********************************************************************
c**********************************************************************
c                                                                     *
c            SUBROUTINES FOR INTERPOLATION OF FIELD MAP               *
c                                                                     *
c**********************************************************************
c**********************************************************************
      subroutine FieldMap(z,iseg,ns,nss)
      use particles
      use wiggler
      use particles_func
      use magmap
      use bmapmod
      implicit double precision(a-h,o-z)
      parameter(nfldpts=120000)
      parameter(twopi=6.283185307179586d0,two=2.d0,ten=10.d0,
     1          tenth=.1d0,hundredth=.01d0)
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      dimension x1a(iparts,3),x2a(iparts,3),x3a(iparts,3),
     1          yltmpx(iparts,3),yltmpy(iparts,3),yltmpz(iparts,3),
     2          ymtmpx(iparts,3),ymtmpy(iparts,3),ymtmpz(iparts,3),
     3          yntmpx(iparts,3),yntmpy(iparts,3),yntmpz(iparts,3),
     4          zk(iparts),xk(iparts),yk(iparts),
     5          bwx(iparts,3),bwy(iparts,3),bwz(iparts,3),
     6          kx(iparts),ky(iparts),kz(iparts)
c----------------------------------------------------------------------
      do k=1,ipart(ns)
         xk(k)=x(k,nss)
         yk(k)=y(k,nss)
         zk(k)=z
      end do
      kzmax=int(1+(zstop(iseg)-zstart(iseg))/delz(iseg))
      kxmax=int(1+xfmax(iseg)/delx(iseg))
      kymax=int(1+yfmax(iseg)/dely(iseg))
      do k=1,ipart(ns)
         kx(k)=min(1+int(1+xk(k)/delx(iseg)),kxmax-1)
         ky(k)=min(1+int(1+yk(k)/dely(iseg)),kymax-1)
         kz(k)=min(1+int(1+(zk(k)-zstart(iseg))/delz(iseg)),kzmax-1)
      end do
      do i=1,3
         do k=1,ipart(ns)
            x1a(k,i)=delx(iseg)*dble(i+kx(k)-3)
            x2a(k,i)=dely(iseg)*dble(i+ky(k)-3)
            x3a(k,i)=zstart(iseg)+delz(iseg)*dble(i+kz(k)-3)
         end do
      end do
      do k=1,3
        do j=1,3
          do i=1,3
            do kk=1,ipart(ns)
               yltmpx(kk,i)=bxarr(i+kx(kk)-2,j+ky(kk)-2,k+kz(kk)-2)
               yltmpy(kk,i)=byarr(i+kx(kk)-2,j+ky(kk)-2,k+kz(kk)-2)
               yltmpz(kk,i)=bzarr(i+kx(kk)-2,j+ky(kk)-2,k+kz(kk)-2)
               end do
             end do 
             call lagrange3(x1a,yltmpx,xk,ymtmpx,j,ns)
             call lagrange3(x1a,yltmpy,xk,ymtmpy,j,ns)
             call lagrange3(x1a,yltmpz,xk,ymtmpz,j,ns)
        end do
        call lagrange3(x2a,ymtmpx,yk,yntmpx,k,ns)
        call lagrange3(x2a,ymtmpy,yk,yntmpy,k,ns)
        call lagrange3(x2a,ymtmpz,yk,yntmpz,k,ns)
      end do
      call lagrange3(x3a,yntmpx,zk,bwx,1,ns)
      call lagrange3(x3a,yntmpy,zk,bwy,1,ns)
      call lagrange3(x3a,yntmpz,zk,bwz,1,ns)
      do k=1,ipart(ns)
         bx(k)=bwx(k,1)
         by(k)=bwy(k,1)
         bz(k)=bwz(k,1)
      end do
      return
      end
c**********************************************************************
      subroutine lagrange3(xa,ya,w,yb,j,ns)
      use particles
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0)
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      dimension xa(iparts,3),ya(iparts,3),yb(iparts,3),w(iparts)
c----------------------------------------------------------------------
      do k=1,ipart(ns)
         fd1=xa(k,1)-xa(k,2)
         fd2=xa(k,1)-xa(k,3)
         fd3=xa(k,2)-xa(k,3)
         fn1=w(k)-xa(k,1)
         fn2=w(k)-xa(k,2)
         fn3=w(k)-xa(k,3)
         tn1=fn2*fn3*ya(k,1)
         tn2=fn1*fn3*ya(k,2)
         tn3=fn1*fn2*ya(k,3)
         td1=fd1*fd2
         td2=-fd1*fd3
         td3=fd2*fd3
         yb(k,j)=(tn1/td1)+(tn2/td2)+(tn3/td3)
      end do
      return
      end
c**********************************************************************
c**********************************************************************
c                                                                     *
c     These are the OPC-related subroutines & functions               *
c                                                                     *
c**********************************************************************
c**********************************************************************
      subroutine fields
      use opticsmod
      use OPCmodes
      use modes
      use mpi_parameters
      use mpi
      use harmonics
      use numbers
      implicit double precision(a-h,o-z)
      parameter(tiny30=1.0D-30)
      character*8 slippage
      character*12 tprofile,teprofile
      double complex expfac,exptheta,eln
      dimension xnorm(0:lmax),ynorm(0:nmax)
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
c----------------------------------------------------------------------
c This routine calculates the electromagnetic field components
c----------------------------------------------------------------------   
      if((MpiIO.eq.'no').and.(myid>0)) return ! do nothing
      do nh=1,nharms
         VP=zero
         if(MpiIO.eq.'yes') then
            nstart=ns_begin
            nend=ns_end
         else
            nstart=1
            nend=nslices
         endif
         do ns=nstart,nend
           if (mtype.eq.1) then !------Gauss-Hermite Modes-------------!
             wh=scale*asde1(nh,ns)                                     !
             a=rt2/wh                                                  !
             do l=0,lmax                                               !
               xnorm(l)=sqrtpi*(two**l)*wh*factorial(l)/(rt2*delbx)    !
             end do                                                    !
             do n=0,nmax                                               !
               ynorm(n)=sqrtpi*(two**n)*wh*factorial(n)/(rt2*delby)    !
             end do                                                    !
             do i=1,ndflx                                              !
               do j=1,ndfly                                            !
                 rsq=(xgrid(i)**2+ygrid(j)**2)/wh**2                   !
                 expfac=exp(-rsq)*exp(dcmplx(zero,-asde2(nh,ns)*rsq))  !
                 do nm=nh1(nh,ns),nh2(nh,ns)                           !
                   l=lnum(nm,ns)                                       ! 
                   n=nnum(nm,ns)                                       !
                   facx=Hm(a*xgrid(i),l)/sqrt(xnorm(l))                !
                   facy=Hm(a*ygrid(j),n)/sqrt(ynorm(n))                !
                   if((abs(RecAmp1(nm,ns)).gt.tiny).or.                !
     1                    (abs(RecAmp2(nm,ns)).gt.tiny30)) then        !
                     VP(l,n,i,j)=facx*facy*expfac                      !
     1                *dcmplx(RecAmp1(nm,ns),-RecAmp2(nm,ns))          !
                   endif                                               !
                 end do                                                !
               end do                                                  !
             end do                                                    !
           end if !----------------------------------------------------!   
           if (mtype.eq.2) then !------Gauss-Laguerre Modes------------!  
             wh=scale*asde1(nh,ns)                                     !
             alpha=asde2(nh,ns)                                        !
             do nm=nh1(nh,ns),nh2(nh,ns)                               !
               l=lnum(nm,ns)                                           !
               n=nnum(nm,ns)                                           !
               eln2_norm=halfpi*wh**2*factorial(abs(l)+n)/factorial(n) !
               anorm=delbx*delby/eln2_norm                             !
               anorm=sqrt(anorm)   ! we are writing the field, not power
               !now calculate the spatial distribution                 !
               do i=1,ndflx                                            !
                 do j=1,ndfly                                          !  OPC propagates a field with phase -i(k z-omega t)
                   rsq=(xgrid(i)**2+ygrid(j)**2)/wh**2                 !  Hence we need the phasor associated with this phase
                   zeta=rt2*sqrt(rsq)                                  !  advance.
                   r=wh*zeta/rt2                                       !
                   exptheta=dcmplx(xgrid(i),-ygrid(j))/(r+tiny30)      !
                   expfac=anorm*exp(-rsq)*exp(dcmplx(zero,-alpha*rsq)) !
                   call laguerre_2(zeta**2,aLnl,abs(l),n)              !
                   if((l.lt.0).and.(rsq.eq.zero)) then                 !
                          eln=dcmplx(zero,zero)                        !
                       else                                            !
                          eln=expfac*aLnl*(zeta**abs(l))*(exptheta**l) !
                   endif                                               !
                   if((abs(RecAmp1(nm,ns)).gt.tiny).or.                !
     1                    (abs(RecAmp2(nm,ns)).gt.tiny)) then          !
                     VP(l,n,i,j)=eln*                                  !
     1                         dcmplx(RecAmp1(nm,ns),-RecAmp2(nm,ns))  !
                   end if                                              !
                 end do                                                !
               end do                                                  !
             end do                                                    !
           end if !----------------------------------------------------!
           call coefs(ns,nh,Power)  
         end do  !  loop over nslices
      end do     !  loop over nharms
      return
      end
c**********************************************************************
      subroutine coefs(ns,nh,power)
      use opticsmod
      use OPCmodes
      use modes
      use harmonics
      implicit double precision(a-h,o-z)
      parameter (fourth=0.25d0,two=2.0D0,rtc=1.731451d4)
      parameter (one=1.0d0,zero=0.0d0,ultratiny=1.0d-50)
      parameter (pi=3.141592653589793d0,radfac=1.08874d9)
      character*8 slippage
      character*12 tprofile,teprofile
      character*100 filename
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
c----------------------------------------------------------------------
      cmplxcoefs=dcmplx(zero,zero)
      do i=1,ndflx
         do j=1,ndfly
            do l=lmin,lmax
               do n=0,nmax
                 cmplxcoefs(i,j)=cmplxcoefs(i,j)+VP(l,n,i,j)
               end do
            end do
         end do
      end do
      do i=1,ndflx
         do j=1,ndfly
            if(abs(cmplxcoefs(i,j)).lt.ultratiny) then
                        cmplxcoefs(i,j)=dcmplx(zero,zero)
            endif
         end do
      end do
c
      if(MpiIO.eq.'yes') then
        if (nh.eq.1) filename=DFLfile ! Determine filename to write to
        if (nh.gt.1) then
          numharm=int(.5d0+omega(nh1(nh,1))/domega)
          write(filename,'(A,i0,A)') DFLfile(1:len(DFLfile)-4) // '_',
     1                               numharm,'.dfl'
        endif 
        call write_mpi_dfl(filename,ns,cmplxcoefs,ndflx,ndfly)
      else
        call write_dfl(ns,nh,ndflx,ndfly)
      endif
      return
      end
c**********************************************************************
      subroutine write_dfl(ns,nh,nx,ny)
      use opticsmod
      use OPCmodes
      use modes
      use harmonics
      implicit double precision(a-h,o-z)
      integer length
      character*62 filename
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
c----------------------------------------------------------------------
      irec=ns
      irecsize=nx*ny*16 ! record size in binary files
c
      if(nh.eq.1) then
         open(unit=16,file=DFLfile,recl=irecsize,access='direct',
     1        status='unknown',err=10)
      endif
      if(nh.gt.1) then
         numharm=int(.5d0+omega(nh1(nh,1))/domega)
         if(numharm.eq.2) write(filename,102)
  102    format('2nd_Harmonic.dfl') 
         if(numharm.eq.3) write(filename,103)
  103    format('3rd_Harmonic.dfl')
         if(numharm.ge.4) write(filename,107) numharm
  107    format(i0,'th_Harmonic.dfl')
         open(unit=16,file=filename,recl=irecsize,access='direct',
     1        status='unknown',err=10)
      endif
      write(unit=16,rec=irec,err=20) ((cmplxcoefs(i,j),i=1,nx),j=1,ny)
      close(unit=16)
      return
 10   print *,'error opening ', DFLfile,' in write_dfl'
      return
 20   print *,'error writing ', DFLfile,' in write_dfl'
      end
c**********************************************************************
      subroutine write_mpi_dfl(filename,irec,var,nx,ny)
      use mpi_parameters
      use mpi
      implicit none ! be strict                                                                              
      character*100        filename
      integer              irec, nx, ny
      double complex       var
      dimension            var(nx,ny)
      integer              OFH
      integer (kind=MPI_OFFSET_KIND)  :: offset
      integer              amode, mpi_err
c----------------------------------------------------------------------
c This subroutine writes to a binary .dfl file. The routine uses MPI-IO
c routines to have parallel access to the file.
c input: filename - name of the file to write to
c        irec     - the record (slice) that needs to be writen
c        var      - a double complex array of size nx by ny containing 
c                   the data
c        nx       - number of grid (data) points in x-direction
c        ny       - number of grid (data) points in y-direction  
c----------------------------------------------------------------------
      amode = IOR(MPI_MODE_CREATE,MPI_MODE_WRONLY)
      call MPI_FILE_OPEN(MPI_COMM_SELF, trim(filename), amode, 
     1                   MPI_INFO_NULL, OFH, mpi_err)
      if (mpi_err == MPI_SUCCESS) then
        offset = (irec-1) * IO_recsize 
        call MPI_FILE_SET_VIEW(OFH, offset, MPI_DOUBLE_COMPLEX, 
     1                  IO_filetype, 'native',  MPI_INFO_NULL, mpi_err)
        call MPI_FILE_WRITE(OFH,var,nx*ny,MPI_DOUBLE_COMPLEX, 
     1        istatus, mpi_err)
        if (mpi_err /= MPI_SUCCESS) then
          print *,'Minerva::write_mpi_dfl->error writing to filename ',
     1            trim(filename), ' IO-status = ', mpi_err
          call MPI_FILE_CLOSE(OFH, mpi_err)
          call MPI_FINALIZE(mpi_err)
          stop
        endif
        calL MPI_FILE_CLOSE(OFH, mpi_err)
      else
        print *,'Minerva::write_mpi_dfl -> Error opening file ', 
     1           trim(filename), ' for output, IO-status = ',mpi_err
        call MPI_FINALIZE(mpi_err)
        stop
      endif
      return
      end subroutine write_mpi_dfl ! subroutine write_dfl
c**********************************************************************
      subroutine setupOPC(waist,wavelnth,zwaist,zmax)
      use opticsmod
      use OPCmodes
      use modes
      use harmonics
      use mpi_parameters
      use mpi
      use numbers, only : zero,one,two,half,twopi,pi,hundredth,sqrtpi
      implicit double precision(a-h,o-z)
      integer itest,mpi_err
      integer (kind=MPI_ADDRESS_KIND) lb
      real rtest
      double precision dtest
      complex ctest
      parameter (cvac=2.99792458d8)
      parameter (tiny=1.0d-30)
      character*8 slippage
      character*12 tprofile,teprofile
      character*62 filename,filename2
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
c----------------------------------------------------------------------     
      if((MpiIO.eq.'no').and.(myid>0)) return ! do nothing
      ndflx=ndfl
      ndfly=ndfl
      if(MpiIO.eq.'yes') then ! set up MPI-IO
        call MPI_TYPE_CONTIGUOUS(ndflx*ndfly,MPI_DOUBLE_COMPLEX,
     1                         IO_filetype,mpi_err)
        call MPI_TYPE_COMMIT(IO_filetype,mpi_err)
      ! determine size of IO_filetype
        call MPI_TYPE_GET_EXTENT(IO_filetype,lb,IO_recsize,mpi_err)
      endif
c
      waist_OPC=hundredth*waist
      if(nsigma.gt.0) then
         xmax=nsigma*waist_OPC
         ymax=nsigma*waist_OPC
      endif
      gridsize=xmax/hundredth
      omoc=twopi/wavelnth ! harmonic number is not included (m)
      zr=pi*waist_OPC**2/wavelnth
      dz=zmax-zwaist
      delbx=two*xmax/dble(ndflx-1)
      delby=two*ymax/dble(ndfly-1)
      xy=two*xmax*two*ymax
      dxdy=delbx
      dxgrid=delbx
      dygrid=delby
      wavelength=wavelnth
      az=dz/zr
      wz=waist_OPC*sqrt(one+az**2)
c
c Setup parameters for OPC
c
      DFLfile=MinervaDFLfile
c
c Allocate the necessary arrays
c
      allocate(RecAmp1(nmodes,nslices))
      allocate(RecAmp2(nmodes,nslices))
      allocate(VP(lmin:lmax,0:nmax,ndflx,ndfly))
      allocate(xgrid(ndflx),ygrid(ndfly),cmplxcoefs(ndflx,ndfly))
c
c Write the parameter file, done by master node
c
      if (myid.eq.0) then
         write(IOPC,*) ' &optics'
         write(IOPC,1150) ndfl,nslcs
 1150    format('   ndfl=',i0,/,'   nslcs=',i0)
         write(IOPC,1151) nsigma
 1151    format('   nsigma=',i0)
         write(IOPC,1152) gridsize
 1152    format(SP,'   gridsize=',1pe17.10)
         write(IOPC,1156) dxdy
 1156    format(SP,'   dxdy=',1pe17.10)
         write(IOPC,1153) dxgrid,dygrid,cavLsc,reprate
 1153    format(SP,'   dxgrid=',1pe17.10,/,'   dygrid=',1pe17.10,/,
     1     '   cavLsc=',e17.10,/,'   reprate=',e17.10)
         write(IOPC,1155) trim(Config_Minerva),trim(Config_Mercury),
     1                    trim(MinervaDFLfile),trim(MercuryDFLfile)
 1155    format("   Config_Minerva='",a,"'",/,
     1          "   Config_Mercury='",a,"'",/,
     2          "   MinervaDFLfile='",a,"'",/,
     3          "   MercuryDFLfile='",a,"'")
         write(IOPC,*) ' /'
c        
         if(slippage.eq.'on') then
            zsep=tslices*cvac/(nslices*wavelength)
         else
            zsep = 1
         endif
         if(nharms.ge.1) then
            open(unit=13,file='OpticsInput.param',form='formatted',
     1           status='replace')  
            write(13,*) ' &optics'
            write(13,*) '  nslices =',nslices
            write(13,*) '  lambda =',wavelength
            write(13,*) '  npoints =',ndfl
            write(13,*) '  zsep =',zsep
            write(13,*) '  mesh =',dxdy
            write(13,*) ' /'
            close(unit=13)
         endif
         if(nharms.gt.1) then
           do nh=2,nharms
            numharm=int(half+omega(nh1(nh,1))/domega)
            if(numharm.eq.2) then
               write(filename,102)
  102          format('2nd_Harmonic.param') 
               write(filename2,103)
  103          format(' &2nd_Harmonic')
            endif
            if(numharm.eq.3) then
               write(filename,104)
  104          format('3rd_Harmonic.param')
               write(filename2,105)
  105          format(' &3rd_Harmonic')
            endif
            if(numharm.ge.4) then
               write(filename,106) numharm
  106          format(i0,'th_Harmonic.param')
               write(filename2,107) numharm
  107          format(' &',i0,'th_Harmonic')
            endif
            open(unit=13,file=filename,form='formatted',
     1           status='replace') 
            write(13,*) filename2
            write(13,*) '  nslices =',nslices
            write(13,*) '  lambda =',wavelength/dble(numharm)
            write(13,*) '  npoints =',ndfl
            write(13,*) '  zsep =',dble(numharm)*zsep
            write(13,*) '  mesh =',dxdy
            write(13,*) ' /'     
            close(unit=13)
           end do
         endif
      endif ! myid = 0
c
c Generate the grid
c
      do i=1,ndflx
         xgrid(i)=(i-1)*delbx-xmax
      end do
      do j=1,ndfly
         ygrid(j)=(j-1)*delby-ymax
      end do
c
c Calculate the appropriate mode coefficients for the 
c electromagnetic field for both Hermite-Gaussian and
c Laguerre-Gaussian decomposition
c
      RecAmp1=zero
      RecAmp2=zero
      if(MpiIO.eq.'yes') then
      	nstart=ns_begin
      	nend=ns_end
      else
        nstart=1
        nend=nslices
      endif
      do nh=1,nharms
        Pall=zero
        numharm=int(half+omega(nh1(nh,1))/domega)
        do ns=nstart,nend
          wh=asde1(nh,ns)
          Ptot=zero
          do nm=nh1(nh,ns),nh2(nh,ns)
            RecAmp1(nm,ns)=a1(nm,ns)/sqrt(pfac(nm,ns))
            RecAmp2(nm,ns)=a2(nm,ns)/sqrt(pfac(nm,ns))
            if((abs(RecAmp1(nm,ns)).gt.tiny).and.
     1         (abs(RecAmp2(nm,ns)).gt.tiny)) then
               Ptot=Ptot+RecAmp1(nm,ns)**2+RecAmp2(nm,ns)**2
               Pall=Pall+RecAmp1(nm,ns)**2+RecAmp2(nm,ns)**2
            endif
          end do
          write(6,410) ns,numharm,Ptot
  410     format(5x,'slice=',i3,' harmonic=',i3,' Power(W)=',1pe12.5)
        end do   !  loop over nslices
        if(MpiIO.eq.'yes') then
          ! collect data from the various computational nodes
          Pall_temp=zero
          call mpi_reduce(Pall,Pall_temp,1,mpi_double_precision,
     1                    mpi_sum,0,mpi_comm_world,ier)
          call mpi_barrier(mpi_comm_world,ier) ! synchronize nodesPsum
          if (myid.eq.0) then
            Pall=Pall_temp
            if (nslices>1) write(6,411) numharm,Pall*tslices/
     1                                               dble(nslices-1)
          endif
        endif
  411   format(/,10x,'harmonic=',i3,' Pulse Energy(J)=',1pe12.5,/)
      end do      !  loop over nharms
      return
      end
c**********************************************************************
c**********************************************************************
c                                                                     *
c     These subroutines handle the mpi broadcasting of the input data *
c     to the different processors when the numprocs is greater than   *
c     one                                                             *
c                                                                     *
c**********************************************************************
c**********************************************************************
      subroutine input1_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      use wakes
      use modes
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(nsegbw,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nsegfoc,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nwrite,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(iprop,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nbeams,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nparts_out,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nbin,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nwig_harms,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(energy,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(current,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(rbmin,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(rbmax,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(xbeam,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(ybeam,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(epsx,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(epsy,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dgam,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dgamz,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(twissalphx,1,mpi_double_precision,0,
     1               mpi_comm_world,ier)
      call mpi_bcast(twissalphy,1,mpi_double_precision,0,
     1               mpi_comm_world,ier)
      call mpi_bcast(psiwidth,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(beamtype,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(sde,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(emmodes,11,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(prebunch,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(Runge_Kutta,9,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(B_ratio,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(matchbeam,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(slippage,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(beam_diags,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(trans_mode,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(Fluence_only,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(FFTchk,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(main_out,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(drifttube,11,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(Msqr,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(Shot_Noise,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(trans_cor,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(newfield,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(OPC,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(Power_vs_z,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(SpentBeam,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(S2E,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(S2E_method,11,mpi_character,0,mpi_comm_world,ier)     
      call mpi_bcast(Wakefield,3,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(acdc,2,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(material,2,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(dump,3,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(restart,3,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(dither,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(noise_seed,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(smallest,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(x_center,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(y_center,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(px_in,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(py_in,1,mpi_double_precision,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input2_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(Nw,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nup,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ndown,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nwh,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nwig_harms,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(lrf,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nrf,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(n1stpass,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(bw,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(zw,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(zfirst,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(zlast,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(ax,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(Edc,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(z1,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(z2,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(Prfin,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(freqrf,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(ztapersegi,1,mpi_double_precision,0,
     1               mpi_comm_world,ier)
      call mpi_bcast(dbwsegi,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(dzwsegi,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(wigtype,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(wigplane,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(RFfield,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(RFmode,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(nbxi,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nbyi,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nbzi,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(xmaxi,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(ymaxi,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(Bmapfilei,10,mpi_character,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine wigh_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(nwh,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(bratio,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      return
      end
c**********************************************************************
      subroutine input3_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(ndfocus,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(Qk,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(bquad,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(bdip,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(zLen,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(angle,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(z1st,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(z2nd,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(zentry,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(Qx0i,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(Qy0i,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(foctype,8,mpi_character,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input4_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(nmodes,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(mselect,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(rg,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(rg_x,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(rg_y,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(wavelnth,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(freq_wg,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(wx,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(wy,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(power,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(cutoff,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(polar,1,mpi_double_precision,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input5_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(nr,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ntheta,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nx,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ny,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(npsi,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nphi,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ndv,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ngam,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(Gauss_Quad,8,mpi_character,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input6_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      use diagnstc
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(ndz,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nstep,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ndiags,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(zmax,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dz,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(ztaper,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(zsat,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dbwdz,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dbw,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dzw,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dbw_sqr,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      return
      end
c**********************************************************************
      subroutine input7_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(enrgycon,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(dcfield,8,mpi_character,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input8_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(nerr,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(iseed,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(error,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(wigerrs,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(source,8,mpi_character,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input9_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(nharm,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(l,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(n,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(pin,1,mpi_double_precision,0,mpi_comm_world,
     1                    ier)
      call mpi_bcast(phase_in,1,mpi_double_precision,0,
     1                    mpi_comm_world,ier)
      call mpi_bcast(waist,1,mpi_double_precision,0,
     1                    mpi_comm_world,ier)
      call mpi_bcast(zwaist,1,mpi_double_precision,0,
     1                    mpi_comm_world,ier)
      call mpi_bcast(direction,8,mpi_character,0,mpi_comm_world,
     1                    ier)
      return
      end
c**********************************************************************
      subroutine input10_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(zdiag,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      return
      end
c**********************************************************************
      subroutine beams_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(x0i,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(y0i,1,mpi_double_precision,0,mpi_comm_world,ier)         
      call mpi_bcast(dEi,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dIi,1,mpi_double_precision,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine slip_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(tprofile,12,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(teprofile,12,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(interpolation,10,mpi_character,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(nslip,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nslices,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(tpulse,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(tlight,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(tslices,1,mpi_double_precision,0,
     1               mpi_comm_world,ier)         
      call mpi_bcast(tshift,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      return
      end
c**********************************************************************
      subroutine optics_bcast
      use mpi_parameters
      use mpi
      use opticsmod
      implicit double precision (a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(ndfl,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ndflx,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ndfly,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nsigma,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nslcs,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(dxdy,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dxgrid,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(dygrid,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(cavlsc,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(reprate,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(Config_minerva,20,mpi_character,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(Config_mercury,20,mpi_character,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(MinervaDFLfile,20,mpi_character,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(MercuryDFLfile,20,mpi_character,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(MpiIO,3,mpi_character,0,mpi_comm_world,ier)
      return
      end      
c**********************************************************************
      subroutine array_dim(x0,dz,xmax,nstep)
      use wiggler
      use focus
      use storage
      use mpi_parameters
      use mpi
      implicit double precision(a-h,o-z)
      parameter(zero=0.d0,tiny=1.d-6)     
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      allocatable zpos(:),zneg(:)
c----------------------------------------------------------------------
c
c     This subroutine calculates the necessary array dimensions for the
c     number of axial steps in the integration
c
c----------------------------------------------------------------------
      allocate(zpos(3*(nsegbw+nsegfoc)),zneg(3*(nsegbw+nsegfoc)))
      iteration=0
      nzlen=1
      dx=dz
      ztest=zero
      if(nsegfoc.eq.0) zstt(1)=xmax
c
    2 if(ztest+dx.gt.xmax) then
           deallocate(zpos,zneg)
           return
      endif
         z=ztest
         isegbw=0
         isegb0=0
         iwcount=0
         do i=1,nsegbw
              if((z.ge.zstart(i)).and.(z.le.zstop(i))) then
                 iwcount=iwcount+1
                 if(iwcount.le.1) then
                    isegbw=i
                    dx=zwi(isegbw)*dz/zwi(1)
                 endif
              endif
         end do
         if((isegbw.eq.0).and.(nsegfoc.gt.0)) then
           do i=1,nsegfoc
              if((z+tiny.ge.zstt(i)).and.(z-tiny.lt.zstp(i))) then
                   isegb0=i
                   dx=(zstp(i)-zstt(i))/dble(ndfoc(i))
              endif
           end do
         endif
         if((isegbw.eq.0).and.(isegb0.eq.0)) then
              if((z.lt.zstart(1)).and.(z.lt.zstt(1))) then
                    deltaz=min(zstart(1),zstt(1))
                    dx=deltaz/int(tiny+deltaz/dz)
                 else
                    npos=0
                    nneg=0
                    do i=1,nsegbw
                     if(z.gt.zstart(i)) then
                           npos=npos+1
                           zpos(npos)=z-zstart(i)
                        else
                           nneg=nneg+1
                           zneg(nneg)=z-zstart(i)
                     endif
                     if(z.gt.zstop(i)-tiny) then
                           npos=npos+1
                           zpos(npos)=z-zstop(i)
                        else
                           nneg=nneg+1
                           zneg(nneg)=z-zstop(i)
                     endif
                    end do
                    do i=1,nsegfoc
                        if(z.gt.zstt(i)) then
                           npos=npos+1
                           zpos(npos)=z-zstt(i)
                         else
                           nneg=nneg+1
                           zneg(nneg)=z-zstt(i)
                        endif
                        if(z.gt.zstp(i)-tiny) then
                           npos=npos+1
                           zpos(npos)=z-zstp(i)
                         else
                           nneg=nneg+1
                           zneg(nneg)=z-zstp(i)
                       endif
                    end do
                    if(nneg.gt.0) then
                        zless=zpos(1)
                        do i=2,npos
                           zless=min(zless,zpos(i))
                        end do
                        zmore=zneg(1)
                        do i=2,nneg
                           zmore=max(zmore,zneg(i))
                        end do
                        deltaz=zless-zmore
                        dx=deltaz/int(tiny+deltaz/dz)
                      else
                        dx=dz
                    endif
              endif
         endif
         ztest=ztest+dx
         iteration=iteration+1
         if(iteration.eq.nstep*(iteration/nstep)) nzlen=nzlen+2
      go to 2
      end
c**********************************************************************
c**********************************************************************
c
c      These subroutines handle the inclusion of wakefields in the
c      particle dynamics
c
c**********************************************************************
c**********************************************************************
      subroutine wakefields(energy,current,rbeam,zmax,zw,rg,rg_y,
     1                      ns_frst,ns_last,dinput_file)
      use wakes
      use numbers
      use distfile_import
      use direct_import
      use PARMELA
      use multi_beams
      implicit double precision(a-h,o-z)
      parameter(cvacm=2.99792458d8)
      character*8 RFfield,slippage
      character*11 drifttube
      character*12 tprofile,teprofile
      character*17 dinput_file
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
      common/RFacc/RFfield,agkwrf,bgkwrf,hagkwrf,hbgkwrf,n1stpass,
     3             drifttube
c----------------------------------------------------------------------
c
c     energy = electron beam energy in MeV
c    current = electron beam current in Amperes
c         rg = cylindrical drift tube radius in meters
c       rg_y = half the rectangular drift tube separation in meters
c      rbeam = electron beam radius in meters
c       zmax = length of wiggler line in meters
c         zw = wiggler period in cm
c
c----------------------------------------------------------------------
      nslices_w=ns_frst-ns_last+1
      if(tprofile.eq.'BEAMCODE') nslices_w=nslices_P
      if(tprofile.eq.'DISTFILE') nslices_w=nslices_g
      if(tprofile.eq.'DIRECT') nslices_w=nslices_d
      s_LCLS=20.d-6
      s_b=cvacm*tpulse
      ds_b=cvacm*dtpulse
      nn=2**npoints
      allocate(wake(nn),energy_loss(nn))
      wake=zero
      energy_loss=zero
      zkw=twopi/(hundredth*zw)
      if(drifttube.eq.'cylindrical') a=hundredth*rg
      if(drifttube.eq.'rectangular') a=hundredth*rg_y
      if(material.eq.'Cu') then
         conductivity=5.8e17
         ctau=8.1e-6
      endif   
      if(material.eq.'Al') then
         conductivity=3.8e17
         ctau=2.4e-6
      endif
      s_0=(cvacm*a**2/(twopi*conductivity))**(one/three)      
c
      call impedance(nn,s,ctau/s_0)
c
c-------Obtain Energy Loss versus position in the bunch
c
      open(unit=15,file='Energy_Loss.txt',form='formatted',
     1     status='replace')
      write(15,*) '              tprofile=',tprofile
      write(15,*) '       dinput_file=',dinput_file
      write(15,*) ' '
c     if(tprofile.eq.'flattop') Q_b=current*tpulse
c     if(tprofile.eq.'parabolic') Q_b=two*current*tpulse/three
c     if(tprofile.eq.'DISTFILE') Q_b=Q_bunch
      write(15,*) '    bunch charge (C)=',Q_bunch
      write(15,*) '             s_0 (m)=',s_0
      write(15,*) '             s_b (m)=',s_b
      write(15,*) ' '
      write(15,*) '         s/sb                   dE/E'
      ds=s_0*twopi/(two*range)
      do is=1,nn
         s=ds*dble(is-1)
         nss=1+(s/s_b)*nslices_w
         if(s.le.s_b) then
            do i=1,nn
               si=ds*dble(i-1)
               if(si.le.s_b) then
               	  if((tprofile.eq.'flattop').or.
     1                       (tprofile.eq.'parabolic')) then
                     rmax=zkw*rbeam
                     gamma_b=one+energy/emass
                     xisqr=c_profile(si,s_b)*
     1                          (0.015d0*sqrt(current)/rmax)**2/gamma_b
                     efac=(zkw*zmax)*(zkw*s_b)*xisqr*(rbeam/(two*a))**2
               	  endif
               	  if((tprofile.eq.'BEAMCODE').or.
     1                     (tprofile.eq.'DISTFILE').or.
     2                           (tprofile.eq.'DIRECT')) then
     	               efac=(s_b*zmax)*(currents(nss)/Alfven)/a**2
     	          endif
                energy_loss(is)=energy_loss(is)+ds*efac*wake(i)
               endif
            end do
         endif
         if(s.le.s_b) write(15,*) s/s_b,energy_loss(is)
      end do
      close(unit=15)
c
c-------Obtain Electric Field
c
      open(unit=15,file='Electric_Field.txt',form='formatted',
     1     status='replace')
      write(15,*) '      slice         Ez(V/m)'
      ds_slice=s_b/dble(nslices_w-1) 
      do ns=1,nslices_w
         s=ds_slice_w*dble(ns-1)
         do i=1,nn
            si=ds*dble(i-1)
            if((si.ge.s).and.(si.le.s_b)) then
            	 if((tprofile.eq.'flattop').or.
     1                  (tprofile.eq.'parabolic')) then
                     rmax=zkw*rbeam
                     gamma_b=one+energy/emass
                     xisqr=c_profile(si,s_b)*
     1                          (0.015d0*sqrt(current)/rmax)**2/gamma_b
                     efac2=(zkw*s_b)*xisqr*(rbeam/(two*a))**2
     	         endif
               if((tprofile.eq.'BEAMCODE').or.
     1                     (tprofile.eq.'DISTFILE').or.
     2                           (tprofile.eq.'DIRECT')) then
     	               efac2=(s_b/zkw)*(currents(ns)/Alfven)/a**2
               endif
               efield(ns_frst+ns)=efield(ns_frst+ns)+ds*efac2*wake(i)
            endif
         end do
         write(15,*) ns,efield(ns_frst+ns)/(2.801*zw)
      end do
      close(unit=15)
      deallocate(wake,energy_loss)
      return
      end
c**********************************************************************
      double precision function c_profile(s,s_b)
      use numbers
      implicit double precision(a-h,o-z)
      character*12 tprofile,teprofile
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
c----------------------------------------------------------------------
      if(tprofile.eq.'flattop') c_profile=one/s_b
      if(tprofile.eq.'parabolic') then
         hs_b=half*s_b
         c_profile=(one-(s-hs_b)**2/hs_b**2)/s_b
      endif
      return
      end
c**********************************************************************
      subroutine impedance(nn,s,gamma)
      use wakes
      use numbers
      implicit double precision(a-h,o-z)
      common/RFacc/RFfield,agkwrf,bgkwrf,hagkwrf,hbgkwrf,n1stpass,
     3             drifttube
      character*8 RFfield
      character*11 drifttube
      double complex x,ci,Z,Z_cyldc,Z_recdc,Z_cylac,Z_recac
      dimension fftdata(2*nn)
c----------------------------------------------------------------------
c
c     range = limits on FFT integration (+/- range)
c
c----------------------------------------------------------------------
      ci=dcmplx(zero,one)
      dx=two*range/dble(nn)
      do i=1,1+nn/2
         n=1+2*(i-1)
         x=dx*dble(i-1)
         if(acdc.eq.'dc') then
            if(drifttube.eq.'cylindrical') Z=Z_cyldc(x)
            if(drifttube.eq.'rectangular') Z=Z_recdc(x)
         endif
         if(acdc.eq.'ac') then
            if(drifttube.eq.'cylindrical') Z=Z_cylac(x,gamma)
            if(drifttube.eq.'rectangular') Z=Z_recac(x,gamma)
         endif
         fftdata(n)=dreal(Z)
         fftdata(n+1)=dimag(Z)
      end do
c
      do i=2,nn/2
         n=nn-1+2*i
         x=-range+dx*dble(i-1)
         if(acdc.eq.'dc') then
            if(drifttube.eq.'cylindrical') Z=Z_cyldc(x)
            if(drifttube.eq.'rectangular') Z=Z_recdc(x)
         endif
         if(acdc.eq.'ac') then
            if(drifttube.eq.'cylindrical') Z=Z_cylac(x,gamma)
            if(drifttube.eq.'rectangular') Z=Z_recac(x,gamma)
         endif
         fftdata(n)=dreal(Z)
         fftdata(n+1)=dimag(Z)
      end do
      call four1(fftdata,nn,-1)
c
      do i=1,nn
         n=1+2*(i-1)
         wake(i)=fftdata(n)
      end do
      return
      end
c**********************************************************************
      double complex function Z_cyldc(x)
      use numbers
      implicit double precision(a-h,o-z)
      double complex x,ci
c----------------------------------------------------------------------
c
c      This function calculates the dc impedance for a cylindrical
c      drift tube
c
c----------------------------------------------------------------------
      ci=dcmplx(zero,one)
      Z_cyldc=sqrt(x)/(dcmplx(one,one)-ci*sqrt(x)*x/two)/(two*twopi)
      return
      end
c**********************************************************************
      double complex function Z_cylac(x,gamma)
      use numbers
      implicit double precision(a-h,o-z)
      double complex x,ci,temp,t1,t2
c----------------------------------------------------------------------
c
c      This function calculates the ac impedance for a cylindrical
c      drift tube
c
c----------------------------------------------------------------------
      ci=dcmplx(zero,one)
      temp=sqrt(one+(x*gamma)**2)
      t_lambda=x*gamma/temp
      t1=sqrt(one+t_lambda)
      t2=sqrt(one-t_lambda)
      Z_cylac=sqrt(x)/((ci*t1+t2)/sqrt(temp)
     1                                  -ci*sqrt(x)*x/two)/(two*twopi)
      return
      end
c**********************************************************************
      double complex function Z_recdc(x)
      use numbers
      implicit double precision(a-h,o-z)
      double complex x,dcsimpson
c----------------------------------------------------------------------
c
c      This function calculates the dc impedance of a rectangular
c      drift tube
c
c----------------------------------------------------------------------
      Z_recdc=dcsimpson(zero,hundred,x)/(two*twopi)
      return
      end

c**********************************************************************
      double complex function f_recdc(x,kappa)
      use numbers
      implicit double precision(a-h,o-z)
      double complex ci,kappa
c----------------------------------------------------------------------
      ci=dcmplx(zero,one)
      coshx=cosh(x)
      if(x.lt.deci) then
      	    sinhcx=one
         else
      	    sinhcx=sinh(x)/x
      endif
      f_recdc=sqrt(kappa)/
     1      (coshx*(dcmplx(one,one)*coshx-ci*sqrt(kappa)*kappa*sinhcx))
      return
      end
c**********************************************************************
      double complex function dcsimpson(x_min,x_max,kappa)
      use numbers
      implicit double precision (a-h,o-z)
      parameter(acc=1.d-3,maxit=25)
      double complex s,ds,prev,kappa,f_recdc
c----------------------------------------------------------------------
c
c      This subroutine integrates the complex function f_recdc via
c      Simpson's rule. The function f_recdc require one free parameter,
c      kappa, which is complex
c
c----------------------------------------------------------------------
      h=(x_max-x_min)/two
      s=f_recdc(x_min,kappa)+f_recdc(x_max,kappa)
      s=s+two*f_recdc(x_min+h,kappa)
      prev=h*s/three
      n=2
      do j=1,maxit
        h=half*h
        ds=zero
        do i=1,n
           x=x_min+h*(one+two*dble(i-1))
           ds=ds+f_recdc(x,kappa)
        end do
        s=s+four*ds
        dcsimpson=h*s/three
        if(abs(dcsimpson-prev).le.acc*abs(dcsimpson)) return
        s=s-two*ds
        prev=dcsimpson
        n=2*n
      end do
      write(6,*) ' convergence not achieved after max iterations'
      return
      end
c**********************************************************************
      double complex function Z_recac(x,gamma)
      use numbers
      implicit double precision(a-h,o-z)
      double complex x,acsimpson
c----------------------------------------------------------------------
c
c      This function calculates the dc impedance of a rectangular
c      drift tube
c
c----------------------------------------------------------------------
      Z_recac=acsimpson(zero,hundred,x,gamma)/(two*twopi)
      return
      end
c**********************************************************************
      double complex function f_recac(x,kappa,gamma)
      use numbers
      implicit double precision(a-h,o-z)
      double complex ci,kappa
c----------------------------------------------------------------------
      ci=dcmplx(zero,one)
      coshx=cosh(x)
      if(x.lt.deci) then
      	    sinhcx=one
         else
      	    sinhcx=sinh(x)/x
      endif
      temp=sqrt(one+(x*gamma)**2)
      t_lambda=x*gamma/temp
      t1=sqrt(one+t_lambda)
      t2=sqrt(one-t_lambda)
      f_recac=sqrt(kappa)/(coshx*((ci*t1+t2)*coshx/sqrt(temp)
     1                                   -ci*sqrt(kappa)*kappa*sinhcx))
      return
      end
c**********************************************************************
      double complex function acsimpson(x_min,x_max,kappa,gamma)
      implicit double precision (a-h,o-z)
      parameter(zero=0.d0,half=.5d0,one=1.d0,two=2.d0,three=3.d0,
     1          four=4.d0,acc=1.d-3,maxit=25)
      double complex s,ds,prev,kappa,f_recac
c----------------------------------------------------------------------
c
c      This subroutine integrates the complex function f_recac via
c      Simpson's rule. The function f_recac requires two free parameters,
c      kappa, which is complex, and gamma, which is real
c
c----------------------------------------------------------------------
      h=(x_max-x_min)/two
      s=f_recac(x_min,kappa,gamma)+f_recac(x_max,kappa,gamma)
      s=s+two*f_recac(x_min+h,kappa,gamma)
      prev=h*s/three
      n=2
      do j=1,maxit
        h=half*h
        ds=zero
        do i=1,n
           x=x_min+h*(one+two*dble(i-1))
           ds=ds+f_recac(x,kappa,gamma)
        end do
        s=s+four*ds
        acsimpson=h*s/three
        if(abs(acsimpson-prev).le.acc*abs(acsimpson)) return
        s=s-two*ds
        prev=acsimpson
        n=2*n
      end do
      write(6,*) ' convergence not achieved after max iterations'
      return
      end
c**********************************************************************
      block data
      implicit double precision(a-h,o-z)
      character*8 RFfield,slippage
      character*11 drifttube
      character*12 tprofile,teprofile
c
      common/spread/dp,n1,n2,npsi,nphi,ndv,npx,npy,ngam
      common/modes_2/polar,polarfac,lmax,nmax,nharms,mtype
      common/afone/domega,rgkw,rgkw2,scale,nmodes,nwrite,iprop,
     1             killsde,iparts,nerr,iberr,nsegbw,nsegfoc
      common/self/rb2,xavgs,yavgs,rbavg2,sfact,rb0avg2,iself
      common/taper/zsat,dbw,dbw_sqr,dzw,nwig_harms
      common/RFacc/RFfield,agkwrf,bgkwrf,hagkwrf,hbgkwrf,n1stpass,
     3             drifttube
      common/temporal/dt_norm,dtpulse,tshift,tpulse,tslices,tcavtune,
     1                z_slip,Q_bunch,tprofile,teprofile,slippage,
     2                slip_fac,nslices,nslip,ns_begin,ns_end
c----------------------------------------------------------------------
      data npsi/10/,ndv/6/,nphi/4/,nmodes/1/,nerr/1/,nwrite/0/,
     1     ngam/1/,iprop/1/,nsegbw/1/,nsegfoc/0/,nslices/1/,
     2     RFfield/'off'/,tprofile/'parabolic'/,slippage/'off'/,
     3     n1stpass/0/,ns_begin/1/,
     4     ns_end/1/,drifttube/'cylindrical'/,teprofile/'parabolic'/,
     5     nslip/10/
c
      data dzw/0.d0/,polar/-1.d0/,tshift/0.d0/,dbw_sqr/0.d0/,dbw/0.d0/,
     1     nwig_harms/0/
      end