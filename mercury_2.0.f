c**********************************************************************
      module mpi_parameters
      use mpi
      implicit double precision(a-h,o-z)
      save myid,numprocs
      integer myid,numprocs,istatus(mpi_status_size)
      end module mpi_parameters
c**********************************************************************
      module opticsmod
      implicit double precision(a-h,o-z)
      save
      parameter (IOPC=19)
      character*20 Plotfile,Config_Minerva,Config_Mercury
      character*20 MercuryDFLfile,MinervaDFLfile
      character*3  MpiIO
      dimension zd(3),Wz2(3),WSQ(3),Wa2(3)
      complex*16 cmplxcoefs(:,:)
      allocatable cmplxcoefs
      namelist/optics/ndfl,ndflx,ndfly,dxdy,dxgrid,dygrid,nslcs,cavLsc,
     1                reprate,nsigma,gridsize,
     2                Config_Minerva,Config_Mercury,
     3                MinervaDFLfile,MercuryDFLfile,
     4                MpiIO
      data Config_Minerva/'MinervaConfig.in'/,
     1     Config_Mercury/'MercuryConfig.in'/,nsigma/0/gridsize/0.d0/,
     2     MpiIO/'yes'/
c
c        cavLsc = cavity length in meters
c       reprate = repetition rate of the electron beam in Hz
c
      end module opticsmod
c**********************************************************************
      module OPCmodes
      implicit double precision(a-h,o-z)
      save
      parameter (maxorder=16,maxharms=1)
      parameter (nmax=(maxorder+1)*(maxorder+1)*maxharms)
      integer icount
      double precision delx,dely,az,zr,wavelength,alpha
      double precision xmax,ymax
      dimension RecAmp1(0:maxorder,0:maxorder,maxharms)
      dimension RecAmp2(0:maxorder,0:maxorder,maxharms)
      dimension Amag(0:maxorder,0:maxorder,maxharms)
      dimension Aphs(0:maxorder,0:maxorder,maxharms)
      character*8 dira(:,:)                             ! inp9 variables
      dimension nharma(:,:),la(:,:),na(:,:)             ! inp9 variables
      dimension waista(:,:),zwaista(:,:)                ! inp9 variables
      dimension pina(:,:),phase_ina(:,:)                ! inp9 variables
      dimension P_temp(:),Ph_temp(:),Ptot(:)
      dimension ln(nmax),nn(nmax)
      dimension xgrid(:),ygrid(:)
      dimension la_wk(:,:),na_wk(:,:),pina_wk(:,:),phase_wk(:,:)
      dimension nmodes_wk(:),nharma_wk(:,:)
      complex*16 simparraycx(:,:)
      allocatable xgrid,ygrid,simparraycx
      allocatable dira,nharma,la,na
      allocatable waista,zwaista,pina,phase_ina,P_temp,Ph_temp,Ptot
      allocatable la_wk,na_wk,pina_wk,phase_wk,nmodes_wk,nharma_wk
      end module OPCmodes
c**********************************************************************
      module params
      implicit double precision(a-h,o-z)
      save
      parameter(zero=0.0d0,one=1.0d0,two=2.0d0,half=0.5d0,sixtn=16.d0,
     1          three=3.d0,four=4.d0,eight=8.0d0,ten=10.d0,
     2          halfpi=1.570796326794897d0,sqrtpi=1.772453850905516d0,
     3          pi=3.141592653589793d0,twopi=6.283185307179586d0,
     4          degtorad=1.745329252d-2,hundred=1.d2,tenth=.1d0,
     5          cvac=2.99792458d10,hundredth=.01d0,radfac=1.08874d9,
     6          quarter=0.25d0)
      end module params
c**********************************************************************
      module the_namelists
      implicit double precision(a-h,o-z)
      save
      character*20 Bmapfilei,DFLinput
      character*20 MinervaOutputFile
      character*3 WriteOpticsFile,FirstPass
      character*8 beamtype,wigtype,foctype,wigerrs,source,enrgycon,sde,
     1            dcfield,emmodes,direction,RFfield,prebunch,RFmode,
     2            wigplane,matchbeam,slippage,beam_diags,trans_mode,
     3            FFTchk,main_out,Msqr,Shot_Noise,trans_cor,
     4            newfield,OPC,Power_vs_z,SpentBeam,S2E,Gauss_Quad
      character*2 modetype,acdc
      character*3 Wakefield,dump,restart
      character*6 material
      character*9 Runge_Kutta
      character*10 interpolation
      character*11 drifttube,S2E_method
      character*12 tprofile,beamloss,teprofile
c----------------------------------------------------------------------
      namelist/inp1/nsegbw,nsegfoc,energy,current,rbmin,rbmax,beamtype,
     1             xbeam,ybeam,epsx,epsy,dgam,dgamz,nwrite,iprop,sde,
     2             emmodes,nbeams,prebunch,psiwidth,B_ratio,matchbeam,
     3             twissalphx,twissalphy,x_center,y_center,
     4             slippage,beam_diags,trans_mode,FFTchk,
     5             main_out,drifttube,Msqr,Shot_Noise,
     6             noise_seed,trans_cor,newfield,OPC,Power_vs_z,
     7             SpentBeam,S2E,S2E_method,nparts_out,dither,nbin,
     8             Wakefield,acdc,material,nwig_harms,dump,restart,
     9             px_in,py_in,Fluence_only,Runge_Kutta
      namelist/slip/tprofile,teprofile,nslices,tpulse,tlight,tslices,
     1              tshift,nslip,nparts_g,dinput_file,interpolation
      namelist/beams/x0i,y0i,dEi,dIi
      namelist/inp2/bw,zw,Nw,zfirst,zlast,nup,ndown,wigtype,ax,m,nwh,
     1           wigplane,RFfield,Edc,z1,z2,Prfin,lrf,nrf,freqrf,
     2           RFmode,n1stpass,nbxi,nbyi,nbzi,xmaxi,ymaxi,Bmapfilei,
     3           ztapersegi,dbwsegi,dzwsegi
      namelist/wigh/bratio,nwh
      namelist/inp3/Qk,bquad,bdip,zLen,angle,z1st,z2nd,zentry,ndfocus,
     1              Qx0i,Qy0i,foctype
      namelist/inp4/rg,rg_x,rg_y,wavelnth,nmodes,mselect,wx,wy,power,
     1              cutoff,polar,freq_wg
      namelist/inp5/nr,ntheta,nx,ny,npsi,nphi,ndv,ngam,Gauss_Quad
      namelist/inp6/zmax,dz,ndz,ztaper,zsat,dbwdz,dbw,dbw_sqr,dzw,
     1              nstep,ndiags
      namelist/inp7/enrgycon,dcfield
      namelist/inp8/wigerrs,source,nerr,error,iseed
      namelist/inp9/nharm,l,n,pin,phase_in,waist,zwaist,direction,imode
      namelist/inp10/zdiag
c
c----------------------------------------------------------------------
c The associated default data statements
c
      data l/0/,n/0/,m/2/,nr/10/,ntheta/10/,nx/10/,ny/10/,iseed/-1/,
     1     ndz/-1/,nbeams/1/,mselect/-1/,nstep/1/,ndfocus/10/,
     2     matchbeam/'on'/, wigtype/'ppf'/,beamtype/'gaussian'/,
     3     wigerrs/'off'/,source/'random'/,enrgycon/'on'/,
     4     dcfield/'off'/,sde/'on'/,emmodes/'hermite'/,OPC/'off'/,
     5     beamloss/'off'/,beam_diags/'on'/,imode/0/,
     6     trans_mode/'on'/,FFTchk/'off'/,main_out/'on'/,
     7     Msqr/'off'/,Shot_Noise/'off'/,trans_cor/'off'/,
     8     newfield/'on'/,wigplane/'x'/,noise_seed/-1/,
     9     RFmode/'TE'/,Bmapfilei/'MapFile'/,Power_vs_z/'on'/
      data SpentBeam/'off'/,S2E/'off'/S2E_method/'statistics'/,
     1     nparts_out/10000/,nbin/11/,dither/0.01d0/,
     2     Runge_Kutta/'4th_order'/,interpolation/'high_order'/
c
      data bw/5.1d0/,zw/.96d0/,energy/470.d0/,beami/10.d0/,
     1     dgamz/0.d0/,rg/.32d0/,dz/.1d0/,rbmin/0.d0/,rbmax/.4d0/,
     2     zmax/1309.d0/,pin/1000.d0/,ax/2.5d0/,x0i/0.d0/,y0i/0.d0/,
     3     xbeam/1.d0/,ybeam/1.d0/,error/.01d0/,prebunch/'off'/,
     4     cutoff/.01d0/,dgam/0.d0/,dEi/0.d0/,twissalphx/0.d0/,
     5     ztaper/-1.d6/,dbwdz/0.d0/,twissalphy/0.d0/,smallest/1.d-2/,
     6     freqrf/0.d0/,prfin/0.d0/,B_ratio/1.d0/,rscale/1.d0/
c
      data npsi/10/,ndv/6/,nphi/4/,nmodes/1/,nerr/1/,nwrite/0/,
     1     ngam/1/,iprop/1/,nsegbw/1/,nsegfoc/0/,ndiags/0/,nslices/1/,
     2     RFfield/'off'/,tprofile/'parabolic'/,slippage/'off'/,
     3     n1stpass/0/,ns_begin/1/,
     4     ns_end/1/,drifttube/'cylindrical'/,teprofile/'parabolic'/,
     5     nslip/10/
c
      data dzw/0.d0/,polar/-1.d0/,tshift/0.d0/
c

      end module the_namelists
c**********************************************************************
      program mercury
      use mpi_parameters
      use mpi
      use opticsmod
      use OPCmodes
      use params
      use the_namelists
      implicit double precision(a-h,o-z)
      parameter (nhplot=101) ! hardcoded grid size 
      integer itest
      real rtest
      double precision dtest
      complex ctest
      complex*16 simpson2Dcx
      character*8 wigtypea(:),wigplanea(:)               ! inp2 variables
      character*8 RFfielda(:),RFmodea(:)                 ! inp2 variables
      character*20 Bmapfileia(:)                         ! inp2 variables
      dimension nwa(:),nupa(:),ndowna(:),ma(:),nwha(:)   ! inp2 variables
      dimension lrfa(:),nrfa(:),n1stpassa(:)             ! inp2 variables
      dimension nbxia(:),nbyia(:),nbzia(:)               ! inp2 variables
      dimension bwa(:),zwa(:),zfirsta(:),zlasta(:)       ! inp2 variables
      dimension axa(:),edca(:),z1a(:),z2a(:)             ! inp2 variables
      dimension prfina(:),freqrfa(:),xmaxia(:),ymaxia(:) ! inp2 variables
      character*8 foctypea(:)                            ! inp3 variables
      dimension ndfocusa(:)                              ! inp3 variables
      dimension Qka(:),bquada(:),bdipa(:),zLena(:)       ! inp3 variables
      dimension anglea(:),z1sta(:),z2nda(:),zentrya(:)   ! inp3 variables
      dimension zdiaga(:)                                ! inp10 variables
      dimension x0(:),y0(:),dE(:),dI(:),l_n_mode(:,:)
      dimension nmodes_sl(:),itemp4(:)
      dimension itemp1(:,:),itemp2(:,:),itemp3(:,:)
      dimension ptemp1(:,:),ptemp2(:,:)
      allocatable x0,y0,dE,dI
      allocatable wigtypea,wigplanea,RFfielda,RFmodea
      allocatable Bmapfileia,l_n_mode
      allocatable nwa,nupa,ndowna,ma,nwha,lrfa,nrfa,n1stpassa
      allocatable nbxia,nbyia,nbzia,bwa,zwa,zfirsta,zlasta
      allocatable axa,edca,z1a,z2a,prfina,freqrfa,xmaxia,ymaxia
      allocatable Qka,bquada,bdipa,zLena,anglea,z1sta,z2nda,zentrya
      allocatable ndfocusa,foctypea,zdiaga,nmodes_sl
      allocatable itemp1,itemp2,itemp3,itemp4,ptemp1,ptemp2
c----------------------------------------------------------------------
c In order to keep the notation in the code aligned with the documentation 
c we will use l and n to represent the Hermite polynomials
c c Since l and n can take the value 0 the arrays are allocated accordingly
c m (not h) will represent the harmonicc
c i and j will be used as grid indices
c----------------------------------------------------------------------
c
c------- MPI Calls
c
      myid=0
      numprocs=1
      call mpi_init(ierr)
      call mpi_comm_rank(mpi_comm_world,myid,ierr)
      call mpi_comm_size(mpi_comm_world,numprocs,ierr)
      if(numprocs.gt.1) starttime=mpi_wtime()
c
c------- Write header line
c
      if(myid.eq.0) write(6,200)
  200 format(/23x,'***** CODE MERCURY MPI Version 1.00 *****',/,
     1        27x,   'Interface between MINERVA and OPC',/,
     2        29x,     'Created by: H.P. Freund',/,
     3        29x,     '            P. van der Slot',/,
     4        33x,         'Date: January 30, 2022',/)
      MinervaOutputFile='OpticsInput.dfl'
      MercuryDFLfile='MercuryInput.dfl'
c
      open(unit=7,file=Config_Mercury,form='formatted',status='old')
c
      nharm=1
      RecAmp1=zero
      RecAmp2=zero
c
c Read the namelist file
c
      nrdlmt=1
      FirstPass='no'
      if(myid.eq.0) read(7,inp1)
      if(numprocs.gt.1) call input1_bcast
      if(slippage.eq.'on') then ! default is 'off'
         if(myid.eq.0) read(7,slip)
         if(numprocs.gt.1) call slip_bcast
         if(teprofile.ne.'variable') then
            FirstPass='yes'
         else
            nrdlmt=nslices
         endif
      endif
c
      if(nbeams.eq.1) then
         x0i=zero
         y0i=zero
         dEi=zero
         dIi=zero
      else
         allocate(x0(nbeams),y0(nbeams),dE(nbeams),dI(nbeams))  ! fix over nslices
         do i=1,nbeams
            if(myid.eq.0) read(7,beams)
            if(numprocs.gt.1) call beams_bcast
            x0(i)=x0i
            y0(i)=y0i
            dE(i)=dEi
            dI(i)=dIi
         end do
      endif
      allocate(wigtypea(nsegbw),wigplanea(nsegbw))
      allocate(RFfielda(nsegbw),RFmodea(nsegbw))
      allocate(Bmapfileia(nsegbw))
      allocate(nwa(nsegbw),nupa(nsegbw),ndowna(nsegbw),ma(nsegbw))
      allocate(nwha(nsegbw),lrfa(nsegbw))
      allocate(nrfa(nsegbw),n1stpassa(nsegbw))
      allocate(nbxia(nsegbw),nbyia(nsegbw),nbzia(nsegbw))
      allocate(bwa(nsegbw),zwa(nsegbw),zfirsta(nsegbw))
      allocate(zlasta(nsegbw),axa(nsegbw),edca(nsegbw))
      allocate(z1a(nsegbw),z2a(nsegbw),prfina(nsegbw))
      allocate(freqrfa(nsegbw),xmaxia(nsegbw),ymaxia(nsegbw))
      do i=1,nsegbw
         ztapersegi=200.d0
         dbwsegi=zero
         dzwsegi=zero
         if(myid.eq.0) read(7,inp2)
         if(numprocs.gt.1) call input2_bcast
         wigtypea(i)=wigtype
         wigplanea(i)=wigplane
         RFfielda(i)=RFfield
         RFmodea(i)=RFmode
         Bmapfileia(i)=Bmapfilei
         nwa(i)=nw
         nupa(i)=nup
         ndowna(i)=ndown
         ma(i)=m
         nwha(i)=nwh
         lrfa(i)=lrf
         nrfa(i)=nrf
         n1stpassa(i)=n1stpass
         nbxia(i)=nbxi
         nbyia(i)=nbyi
         nbzia(i)=nbzi
         bwa(i)=bw
         zwa(i)=zw
         zfirsta(i)=zfirst
         zlasta(i)=zlast
         axa(i)=ax
         edca(i)=edc
         z1a(i)=z1
         z2a(i)=z2
         prfina(i)=prfin
         freqrfa(i)=freqrf
         xmaxia(i)=xmaxi
         ymaxia(i)=ymaxi
      end do
      if(nsegfoc.gt.0) then
         allocate(Qka(nsegfoc),bquada(nsegfoc),bdipa(nsegfoc))
         allocate(zLena(nsegfoc),anglea(nsegfoc),z1sta(nsegfoc))
         allocate(z2nda(nsegfoc),zentrya(nsegfoc))
         allocate(ndfocusa(nsegfoc),foctypea(nsegfoc))
         do i=1,nsegfoc
            if(myid.eq.0) read(7,inp3)
            if(numprocs.gt.1) call input3_bcast
            Qka(i)=Qk
            bquada(i)=bquad
            bdipa(i)=bdip
            zLena(i)=zLen
            anglea(i)=angle
            z1sta(i)=z1st
            z2nda(i)=z2nd
            zentrya(i)=zentry
            ndfocusa(i)=ndfocus
            foctypea(i)=foctype
         enddo
      endif
c
      if(myid.eq.0) read(7,inp4)
      if(numprocs.gt.1) call input4_bcast
      if(myid.eq.0) read(7,inp5)
      if(numprocs.gt.1) call input5_bcast
      if(myid.eq.0) read(7,inp6)
      if(numprocs.gt.1) call input6_bcast
      if(myid.eq.0) read(7,inp7)
      if(numprocs.gt.1) call input7_bcast
      if(myid.eq.0) read(7,inp8)
      if(numprocs.gt.1) call input8_bcast
c
      nmode=10*nmodes
      allocate(dira(nmode,nslices),nmodes_sl(nslices))   
      allocate(nharma(nmode,nslices),la(nmode,nslices))
      allocate(waista(nmode,nslices),zwaista(nmode,nslices))
      allocate(pina(nmode,nslices),phase_ina(nmode,nslices))
      allocate(P_temp(nmode),Ph_temp(nmode),Ptot(nslices))
      allocate(l_n_mode(2,nmode),na(nmode,nslices))
c
      la=0
      na=0
      nharma=0
      nmodes_sl=0
      pina=zero
      phase_ina=zero
      l_n_mode=0
c
c     if(myid.eq.0) then ! read inp9 and fill arrays
      if(numprocs.gt.1) then
         open(unit=2020,file='inp9_mercury.dat',form='unformatted',
     1        status='old')
      endif
      do ns=1,nrdlmt
         do nm=1,nmodes ! We only use direction, waist and zwaist
            direction='x'
            nharm=1
            phase_in=zero
            zwaist=zero
            if(myid.eq.0) then
               if(numprocs.eq.1) read(7,inp9)
               if(numprocs.gt.1) then
                  read(2020) nharm,l,n,pin,phase_in,waist,zwaist,
     1                       direction,imode
               endif
            endif
            if(numprocs.gt.1) call input9_bcast
            la(nm,ns)=l
            na(nm,ns)=n
            nharma(nm,ns)=nharm
            pina(nm,ns)=pin
            phase_ina(nm,ns)=phase_in
            waista(nm,ns)=waist
            zwaista(nm,ns)=zwaist
            dira(nm,ns)=direction
         enddo
      enddo
      close(unit=2020)
      if(FirstPass.eq.'yes') then ! fill out arrays
         do ns=2,nslices
            do nm=1,nmodes
               la(nm,ns)=la(nm,1)
               na(nm,ns)=na(nm,1)
               nharma(nm,ns)=nharma(nm,1)
               phase_ina(nm,ns)=phase_ina(nm,1)
               waista(nm,ns)=waista(nm,1)
               zwaista(nm,ns)=zwaista(nm,1)
               dira(nm,ns)=dira(nm,1)
            enddo
         enddo
         do nm=1,nmodes
            P_temp(nm)=pina(nm,1)
         end do
         t_min=half*(tslices-tlight)+tshift
         t_max=tlight+t_min+tshift
         t_center=half*tlight+t_min
         do nm=1,nmodes
            do ns=1,nslices
               t_slice=dble(ns-1)*dtpulse
               if(tprofile.eq.'tophat') then
                  if((t_slice.ge.t_min).and.(t_slice.le.t_max)) then
                        pina(nm,ns)=P_temp(nm)
                     else
                        pina(nm,ns)=zero
                  endif
               endif
               if(tprofile.eq.'parabolic') then
                  if((t_slice.gt.t_min).and.(t_slice.lt.t_max)) then
                     para=abs(one-four*(t_slice-t_center)**2/tlight**2)
                     pina(nm,ns)=sqrt(para)*P_temp(nm)
                     else
                        pina(nm,ns)=zero
                  endif
               endif
            enddo
         enddo
      endif ! FirstPass
c     endif ! myid=0
c
c Zero array elements on processors not using those elements.
c Necessary becayse we are using mpi_reduce.
c
      if(numprocs.gt.1) then
         allocate(itemp1(nmode,nslices))
         allocate(itemp2(nmode,nslices))
         allocate(itemp3(nmode,nslices))
         allocate(itemp4(nslices))
         allocate(ptemp1(nmode,nslices))
         allocate(ptemp2(nmode,nslices))
         itemp1=0
         itemp2=0
         itemp3=0
         itemp4=0
         ptemp1=zero
         ptemp2=zero
         do ns=1+myid,nslices,numprocs
            do nss=1,nslices
               if(ns.eq.nss) then
                  do nm=1,nmode
                     itemp1(nm,ns)=la(nm,ns)
                     itemp2(nm,ns)=na(nm,ns)
                     itemp3(nm,ns)=nharma(nm,ns)
                     ptemp1(nm,ns)=pina(nm,ns)
                     ptemp2(nm,ns)=phase_ina(nm,ns)
                  enddo
                  itemp4(ns)=nmodes_sl(ns)
               endif
            enddo
         enddo
         la=0
         na=0
         nharma=0
         nmodes_sl=0
         pina=zero
         phase_ina=zero
         do ns=1+myid,nslices,numprocs
            do nm=1,nmode
               la(nm,ns)=itemp1(nm,ns)
               na(nm,ns)=itemp2(nm,ns)
               nharma(nm,ns)=itemp3(nm,ns)
               pina(nm,ns)=ptemp1(nm,ns)
               phase_ina(nm,ns)=ptemp2(nm,ns)
            enddo
            nmodes_sl(ns)=itemp4(ns)
         enddo
      endif
c
      if(slippage.eq.'on') teprofile='variable'
      if(ndiags.gt.0) then
         allocate (zdiaga(ndiags))
         do i=1,ndiags
            if(myid.eq.0) read(7,inp10)
            if(numprocs.gt.1) call input10_bcast
            zdiaga(i)=zdiag
         end do
      endif
      if(myid.eq.0) read(7,optics)
      if(myid.eq.0) then
         ndflx=ndfl
         ndfly=ndfl
      endif
      if(numprocs.gt.1) call optics_bcast     
c
c Arrays cannot be allocated until after optics is read because 
c that's where nbxi and nbyi are defined
c
      allocate(xgrid(ndflx),ygrid(ndfly))
      allocate(simparraycx(ndflx,ndfly))
      allocate(cmplxcoefs(ndflx,ndfly))
c
c Calculate some needed constants
c 
c Change units from cm to m (01/12/07)
c Must change variable names in order to write out the variables
c in their original units zw -> zwm and waist -> waistm (cm -> m)
c
      zwm=hundredth*zwa(1) ! Note: zw is zwiggler (not zwaist)
      waistm=hundredth*waist
      domega=zwm/wavelnth ! from Minerva (hundredth* omitted)
c
      akw=twopi/zwm
      akwwaist=akw*waistm         ! from Minerva 
      wm0=akw*waistm              ! from Minerva
      wm02=wm0*wm0                ! from Minerva 
      omega=dble(nharm)*domega    ! from Minerva 
c Note: factor of hundred removed because zwiggler scaled before calculation        
      zwaste=akw*zwaist           ! from Minerva
      zrHF=half*omega*wm02
      xmax=dble(ndflx-1)*dxgrid/two
      ymax=dble(ndfly-1)*dygrid/two
      omoc=half*nharm*twopi/wavelnth
      delx=dxgrid
      dely=dygrid
      wavelength=wavelnth
      zr=omoc*waistm*waistm
c
      dhz=zero ! z=zwaist
      az=zero ! z-zwaist = 0
      wz=waistm*sqrt(one+(zwaist/zr)**2)
      alpha=zwaist/zr
c
c Generate the grids
c
      do i=1,ndflx
         xgrid(i)=dble(i-1)*delx-xmax
      end do
      do j=1,ndfly
         ygrid(j)=dble(j-1)*dely-ymax
      end do
c
c Fill in the coefficient array with the data from the .dfl file
c Compute weighted and unweighted integrals
c
      nmodes_max=0
      nmodes_old=nmodes
      do ns=1+myid,nslices,numprocs ! Loop over slices
         Ptot(ns)=zero
         nmodes_sl(ns)=nmodes_old
         call read_dfl(ns,ndflx,ndfly)
         Powers=zero
         do i=1,ndflx
            do j=1,ndfly
               Powers=Powers+abs(cmplxcoefs(i,j))**2
            end do
         end do
         Ptot(ns)=Powers
         if(nslices.gt.1) write(6,1004) ns,Powers
 1004    format('  slice=',i4,' Power(W)=',1pe12.5)
         if(Powers.ne.zero) then
            w_0=hundredth*waista(1,ns)
            z_R=pi*w_0**2/wavelength
            wz=w_0*sqrt(one+zwaista(1,ns)**2/z_R**2)
            call amplitudes(wz,Powers,ns)
            nmodes_sl(ns)=icount
         endif
      end do ! loop over slices
c
      if(numprocs.gt.1) then
         allocate(pina_wk(nmode,nslices),phase_wk(nmode,nslices))
         allocate(la_wk(nmode,nslices),na_wk(nmode,nslices))
         allocate(nmodes_wk(nslices),nharma_wk(nmode,nslices))
c
         la_wk=0
         na_wk=0
         nmodes_wk=0
         nharma_wk=0
         pina_wk=zero
         phase_wk=zero
c
         call mpi_reduce(nmodes_sl,nmodes_wk,nslices,mpi_integer,
     1                   mpi_sum,0,mpi_comm_world,ier)
         call mpi_reduce(la,la_wk,nmode*nslices,mpi_integer,
     1                   mpi_sum,0,mpi_comm_world,ier)
         call mpi_reduce(na,na_wk,nmode*nslices,mpi_integer,
     1                   mpi_sum,0,mpi_comm_world,ier)
         call mpi_reduce(nharma,nharma_wk,nmode*nslices,mpi_integer,
     1                   mpi_sum,0,mpi_comm_world,ier)
         call mpi_reduce(pina,pina_wk,nmode*nslices,
     1                   mpi_double_precision,mpi_sum,0,
     2                   mpi_comm_world,ier)
         call mpi_reduce(phase_ina,phase_wk,nmode*nslices,
     1                   mpi_double_precision,mpi_sum,0,
     2                   mpi_comm_world,ier)
         if(myid.eq.0) then
            nmodes_sl=nmodes_wk
            la=la_wk
            na=na_wk
            nharma=nharma_wk
            pina=pina_wk
            phase_ina=phase_wk
         endif
      endif
c
c The remainder of the code is done on the master node myid = 0
c
      if(myid.eq.0) then
         do ns=1,nslices
            do i=1,nmodes_sl(ns)
               Ptot(ns)=Ptot(ns)+pina(i,ns)
            end do
         end do
         if(nslices.eq.1) write(6,21) Ptot(ns)
 21      format(18x,'Total',es12.5)
c
      if(slippage.eq.'off') nmodes=nmodes_sl(1)
      if(slippage.eq.'on') then
         ncount=nmodes_sl(1)
         do nm=1,nmodes_sl(1)
            l_n_mode(1,nm)=la(nm,1)
            l_n_mode(2,nm)=na(nm,1)
         end do
         do ns=2,nslices
            ncc=0
            do nm=1,nmodes_sl(ns)
               icheck=0
               do nc=1,ncount
                  lnc=l_n_mode(1,nc)
                  nnc=l_n_mode(2,nc)
                  if((la(nm,ns).eq.lnc).and.(na(nm,ns).eq.nnc)) then
                    icheck=icheck+1
                  endif
               end do
               if(icheck.eq.0) then
                  ncc=ncc+1
                  l_n_mode(1,ncount+ncc)=la(nm,ns)
                  l_n_mode(2,ncount+ncc)=na(nm,ns) 
               endif
            end do
            ncount=ncount+ncc
         end do
         nmodes=ncount
c
         do ns=1,nslices
            do nm=nmodes_sl(ns)+1,nmodes
               pina(nm,ns)=zero
               phase_ina(nm,ns)=zero
               nharma(nm,ns)=nharma(1,ns)  ! fix logic for multiple harmonics
               icheck1=0
               do nmm=1,nmodes
                  icheck2=0
                  lnc=l_n_mode(1,nmm)
                  nnc=l_n_mode(2,nmm)
                  do nnm=1,nm
                     if((lnc.eq.la(nnm,ns))
     1                      .and.(nnc.eq.na(nnm,ns))) icheck2=icheck2+1
                  end do
                  if((icheck1.eq.0).and.(icheck2.eq.0)) then
                     la(nm,ns)=lnc
                     na(nm,ns)=nnc
                     icheck1=icheck+1
                  endif
               end do
            end do
         end do
c
c      Apply Cavity Tuning Condition
c
         t_rndtrp=two*cavLsc/(hundredth*cvac)
c        L_w=hundredth*zw*(Nw-nup-ndown)  !---------------------------!
c        K_rms=0.09337*bw*zw/sqrt(two)                                !
c        gamma_b=one+energy/0.510976d0                                !--vgroup correction
c        v_gr=cvac*(one-(one+K_rms**2)/(three*gamma_b**2))            !
c        t_correct=L_w*(one-v_gr/cvac)/(hundredth*cvac) !-------------!
         t_sep=one/reprate
         dtpulse=tslices/dble(nslices-1)
         if(t_rndtrp.ge.t_sep) then
             Nsync=int(half+t_rndtrp/t_sep)
             tcavtune=dble(Nsync)*t_sep-t_rndtrp
         endif
         if(t_rndtrp.lt.t_sep) then
            Nsync=int(half+t_sep/t_rndtrp)
            tcavtune=t_sep-dble(Nsync)*t_rndtrp
         endif
         if(abs(tcavtune).gt.tpulse) then
            if(myid.eq.0) write(6,2468) 
 2468       format(/,' WARNING: Cavity Detuning Greater than',
     1             /,'          the Total Pulse Time',
     2             /,'          Terminate MERCURY')
            if(myid.eq.0) write(6,2469) tcavtune, tpulse,cavLsc,
     1                                  t_rndtrp
 2469       format(/,'          tcavtune = ',e17.10,
     1             /,'          tpulse   = ',e17.10,
     2             /,'          cavLsc   = ',e17.10,
     3             /,'          t_rndtrp = ',e17.10)
            call MPI_Finalize(ier)
            stop
         endif
         tcavtune=tcavtune
     1                 -(Nw-nup-ndown)*wavelnth/(hundredth*cvac) !--correct for slippage
c    1                 -half*(Nw-nup-ndown)*wavelnth/(hundredth*cvac) !--correct for slippage
         call cav_tune(tcavtune,dtpulse,nmodes,nslices)
      endif   ! if slippage = on
c
c Write the namelist file
c
      open(unit=IOPC,file=Config_Minerva,
     1     form='formatted',status='replace')
         write(IOPC,*) ' &inp1'
         write(IOPC,1111) nsegbw,nsegfoc,iprop,nwrite,nbeams,
     1                    noise_seed,nparts_out,nbin,nwig_harms
 1111    format('   nsegbw=',i0,/,'   nsegfoc=',i0,/,'   iprop=',i0,/,
     1          '   nwrite=',i0,/,'   nbeams=',i0,/,
     2          '   noise_seed=',i0,/,'   nparts_out=',i0,/,
     3          '   nbin=',i0,/,'   nwig_harms=',i0)
         write(IOPC,1131) energy,current,rbmin,rbmax,xbeam,ybeam,epsx,
     1                    epsy,dgam,dgamz,psiwidth,B_ratio,twissalphx,
     2                    twissalphy,dither,x_center,y_center
 1131    format(SP,'   energy=',1pe17.10,/,'   current=',e17.10,/,
     1          '   rbmin=',e17.10,/,'   rbmax=',e17.10,/,'   xbeam=',
     2          e17.10,/,'   ybeam=',e17.10,/,'   epsx=',e17.10,/,
     3          '   epsy=',e17.10,/,'   dgam=',e17.10,/,'   dgamz=',
     4          e17.10,/,'   psiwidth=',e17.10,/,'   B_ratio=',
     5          e17.10,/,'   twissalphx=',e17.10,/,'   twissalphy=',
     6          e17.10,/,'   dither=',e17.10,/,'   x_center=',
     7          e17.10,/,'   y_center=',e17.10)
         write(IOPC,8000) trim(beamtype),trim(sde),trim(emmodes),
     1                    trim(prebunch),trim(matchbeam),
     2                    trim(slippage),trim(beam_diags),
     3                    trim(trans_mode),trim(FFTchk),trim(main_out),
     4                    trim(drifttube),trim(Msqr),trim(Shot_Noise),
     5                    trim(trans_cor),trim(newfield),trim(OPC),
     6                    trim(Power_vs_z),trim(SpentBeam),
     7                    trim(Runge_Kutta)
 8000    format("   beamtype='",a,"'",/,"   sde='",a,"'",/,
     1          "   emmodes='",a,"'",/,"   prebunch='",a,"'",/,
     2          "   matchbeam='",a,"'",/,
     3          "   slippage='",a,"'",/,"   beam_diags='",a,"'",/,
     4          "   trans_mode='",a,"'",/,"   FFTchk='",a,"'",/,
     5          "   main_out='",a,"'",/,"   drifttube='",a,"'",/,
     6          "   Msqr='",a,"'",/,"   Shot_Noise='",a,"'",/,
     7          "   trans_cor='",a,"'",/,"   newfield='",a,"'",/,
     8          "   OPC='",a,"'",/,"   Power_vs_z='",a,"'",/,
     9          "   SpentBeam='",a,"'",/,"   Runge_Kutta='",a,"'")

         write(IOPC,7999) trim(S2E),trim(S2E_method),trim(Wakefield),
     1                    trim(acdc),trim(material)
 7999    format("   S2E='",a,"'",/,"   S2E_method='",a,"'",/,
     1          "   Wakefield='",a,"'",/,"   acdc='",a,"'",/,
     2          "   material='",a,"'")
         write(IOPC,*) ' /' 
      if(slippage.eq.'on') then ! default is 'off'
            write(IOPC,*) ' &slip'
            write(IOPC,8001) trim(tprofile),trim(teprofile),
     1                       trim(interpolation)
 8001       format("   tprofile='",a,"'",/,"   teprofile='",a,"'",
     1             "   interpolation='",a,"'")
            write(IOPC,1112) nslices,nslip
 1112       format('   nslices=',i0,/,'   nslip=',i0)
            write(IOPC,1132) tpulse,tlight,tslices,tshift
 1132       format(SP,'   tpulse=',1pe17.10,/,'   tlight=',e17.10,/,
     1                '   tslices=',e17.10,/,'   tshift=',e17.10)
            write(IOPC,*) ' /'
      endif
      if(nbeams.gt.1) then 
         do i=1,nbeams         
            x0i=x0(i)
            y0=y0(i)
            dEi=dE(i)
            dIi=dI(i)
                  write(IOPC,*) '  &beams'
                  write(IOPC,1133) x0i,y0i,dEi,dIi
 1133             format(SP,'   x0i=',1pe17.10,/,'   y0i=',e17.10,/,
     1                      '   dEi=',e17.10,/,'   dIi=',e17.10)
                  write(IOPC,*) '  /'
         enddo
      endif
      if(nsegbw.eq.1) then
           write(IOPC,*) ' &inp2'
           write(IOPC,1113) Nw,nup,ndown,m,nwh,lrf,nrf,n1stpass,
     1                      nbxi,nbyi,nbzi
 1113      format('   Nw=',i0,/,'   nup=',i0,/,'   ndown=',i0,/,
     1            '   m=',i0,/,'   nwh=',i0,/,'   lrf=',i0,/,
     2            '   nrf=',i0,/,'   n1stpass=',i0,/,'   nbxi=',i0,/,
     3            '   nbyi=',i0,/,'   nbzi=',i0)
           write(IOPC,1134) bw,zw,zfirst,zlast,ax,Edc,z1,z2,Prfin,
     1                    freqrf,xmaxi,ymaxi,ztapersegi,dbwsegi,dzwsegi
 1134      format(SP,'   bw=',1pe17.10,/,'   zw=',e17.10,/,
     1               '   zfirst=',e17.10,/,'   zlast=',e17.10,/,
     2               '   ax=',e17.10,/,'   Edc=',e17.10,/,
     3               '   z1=',e17.10,/,'   z2=',e17.10,/,
     4               '   Prfin=',e17.10,/,'   freqrf=',e17.10,/,
     5               '   xmaxi=',e17.10,/,'   ymaxi=',e17.10,
     6               '   ztapersegi=',e17.10,/,'   dbwsegi=',e17.10,/,
     7               '   dzwsegi=',e17.10)
           write(IOPC,8002) trim(wigtype),trim(wigplane),trim(RFfield),
     1                      trim(RFmode),trim(Bmapfilei)
 8002      format("   wigtype='",a,"'",/,"   wigplane='",a,"'",/,
     1            "   RFfield='",a,"'",/,"   RFmode='",a,"'",/,
     2            "   Bmapfilei='",a,"'")
           write(IOPC,*) ' /'
      else
        do i=1,nsegbw
            wigtype=wigtypea(i)
            wigplane=wigplanea(i)
            RFfield=RFfielda(i)
            RFmode=RFmodea(i)
            Bmapfilei=Bmapfileia(i)
            nw=nwa(i)
            nup=nupa(i)
            ndown=ndowna(i)
            m=ma(i)
            nwh=nwha(i)
            lrf=lrfa(i)
            nrf=nrfa(i)
            n1stpass=n1stpassa(i)
            nbxi=nbxia(i)
            nbyi=nbyia(i)
            nbzi=nbzia(i)
            bw=bwa(i)
            zw=zwa(i)
            zfirst=zfirsta(i)
            zlast=zlasta(i)
            ax=axa(i)
            edc=edca(i)
            z1=z1a(i)
            z2=z2a(i)
            prfin=prfina(i)
            freqrf=freqrfa(i)
            xmaxi=xmaxia(i)
            ymaxi=ymaxia(i)
            write(19,inp2)
         enddo
      endif
      if(nsegfoc.gt.0) then
         do i=1,nsegfoc
            Qk=Qka(i)
            bquad=bquada(i)
            bdip=bdipa(i)
            zLen=zLena(i)
            angle=anglea(i)
            z1st=z1sta(i)
            z2nd=z2nda(i)
            zentry=zentrya(i)
            ndfocus=ndfocusa(i)
            foctype=foctypea(i)
           write(IOPC,*) ' &inp3'
           write(IOPC,8003) trim(foctype)
 8003      format("   foctype='",a,"'")
           write(IOPC,1135) Qk,bquad,bdip,zLen,angle,z1st,z2nd,zentry
 1135      format(SP,'   Qk=',1pe17.10,/,'   bquad=',e17.10,/,
     1               '   bdip=',e17.10,/,'   zLen=',e17.10,/,
     2               '   angle=',e17.10,/,'  z1st=',e17.10,/,
     3               '   z2nd=',e17.10,/,'   zentry=',e17.10)
           write(IOPC,1114) ndfocus
 1114      format('   ndfocus=',i0)
           write(IOPC,*) ' /'
         enddo
      endif
         write(IOPC,*) ' &inp4'
         write(IOPC,1136) rg,rg_x,rg_y,wavelnth,wx,wy,power,cutoff,
     1                    polar,freq_wg
 1136    format(SP,'   rg=',1pe17.10,/,'   rg_x=',e17.10,/,
     1             '   rg_y=',e17.10,/,'   wavelnth=',e17.10,/,
     2             '   wx=',e17.10,/,'   wy=',e17.10,/,
     3             '   power=',e17.10,/,'   cutoff=',e17.10,/,
     4             '   polar=',e17.10,/,'  freq_wg=',e17.10)
         write(IOPC,1115) nmodes,mselect
 1115    format('   nmodes=',i0,/,'   mselect=',i0)
         write(IOPC,*) ' /'
         write(IOPC,*) ' &inp5'
         write(IOPC,1116) nr,ntheta,nx,ny,npsi,nphi,ndv,ngam
 1116    format('   nr=',i0,/,'   ntheta=',i0,/,'   nx=',i0,/,
     1          '   ny=',i0,/,'   npsi=',i0,/,'   nphi=',i0,/,
     2          '   ndv=',i0,/,'   ngam=',i0)
         write(IOPC,*) ' /'
         write(IOPC,*) ' &inp6' !-------------------------------------!
         write(IOPC,1137) zmax,dz,ztaper,zsat,dbwdz,dbw,dbw_sqr,dzw   !
 1137    format(SP,'   zmax=',1pe17.10,/,'   dz=',e17.10,/,           !
     1             '   ztaper=',e17.10,/,'   zsat=',e17.10,/,         !
     2             '   dbwdz=',e17.10,/,'   dbw=',e17.10,/,           !
     3             '   dbw_sqr=',e17.10,/,'   dzw=',e17.10)           !
         write(IOPC,1117) ndz,nstep,ndiags                            !
 1117    format('   ndz=',i0,/,'   nstep=',i0,/,'   ndiags=',i0)      !
         write(IOPC,*) ' /'  !----------------------------------------!
         write(IOPC,*) ' &inp7'
         write(IOPC,8007) trim(enrgycon),trim(dcfield)
 8007    format("   enrgycon='",a,"'",/,"   dcfield='",a,"'")
         write(IOPC,*) ' /'
         write(IOPC,*) ' &inp8'
         write(IOPC,8008) trim(wigerrs),trim(source)
 8008    format("   wigerrs='",a,"'",/,"   source='",a,"'")
         write(IOPC,1118) nerr,iseed
 1118    format('   nerr=',i0,/,'   iseed=',i0)
         write(IOPC,1138) error
 1138    format(SP,'   error=',1pe17.10)
         write(IOPC,*) ' /'
      if((numprocs.gt.1).and.(teprofile.eq.'variable')) then
         open(unit=2019,file='inp9_minerva.dat',form='unformatted',
     1        status='replace')      
      endif
      do ns=1,nslices
         do nm=1,nmodes
            direction=dira(1,ns)
            waist=waista(1,ns)
            zwaist=zwaista(1,ns)
            nharm = nharma(nm,ns)
            l=la(nm,ns)
            n=na(nm,ns)
            pin=pina(nm,ns)
            phase_in=phase_ina(nm,ns)
            if((numprocs.eq.1).or.(teprofile.ne.'variable')) then
               write(IOPC,*) ' &inp9'
               write(IOPC,1119) nharm,l,n,imode
 1119          format('   nharm=',i0,/,'   l=',i0,/,'   n=',i0,/,
     1                '   imode=',i0)
               write(IOPC,1139) pin,phase_in,waist,zwaist
 1139          format(SP,'   pin=',1pe17.10,/,'   phase_in=',e17.10,/,
     1                   '   waist=',e17.10,/,'   zwaist=',e17.10)
               write(IOPC,8009) trim(direction)
 8009          format("   direction='",a,"'")
               write(IOPC,*) ' /'
            endif
            if((numprocs.gt.1).and.(teprofile.eq.'variable')) then
                write(2019) nharm,l,n,pin,phase_in,waist,zwaist,
     1                         direction             
            endif
         enddo
      enddo
      close(unit=2019)
      do i=1,ndiags
            zdiag=zdiaga(i)
             write(IOPC,*) ' &inp10'
             write(IOPC,1141) zdiag
 1141        format(SP,'   zdiag=',1pe17.10)
             write(IOPC,*) ' /'
      end do
      write(IOPC,*) ' &optics'
      write(IOPC,1150) ndfl,nslcs
 1150 format('   ndfl=',i0,/,'   nslcs=',i0)
      write(IOPC,1151) nsigma
 1151 format('   nsigma=',i0)
      write(IOPC,1152) gridsize
 1152 format(SP,'   gridsize=',1pe17.10)
 1156 format(SP,'   dxdy=',1pe17.10)
      write(IOPC,1153) dxgrid,dygrid,cavLsc,reprate
 1153 format(SP,'   dxgrid=',1pe17.10,/,'   dygrid=',1pe17.10,/,
     1  '   cavLsc=',e17.10,/,'   reprate=',e17.10)
      write(IOPC,1155) trim(Config_Minerva),trim(Config_Mercury),
     1                 trim(MinervaDFLfile),trim(MercuryDFLfile),
     2                 trim(MpiIO)
 1155 format("   Config_Minerva='",a,"'",/,"   Config_Mercury='",a,"'",/,
     1       "   MinervaDFLfile='",a,"'",/,"   MercuryDFLfile='",a,"'",/,
     2       "   MpiIO='",a,"'")
      write(IOPC,*) ' /'
c
      close(unit=7)
      close(unit=IOPC)
      endif ! end of myid = 0 if statement
c
      call mpi_finalize(ierr)
      stop
      end
c**********************************************************************
      subroutine cav_tune(tcavtune,dtpulse,nmodes,nslices)
      use opticsmod
      use OPCmodes
      use params
      implicit double precision(a-h,o-z)
      dimension a1(nmodes,nslices),a2(nmodes,nslices),
     1          a1_n(nmodes,nslices),a2_n(nmodes,nslices),
     2          waista2(nmodes,nslices),zwaista2(nmodes,nslices)
c----------------------------------------------------------------------
      if(tcavtune.ge.zero) nshift=Nw+int(tcavtune/dtpulse+half)
      if(tcavtune.lt.zero) nshift=Nw+int(tcavtune/dtpulse-half)
      if(nshift.eq.0) return
      do ns=1,nslices
         do nm=1,nmodes
            a1(nm,ns)=sqrt(pina(nm,ns))*cos(phase_ina(nm,ns))
            a2(nm,ns)=sqrt(pina(nm,ns))*sin(phase_ina(nm,ns))
            a1_n(nm,ns)=a1(nm,ns)
            a2_n(nm,ns)=a2(nm,ns)
         end do
      end do
      do ns=1,nslices
         do nm=1,nmodes
            a1(nm,ns)=zero
            a2(nm,ns)=zero
         end do
      end do
c
      if(nshift.gt.0) then
         fac1=(tcavtune-dble(nshift)*dtpulse)/dtpulse
         fac2=one-fac1
         do nm=1,nmodes
            a1(nm,nshift)=fac2*a1_n(nm,1)
            a2(nm,nshift)=fac2*a2_n(nm,1)
         end do
         do ns=nshift+1,nslices
            ns1=ns-nshift
            ns2=ns-nshift+1
            do nm=1,nmodes
               a1(nm,ns)=fac1*a1_n(nm,ns1)+fac2*a1_n(nm,ns2)
               a2(nm,ns)=fac1*a2_n(nm,ns1)+fac2*a2_n(nm,ns2)
            end do
         end do
      endif
c
      if(nshift.lt.0) then
         fac1=abs(tcavtune-dble(nshift)*dtpulse)/dtpulse
         fac2=one-fac1
         do nm=1,nmodes
            a1(nm,nslices+nshift)=fac2*a1_n(nm,nslices)
            a2(nm,nslices+nshift)=fac2*a2_n(nm,nslices)
         end do
         do ns=nslices+nshift-1,1,-1
            ns1=ns-nshift+1
            ns2=ns-nshift
            do nm=1,nmodes
               a1(nm,ns)=fac1*a1_n(nm,ns1)+fac2*a1_n(nm,ns2)
               a2(nm,ns)=fac1*a2_n(nm,ns1)+fac2*a2_n(nm,ns2)
            end do
         end do
      endif
c
      do ns=1,nslices
         do nm=1,nmodes
            pina(nm,ns)=a1(nm,ns)**2+a2(nm,ns)**2
            phase_ina(nm,ns)=datan2(a2(nm,ns),a1(nm,ns))
         end do
      end do
      return
      end
c**********************************************************************
      subroutine read_dfl(irec, nx, ny)
c
c     Read binary fieldfile and store it in var
c
      use opticsmod
      use mpi_parameters
      use mpi
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      irecsize=nx*ny*16 ! record size in binary files
c     print *,'opening ', MercuryDFLfile
      open(unit=62,file=MercuryDFLfile,recl=irecsize,access='direct',
     1     status='old',err=10)

c     print *,'reading ', MercuryDFLfile
      read(unit=62,rec=irec,err=20)
     1                      ((cmplxcoefs(ix,iy),ix=1,nx),iy=1,ny)
      close(unit=62)  
      return
 10      print *,'error opening ', MercuryDFLfile,' in read_dfl'
      return
 20      print *,'error reading ', MercuryDFLfile,' in read_dfl'
      return
      end
c**********************************************************************
      subroutine hermite_pt(n,x,hn,ndflh)
      use opticsmod
      implicit double precision(a-h,o-z)
      parameter(one=1.0d0,two=2.0d0)
      dimension x(ndflh),hn(ndflh),vnm1(ndflh),vnm2(ndflh)
c----------------------------------------------------------------------
      if(n.eq.0) then
         do i=1,ndflh
            hn(i)=one
         end do
         return
      endif
c
      if(n.eq.1) then
         do i=1,ndflh
            hn(i)=two*x(i)
         end do
         return
      endif
c
      if(n.ge.2) then
         do i=1,ndflh
            vnm2(i)=one
            vnm1(i)=two*x(i)
         end do
         do l=2,n
            do i=1,ndflh
               hn(i)=two*x(i)*vnm1(i)-dble(2*(l-1))*vnm2(i)
               vnm2(i)=vnm1(i)
               vnm1(i)=hn(i)
            end do
         end do
      endif
      return
      end
c**********************************************************************
c     function factorial(n)
      double precision function factorial(n)
      implicit double precision(a-h,o-z)
c     parameter(one=1.)
      parameter(one=1.d0)
c----------------------------------------------------------------------
      factorial=one
      if(n.le.1) return
      do i=2,n
         factorial=dble(i)*factorial
      end do
      return
      end
c**********************************************************************
      complex*16 function simpson2Dcx(nx,ny,dx,dy,mtx)
      implicit double precision(a-h,o-z)
      parameter(zero=0.0d0,two=2.0d0,for=4.0d0)
      parameter(eht=8.0d0,ein=9.0d0,sxt=16.0d0)
      complex*16 mtx(nx,ny),p1,p2,sum1,sum2,sum3
c----------------------------------------------------------------------
c
c This routine calculates the 2D integral of a function f(x,y) that has
c been evaluated on a uniformly spaced (dx,dy) 2D grid of dimension (nx,ny). 
c The number of points in each direction must be odd and greater than 3.
c
      if(nx.lt.3) then
         print *,'X dimension is less than 3'
         return
      endif
      if(ny.lt.3) then
         print *,'Y dimension is less than 3'
         return
      endif
      if(MOD(nx,2).ne.1) then
         print *,'NX is not an odd number'
         return
      endif
      if(MOD(ny,2).ne.1) then
         print *,'NY is not an odd number'
         return
      endif
c
c Initialize sums
c
      p1=DCMPLX(zero,zero)
      p2=DCMPLX(zero,zero)
      sum1=DCMPLX(zero,zero)
      sum2=DCMPLX(zero,zero)
      sum3=DCMPLX(zero,zero)
c
      m=nx/2
      n=ny/2
c
      p1=mtx(1,1)+mtx(nx,1)+mtx(1,ny)+mtx(nx,ny)
      p2=for*(mtx(1,2)+mtx(2,1)+mtx(2,ny)+mtx(nx,2))+sxt*mtx(2,2)
c
      do i=2,m
         sum1=sum1+two*(mtx(2*i-1,1)+mtx(2*i-1,ny))
     1            +for*(mtx(2*i,1)+mtx(2*i,ny))
     2            +eht*mtx(2*i-1,2)+sxt*mtx(2*i,2)
      enddo
      do j=2,n
         sum2=sum2+two*(mtx(1,2*j-1)+mtx(nx,2*j-1))
     1            +for*(mtx(1,2*j)+mtx(nx,2*j))
     2            +eht*mtx(2,2*j-1)+sxt*mtx(2,2*j)
      enddo
      do i=2,m
         do j=2,n
            sum3=sum3+for*mtx(2*i-1,2*j-1)+sxt*mtx(2*i,2*j)
     1               +eht*(mtx(2*i,2*j-1)+mtx(2*i-1,2*j))
         enddo
      enddo
      simpson2Dcx=dx*dy*(p1+p2+sum1+sum2+sum3)/ein
      return
      end
c**********************************************************************
      subroutine amplitudes(wz,Pnorm,nsl)
      use opticsmod
      use OPCmodes
      implicit double precision(a-h,o-z)
      parameter (zero=0.0d0,one=1.0d0,two=2.0d0,half=0.5d0) 
      parameter (sqrtpi=1.772453850905516d0,rt2=1.414213562373095d0)
      parameter (pi=3.141592653589793d0,twopi=6.283185307179586d0)
      parameter (anine=9.d0,four=4.d0,AmpEps=1.0d-3)
      complex*16 simpson2Dcx,answer
      dimension argx(ndflx),argy(ndfly),hxval(ndflx),hyval(ndfly)
c----------------------------------------------------------------------
c This routine calculates the amplitude and phase of the vector potential 
c in the Gauss-Hermite representation given the complex coefficients
c of the electric fields
c----------------------------------------------------------------------
      simparraycx=DCMPLX(zero,zero)
      do l=0,maxorder
         coefl=factorial(l)*two**(l)
         do n=0,maxorder
            coefn=factorial(n)*two**(n)
            do m=1,maxharms
               do i=1,ndflx
                  argx(i)=rt2*xgrid(i)/wz
               end do
               call hermite_pt(l,argx,hxval,ndflx)
               do j=1,ndfly
                  argy(j)=rt2*ygrid(j)/wz
               end do 
               call hermite_pt(n,argy,hyval,ndfly)
               do i=1,ndflx
                  do j=1,ndfly
                     rsq=(xgrid(i)**2+ygrid(j)**2)/wz**2
                     simparraycx(i,j)=two*(hxval(i)/coefl)
     1                         *(hyval(j)/coefn)
     2                            *exp(-rsq)*cmplxcoefs(i,j)/(pi*wz**2)
     3                              *exp(dcmplx(zero,alpha*rsq))
c    3                              *exp(dcmplx(zero,-alpha*rsq))
                  end do
               end do
               answer=simpson2Dcx(ndflx,ndfly,delx,dely,simparraycx)
               Amag(l,n,m)=abs(answer)
               Aphs(l,n,m)=DATAN2(-DIMAG(answer),DREAL(answer))
            enddo
         enddo
      enddo
c
      AmpMax=MAXVAL(Amag)
      Psum=zero
      icount=0
      do m=1,maxharms
         do l=0,maxorder
            do n=0,maxorder
               if(Amag(l,n,m).lt.AmpMax*AmpEps) then
                  Amag(l,n,m)=zero
                  Aphs(l,n,m)=zero
               else
                  icount=icount+1
                  nharma(icount,nsl)=m
                  ln(icount)=l
                  nn(icount)=n
c                 P_temp(icount)=Amag(l,n,m)/(four*xmax*ymax)
                  P_temp(icount)=Amag(l,n,m)
                  Ph_temp(icount)=Aphs(l,n,m)
                  Psum=Psum+P_temp(icount)
               endif
            enddo
         enddo
      enddo
      imax=MAXVAL(ln)+MAXVAL(nn)
      nmodes=0
      Psum2=zero
      do i=0,imax
         do n=1,icount
            if(ln(n)+nn(n).eq.i) then
               nmodes=nmodes+1
               pina(nmodes,nsl)=Pnorm*P_temp(n)/Psum
               Psum2=Psum2+pina(nmodes,nsl)
               phase_ina(nmodes,nsl)=Ph_temp(n)
               la(nmodes,nsl)=ln(n)
               na(nmodes,nsl)=nn(n)
            endif
         end do
      end do
      return
      end
c**********************************************************************
      subroutine W_ratio(W_rat,Pnorm,nsl)
      use opticsmod
      use OPCmodes
      implicit double precision(a-h,o-z)
      parameter (zero=0.0d0,one=1.0d0,two=2.0d0,half=0.5d0) 
c----------------------------------------------------------------------
      t1=zero
      t2=zero
      t3=zero
      do nm=1,icount
         l=la(nm,nsl)
         n=na(nm,nsl)
         t1=t1+dble(l+n+1)*pina(nm,nsl)
         do nmm=1,icount
            lnm=la(nmm,nsl)
            nnm=na(nmm,nsl)
            if(lnm.eq.l+2) then
              t2=t2+sqrt(pina(nm,nsl)*pina(nmm,nsl))
     1                *sqrt(dble((l+1)*(l+2)))
     2                  *cos(phase_ina(nm,nsl)-phase_ina(nmm,nsl))
            endif
            if(nnm.eq.n+2) then
              t3=t3+sqrt(pina(nm,nsl)*pina(nmm,nsl))
     1                *sqrt(dble((n+1)*(n+2)))
     2                  *cos(phase_ina(nm,nsl)-phase_ina(nmm,nsl))
            endif
         end do
      end do
      W_rat=(t1+t2+t3)/Pnorm
      return
      end
c**********************************************************************
      subroutine input1_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------   
      call mpi_bcast(nsegbw,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nsegfoc,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nwrite,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(iprop,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nbeams,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nparts_out,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nbin,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nwig_harms,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(energy,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(current,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(rbmin,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(rbmax,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(xbeam,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(ybeam,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(epsx,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(epsy,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dgam,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dgamz,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(twissalphx,1,mpi_double_precision,0,
     1               mpi_comm_world,ier)
      call mpi_bcast(twissalphy,1,mpi_double_precision,0,
     1               mpi_comm_world,ier)
      call mpi_bcast(psiwidth,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(beamtype,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(sde,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(emmodes,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(prebunch,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(B_ratio,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(matchbeam,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(slippage,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(beam_diags,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(trans_mode,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(FFTchk,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(main_out,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(drifttube,11,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(Msqr,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(Shot_Noise,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(trans_cor,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(newfield,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(OPC,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(Power_vs_z,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(SpentBeam,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(S2E,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(S2E_method,11,mpi_character,0,mpi_comm_world,ier)     
      call mpi_bcast(Wakefield,3,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(acdc,2,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(material,2,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(dump,3,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(restart,3,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(dither,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(noise_seed,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(smallest,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(rscale,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      return
      end
c**********************************************************************
      subroutine input2_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
        call mpi_bcast(Nw,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(nup,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(ndown,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(nwh,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(lrf,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(nrf,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(n1stpass,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(bw,1,mpi_double_precision,0,mpi_comm_world,ier)
        call mpi_bcast(zw,1,mpi_double_precision,0,mpi_comm_world,ier)
        call mpi_bcast(zfirst,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(zlast,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(ax,1,mpi_double_precision,0,mpi_comm_world,ier)
        call mpi_bcast(Edc,1,mpi_double_precision,0,mpi_comm_world,ier)
        call mpi_bcast(z1,1,mpi_double_precision,0,mpi_comm_world,ier)
        call mpi_bcast(z2,1,mpi_double_precision,0,mpi_comm_world,ier)
        call mpi_bcast(Prfin,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(freqrf,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(ztapersegi,1,mpi_double_precision,0,
     1                 mpi_comm_world,ier)
        call mpi_bcast(dbwsegi,1,mpi_double_precision,0,
     1                 mpi_comm_world,ier)
        call mpi_bcast(dzwsegi,1,mpi_double_precision,0,
     1                 mpi_comm_world,ier)
        call mpi_bcast(wigtype,8,mpi_character,0,mpi_comm_world,ier)
        call mpi_bcast(wigplane,8,mpi_character,0,mpi_comm_world,ier)
        call mpi_bcast(RFfield,8,mpi_character,0,mpi_comm_world,ier)
        call mpi_bcast(RFmode,8,mpi_character,0,mpi_comm_world,ier)
        call mpi_bcast(nbxi,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(nbyi,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(nbzi,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(xmaxi,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(ymaxi,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(Bmapfilei,10,mpi_character,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input3_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
        call mpi_bcast(ndfocus,1,mpi_integer,0,mpi_comm_world,ier)
        call mpi_bcast(Qk,1,mpi_double_precision,0,mpi_comm_world,ier)
        call mpi_bcast(bquad,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(bdip,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(zLen,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(angle,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(z1st,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(z2nd,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(zentry,1,mpi_double_precision,0,mpi_comm_world,
     1                 ier)
        call mpi_bcast(foctype,8,mpi_character,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input4_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(nmodes,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(mselect,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(rg,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(rg_x,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(rg_y,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(wavelnth,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(freq_wg,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(wx,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(wy,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(power,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(cutoff,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(polar,1,mpi_double_precision,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input5_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(nr,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ntheta,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nx,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ny,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(npsi,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nphi,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ndv,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ngam,1,mpi_integer,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input6_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(ndz,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nstep,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ndiags,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(zmax,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dz,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(ztaper,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(zsat,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dbwdz,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dbw,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dzw,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(dbw_sqr,1,mpi_double_precision,0,mpi_comm_world,
     1               ier)
      return
      end
c**********************************************************************
      subroutine input7_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(enrgycon,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(dcfield,8,mpi_character,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input8_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(nerr,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(iseed,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(error,1,mpi_double_precision,0,mpi_comm_world,ier)
      call mpi_bcast(wigerrs,8,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(source,8,mpi_character,0,mpi_comm_world,ier)
      return
      end
c**********************************************************************
      subroutine input9_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(nharm,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(l,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(n,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(pin,1,mpi_double_precision,0,mpi_comm_world,
     1                    ier)
      call mpi_bcast(phase_in,1,mpi_double_precision,0,
     1                    mpi_comm_world,ier)
      call mpi_bcast(waist,1,mpi_double_precision,0,
     1                    mpi_comm_world,ier)
      call mpi_bcast(zwaist,1,mpi_double_precision,0,
     1                    mpi_comm_world,ier)
      call mpi_bcast(direction,8,mpi_character,0,mpi_comm_world,
     1                    ier)
      return
      end
c**********************************************************************
      subroutine input10_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(zdiag,1,mpi_double_precision,0,mpi_comm_world,
     1                   ier)
      return
      end
c**********************************************************************
      subroutine beams_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(x0i,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(y0i,1,mpi_double_precision,0,
     1                  mpi_comm_world,ier)         
      call mpi_bcast(dEi,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(dIi,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      return
      end
c**********************************************************************
      subroutine slip_bcast
      use mpi_parameters
      use mpi
      use the_namelists
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(tprofile,12,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(teprofile,12,mpi_character,0,mpi_comm_world,ier)
      call mpi_bcast(interpolation,10,mpi_character,0,mpi_comm_world,
     1               ier)
      call mpi_bcast(nslip,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(nslices,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(tpulse,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(tlight,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(tslices,1,mpi_double_precision,0,
     1                  mpi_comm_world,ier)         
      call mpi_bcast(tshift,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      return
      end
c**********************************************************************
      subroutine optics_bcast
      use mpi_parameters
      use mpi
      use opticsmod
      implicit double precision(a-h,o-z)
c----------------------------------------------------------------------
      call mpi_bcast(ndflx,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(ndfly,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(dxdy,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(dxgrid,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(dygrid,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(nslcs,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(cavLsc,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(Rdown,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(Rup,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(Rout_Up,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(Rout_Dn,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(reprate,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(Config_Minerva,20,mpi_character,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(Config_Mercury,20,mpi_character,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(MinervaDFLfile,20,mpi_character,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(MercuryDFLfile,20,mpi_character,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(nsigma,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(Out_Coupling,12,mpi_character,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(tilt_x_Up,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(tilt_y_Up,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(r_hole_Up,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(x_offset_Up,1,mpi_double_precision,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(y_offset_Up,1,mpi_double_precision,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(tilt_x_Dn,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(tilt_y_Dn,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(r_hole_Dn,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
      call mpi_bcast(x_offset_Dn,1,mpi_double_precision,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(y_offset_Dn,1,mpi_double_precision,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(aberrations_Up,8,mpi_character,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(aberrations_Dn,8,mpi_character,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(MirLoss_Up,1,mpi_double_precision,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(MirLoss_Dn,1,mpi_double_precision,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(Machine_File,20,mpi_character,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(n_sat,1,mpi_integer,0,mpi_comm_world,ier)
      call mpi_bcast(t_thermal_Up,1,mpi_double_precision,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(t_thermal_Dn,1,mpi_double_precision,0,
     1                  mpi_comm_world,ier)
      call mpi_bcast(tmax,1,mpi_double_precision,0,mpi_comm_world,
     1                  ier)
c
      return
      end
      
