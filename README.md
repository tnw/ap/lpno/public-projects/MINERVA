MINERVA is a three-dimensional, time-dependent simulation code that enables modeling of various FEL designs using a Gaussian modal decomposition. No average over the undulator period is applied to electron trajectories.

Version 2 models the optical field with a fixed polarization state that can be either linear, circular or elliptical.
Version 4 models the optical field using two perpendicular components of the vector potential (electric field) and dynamically follows the evolution of the polarization state of the optical fiel. This version allows for an arbitrary sequency of undulator segments with varying magnetic polarization.


Executables and manuals for the MINERVA simulation code will be published here shortly.
